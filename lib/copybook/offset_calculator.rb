# frozen_string_literal: true

module Copybook
  # Recalculate offsets for a pair of record definitions
  class OffsetCalculator
    class << self
      def calculate_offset(prev, curr)
        return curr.redefines.offset if curr.redefines

        calculate_prev_length(prev) + calculate_prev_offset(prev)
      end

      def calculate_prev_length(prev)
        return 0 if prev.container?
        return prev.redefines.length if prev.redefines

        prev.length(:field)
      end

      def calculate_prev_offset(prev)
        return prev.redefines.offset if prev.redefines

        prev.offset
      end
    end
  end
end
