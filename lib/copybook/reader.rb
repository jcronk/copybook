# frozen_string_literal: true

module Copybook
  # Interface to the COBOL copybook.
  # This takes care of EOL, comments, and line
  # numbers to make the parsing easier.
  class Reader
    attr_reader :lines

    def initialize(data)
      lines = case data
              when String then File.open(data).readlines
              when Array then data
              else data.readlines
              end
      @lines = normalize(lines)
    end

    def normalize(lines)
      lines.reduce([]) do |result, input|
        stripped_line = input[6...72]
        next result unless stripped_line && stripped_line !~ /\*/

        copybook_line_reducer(result, stripped_line)
      end
    end

    # :reek:FeatureEnvy
    def copybook_line_reducer(result, input)
      last_line = result.last
      if result.empty? || line_ends?(last_line)
        result << input
      else
        last_line << input
      end
      result
    end

    # Yields a cleaned, complete line from the copybook
    # to the block.
    def next_line
      @lines.each { |line| yield line }
    end

    private

    def line_ends?(string)
      string =~ /\.\s*$/
    end
  end
end
__END__
original:
  # Interface to the COBOL copybook.
  # This takes care of EOL, comments, and line
  # numbers to make the parsing easier.
  class Reader
    attr_reader :lines

    def initialize(data)
      @original_lines = case data
                        when String then File.open(data).readlines
                        when Array then data
                        else data.readlines
                        end
      @lines = filter_lines
      @queue = @lines
    end

    def filter_lines
      @original_lines.map { |l| l[6...72] }.reject { |l| l.nil? || l =~ /\*/ }
    end

    # Yields a cleaned, complete line from the copybook
    # to the block.
    def get_line
      line_part = ''
      while (line = @queue.shift)
        line_part << line
        next if line !~ /\.\s*$/
        yield line_part
        line_part.clear
      end
    end

    def eoc?
      @queue.empty?
    end
  end
