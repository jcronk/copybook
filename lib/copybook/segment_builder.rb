# frozen_string_literal: true

module Copybook
  # A builder class to create a segment hierarchy from a copybook and the
  # data reader.
  class SegmentBuilder
    # The "empty value" rule mechanism is in here
    # because it needs to know things about the field
    # definition from the copybook, and this is the last
    # time it will be available.  Any rules that depend on
    # knowledge about the copybook should go here.
    DEFAULT_EMPTY = lambda { |field, value|
      return true if field.numeric? && value.to_f == 0
      return true if field.name =~ /FILLER/
      return true if value.to_s =~ /^\s*$/

      false
    }

    # @param reader [DataReader] the data reader
    # @param opts [Hash] options
    # @option opts [Boolean] :skip_empty whether to skip empty fields
    # @option opts [Array] :empty_when an array of Procs that take the field and value
    #   and returns true or false depending on whether the value is "empty"
    #   according to the field definition.  Pass in [] if nothing is ever empty.
    def initialize(reader, opts = {})
      @reader = reader
      @opts = opts
      reset
    end

    def reset
      @parents = []
      @current = nil
      @root = nil
      @skip_empty = @opts.fetch(:skip_empty) { true }
      @empty_when = @opts.fetch(:empty_when) { [DEFAULT_EMPTY] }
    end

    def sync_parents(target)
      return if @parents.empty?

      until @parents.last.equal?(target)
        @parents.pop
        @current = @current.parent
      end
    end

    # If there are empty rules other than the default, you
    # can add them here
    def add_empty_rule(rule)
      @empty_when << rule
    end

    def set_new_parent(field)
      @parents << field
      return init_segment(field) if @root.nil?

      segment = new_segment(field)
      @current.add_child(segment)
      @current = segment
    end

    def init_segment(field)
      @root = new_segment(field)
      @current = @root
    end

    def new_segment(field)
      RecordSegment.new(name: field.name, record: {})
    end

    def empty(field, value)
      @empty_when.any? do |rule|
        rule[field, value]
      end
    end

    # Build the record segment hierarchy
    def build(cb, _opts = {})
      reset
      @reader.get_field_values(cb) do |field, value|
        sync_parents(field.parent)
        if field.container?
          set_new_parent(field)
          next
        end
        @current.empty_fields << field.name if empty(field, value)
        @current.record[field.name] = value
      end
      @root
    end
  end
end
