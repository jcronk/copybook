# frozen_string_literal: true

require 'copybook/format_converter'

module Copybook
  class EbcdicReformatter < FormatConverter
    # Constructor
    # @param flags [Hash]
    # @option flags [Boolean] :trim_whitespace @see FormatConverter
    # @option flags [Boolean] :decode_nonprinting (true) Attempt to decode all characters
    #   sent in the data, even if they're nonprinting or don't exist on the ASCII table.
    #   Setting this to false will replace these characters with spaces.
    def initialize(flags)
      @trim_whitespace = flags.fetch(:trim_whitespace) { true }
      @decode_nonprinting = flags.fetch(:decode_nonprinting) { true }
    end

    def hexdump(value)
      hexify(value)
    end

    def convert_value(value)
      return eb2asc(value) if @decode_nonprinting

      eb2ascp(value)
    end

    def convert_comp3(value, ndec)
      packed2num(value, ndec)
    end

    def convert_comp(value)
      case value.length
      when 2 then halfwd2num(value)
      when 4 then fullwd2num(value)
      end
    end

    def convert_signed(value, ndec)
      zoned2num(value, ndec)
    end

    def convert_numeric(value, ndec)
      num2ascnum(value, ndec)
    end
  end
end
