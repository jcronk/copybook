# frozen_string_literal: true

require 'copybook/const'

module Copybook
  class PicTokenizer
    TOKENS = [
      [:whitespace, /\s+/],
      [:pic_clause, /PIC(TURE)?(?: IS)?/],
      [:pic_char, Const::PIC_CHAR],
      [:pic_repeat, Const::PIC_REPEAT],
      [:pic_literal, Const::PIC_LITERAL]
    ].freeze

    def initialize(pic_string)
      @scanner = StringScanner.new(pic_string)
      @tokens = []
    end

    def next_token
      TOKENS.each_with_index do |(tname, pattern), _ix|
        @scanner.scan(pattern)
        next unless @scanner.matched?

        @tokens << [@scanner.pos, [tname, @scanner.matched]]
        return @tokens.last[1]
      end
      nil
    end

    def scan
      while token = next_token
        yield token
      end
    end

    def peek_next_token
      t = next_token
      return unless t

      rewind 1
      t
    end

    def prev_token
      @tokens[-2]
    end

    def rewind(steps)
      0.upto(steps) do |_step|
        tinfo = @tokens.pop
        @scanner.pos = @tokens.empty? ? 0 : @tokens.last.first
        return tinfo[1]
      end
    end
  end
end
