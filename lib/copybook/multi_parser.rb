# frozen_string_literal: true

require 'copybook'

module Copybook
  class MultiParser
    # This should either take a single file or a list of files,
    # either way it will look for 01 levels and call the parser
    # on each one.
    #
    def initialize(files)
      @files = files
      @copybook_listing = {}
      @copybook_lines = []
    end

    def parse_all
      Array(@files).each do |file|
        get_copybooks(file)
      end
      store_copybooks
      @copybook_listing
    end

    def get_copybooks(file)
      @all_lines = File.open(file).readlines
      @copybook_lines += @all_lines.slice_before(/^\s*01/).reject { |e| entry_is_empty?(e) }
    end

    def entry_is_empty?(array)
      array.all? do |r|
        r =~ /^\s*$/
      end
    end

    def store_copybooks
      @copybook_lines.each do |cb_rows|
        begin
          cb = Copybook::Parse(cb_rows)
        rescue StandardError => e
          raise e, "Problem!\n#{cb_rows.join("\n")}\n#{e.message}"
        end
        @copybook_listing[cb.name] = cb
      end
    end
  end
end
