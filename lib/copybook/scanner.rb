# frozen_string_literal: true

require 'strscan'
require 'copybook/const'

module Copybook
  # Scanner/tokenizer for copybook parser.
  # This is a vanilla scanner with some convenience methods and
  # constants added in.
  class Scanner < StringScanner
    Const::PATTERNS.each do |name, pattern|
      define_method("match_#{name}") do
        skipws
        scan(pattern)
        return unless matched?

        return matched.strip
      end
    end

    def skipws
      skip(/\s+/)
    end

    def skipline
      skip_until(/\.\s*/)
    end
  end
end
