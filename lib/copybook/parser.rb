# frozen_string_literal: true

require 'copybook/reader'
require 'copybook/scanner'
require 'copybook/record_definition'
require 'copybook/pic_parser'
require 'copybook/const'

# Module for handling COBOL copybooks and data
module Copybook
  class << self
    # Convenience method for parsing the copybook
    # @return [Copybook::RecordDefinition] the record-level definition
    #   of the copybook
    def Parse(file, opts = {})
      cbreader = Reader.new(file)
      Parser.new(cbreader, opts).parse
    end
  end

  # Main parser for copybook.  Assembles RecordDefinition objects in a hierarchy.
  class Parser
    ParseError = Class.new(StandardError)

    # @param reader [Copybook::Reader] the interface to the copybook file
    # @option opts [Boolean] :wrap_content whether to wrap the definitions in a root definition,
    #   useful for when the copybook contains multiple record definitions.
    def initialize(reader, opts = {})
      @reader = reader
      @ss = Scanner.new('')
      @parents = []
      @root = nil
      @wrap_content = opts.fetch(:wrap_content) { false }
    end

    # Main parsing loop
    def parse
      @reader.next_line do |line|
        @ss.string = line
        attributes = {}
        if @ss.match_copy
          insert_copybook(@ss[1])
          @ss.skipline
          next
        end
        attributes[:level] = @ss.match_level
        raise ParseError, "No level found in '#{line}': #{attributes}" unless @ss.matched?

        if @ss.matched =~ /88/
          @ss.skipline
          next
        end
        attributes[:name] = @ss.match_cobol_word
        attributes.merge!(get_field_def_attributes(line))
        process_copybook_field_def(attributes)
      end
      @root.list(with_redefines: true).each_cons(2) do |(prev, curr)|
        next if curr.nil?
        next unless curr.occurs > 1

        repeat_occurs_group(curr, prev)
      end
      @root
    end

    # For processing the COPY directive.
    # TODO: This could be better.  It ought to search the same directory
    # the original copybook is in.
    def insert_copybook(file)
      raise IOError, "'#{file}' could not be opened: File not found." unless File.exist?(file)

      r = Reader.new(file)
      cb = process_secondary_copybook(r)
      cb.children.each do |fld|
        @parents.last.add_child(fld)
      end
    end

    # Parse a copybook while parsing a copybook
    def process_secondary_copybook(reader)
      Parser.new(reader).parse
    rescue StandardError => e
      raise e, "Error occurred while processing secondary copybook '#{file}': #{e.message}"
    end

    # Wrap the lowest levels of the copybook in a pseudo-level
    # @param field_def [Copybook::RecordDefinition] the copybook level to be wrapped
    # @option opts [String] :level the level to use
    # @option opts [String] :name the field name
    def add_wrapper(field_def, opts = { level: '00', name: 'DATA-DIVISION', offset: 0 })
      @root = RecordDefinition.new(opts)
      @parents << @root
      @root.add_child(field_def)
      @parents << field_def
    end

    def set_root(field)
      @root = field
      @parents << @root
    end

    def start_hierarchy(field_def)
      @wrap_content ? add_wrapper(field_def) : set_root(field_def)
      field_def
    end

    def synchronize_levels(field_def)
      until field_def > @parents.last
        @parents.pop
        if @parents.empty?
          raise ParseError, "Error finding parent for #{field_def}!"
        end
      end
    end

    def start_new_level_group?(field_def)
      field_def <= @parents.last
    end

    def repeat_occurs_group(occurs, _prev_field)
      parent = occurs.parent
      occurrences = occurs.occurs.to_i
      last_inserted = occurs
      1.upto(occurrences - 1) do |_nbr|
        newocc = occurs.dup
        parent.insert_after(last_inserted, newocc)
        last_inserted = newocc
      end
      occurs.recalculate_offsets
    end

    # def update_offsets(occ, occ_nbr)
    #   size = occ.length(:record)
    #   occ.update_offset(occ.offset + (size * occ_nbr))
    #   stack = [occ]
    #   occ.children.each do |child|
    #     child.list(with_redefines: true).each do |field|
    #       prev = stack.last
    #       if field.redefines
    #         field.update_offset(field.redefines.offset)
    #       else
    #         field.update_offset(prev.length + prev.offset)
    #         stack << field
    #       end
    #     end
    #   end
    # end

    # Builds the copybook level hierarchy and manages the stack of parents
    # @param attributes [Hash] the definition attributes as a hash.
    # @return [Copybook::RecordDefinition] the definition being processed
    def process_copybook_field_def(attributes)
      field_def = RecordDefinition.new(refine_field_attributes(attributes))
      return start_hierarchy(field_def) if @parents.empty?

      synchronize_levels(field_def) if start_new_level_group?(field_def)
      @parents.last.add_child field_def
      @parents << field_def if field_def.container?
      field_def
    end

    def refine_field_attributes(attributes)
      redefines_name = attributes.delete(:redefines)
      ref = get_redefines_ref(redefines_name) if redefines_name
      attributes[:redefines] = ref
      attributes[:offset] = calculate_offset
      attributes
    end

    # get the most recent field definition that was not part of a redefines clause
    def get_last_primary_field
      return @parents.last if @parents.last.children.empty?

      @parents.last.children.select do |child|
        child.redefines.nil?
      end.last
    end

    def calculate_offset(prev_field = nil)
      return 0 if @parents.empty?

      field = prev_field || get_last_primary_field
      field.length + field.offset
    end

    def get_redefines_ref(name)
      pool = [@parents.last, @parents.last.children].flatten
      ref = pool.find do |defn|
        defn.name == name
      end
      return ref if ref

      raise ParseError, "Could not connect redefines name '#{name}' to record definition"
    end

    # Assigns field definition (copybook level) attributes based on
    # the string scanner output.
    def get_field_def_attributes(line)
      counter = 0
      attributes = {}
      until @ss.match_eol
        raise ParseError, "ran through line #{counter} times and didn't terminate.  Original line: \n'#{line}'\nScanner leftovers: '#{@ss.rest}'" if counter > 25

        if @ss.match_picture_clause
          offset = @ss.pos - @ss.matched.length
          pic_parser = PicParser.new(@ss.matched)
          attributes[:pic] = pic_parser.parse
          @ss.pos = offset + pic_parser.parsed_string.length
        elsif @ss.match_redefines_clause
          attributes[:redefines] = @ss[1].strip
        elsif @ss.match_occurs_clause
          # This can accidentally capture a PIC clause
          pos = @ss.string =~ Const::PATTERNS[:picture_clause]
          @ss.pos = pos - 1 if pos
          attributes[:occurs] = @ss[1]
        elsif @ss.match_usage_clause
          attributes[:usage] = @ss[1]
        elsif @ss.match_value_clause
          attributes[:value] = @ss[1]
        end
        @ss.skipws
        counter += 1
      end
      attributes
    end
  end
end
