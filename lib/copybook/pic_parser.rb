# frozen_string_literal: true

require 'copybook/pic_tokenizer'
require 'copybook/picture_clause'

module Copybook
  # Parse the pic clause
  class PicParser
    attr_reader :parsed_string

    ParseError = Class.new(StandardError)

    def initialize(pic_string)
      @tokenizer = PicTokenizer.new(pic_string)
      @attrs = {
        type: nil,
        base_length: 0,
        imp_decimals: 0,
        literals: +''
      }
      @pic_string = pic_string
      @parsed_string = +''
      @power = 1
    end

    # Main parser
    def parse
      raise ParseError, "'#{@pic_string}' is not a PIC clause" unless @tokenizer.peek_next_token.first == :pic_clause

      @tokenizer.scan do |(type, value)|
        @parsed_string << value
        case type
        when :pic_clause then next
        when :whitespace then next
        when :pic_char then handle_pic_char(value)
        when :pic_repeat then handle_pic_repeat(value)
        when :pic_literal then handle_pic_literal(value)
        end
      end
      PictureClause.new(@pic_string, @attrs)
    end

    def handle_pic_repeat(value)
      repeat = value.tr('()', '').to_i
      if @impl_dec
        @attrs[:imp_decimals] += repeat
      else
        @attrs[:base_length] += repeat
      end
    end

    def handle_pic_char(value)
      case value
      when 'S', 's' then set_type(value)
      when 'X', 'x', '9' then update_pic_length(value)
      when 'V', 'v' then start_implied_decimal
      when 'P' then @power *= 10
      end
    end

    def set_type(value)
      return if @attrs[:type]

      @attrs[:type] = case value.upcase
                      when 'X' then :char
                      when '9' then :num
                      when 'S' then :signed
      end
    end

    def handle_pic_literal(value)
      @attrs[:literals] << value
      @attrs[:type] = :char if value != '.'
    end

    def start_implied_decimal
      @impl_dec = true
    end

    def update_pic_length(value)
      @expected_pic_char ||= value
      set_type(value)
      check_pic_char(value)
      if @impl_dec
        update_decimal_length(value)
      else
        update_primary_length(value)
      end
    end

    def update_primary_length(_value)
      return if repeating?

      @attrs[:base_length] += 1
    end

    def repeating?
      next_token = @tokenizer.peek_next_token
      return false unless next_token

      next_token.first == :pic_repeat
    end

    def update_decimal_length(_value)
      return if repeating?

      @attrs[:imp_decimals] += 1
    end

    def check_pic_char(value)
      return if @expected_pic_char == value

      raise ParseError, "Mix of PIC types in the same clause is not supported (found #{@expected_pic_char} and #{value})"
    end
  end
end
