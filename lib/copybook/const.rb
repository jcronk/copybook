# frozen_string_literal: true

module Copybook
  class Const
    COBOL_WORD = /([A-Za-z0-9]+(-+[A-Za-z0-9]+)*)\s*/.freeze
    PIC_CHAR = /([ABEGSVXZabegsvxz90+\*$])/.freeze
    PIC_REPEAT = /\((\d+)\)/.freeze
    PIC_LITERAL = /[-+.](?!\s*$)/.freeze
    # PIC_EXPONENT = /P/.freeze
    # PICTURE_STRING = /((?:#{PIC_CHAR}+#{PIC_REPEAT}?)\s*(V9+(#{PIC_REPEAT})?)?)/.freeze

    PATTERNS = { level: /([0][1-9]|[1-4][0-9]|66|77|88)\s*/,
                 cobol_word: COBOL_WORD,
                 occurs_clause: /OCCURS\s*(\d+)(?:\s* TIMES)?\s*(INDEXED BY\s* (#{COBOL_WORD})+)?\s*/,
                 redefines_clause: /REDEFINES\s*(#{COBOL_WORD})\s*/,
                 picture_clause: /PIC(?:TURE)?(?: IS)?\s+.+/,
                 usage_clause: /(COMP(-3)?)/,
                 eol: /\./,
                 whitespace: /\s+/,
                 copy: /COPY (#{COBOL_WORD})/,
                 value_clause: /VALUE\s+(?:IS )?([^.]+)/ }.freeze
    private_constant :COBOL_WORD
    public_constant :PATTERNS, :PIC_CHAR, :PIC_REPEAT, :PIC_LITERAL # , :PIC_EXPONENT, :PICTURE_STRING
  end
end
