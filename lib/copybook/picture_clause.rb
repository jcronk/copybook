# frozen_string_literal: true

require 'copybook/null_picture_clause'
module Copybook
  # The data field information from the PIC clause
  class PictureClause
    attr_reader :type, :imp_decimals, :base_length

    # Normally this is created by Copybook::PicParser
    # @param pic String The text of the PIC clause, minus
    #   the "PIC" string
    # @option opts [Integer] :base_length the base
    #   (declared) length of the field
    # @option opts [Integer] :imp_decimals the number of
    #   implied decimals, e.g., 2 for V99
    # @option opts [Symbol] :type a symbol for the data
    #   type of the field, either :char, :num, or :signed
    def initialize(pic, opts)
      @pic = pic
      @base_length = opts.fetch(:base_length)
      @imp_decimals = opts.fetch(:imp_decimals)
      @literals = opts.fetch(:literals) { '' }
      @type = opts.fetch(:type)
    end

    # Returns the PIC string
    def to_s
      @pic
    end

    # The length, including implied decimals
    # for example, 9(4)V99 is 6.
    def length
      @base_length + @imp_decimals + @literals.length
    rescue TypeError => e
      raise e, e.message + ' ' + to_s
    end

    # The actual length in bytes of this field if the
    # usage is COMP-3
    def comp3_length
      ((length.to_f + 1) / 2).ceil.to_i
    end

    # The actual length in bytes if the usage is COMP
    def comp_length
      length < 5 ? 2 : 4
    end
  end
end
