# frozen_string_literal: true

module Copybook
  class FormatConverter
    include Convert::IBM390

    # A factory method
    # @param flags [Hash]
    # @option flags [Boolean] :ebcdic use ebcdic conversion
    # @option flags [Boolean] :trim_whitespace (true) trim whitespace from field values
    # @return [EbcdicReformatter, AsciiReformatter]
    def self.create(flags)
      ebcdic = flags.fetch(:ebcdic)
      if ebcdic
        EbcdicReformatter.new(flags)
      else
        AsciiReformatter.new(flags)
      end
    end

    def initialize(flags)
      @trim_whitespace = flags.fetch(:trim_whitespace) { true }
    end

    # Convert a value to its ascii equivalent
    # This will convert packed or zoned values to the numbers they
    # represent.
    # @param field [RecordDefinition]
    # @param value [String] The text from the data file
    # @return [Integer, Float, String] the converted value
    def convert_field_value(field, value)
      return '' if field.container?
      return '' if value =~ /^\x00+$/

      converted = convert_packed_or_numeric(field, value)
      return converted if converted

      value = convert_value(value)
      value = value.rstrip if @trim_whitespace
      value
    end

    private

    def convert_packed_or_numeric(field, value)
      case field.usage
      when 'COMP-3' then return convert_comp3(value, field.imp_decimals)
      when 'COMP' then return convert_comp(value)
      end
      return convert_signed(value, field.imp_decimals) if field.type == :signed
      return convert_numeric(value, field.imp_decimals) if field.type == :num
    rescue Convert::IBM390::ConversionError => e
      raise e, "Error when converting '#{value}' in #{field}: #{e.message}"
    end
  end
end
