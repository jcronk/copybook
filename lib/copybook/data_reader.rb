# frozen_string_literal: true

require 'convert/ibm390'
require 'copybook/ebcdic_reformatter'
require 'copybook/ascii_reformatter'

module Copybook
  # Base class for mainframe data format conversion
  # utility

  # An iterator that decodes records, given a copybook.
  # @example
  #   reader = DataReader.new(@io_object, nonprinting: :ignore, separator: "\x0d\x0a")
  #   reader.get_field_values(@copybook) do |field, value|
  #     # do stuff with the data
  #   end
  class DataReader
    extend Forwardable

    def_delegators :@data, :eof?
    # Raised when the data file has a length mismatch
    RecordLengthError = Class.new(StandardError)

    # Raised when record separator is not found at EOR.
    RecordSeparatorError = Class.new(StandardError)

    # @param opts [Hash] options
    # @option opts [String] :separator (''), the character sequence used as a record separator.
    #    This is not part of the copybook.  At the end of the record, it will look for this and throw
    #    an exception if this character sequence isn't found.
    # @option opts [Boolean] :trim (true) trim whitespace from values
    # @option opts [Symbol] :nonprinting (:decode) if this is set to :ignore, it will ignore
    #   nonprinting characters in the decoded output.
    # @option opts [Boolean] :ebcdic (true) Whether to decode as EBCDIC or ASCII.  EBCDIC decoding will also
    #   decode packed values such as COMP-3.
    # @option opts [Boolean] :ignore_errors (false) Continue when a conversion error is raised.  Otherwise it
    #   will reraise the error with the pointer position in the file where the error occurred.
    # @option opts [Boolean] :with_redefines (false) Include redefined fields in the output.
    # @option opts [Boolean] :read_linewise (false) Instead of reading by record length, read by line
    #   using the record separator character.  This will raise an error if the record separator is empty.
    def initialize(io, opts = {})
      @data = io
      @pointer = init_pointer
      @config = Conf.new(opts)

      @formatter = FormatConverter.create(ebcdic: @config.ebcdic,
                                          trim_whitespace: @config.trim_whitespace,
                                          decode_nonprinting: @config.nonprinting == :decode)
    end

    def init_pointer
      pointer = Breadcrumb.new
      pointer.add(@data)
      pointer
    end

    def tell
      @data.tell
    end

    # This is the primary loop.  It yields a RecordDefinition and a value for each
    # field in the copybook record definition.
    def get_field_values(copybook)
      return if @data.eof?

      read_linewise, include_redefines, record_separator = @config.values(:read_linewise, :include_redefines, :record_separator)
      validate_remaining_length(copybook) unless read_linewise
      record = get_record(copybook)
      copybook.list(with_redefines: include_redefines).each do |field|
        value = record[field.offset..field.end_position]
        field, value = get_formatted_value(field, value)
        yield field, value
      end
      skip_and_validate_record_separator(@data.read(record_separator.length)) unless read_linewise || @data.eof?
      @pointer.add(@data)
    rescue StandardError => e
      raise e, "Error at position #{@data.tell}: #{e.message}"
    end

    def parse_record(copybook)
      record = get_record(copybook)
      copybook.list.each_with_object([]) do |field, coll|
        value = record[field.offset..field.end_position]
        field, value = get_formatted_value(field, value)
        yield field, value if block_given?
        coll << [field, value]
      end
    end

    # Get a record based on either line or copybook length
    def get_record(copybook)
      return @data.readline(@config.record_separator) if @config.read_linewise

      @data.read(copybook.entire_record_length)
    end

    # Back up the pointer by one or more records.
    # @param records [Integer] (1) the number of records to "un"-read.
    def unread_record(records = 1)
      position = @pointer.backtrack(records)
      @data.seek(position)
    end

    # Read the next value based on the field length, then format the field
    # according to the PIC clause in the record definition.
    # @param field [RecordDefinition] the field that the value to be read belongs to
    # @yieldparam [RecordDefinition] field the field that was passed in
    # @yieldparam [String, Integer, Float] value the formatted value
    def get_formatted_value(field, value)
      value = @formatter.convert_field_value(field, value)
      [field, value]
    rescue Convert::IBM390::ConversionError => e
      message = "Error at position #{@data.tell}: #{e.message}"
      raise e, message unless @config.ignore_error

      if block_given?
        yield field, '!ERROR!'
      else
        [field, message]
      end
    end

    def peek(length)
      string = @data.read(length)
      @data.seek(@pointer.location)
      return Convert::IBM390.eb2asc(string) if @config.ebcdic

      string
    end

    # Used internally.  It checks what is assumed to be the record separator
    # and throws an informative exception if it doesn't match the set record
    # separator
    # @param sep [String] the likely record separator (from the data)
    def skip_and_validate_record_separator(sep)
      expected_separator = @config.record_separator
      if sep != expected_separator
        position = @data.tell
        @data.seek(-100, IO::SEEK_CUR)
        context = @data.read 200
        raise RecordSeparatorError, "'#{sep.unpack1('H*')}' (hex characters) found where record separator " \
          "'#{expected_separator.unpack1('H*')}' expected at position #{position}.  " \
          "Context:\n#{context}"
      end
    end

    # Check the remaining record length against the copybook. This will throw
    # an exception if the remaining data is smaller than the calculated record
    # size from the copybook.
    # @param cb [Copybook::Layout] the active copybook
    def validate_remaining_length(cb)
      record_length = cb.entire_record_length
      difference = remaining
      return unless difference < record_length

      raise RecordLengthError, 'Insufficient data remaining for record ' \
        "definition.  Definition length is #{record_length}, but only " \
        "#{difference} is left"
    end

    def remaining
      @data.size - @data.tell
    end

    # TODO: Refactor and document this

    def skip(length)
      skipped = @data.read(length)
      @pointer.add(@data)
      skipped
    end

    def readline
      skipped = @data.readline
      @pointer.add(@data)
      skipped
    end
  end
  # :reek:TooManyInstanceVariables
  class Conf
    attr_reader :record_separator, :trim_whitespace, :nonprinting, :ebcdic, :ignore_error, :read_linewise, :include_redefines
    def initialize(opts)
      @record_separator = opts.fetch(:separator) { '' }
      @trim_whitespace = opts.fetch(:trim) { true }
      @nonprinting = opts.fetch(:nonprinting) { :decode }
      @ebcdic = opts.fetch(:ebcdic) { true }
      @ignore_error = opts.fetch(:ignore_errors) { false }
      @read_linewise = opts.fetch(:read_linewise) { false }
      @include_redefines = opts.fetch(:with_redefines) { false }
    end

    def validate_configuration
      return unless @read_linewise && @record_separator.empty?

      raise RecordSeparatorError, 'Record separator character is required if :read_linewise is set'
    end

    def values(*keys)
      keys.map { |key| instance_variable_get("@#{key}") }
    end

    def to_s
      <<~INFO
        Data Reader Configuration:
        record_separator: #{@record_separator.unpack1('H*')}
        trim_whitespace: #{@trim_whitespace}
        nonprinting characters: #{@nonprinting}
        EBCDIC input: #{@ebcdic}
        ignore errors: #{@ignore_error}
        read by line: #{@read_linewise}
        include redefines: #{@include_redefines}
      INFO
    end
  end

  class Breadcrumb
    attr_reader :trail
    def initialize
      @trail = []
    end

    def add(io)
      @trail << io.tell
    end

    def location
      @trail.last
    end

    def backtrack(steps)
      steps.times do
        @trail.pop
      end
      location
    end

    def start_over
      @trail = Array(@trail.first)
      location
    end
  end
end
