# frozen_string_literal: true

require 'ivans/component'

module Copybook
  # A simple n-ary tree used to build a hierarchical data
  # structure based on the copybook.
  # @example
  #   record = SegmentBuilder.new(@reader, opts).build
  #   # Suppose this copybook shows the following:
  #   # A 10-level VEHICLE-INFO that OCCURS 5 times
  #   # A 15-level COVERAGE that OCCURS 5 times (under 10 VEHICLE-INFO)
  #   # A 15-level VEH-IDENT-NUM that is a string
  #   # A 20-level LIMIT-AMT that appears under every COVERAGE and is a number
  #   record[VEHICLE-INFO][2] # => This is the 3rd occurrence of VEHICLE-INFO and
  #                           #    returns the entire record segment object
  #   record[VEH-IDENT-NUM] # => Returns an Array of 5 strings (1 for each vehicle)
  #   record[VEH-IDENT-NUM][4] # => Returns a string, the VIN for vehicle 5
  #   record[LIMIT-AMT][0] # => This is where it starts getting complicated.
  #                        #    Limit 0 is the limit for the first coverage under
  #                        #    Vehicle 1.  However:
  #   record[LIMIT-AMT][5] # => This is the first limit under vehicle #2.
  class RecordSegment < IVANS::BasicComponent
    attr_accessor :empty_fields

    def initialize(*args)
      super
      @empty_fields = Set.new
    end

    # Concatenate the values in the record.
    def flatten
      record.values.join('') + children.map(&:flatten).join('')
    end

    def [](key)
      field_val = super
      return field_val if field_val

      @index ||= reindex
      children = @index[key]
      return unless children
      return children.first if children.count == 1

      children
    end

    def reindex
      @index = index_children
    end

    def index_children
      empty_index = Hash.new do |h, k|
        h[k] = []
      end
      each_with_object(empty_index) do |segment, index|
        index[segment.name] ||= []
        index[segment.name] << segment
        segment.record.each do |key, value|
          index[key] << value
        end
      end
    end

    # Attach the children of a different segment to the current one.
    # This does not uninstantiate the other segment or remove them from
    # the children array of the other segment, but the children each have
    # a new parent.
    def merge!(segment)
      segment.children.each do |child|
        add_child(child)
      end
    end

    def clean!
      return if @empty_fields.empty?

      record.delete_if do |k, _v|
        @empty_fields.include?(k)
      end
    end

    def clean_all!
      clean!
      empty_children = []
      children.each do |child|
        child.clean!
        empty_children << child if child.record.empty? && child.children.empty?
      end
      empty_children.each do |child|
        remove_child(child)
      end
      children.each(&:clean_all!)
    end

    def setup(opts)
      super
    end

    # Return the root node of the hierarchy
    def root
      return self unless parent

      parent.root
    end
  end
end
