# frozen_string_literal: true

require 'copybook/format_converter'

module Copybook
  # For converting mainframe files that have been reencoded from EBCDIC
  # to ASCII.  This will attempt to convert zoned values correctly using the
  # last byte of the value, but it ignores packed values and inserts placeholder
  # text instead of attempting to convert them.
  class AsciiReformatter < FormatConverter
    def convert_value(value)
      value
    end

    def convert_comp3(_value, _ndec)
      'PACKED COMP-3'
    end

    def convert_comp(_value)
      'PACKED COMP'
    end

    def convert_signed(value, ndec)
      asc_zoned2num(value, ndec)
    end

    def convert_numeric(value, ndec)
      return value.to_f if value =~ /\./

      add_decimals(value.to_i, ndec)
    end
  end
end
