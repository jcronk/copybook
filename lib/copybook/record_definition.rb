# frozen_string_literal: true

require 'copybook/picture_clause'
require 'copybook/null_picture_clause'
require 'copybook/offset_calculator'
require 'forwardable'

module Copybook
  # Raised when no redefine exists for requested field
  class InvalidRedefineError < StandardError; end

  # One 'level' from a copybook
  class RecordDefinition
    attr_reader :level, :name, :pic, :redefines, :usage, :occurs, :children
    attr_accessor :parent, :offset
    include Comparable
    extend Forwardable

    def_delegators :@pic, :type, :imp_decimals

    # This is normally created by Copybook::Parser
    # @option args [String] :level the level number
    # @option args [String] :name the field name
    # @option args [Copybook::PictureClause] :pic the parsed PIC clause
    # @option args [String] :redefines the field this definition redefines,
    #   if any
    # @option args [String] :usage the usage clause, either COMP or COMP-3
    # @option args [Integer] :occurs the number of times this field OCCURS
    def initialize(args)
      @orig_args = args
      @level = args.fetch(:level)
      @name = args.fetch(:name)
      @pic = args.fetch(:pic) { NullPictureClause.new }
      @redefines = args[:redefines]
      @usage = args.fetch(:usage) { '' }
      @occurs = args[:occurs] ? args[:occurs].to_i : 1
      @offset = set_offset(args)
      @children = []
      @child_index = Hash.new do |h, k|
        h[k] = []
      end
      @parent = nil
      @redef_index = {}
    end

    def recalculate_offsets
      fields = root.list(with_redefines: true)
      fields.each_cons(2) do |prev, curr|
        curr.offset = OffsetCalculator.calculate_offset(prev, curr)
      end
    end

    # Force updating an offset value.
    # @param nbr [FixNum] the new offset value
    # @note This is used by the parser to recalculate offsets
    #   when expanding OCCURS clauses into multiple fields.
    def update_offset(nbr)
      @offset = nbr
    end

    # The ending position of the field
    def end_position
      @offset + length - 1
    end

    # Returns true if an ancestor of this field
    # has a REDEFINES.
    def part_of_redefines?
      return false unless @parent

      !@redefines.nil? || @parent.part_of_redefines?
    end

    # Returns true if an ancestor of this field
    # has an OCCURS clause
    def part_of_occurs?
      return true if @occurs > 1
      return false unless @parent

      @parent.part_of_occurs?
    end

    # Return the ancestor of the current node
    # that has an OCCURS clause
    def occurs_parent
      return unless @parent && part_of_occurs?
      return @parent if @parent.occurs > 1

      @parent.occurs_parent
    end

    # Deep copy of the object.  Any REDEFINES references
    # are updated to refer to the corresponding field in
    # the duplicate.
    def dup
      RecordDefinition.new(@orig_args).tap do |rd|
        @children.each do |child|
          copy = child.dup
          rd.add_child(copy)
          copy.update_redefines
        end
      end
    end

    # Return the ancestor of the current node
    # that has a REDEFINES
    def redefining_parent
      return unless @parent && part_of_redefines?
      return @parent unless @parent.redefines.nil?

      @parent.redefining_parent
    end

    # Access this level's "children" (sub-levels) by name.
    # If a child is an OCCURS, it will return an array
    # @param child_name [String] the field name
    # @return [RecordDefinition, Array<RecordDefinition>] either a field or an array of fields.
    def [](child_name)
      @child_index.fetch(child_name) do
        desc = list(with_redefines: true).select { |df| df.name == child_name }
        if desc.empty?
          raise KeyError,
                "'#{child_name}' is not a child or descendant of #{name}"
        else
          @child_index[child_name] = desc
        end
      end
      return @child_index[child_name].first if @child_index[child_name].size == 1

      @child_index[child_name]
    end

    def root
      return self if @parent.nil?

      @root ||= @parent.root
    end

    # The actual length in bytes, in the data, of this field.
    # This is adjusted based on the usage clause
    def field_length
      return @length ||= 0 unless @pic

      case @usage
      when 'COMP' then @pic.comp_length
      when 'COMP-3' then @pic.comp3_length
      else @pic.length
      end
    end

    def length(scope = :field)
      case scope
      when :field then field_length
      when :record then entire_record_length
      end
    end

    # Get the total, actual length of this field and all fields below it,
    # recursively.  At the base level of the record definition, this calculates
    # the entire record length.
    def entire_record_length
      list(with_redefines: false).map(&:length).inject(:+)
    end

    # Returns something similar to a copybook level, but with the actual
    # length added in parentheses
    def to_s
      indent = @level.to_i < 30 ? (@level.to_i / 2).ceil : 15
      base = "#{@level} #{@name} "
      pic = @pic ? "#{@pic} |#{length}| " : ''
      usage = @usage
      redefines = @redefines ? "Redefines #{@redefines.name}  " : ''
      occurs = @occurs > 1 ? "Occurs #{@occurs} times." : ''
      offset = "{#{@offset}}"
      (' ' * indent) + [base, redefines, pic, offset, usage, occurs].join(' ')
    end

    # Add a child to the list of children.
    def add_child(child)
      @children << child
      @child_index[child.name] << child
      child.parent = self
    end

    def insert_after(existing_child, new_child)
      index = @children.index { |item| item.eql?(existing_child) }
      @children.insert(index + 1, new_child)
      @child_index[new_child.name] << new_child
      new_child.parent = self
    end

    # A field is numeric if it's not PIC X of
    # some kind
    def numeric?
      return false unless pic
      return false if pic.type == :char

      true
    end

    # List self and all children recursively
    # @param args [Hash]
    # @option args [Boolean] :with_redefines (false)
    #   include redefinition fields in the listing.
    # @return [Array<RecordDefinition>]
    def list(args = {})
      return [] unless redefines.nil? || args[:with_redefines]

      [self, children.map { |ch| ch.list(args) }].flatten
    end

    # Return true if this is a "container" definition with other
    # definitions below it.
    def container?
      length.zero?
    end

    def <=>(other)
      level.to_i <=> other.level.to_i
    end

    # Used internally to change redefine references
    # when duplicating a RecordDefinition
    def update_redefines
      return if @parent.nil?
      return if @redefines.nil?

      @redefines = @parent.children.find do |child|
        child.name == @redefines.name
      end
    end

    # Logic for setting offset to take redefinitions
    # into account
    def set_offset(args)
      return @redefines.offset unless @redefines.nil?

      args[:offset] || 0
    end

    def attributes
      {
        level: @level,
        name: @name,
        length: length(:field),
        record_length: length(:record),
        redefines: @redefines.to_s,
        redefines_length: @redefines ? @redefines.length(:field) : '',
        redefines_record_length: @redefines ? @redefines.length(:record) : '',
        usage: @usage,
        occurs: @occurs,
        offset: @offset,
        pic: @pic.to_s,
        children: @children.map(&:name),
        parent: @parent ? @parent.name : nil
      }
    end
  end
end
