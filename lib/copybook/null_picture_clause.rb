# frozen_string_literal: true

module Copybook
  # Null object for PICs
  class NullPictureClause
    attr_reader :type, :imp_decimals, :base_length

    def initialize(*_args)
      @type = :null
      @imp_decimals = 0
      @base_length = 0
    end

    def length
      0
    end

    def parse(*_args)
      self
    end

    def to_s
      ''
    end

    def nil?
      true
    end
  end
end
