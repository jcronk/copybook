require 'forwardable'

module IVANS

  module XMLCreator
    attr_accessor :filter

    def to_xml(opts={})
      cleanup = opts.fetch(:cleanup) { true }
      Nokogiri::XML::Document.new.tap do |doc|
        doc.root = build_all_xml(doc)
        cleanup_xml(doc) if cleanup
      end
    end

    def cleanup_xml(doc)
      doc.traverse do |n|
        next unless n.element?
        n.unlink if n.children.empty?
      end
    end

    def build_all_xml(doc, root=nil)
      node = self.record_to_xml(doc)
      root << node if root
      children.each do |child|
        child.build_all_xml(doc, node)
      end
      root || node
    end

    def format(value)
      value
    end

    # name filters will go in here somewhere via command objects

    def record_to_xml(doc)
      record.each_with_object(doc.create_element(apply_filter(name))) do |(field, value), root|
        root << doc.create_element(apply_filter(field), format(value))
      end
    end

    def apply_filter(name)
      return @filter.call(name) if @filter
      name
    end

  end

  # A generic tree data structure intended to represent
  # insurance policies
  class BasicComponent
    extend Forwardable
    include Enumerable
    attr_reader :record, :children, :child_index
    attr_accessor :name, :parent
    def_delegators :@record, :[], :each, :delete

    # @param [Hash] opts the options for creating the component
    # @option opts [String] :name A name for the component
    # @option opts [Hash] :record Any data that belongs to the component.
    def initialize(opts)
      setup(opts)
      yield self if block_given?
    end

    # initialize instance variables.  This is separate from the constructor
    # to support subclassing.
    def setup(opts)
      @name = opts[:name]
      @record = opts.fetch(:record) {
        raise ArgumentError, "Cannot initialize component without a record."
      }
      @children = []
      @child_index = FlexibleIndex.new
    end

    # Return a child by index or name.
    # @param [String, Integer, Symbol] a reference for the child component.
    #   This is either the component name (a string or symbol) or the index in the
    #   children array.
    def child(ref)
      case ref
      when Integer then return children[ref]
      when String, Symbol then return child_index[ref]
      else raise ArgumentError, "No valid argument provided"
      end
    end

    # @param [BasicComponent] item a component that will be
    #   a child of the current component
    def add_child(item)
      item.parent = self
      @child_index[item.name] = item
      @children << item
    end

    # Iterates recursively through all components at or below
    # the current hierarchy level
    def each(&block)
      yield self
      children.each do |child|
        child.each(&block)
      end
    end

    # Remove this component from its parent
    def unlink
      parent.remove_child(self)
    end

    # Remove a child
    # @param [BasicComponent] object the child object to be removed
    def remove_child(object)
      if object.class == self.class
        @children.delete(object)
        @child_index.delete(object.name)
      elsif Array === object
        object.each do |component|
          remove_child(component)
        end
      end
    end

    # Add a new child from a hash.  Creates a new component, adds it to
    # the current children array, and returns it.
    # @param [Hash] rec the hash that will be the record belonging to
    #   the new child component
    # @param [String] table the 'table' or name of the new component to
    #   be created.
    # @return [BasicComponent] the newly created child component
    def add_child_from_record(rec, table)
      self.class.new(record: rec, name: table) do |comp|
        add_child(comp)
      end
    end

    def inspect
      "<< #{self.class} #{object_id} #{@record}, @children=#{@children.map(&:name)}, @parent=#{@parent.inspect} >>"
    end

  end

  class FlexibleIndex

    def initialize
      @index = {}
    end

    def []=(key, value)
      return @index[key] = value unless @index.include?(key)
      return @index[key] << value if Array === @index[key]
      original_value = @index.delete(key)
      @index[key] = [ original_value, value ]
    end

    def [](key)
      @index[key]
    end

    def <<(hash)
      hash.each do |key, value|
        @index[key] = value
      end
    end

    def delete(obj)
      return @index.delete(obj)
    end

  end



end
