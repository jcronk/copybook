require 'logger'

module IVANS
  module Logger
    ConfigurationError = Class.new(StandardError)
    # Access the logger for the class that includes this mixin
    def log
      @log ||= IVANS::Logger.logger_for(self.class.name)
    end

    @loggers = {}

    class << self
      NULL_LOGGER = ::Logger.new(IO::NULL)
      private_constant :NULL_LOGGER

      # Global configuration for all loggers
      def configure(config = nil)
        return NULL_LOGGER unless config

        @device = config.fetch(:device) { STDERR }
        @level = config.fetch(:level) { :info }
        @shift_age = config.fetch(:shift_age) { :daily }
        @configured = true
      end

      # Create a logger for the current instance
      def logger_for(classname)
        return null_logger unless @configured

        log = ::Logger.new(@device, @shift_age)
        log.level = @level
        log.progname = classname
        log
      end
    end
  end
end
