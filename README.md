# Copybook

A library for reading copybooks and using them to read fixed-width ASCII or EBCDIC data.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'copybook'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install copybook

## Usage

```ruby
@cb = Copybook::Parse('COPYBOOK.cpy')


```

## Contributing

1. Fork it ( https://github.com/jcronk/copybook/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
