# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'copybook/version'

Gem::Specification.new do |spec|
  spec.name          = 'copybook'
  spec.version       = Copybook::VERSION
  spec.authors       = ['Jeremy Cronk']
  spec.email         = ['jcronk@ivansinsurance.com']
  spec.summary       = 'Copybook parser/mainframe data reader'
  spec.description   = 'Allows you to parse a copybook and assemble mainframe data into a hierarchical structure based on the copybook layout.'
  spec.homepage      = 'http://www.ivansinsurance.com'
  spec.license       = 'None'

  spec.files         = `git ls-files -z`.split("\x0")
  spec.required_ruby_version = '>= 2.5'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_development_dependency 'awesome_print'
  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'byebug'
  spec.add_development_dependency 'geminabox'
  spec.add_development_dependency 'guard'
  spec.add_development_dependency 'guard-bundler'
  spec.add_development_dependency 'guard-minitest'
  spec.add_development_dependency 'guard-rubycritic'
  spec.add_development_dependency 'guard-yard'
  spec.add_development_dependency 'interactive_editor'
  spec.add_development_dependency 'listen'
  spec.add_development_dependency 'minitest'
  spec.add_development_dependency 'minitest-byebug'
  spec.add_development_dependency 'minitest-reporters'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'redcarpet'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'rubocop-performance'
  spec.add_development_dependency 'rubocop-require_tools'
  spec.add_development_dependency 'rubycritic'
  spec.add_development_dependency 'shoulda-context'
  spec.add_development_dependency 'simplecov'
  spec.add_development_dependency 'simplecov-console'
  spec.add_development_dependency 'terminal-notifier'
  spec.add_development_dependency 'terminal-notifier-guard'
  spec.add_development_dependency 'yard'
  spec.add_dependency 'convert_ibm390', '>= 2019.5.1'
  spec.add_dependency 'nokogiri'
end
