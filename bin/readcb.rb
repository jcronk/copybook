require 'copybook'
require 'optparse'


OptionParser.new do |opts|
  opts.on('-e','--ebcdic', 'Input data is EBCDIC') do |val|
    @ebcdic = val
  end
  opts.on('-c','--copybook COPYBOOK_FILE', 'Copybook to use to read the data') do |val|
    @cb = Copybook::Parse(val)
  end
  opts.on('-s','--record-separator ENUM', 'Record separator') do |val|
    @rs = case val
          when 'CRLF' then "\x0d\x0a"
          when 'EBCRLF' then "\x0d\x25"
          else ''
          end
  end
  opts.on('-n','--number-of-records INT', 'Number of records to read from file') do |val|
    @nbr = val.to_i
  end
end.parse!

@nbr = -1 unless @nbr
data = File.open(ARGV.shift, 'rb')

reader = Copybook::DataReader.new(data, trim: false, ebcdic: @ebcdic, separator: @rs)

while !data.eof? && @nbr != 0
  reader.get_field_values(@cb) do |field, value|
    puts "#{field}: '#{value}'"
    @nbr -= 1
  end
end




