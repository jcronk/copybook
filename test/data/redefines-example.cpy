      ******************************************************************
      *    INSURED INFORMATION FILE LAYOUT                             *
      ******************************************************************
      *COPYBOOK: PCB02NUPLD
      *FILENAME: PDKUPLOAD
      *RECSIZE:  300

       01  INSURED-RECORD.
           05  A15-MINS-REC-ID         PIC X(02).
           05  B94-MINS-LOCATION       PIC 9(03).
           05  B26-MINS-NAME-SEQ       PIC 9(03).
           05  B25-MINS-NAME-TYPE      PIC X(02).
           05  B33-MNAME-TYPE          PIC X(01).
           05  NAME-DETAIL.
               10 B27-MINS-FIRST-NAME  PIC X(40).
               10 B27-MINS-MIDDLE-NAME PIC X(40).
               10 B27-MINS-LAST-NAME   PIC X(40).
           05  NAME-DETAIL-OTHER REDEFINES NAME-DETAIL.
               10 B27-MINS-NAME1       PIC X(40).
               10 B27-MINS-NAME2       PIC X(40).
               10 B27-MINS-NAME3       PIC X(40).
           05  B39-MOCCUPATION         PIC X(20).
           05  B39-MEMPLOYER           PIC X(30).
           05  B37-MAREA-CD            PIC X(03).
           05  B37-MEXCHG              PIC X(03).
           05  B37-MPHONE              PIC X(04).
           05  D63-SS-NUM              PIC X(09).
           05  B41-MBIRTH-DATE         PIC 9(08).
           05  SOME-DECIMAL-VALUE      PIC 9(5).99.
           05  FILLER                  PIC X(084).
