       01 CIS-EXTRACT-HEADER.
          05 CIS-HDR-HEADER                          PIC X(9).
          05 CIS-HDR-SUB-UNIT                        PIC X(2).
          05 CIS-HDR-QUOTE-NUM                       PIC X(8).
          05 CIS-HDR-EDITION                         PIC X(2).
          05 CIS-HDR-ACT-NUM                         PIC 9(3).
          05 CIS-HDR-TRAN-TYPE                       PIC X(2).
          05 CIS-HDR-MACHINE-ADDR                    PIC X(12).
          05 CIS-HDR-SYS-TYPE                        PIC X(6).
          05 CIS-HDR-SYS-REL                         PIC X(4).
          05 CIS-HDR-ACCOUNT                         PIC X(08).
          05 CIS-HDR-USER-ID                         PIC X(08).
          05 CIS-HDR-PROD-CODE                       PIC X(08).

       01 CWS-PL02LOC.
          05 CWS-PL02-BS                             PIC X.
          05 CWS-PL02-NM                             PIC X(8).
          05 CWS-PL02-LENGTH                         PIC 9(5).
          05 CWS-S006S-SUB                           PIC X(2).
          05 CWS-S006S-QTE-NR                        PIC X(8).
          05 CWS-S006S-ED-NR                         PIC X(2).
          05 CWS-S006S-ACT-NR                        PIC X(3).
          05 CWS-S006T-CIS-POL-ID                    PIC X(14).
          05 CWS-S006P-INSD-LOC-NR                   PIC X(3).
          05 CWS-S006P-INSD-LOC-ADDR                 PIC X(60).
          05 CWS-S006P-INSD-LOC-CITY                 PIC X(20).
          05 CWS-S006P-INSD-LOC-STATE                PIC X(2).
          05 CWS-S006P-INSD-LOC-ZIP-CD               PIC X(5).
          05 CWS-S006P-INSD-LOC-ZIP-6-9              PIC X(4).
          05 CWS-S006P-INSD-LOC-TAX-TERR             PIC X(5).
          05 CWS-S00CH-BM-BV-IND                     PIC X.
          05 CWS-S00CH-BM-AV-IND                     PIC X.
          05 CWS-S00CH-BM-DC-IND                     PIC X.
          05 CWS-S00CH-BM-LMT                        PIC S9(8).
          05 CWS-S006P-COV-PREM-CR                   PIC X(3).
          05 CWS-S006P-COV-PREM-GS                   PIC X(3).
          05 CWS-S006P-COV-PREM-GL                   PIC X(3).
          05 CWS-S006P-PLACE-CODE                    PIC X(5).
          05 CWS-S006P-LOCALIZING-IND                PIC X(1).
          05 CWS-S006P-NAMED-INSURED-NO              PIC X(2).

       01 CWS-KB-KZ-TABLE.
          05 CWS-KB-ID                               PIC X(9).
          05 CWS-KB-COV-CD                           PIC X(2).
          05 CWS-KB-ST-CD                            PIC X(2).
          05 CWS-KB-FORM-NO                          PIC X(20).
          05 CWS-KZ-FORM-TITLE                       PIC X(50).

       01 CWS-CB-TABLE.
          05 CWS-CB-ID                               PIC X(9).
          05 CWS-CB-LSS-YR                           PIC X(4).
          05 CWS-CB-LSS-MO                           PIC X(2).
          05 CWS-CB-LSS-DA                           PIC X(2).
          05 CWS-CB-PRIINS-POL-TYP                   PIC X(04).
          05 CWS-CB-LSS-PD-AMT                       PIC 9(09).
          05 CWS-CB-CLM-STUS                         PIC X(01).
          05 CWS-CB-LSS-DESC                         PIC X(50).
          05 CWS-CB-CLOTHR-RSV-AMT                   PIC 9(11)V99.
          05 WS-CB-CLOTHR REDEFINES CWS-CB-CLOTHR-RSV-AMT.
             10 CWS-CB-CLOTHR-RSV-AMT-X             PIC X(13).
          05 CWS-CB-NO-WC-CLAIMS                    PIC 9(05).

       01 CWS-AD-TABLE.
          05 CWS-AD-ID                              PIC X(9).
          05 CWS-AD-COV-SEQ-NO                      PIC 9(03).
          05 CWS-AD-NON-FFIC-INS-NM                 PIC X(60).
          05 CWS-AD-PRIINS-POL-ID                   PIC X(25).
          05 CWS-AD-PRIINS-EFF-YR                   PIC X(04).
          05 CWS-AD-PRIINS-EXP-YR                   PIC X(04).
          05 CWS-AD-PRIINS-POL-TYP                  PIC X(04).
          05 CWS-AD-FNL-MOD-FCTR                    PIC 9(5)V999.
          05 CWS-AD-PRIINS-EFF-MO                   PIC X(2).
          05 CWS-AD-PRIINS-EFF-DA                   PIC X(2).
          05 CWS-AD-PRIINS-EXP-MO                   PIC X(2).
          05 CWS-AD-PRIINS-EXP-DA                   PIC X(2).
          05 CWS-AD-PRIINS-PREM-AMT                 PIC 9(13)V99.

       01 CWS-SU-TABLE.
          05 CWS-SU-ID                              PIC X(9).
          05 CWS-SU-COV-DECL-IND                    PIC X(01).
          05 CWS-SU-DB-PAYOR-NM                     PIC X(60).
          05 CWS-SU-DB-PAYOR-ADDR                   PIC X(30).
          05 CWS-SU-DB-PAYOR-CITY                   PIC X(25).
          05 CWS-SU-DB-PAYOR-ST                     PIC X(02).
          05 CWS-SU-DB-PAYOR-ZIP                    PIC X(05).
          05 CWS-SU-DB-PAYOR-ZIP-X                  PIC X(04).
          05 CWS-SU-AGT-ID4-INSD                    PIC X(30).
          05 CWS-SU-ACCT-CNTCT-NM                   PIC X(35).
          05 CWS-SU-ACCT-CNTCT-PHON                 PIC X(15).
          05 CWS-SU-INSD-PHONE-NO                   PIC X(14).
          05 CWS-SU-YR-BUS-STRTED                   PIC X(04).
          05 CWS-SU-INSP-CNTCT-NM                   PIC X(50).
          05 CWS-SU-INSP-CNTCT-PHON                 PIC X(15).
          05 CWS-SU-OCC-DESC                        PIC X(70).
          05 CWS-SU-LSS-RUNS-IND                    PIC X(01).
          05 CWS-SU-ARSON-Q                         PIC X(01).
          05 CWS-SU-COV-BOUND-Q                     PIC X(01).
          05 CWS-SU-COV-DECL-IND                    PIC X(01).
          05 CWS-SU-FLAM-EXP-CHEM-Q                 PIC X(01).
          05 CWS-SU-NO-LOSS-IND                     PIC X(01).
          05 CWS-SU-OTH-INS-IND                     PIC X(01).
          05 CWS-SU-SUBSID-Q                        PIC X(01).

       01 CWS-DR-TABLE.
          05 CWS-DR-ID                              PIC X(9).
          05 CWS-DR-DRVR-NO                         PIC 9(03).
          05 CWS-DR-DRVR-LST-NM                     PIC X(20).
          05 CWS-DR-DRVR-FRST-NM                    PIC X(16).
          05 CWS-DR-DRVR-MDL-NM                     PIC X(11).
          05 CWS-DR-DRVR-SFX-NM                     PIC X(04).
          05 CWS-DR-DRVR-DOB-MO                     PIC X(02).
          05 CWS-DR-DRVR-DOB-DA                     PIC X(02).
          05 CWS-DR-DRVR-DOB-YR                     PIC X(04).
          05 CWS-DR-DRVR-LIC-ID                     PIC X(25).
          05 CWS-DR-DRVR-LIC-ST                     PIC X(02).
          05 CWS-DR-VEH-USED-DRVR                   PIC X(25).
          05 CWS-DR-DRVR-ADDR                       PIC X(30).
          05 CWS-DR-DRVR-CITY                       PIC X(20).
          05 CWS-DR-DRVR-ST                         PIC X(02).
          05 CWS-DR-DRVR-ZIP                        PIC X(05).
          05 CWS-DR-DRVR-ZIP-X                      PIC X(04).
          05 CWS-DR-DRVR-PRFX-NM                    PIC X(08).
          05 CWS-DR-VEH-ITM-NO                      PIC 9(04).
          05 CWS-DR-DRVR-LIC-YR                     PIC 9(4).
          05 CWS-DR-PCT-VEH-USED                    PIC 9(3).

       01 CWS-FORM.
          05 CWS-FORM-BS                            PIC X.
          05 CWS-FORM-NM                            PIC X(8).
          05 CWS-FORM-LENGTH                        PIC 9(5).
          05 CWS-S014C-SUB                          PIC X(2).
          05 CWS-S014C-QTE-NR                       PIC X(8).
          05 CWS-S014C-ED-NR                        PIC X(2).
          05 CWS-S014C-ACT-NR                       PIC X(3).
          05 CWS-S006T-CIS-POL-UND-GRP              PIC X.
          05 CWS-S006T-CIS-POL-BR                   PIC X(2).
          05 CWS-S006T-CIS-POL-NO                   PIC X(11).
          05 CWS-KB-COV-CD                          PIC X(2).
          05 CWS-KB-ST-CD                           PIC X(2).
          05 CWS-KB-FORM-NO                         PIC X(20).
          05 CWS-KB-FORM-ED-NO                      PIC X(4).
          05 CWS-KB-FORM-REV-NO                     PIC X(2).
          05 CWS-KB-INPUT-REQ                       PIC X(1).
          05 CWS-KB-SYSTEM-GEN                      PIC X(1).
          05 FIL001                                 PIC X.

       01 CWS-PL01SNG.
          05 CWS-PL01SNG-BS                         PIC X.
          05 CWS-PL01SNG-NM                         PIC X(8).
          05 CWS-PL01SNG-LENGTH                     PIC 9(5).
          05 CWS-S006S-SUB                          PIC X(2).
          05 CWS-S006S-QTE-NR                       PIC X(8).
          05 CWS-S006S-ED-NR                        PIC X(2).
          05 CWS-S006S-ACT-NR                       PIC X(3).
          05 CWS-S006T-CIS-POL-ID                   PIC X(14).
          05 CWS-S006R-INSD-ITM-NO                  PIC X(2).
          05 CWS-S006R-INSD-NAME1                   PIC X(70).
          05 CWS-S006R-INSD-NAME2                   PIC X(70).
          05 CWS-S006R-INSD-NAME3                   PIC X(70).

       01 CWS-COREPL01.
          05 CWS-COREPL01-BS                        PIC X.
          05 CWS-COREPL01-NM                        PIC X(8).
          05 CWS-COREPL01-LENGTH                    PIC 9(5).
          05 CWS-S006S-SUB                          PIC X(2).
          05 CWS-S006S-QTE-NR                       PIC X(8).
          05 CWS-S006S-ED-NR                        PIC X(2).
          05 CWS-S006S-ACT-NR                       PIC X(3).
          05 CWS-S006T-CIS-POL-UND-GRP              PIC X.
          05 CWS-S006T-CIS-POL-BR                   PIC X(2).
          05 CWS-S006T-CIS-POL-NO                   PIC X(11).
          05 CWS-S006T-PROD-ID-01                   PIC X(8).
          05 CWS-S006T-POL-EFF-DATE                 PIC X(8).
          05 CWS-S006T-INSD-NM                      PIC X(35).
          05 CWS-CORE-REQ-HAS-PR                    PIC X.
          05 CWS-CORE-REQ-HAS-GL                    PIC X.
          05 CWS-CORE-REQ-HAS-GS                    PIC X.
          05 CWS-CORE-REQ-HAS-CR                    PIC X.
          05 CWS-CORE-REQ-HAS-IM                    PIC X.
          05 CWS-CORE-REQ-HAS-AU                    PIC X.
          05 CWS-CORE-REQ-HAS-GA                    PIC X.
          05 CWS-CORE-REQ-HAS-BM                    PIC X.
          05 CWS-CORE-REQ-HAS-WC                    PIC X.
          05 CWS-CORE-REQ-HAS-AB                    PIC X.
          05 CWS-S006T-SIMP-ABC-IND                 PIC X.
          05 CWS-S006T-SIMP-AU-IND                  PIC X.
          05 CWS-S00W4-QUOT-ED-NO-02                PIC X(2).
          05 CWS-S00W5-OSE-STATUS                   PIC X.
          05 CWS-S006S-ACTIVITY-NO-6S               PIC 999.
          05 CWS-S00W5-AU-SEN                       PIC X(3).
          05 CWS-S00W5-WC-SEN                       PIC X(3).
          05 CWS-S00W5-ABC-SEN                      PIC X(3).
          05 CWS-S00W5-POL-SEN                      PIC X(3).
          05 CWS-S00W5-AU-ENDST-EFF-DT              PIC X(8).
          05 CWS-S00W5-WC-ENDST-EFF-DT              PIC X(8).
          05 CWS-S00W5-ABC-ENDST-EFF-DT             PIC X(8).
          05 CWS-S00W5-POL-ENDST-EFF-DT             PIC X(8).
          05 CWS-S00W5-AU-ENDST-STATUS              PIC X(1).
          05 CWS-S00W5-WC-ENDST-STATUS              PIC X(1).
          05 CWS-S00W5-ABC-ENDST-STATUS             PIC X(1).
          05 CWS-S00W5-POL-ENDST-STATUS             PIC X(1).
          05 CWS-S006T-POL-RT-EFF-DT                PIC X(8).
          05 CWS-S006T-AOSIF-IND                    PIC X.
          05 CWS-S00W4-CANCELLED-QUOTE              PIC X.
          05 CWS-S006T-NEW-ABC-PLUS-IND             PIC X.
          05 CWS-S006T-BROKER-OF-RECORD-IND         PIC X.
          05 CWS-AA-ABC-CONVERT-IND                 PIC X.
          05 CWS-S006T-POL-EXP-YYMMDD               PIC X(8).
          05 CWS-S006T-FILLER                       PIC X(8).

       01 CWS-PL01.
          05 CWS-PL01-BS                            PIC X.
          05 CWS-PL01-NM                            PIC X(8).
          05 CWS-PL01-LENGTH                        PIC 9(5).
          05 CWS-S006S-SUB                          PIC X(2).
          05 CWS-S006S-QTE-NR                       PIC X(8).
          05 CWS-S006S-ED-NR                        PIC X(2).
          05 CWS-S006S-ACT-NR                       PIC X(3).
          05 CWS-S006T-CIS-POL-ID                   PIC X(14).
          05 CWS-S006T-INS-ENT-CD                   PIC X.
          05 CWS-S006Q-INSD-MAIL-ADDR               PIC X(34).
          05 CWS-S006Q-INSD-MAIL-ADDR2              PIC X(25).
          05 CWS-S006Q-INSD-MAIL-CITY               PIC X(20).
          05 CWS-S006Q-INSD-MAIL-STATE              PIC X(2).
          05 CWS-S006Q-INSD-MAIL-ZIP-CD             PIC X(5).
          05 CWS-S006Q-INSD-MAIL-ZIP-6-9            PIC X(4).
          05 CWS-S006T-FFA-CO-NR                    PIC X(2).
          05 CWS-S006T-WC-CO-NR                     PIC X(2).
          05 CWS-S006T-PROD-AID                     PIC X(2).
          05 CWS-S006T-COML-POL-TYPE                PIC X.
          05 CWS-S006T-POLICY-UG                    PIC X.
          05 CWS-S006T-GRP-ID-01-RISK-ID            PIC X(3).
          05 CWS-S006T-MBR-ID-01                    PIC X(7).
          05 CWS-S006T-PRODUCING-BRANCH             PIC X(2).
          05 CWS-S006T-AIMS-BILL-CODE               PIC X(2).
          05 CWS-S00W2-PRIOR-WC-POL-NO              PIC X(14).
          05 CWS-S00W2-PRIOR-NON-WC-POL-NO          PIC X(14).
          05 CWS-S006T-OTH-END-DESC                 PIC X(30).
          05 CWS-S006T-POL-SUB-CODE                 PIC X.
          05 CWS-S006T-INSTL-PMNT-IND               PIC X(2).
          05 CWS-S006T-POL-SIC-CODE                 PIC X(5).
          05 CWS-S006T-AIMS-ACCOUNT-NO              PIC X(9).
          05 CWS-S00F7-TOT-EST-ANN-PREM             PIC S9(7)V9(2).
          05 CWS-S00F7-PROP-AO-RP-SCHED-MOD         PIC X.
          05 CWS-S006T-POL-BUS-DESC                 PIC X(30).
          05 CWS-S006T-CEI-AUTO-RELSE-IND           PIC X.
          05 CWS-S006T-CURR-CIS-WC-POL-ID           PIC X(14).
          05 CWS-S006T-CPI-AUTO-RELSE-IND           PIC X.
          05 CWS-S006T-HDQTR-STATE                  PIC 99.
          05 CWS-S006T-BMR-ID                       PIC X(11).
          05 CWS-S006T-CEI-REINSURANCE-IND          PIC X.
          05 CWS-S006T-CEI-BINDER-IND               PIC X.
          05 CWS-S006T-CEI-SIF-INSTAL-IND           PIC X.
          05 SW5-ENDST-ISSUED-PROCESS-DT-DA         PIC X(2).
          05 SW5-ENDST-ISSUED-PROCESS-DT-MO         PIC X(2).
          05 SW5-ENDST-ISSUED-PROCESS-DT-YR         PIC X(4).
          05 CWS-S006T-PROD-FEIN-SSN-IND            PIC X.
          05 CWS-S006T-PROD-FEIN-SSN-NO             PIC X(9).
          05 CWS-S006T-POLICY-TYPE-CODE             PIC X(2).
          05 CWS-S006S-UNDERWRITING-PROGRAM         PIC X(3).

       01 CWS-K6-TABLE.
          05 CWS-K6-ID                              PIC X(9).
          05 CWS-K6-MIL-DOL-LMT-REQ                 PIC X(01).
          05 CWS-K6-PROD-ACRAFT-Q                   PIC X(01).
          05 CWS-K6-FRGN-PROD-Q                     PIC X(01).
          05 CWS-K6-GUAR-WARR-Q                     PIC X(01).
          05 CWS-K6-EMPL-LEASE-Q                    PIC X(01).
          05 CWS-K6-MACH-LOAN-Q                     PIC X(01).
          05 CWS-K6-SELL-OTHRS-Q                    PIC X(01).
          05 CWS-K6-OPS-SOLD-Q                      PIC X(01).
          05 CWS-K6-PROD-LABEL-Q                    PIC X(01).
          05 CWS-K6-PROD-RECALL-Q                   PIC X(01).
          05 CWS-K6-PROD-OTHRS-Q                    PIC X(01).
          05 CWS-K6-SPONSOR-Q                       PIC X(01).
          05 CWS-K6-SUBCONT-LMT-Q                   PIC X(01).
          05 CWS-K6-SUBCONT-CERT-Q                  PIC X(01).
          05 CWS-K6-PROD-OPS-AGG                    PIC 9(09).
          05 CWS-K6-PERS-INJ-LMT                    PIC 9(09).
          05 CWS-K6-GENERAL-AGG                     PIC 9(09).
          05 CWS-K6-MED-XPNS-LMT                    PIC 9(07).

       01 CWS-K9-TABLE.
          05 CWS-K9-ID                              PIC X(9).
          05 CWS-K9-BJ-LOC-NO                       PIC 9(03).
          05 CWS-K9-BLDG-OCC-IND                    PIC X(01).
          05 CWS-K9-BLDG-NO-STORIES                 PIC 9(4).
          05 CWS-K9-NM-INSD-INT                     PIC X(02).
          05 CWS-K9-OTH-BLDG-OCC                    PIC X(50).
          05 CWS-K9-BLDG-SQ-FT                      PIC 9(09).
          05 CWS-K9-INSD-SQ-FT                      PIC 9(07).
          05 CWS-K9-ANN-GROSS-SALES                 PIC 9(08).
          05 CWS-K9-ALRM-TYP                        PIC X(01).
          05 CWS-K9-TYPE-ROOF-DESC                  PIC X(40).
          05 CWS-K9-YR-BLDG-BUILT                   PIC X(04).
          05 FIL001                                 PIC X(03).
          05 CWS-K9-RIGHT-XPO-DESC                  PIC X(40).
          05 CWS-K9-LEFT-XPO-DESC                   PIC X(40).
          05 CWS-K9-POOL-SLIDE-Q                    PIC X(01).
          05 CWS-K9-FIRE-ALRM-TYP                   PIC X.
          05 CWS-K9-LEFT-XPO-NO-FT                  PIC 9(05).
          05 CWS-K9-PART-OCC-PCT                    PIC 9(03).
          05 CWS-K9-WIRE-UPDT                       PIC 9(05).
          05 CWS-K9-ROOF-UPDT                       PIC 9(05).
          05 CWS-K9-HEAT-UPDT                       PIC 9(05).
          05 CWS-K9-PLUMB-UPDT                      PIC 9(05).
          05 CWS-K9-ARM-GUARD-NO                    PIC 9(02).
          05 CWS-K9-RIGHT-XPO-FT                    PIC 9(05).
          05 CWS-K9-REAR-XPO-FT                     PIC 9(05).
          05 CWS-K9-REAR-XPO-DESC                   PIC X(40).

       01 CWS-PL01B.
          05 CWS-PL01B-BS                           PIC X.
          05 CWS-PL01B-NM                           PIC X(8).
          05 CWS-PL01B-LENGTH                       PIC 9(5).
          05 CWS-S006S-SUB                          PIC X(2).
          05 CWS-S006S-QTE-NR                       PIC X(8).
          05 CWS-S006S-ED-NR                        PIC X(2).
          05 CWS-S006S-ACT-NR                       PIC X(3).
          05 CWS-S00F7-TOT-AP-RP-DUE                PIC S9(7)V9(2).
          05 CWS-S00XG-DPL-FNL-PREM                 PIC S9(9)V9(2).

       01 CWS-CANCEL.
          05 CWS-CANCEL-BS                          PIC X.
          05 CWS-CANCEL-NM                          PIC X(8).
          05 CWS-CANCEL-LENGTH                      PIC 9(5).
          05 CWS-S014C-SUB                          PIC X(2).
          05 CWS-S014C-QTE-NR                       PIC X(8).
          05 CWS-S014C-ED-NR                        PIC X(2).
          05 CWS-S014C-ACT-NR                       PIC X(3).
          05 CWS-S006T-CIS-POL-ID                   PIC X(14).
          05 CWS-S00W3-REINST-INIT-BY               PIC X.
          05 CWS-S00W3-REINST-EFF-YR                PIC X(4).
          05 CWS-S00W3-REINST-EFF-MO                PIC X(2).
          05 CWS-S00W3-REINST-EFF-DA                PIC X(2).
          05 CWS-S00W3-CANCEL-EFF-YR                PIC X(4).
          05 CWS-S00W3-CANCEL-EFF-MO                PIC X(2).
          05 CWS-S00W3-CANCEL-EFF-DA                PIC X(2).
          05 CWS-S00W3-CANCEL-REASON                PIC X.
          05 CWS-S00W3-CANCEL-INIT-BY               PIC X.
          05 CWS-S00W3-CANCEL-PROC-YR               PIC X(4).
          05 CWS-S00W3-CANCEL-PROC-MO               PIC X(2).
          05 CWS-S00W3-CANCEL-PROC-DA               PIC X(2).
          05 CWS-S00W3-REINST-PROC-YR               PIC X(4).
          05 CWS-S00W3-REINST-PROC-MO               PIC X(2).
          05 CWS-S00W3-REINST-PROC-DA               PIC X(2).
          05 CWS-S00W3-CANCEL-MAILED-DA             PIC XX.
          05 CWS-S00W3-CANCEL-MAILED-MO             PIC XX.
          05 CWS-S00W3-CANCEL-MAILED-YR             PIC XX.
          05 CWS-S00W3-VOID-YR                      PIC XX.
          05 CWS-S00W3-VOID-MO                      PIC XX.
          05 CWS-S00W3-VOID-DA                      PIC XX.

       01 CWS-ABCCOV.
          05 CWS-ABCCOV-BS                          PIC X.
          05 CWS-ABCCOV-NM                          PIC X(8).
          05 CWS-ABCCOV-LENGTH                      PIC 9(5).
          05 CWS-S014C-SUB                          PIC X(2).
          05 CWS-S014C-QTE-NR                       PIC X(8).
          05 CWS-S014C-ED-NR                        PIC X(2).
          05 CWS-S014C-ACT-NR                       PIC X(3).
          05 CWS-S006T-CIS-POL-ID                   PIC X(14).
          05 CWS-S14C-BLDG-ACV-IND                  PIC X.
          05 CWS-S14C-BPP-ACV-IND                   PIC X.
          05 CWS-S14C-BUS-LIAB-OCC-LMT              PIC S9(7).
          05 CWS-S14C-BUS-LIAB-AGG-LMT              PIC S9(7).
          05 CWS-S14C-LOSS-INCOME-IND               PIC X.
          05 CWS-S14C-DED-AMT                       PIC S9(4).
          05 CWS-S14C-EMP-DIS-LMT                   PIC S9(7).
          05 CWS-S14C-GUAR-REP-CST-IND              PIC X.
          05 CWS-S14C-THEFT-IND                     PIC X.
          05 CWS-S14C-LIAB-COV-PLUS-IND             PIC X.
          05 CWS-S14C-PPOP-LMT                      PIC S9(7).
          05 CWS-S14C-SABC-HIRED-AUTO-IND           PIC X.
          05 CWS-S14C-ABC-COMM-PCT                  PIC S9(2)V9.
          05 CWS-S14C-MTG-AIR-COMM-PCT              PIC S9(2)V9.
          05 CWS-S14C-EQ-COMM-PCT                   PIC S9(2)V9.
          05 CWS-S14C-SIF-COMM-PCT                  PIC S9(2)V9.
          05 CWS-S14C-ABC-FNL-MOD                   PIC S9.9(3).
          05 CWS-S14C-GRAND-TOT-POL-PREM            PIC S9(7).
          05 CWS-NEW-FILLER                         PIC 9(5).
          05 CWS-S14C-EMP-BEN-LMT-OPT               PIC S9(9).
          05 CWS-S14C-SABC-DENT-BI-AMT              PIC S9(7).
          05 CWS-S14C-SABC-DENT-DAYS                PIC S9(7).
          05 CWS-S14C-SABC-DENT-IND                 PIC X.
          05 CWS-NEW-FILLER2                        PIC X.

       01 CWS-ABCLOC.
          05 CWS-ABCLOC-BS                          PIC X.
          05 CWS-ABCLOC-NM                          PIC X(8).
          05 CWS-ABCLOC-LENGTH                      PIC 9(5).
          05 CWS-S014C-SUB                          PIC X(2).
          05 CWS-S014C-QTE-NR                       PIC X(8).
          05 CWS-S014C-ED-NR                        PIC X(2).
          05 CWS-S014C-ACT-NR                       PIC X(3).
          05 CWS-S006T-CIS-POL-ID                   PIC X(14).
          05 CWS-S14D-LOC-NBR                       PIC 9(3).
          05 CWS-S14D-TERR                          PIC 9(3).
          05 CWS-S14D-CLASS-CODE                    PIC 9(5).
          05 CWS-S14D-CONST-CODE                    PIC 9.
          05 CWS-S14D-PROGRAM-CODE-IND              PIC X.
          05 CWS-S14D-PROT-CODE                     PIC 9.
          05 CWS-S14D-SPKL-SYSTEM-IND               PIC X.
          05 CWS-S14D-BLDG-OCC-IND                  PIC X.
          05 CWS-S14D-LOC-RATE-GROUP                PIC 9(2).
          05 CWS-S14D-ST-CD                         PIC X(2).
          05 CWS-S15B-APT-COMM-OCC                  PIC X.
          05 CWS-S15B-BLDG-LMT                      PIC S9(8).
          05 CWS-S15B-APT-UNITS                     PIC 9(3).
          05 CWS-S158-BPP-LMT                       PIC S9(8).
          05 CWS-S14L-SWIM-POOL-NBR                 PIC 99.
          05 CWS-S157-MON-SEC-EXPO                  PIC S9(5).
          05 CWS-S14L-GLASS-FEET                    PIC 9(4).
          05 CWS-S14D-ALARM-IND                     PIC X.
          05 CWS-S14L-SIGN-LMT                      PIC S9(5).
          05 CWS-S14Q-SABC-AI-FRANCH-NO             PIC 9.
          05 CWS-S14Q-SABC-AI-END-OR-SALES          PIC S9(7).
          05 CWS-S157-EDP-LMT                       PIC S9(7).
          05 CWS-S157-LIQUOR-LIAB-LMT               PIC S9(7).
          05 CWS-S15C-ACCTS-REC-LMT                 PIC S9(7).
          05 CWS-S15C-SABC-PROP-OTHRS-LMT           PIC S9(7).
          05 CWS-S15C-VAL-PAPERS-LMT                PIC S9(7).
          05 CWS-S15C-LOSS-REFRIG-LMT               PIC S9(7).
          05 CWS-S14D-LOC-HAS-LIQUOR                PIC X.
          05 CWS-S14D-LOC-HAS-GLASS                 PIC X.
          05 CWS-S14S-RP-CNTL-INS-LMT               PIC S9(7).
          05 CWS-S14D-BM-WO-HEAT-IND                PIC X.
          05 CWS-S14D-BM-WITH-HEAT-IND              PIC X.
          05 CWS-S14K-SABC-TENT-GROSS-SALES         PIC S9(7).
          05 CWS-S14D-LOC-GUAR-REP-CST-IND          PIC X.
          05 CWS-S14D-LOC-BLDG-ACV-IND              PIC X.
          05 CWS-S14D-LOC-BPP-ACV-IND               PIC X.
          05 CWS-S14D-LOC-CSP-TERR-CODE             PIC X(4).
          05 CWS-S14D-LOC-GULF-WIND-IND             PIC X.
          05 CWS-S14D-WIND-DED-OPT                  PIC X.
          05 CWS-S14D-WIND-DED-PCT                  PIC S9(2).
          05 CWS-S14D-WIND-EXCL-IND                 PIC X.
          05 CWS-S14Q-SABC-AI-TYPE-IND              PIC X.
          05 CWS-S14Q-SABC-AI-LESSR-NO              PIC 9.
          05 CWS-S14D-LOC-HAS-EQ-BPP                PIC X.
          05 CWS-S14D-LOC-HAS-EQSL-BPP              PIC X.
          05 CWS-S158-BPP-EQ-DED-PCT                PIC S9(2).
          05 CWS-S158-BPP-EQ-BS-RT                  PIC S9(4)V9(3).
          05 CWS-S158-BPP-EQSL-BS-RT                PIC S9(4)V9(3).
          05 CWS-S15B-EQ-BS-RT-COVA                 PIC S9(4)V9(3).
          05 CWS-S15B-BLDG-EQSL-BS-RT               PIC S9(4)V9(3).
          05 CWS-S14D-LOC-HAS-MEPE-BRP              PIC X.
          05 CWS-S14D-LOC-HAS-MEPE-BPP              PIC X.
          05 CWS-S14K-MEPE-TENT-LMT                 PIC S9(7).
          05 CWS-S158-THEFT-EXCL-IND                PIC X.
          05 CWS-S14D-SPRINKLER-IND                 PIC X.
          05 CWS-S14C-M-S-ADV-LMT-OPT               PIC X.
          05 CWS-S157-WATER-DMG-LMT                 PIC S9(9).
          05 CWS-S157-OFF-PMS-SRV-LMT               PIC S9(9).
          05 CWS-S15B-T-N-L-LMT                     PIC S9(9).
          05 CWS-S15B-T-N-L-LMT-Q                   PIC X.
          05 CWS-S158-SABC-DENT-NBR                 PIC 9(5).
          05 CWS-S158-FILLER                        PIC X(9).

       01 CWS-ABADDINT.
          05 CWS-ABCAILOC-BS                        PIC X.
          05 CWS-ABCAILOC-NM                        PIC X(8).
          05 CWS-ABCAILOC-LENGTH                    PIC 9(5).
          05 CWS-S014C-SUB                          PIC X(2).
          05 CWS-S014C-QTE-NR                       PIC X(8).
          05 CWS-S014C-ED-NR                        PIC X(2).
          05 CWS-S014C-ACT-NR                       PIC X(3).
          05 CWS-S006T-CIS-POL-ID                   PIC X(14).
          05 CWS-ABC-AI-SUPP-IND                    PIC X(1).
          05 CWS-ABC-AI-ENDT-FILL-KEY               PIC 9(2).
          05 CWS-ABC-AI-ENDT-LOC-NO                 PIC X(3).
          05 CWS-ABC-AI-ENDT-NAME1                  PIC X(60).
          05 CWS-ABC-AI-ENDT-NAME2                  PIC X(60).
          05 CWS-ABC-AI-ENDT-STREET                 PIC X(60).
          05 CWS-ABC-AI-ENDT-CITY                   PIC X(25).
          05 CWS-ABC-AI-ENDT-ST                     PIC X(2).
          05 CWS-ABC-AI-ENDT-ZIP                    PIC X(5).
          05 CWS-ABC-AI-PROP-DESC1                  PIC X(50).
          05 CWS-ABC-AI-PROP-DESC2                  PIC X(50).
          05 CWS-ABC-AI-NS-OTH-DESC                 PIC X(50).
          05 CWS-ABC-AI-NS-TYPE                     PIC X.
          05 CWS-ABC-AI-NS-FORM-NO                  PIC X(20).
          05 CWS-ABC-AI-PROP-SER-NO                 PIC X(20).
          05 CWS-ABC-AI-PROP-MODEL-NO               PIC X(20).
          05 CWS-ABC-AI-PROP-MODEL-YR               PIC XX.
          05 CWS-ABC-AI-PROP-INS-AMT                PIC S9(9).
          05 CWS-X2-AI-LIAB-IND                     PIC XX.
          05 CWS-ABC-159-FORM-ED-NO                 PIC X(4).
          05 CWS-ABC-159-FORM-REV-NO                PIC X(2).

       01 CWS-IC-TABLE.
          05 CWS-IC-ID                              PIC X(9).
          05 IC-VEH-MAIN-Q                          PIC X(001).
          05 IC-MVR-Q                               PIC X(001).
          05 IC-RECRUIT-Q                           PIC X(001).
          05 IC-MAX-XPO-OWN-VEH                     PIC 9(9).
          05 IC-VEH-LEASE-Q                         PIC X(001).
          05 IC-WC-Q                                PIC X(001).
          05 IC-EMP-VEH-Q                           PIC X(001).
          05 IC-FAM-VEH-Q                           PIC X(001).
          05 IC-NON-SCHED-Q                         PIC X.
          05 IC-FILING-Q                            PIC X(001).
          05 IC-TRANS-HAZ-Q                         PIC X(001).
          05 IC-HARMLESS-Q                          PIC X(001).
          05 IC-APIP-LMT-AMT                        PIC 9(9).
          05 IC-PIP-DED-AMT                         PIC 9(9).
          05 IC-UM-LMT-AMT                          PIC 9(9).
          05 IC-UIM-LMT-AMT                         PIC 9(9).
          05 IC-COMP-DED-1                          PIC 9(9).
          05 IC-SPCF-PRLS-DED-1                     PIC 9(9).
          05 IC-APIP-SYM-1                          PIC X(002).
          05 IC-APIP-SYM-2                          PIC X(002).
          05 IC-DVR-MVNG-VIO-Q                      PIC X(01).
          05 IC-NOT-OWN-Q                           PIC X(01).
          05 IC-VEH-CSTM-Q                          PIC X(01).
          05 IC-UIM-SYM-1                           PIC X(02).
          05 IC-UIM-SYM-2                           PIC X(02).
          05 IC-RR-COV-LMT                          PIC 9(09).

       01 CWS-SV-TABLE.
          05 CWS-SV-ID                              PIC X(9).
          05 SV-VEH-ITM-NO                          PIC 9(004).
          05 SV-VEH-USE                             PIC X(010).
          05 SV-VEH-RADIUS                          PIC X(015).
          05 SV-VEH-GROSS-WT                        PIC 9(006).
          05 SV-VEH-ACV-IND                         PIC X.

       01 CWS-NV-TABLE.
          05 CWS-NV-ID                              PIC X(9).
          05 NV-PD-COMP-DED                         PIC 9(007).
          05 NV-HIRE-PD-NO-DAY                      PIC 9(005).
          05 NV-HIRE-PD-NO-VEH                      PIC 9(005).
          05 NV-PD-SPCF-CL-DED                      PIC 9(007).
          05 NV-K7-ST-CD                            PIC X(002).
          05 NV-NON-SCHD-COV                        PIC X(001).
          05 NV-COLL-DED                            PIC 9(007).
          05 NV-VOL-GRP-TYP                         PIC 9(005).
          05 NV-HIRE-AU-IND                         PIC X(001).

       01 CIS-EXTRACT-ERROR.
          05 CIS-ERR-HEADER                         PIC X(9).
          05 CIS-ERR-SUB-UNIT                       PIC X(2).
          05 CIS-ERR-QUOTE-NUM                      PIC X(8).
          05 CIS-ERR-EDITION                        PIC X(2).
          05 CIS-ERR-ACT-NUM                        PIC 9(3).
          05 CIS-ERR-ERROR-CODE                     PIC X(10).

       01 CIS-EXTRACT-DATA.
          05 CIS-DAT-PKT-NAME                       PIC X(09).
          05 CIS-DAT-PKT-LENGTH                     PIC 9(5).
          05 CIS-DAT-PKT-QUOTE                      PIC X(15).
          05 CIS-DAT-PKT-POLICY-NUM                 PIC X(14).
          05 CIS-DAT-DATA-AREA                      PIC X(450).

       01 CWS-AUCOV.
          05 CWS-AUCOV-BS                           PIC X.
          05 CWS-AUCOV-NM                           PIC X(8).
          05 CWS-AUCOV-LENGTH                       PIC 9(5).
          05 CWS-S0098-SUB                          PIC X(2).
          05 CWS-S0098-QTE-NR                       PIC X(8).
          05 CWS-S0098-ED-NR                        PIC X(2).
          05 CWS-S0098-ACT-NR                       PIC X(3).
          05 CWS-S006T-CIS-POL-ID                   PIC X(14).
          05 CWS-S0098-ABI-PER-OCCUR-LMT            PIC S9(9).
          05 CWS-S0098-ABI-PER-PRSN-LMT             PIC S9(9).
          05 CWS-S0098-APD-OCCUR-LMT                PIC S9(9).
          05 CWS-S0098-APD-DED-AMT                  PIC S9(5).
          05 CWS-S0098-ALL-VEH-MED-PAY-LMT          PIC S9(7).
          05 CWS-S009L-COMP-COV-SYM-1               PIC XX.
          05 CWS-S009L-COMP-COV-SYM-2               PIC XX.
          05 CWS-S009L-LIAB-COV-SYM-1               PIC XX.
          05 CWS-S009L-LIAB-COV-SYM-2               PIC XX.
          05 CWS-S009L-LIAB-COV-SYM-3               PIC XX.
          05 CWS-S009L-LIAB-COV-SYM-4               PIC XX.
          05 CWS-S009L-MED-COV-SYM-1                PIC XX.
          05 CWS-S009L-MED-COV-SYM-2                PIC XX.
          05 CWS-S009L-UM-COV-SYM-1                 PIC XX.
          05 CWS-S009L-UM-COV-SYM-2                 PIC XX.
          05 CWS-S009L-NF-COV-SYM                   PIC XX.
          05 CWS-S009L-TOW-COV-SYM                  PIC XX.
          05 CWS-S009L-SP-COV-SYM-1                 PIC XX.
          05 CWS-S009L-SP-COV-SYM-2                 PIC XX.
          05 CWS-S009L-COLL-COV-SYM-1               PIC XX.
          05 CWS-S009L-COLL-COV-SYM-2               PIC XX.
          05 CWS-S009L-COMP-COV-DED-1               PIC X(4).
          05 CWS-S009L-COMP-COV-DED-2               PIC X(4).
          05 CWS-S009L-COLL-COV-DED-1               PIC X(4).
          05 CWS-S009L-COLL-COV-DED-2               PIC X(4).
          05 CWS-S0098-CSL-IND                      PIC X(1).
          05 CWS-S0098-CAUTO-FNL-PREM               PIC S9(7).
          05 CWS-S0098-LMTS-XCPTN-IND               PIC X.
          05 CWS-S0098-SCRN80-IND                   PIC X.
          05 CWS-S009L-UI-SCRN-90                   PIC X.
          05 CWS-S009L-UI-SCRN-96                   PIC X.
          05 S9L-ALL-VEH-ALL-COV-COMM-PCT           PIC S9(2)V9.
          05 S9L-ALL-VEH-LIAB-COMM-PCT              PIC S9(2)V9.
          05 S9L-ALL-VEH-APD-COMM-PCT               PIC S9(2)V9.
          05 S9L-ALL-VEH-PPT-COMM-PCT               PIC S9(2)V9.
          05 S9L-PPT-LIAB-COMM-PCT                  PIC S9(2)V9.
          05 S9L-PPT-APD-COMM-PCT                   PIC S9(2)V9.
          05 S9L-CAUTO-ALL-VEH-COMM-PCT             PIC S9(2)V9.
          05 S9LCAUTO-ALL-VEH-LIAB-COMM-PCT         PIC S9(2)V9.
          05 S9L-CAUTO-ALL-VEH-APD-COMM-PCT         PIC S9(2)V9.
          05 CWS-S0098-LIAB-MOD                     PIC S9V9(4).
          05 CWS-S0098-PHYDM-MOD                    PIC S9V9(4).
          05 CWS-S009L-SIF-COMM-PCT                 PIC S9(2)V9.
          05 CWS-S0098-FLEETCOVER-PREMIUM           PIC S9(7).
          05 CWS-S0098-FLEETCOVER-IND               PIC X.
          05 AUCOV-FILLER                           PIC XX.

       01 CWS-AUSCHED.
          05 CWS-AUSCHED-BS                         PIC X.
          05 CWS-AUSCHED-NM                         PIC X(8).
          05 CWS-AUSCHED-LENGTH                     PIC 9(5).
          05 CWS-S0098-SUB                          PIC X(2).
          05 CWS-S0098-QTE-NR                       PIC X(8).
          05 CWS-S0098-ED-NR                        PIC X(2).
          05 CWS-S0098-ACT-NR                       PIC X(3).
          05 CWS-S006T-CIS-POL-ID                   PIC X(14).
          05 CWS-99-VEH-ITM-NO                      PIC X(4).
          05 CWS-99-VEH-MDL-YR                      PIC 9(4).
          05 CWS-99-VEH-SYM-CD                      PIC X(2).
          05 CWS-99-VEH-CST-NEW                     PIC S9(9).
          05 CWS-99-VEH-ALPHA-PRIM-CLSS-CD          PIC X(8).
          05 CWS-99-VEH-ALPHA-SEC-CLSS-CD           PIC X(3).
          05 CWS-99-CAUTO-VEH-RT-ST-CD              PIC X(2).
          05 CWS-99-CAUTO-VEH-RT-TERR-CD            PIC X(3).
          05 CWS-99-VEH-LIAB-IND                    PIC X.
          05 CWS-99-VEH-MED-PAY-IND                 PIC X.
          05 CWS-99-VEH-UM-IND                      PIC X.
          05 CWS-99-VEH-UIM-IND                     PIC X.
          05 CWS-99-VEH-OTC-CD                      PIC X(4).
          05 CWS-99-VEH-OTC-DED-AMT                 PIC S9(7).
          05 CWS-99-VEH-OTC-LMT                     PIC S9(9).
          05 CWS-99-VEH-COLL-DED                    PIC S9(5).
          05 CWS-99-VEH-TOW-IND                     PIC X.
          05 CWS-99-VEH-PIP-IND                     PIC X.
          05 CWS-99-VEH-MED-PAY-BS-PREM             PIC S9(5).
          05 CWS-9C-VEH-DESC-TXT                    PIC X(24).
          05 CWS-9C-VEH-NO                          PIC X(17).
          05 CWS-9C-VEH-XCEP-GRG-CITY-NM            PIC X(20).
          05 CWS-9C-VEH-ZIP-TERR                    PIC X(5).
          05 CWS-99-VEH-TRDE-NM                     PIC X(10).
          05 CWS-99-VEH-COLL-DEV-FCTR               PIC S9(2)V9(3).
          05 CWS-99-VEH-OTC-DEV-FCTR                PIC S9(2)V9(3).
          05 CWS-99-VEH-PPI-DEV-FCTR                PIC S9(2)V9(3).
          05 CWS-99-VEH-APIP-DEV-FCTR               PIC S9(2)V9(3).
          05 CWS-99-VEH-PIP-DEV-FCTR                PIC S9(2)V9(3).
          05 CWS-99-VEH-UM-DEV-FCTR                 PIC S9(2)V9(3).
          05 CWS-99-VEH-MED-DEV-FCTR                PIC S9(2)V9(3).
          05 CWS-99-VEH-LIAB-DEV-FCTR               PIC S9(2)V9(3).
          05 CWS-99-VEH-LIAB-ILF-FCTR               PIC S9(2)V9(3).
          05 CWS-9B-ALIAB-XCEP-ILF                  PIC S9(2)V9(3).
          05 CWS-9B-RNTRE-NO-DYS                    PIC S9(3).
          05 CWS-9B-RNTRE-RT-PER-DY                 PIC S9(3).
          05 CWS-9B-RNTRE-SP-IND                    PIC X(1).
          05 CWS-9B-RNTRE-COLL-IND                  PIC X(1).
          05 CWS-9B-RNTRE-COMP-IND                  PIC X(1).
          05 CWS-9C-VEH-TAX-TERR                    PIC X(5).
          05 CWS-99-VEH-NF-WC-EMPLR-IND             PIC X.
          05 CWS-99-VEH-STER-IND                    PIC X.
          05 CWS-99-VEH-PIP-NJ1-IND                 PIC X.
          05 CWS-99-VEH-NY-GLSS-IND                 PIC X.
          05 CWS-9B-CB-EQUIP-CST-NEW                PIC 9(5).
          05 AUSCHED-FILLER                         PIC X(4).

       01 CWS-AUNSCOV.
          05 CWS-AUNSEDH-BS                         PIC X.
          05 CWS-AUNSEDH-NM                         PIC X(8).
          05 CWS-AUNSEDH-LENGTH                     PIC 9(5).
          05 CWS-S0098-SUB                          PIC X(2).
          05 CWS-S0098-QTE-NR                       PIC X(8).
          05 CWS-S0098-ED-NR                        PIC X(2).
          05 CWS-S0098-ACT-NR                       PIC X(3).
          05 CWS-S006T-CIS-POL-ID                   PIC X(14).
          05 CWS-S009F-SEG-ID                       PIC X.
          05 CWS-S009F-ST-CD                        PIC XX.
          05 CWS-S009F-TERR-CD                      PIC X(3).
          05 CWS-S009F-ENO-EMP-CNT                  PIC S9(7).
          05 CWS-S009F-PNO-PTNR-CNT                 PIC S9(3).
          05 CWS-S009F-HIRE-CST-PER-YR              PIC S9(9).
          05 CWS-S0098-DOC-IND                      PIC X.
          05 CWS-S009F-DOC-ALIAB-IND                PIC X.
          05 CWS-S009F-DOC-MED-PAY-IND              PIC X.
          05 CWS-S009F-DOC-UM-IND                   PIC X.
          05 CWS-S009F-DOC-COMP-IND                 PIC X.
          05 CWS-S009F-DOC-COLL-IND                 PIC X.
          05 CWS-S009F-DOC-INDV-CNT                 PIC 9(3).
          05 CWS-S009F-HIRE-TAX-TC                  PIC X(5).
          05 CWS-S009F-EMP-TAX-TC                   PIC X(5).
          05 CWS-S009F-PART-TAX-TC                  PIC X(5).
          05 CWS-S009F-DOC-TAX-TC                   PIC X(5).
          05 CWS-S009F-HIRED-TERR-NO                PIC X(3).

       01 CWS-AUADDINT.
          05 CWS-AUAOI-BS                           PIC X.
          05 CWS-AUAOI-NM                           PIC X(8).
          05 CWS-AUAOI-LENGTH                       PIC 9(5).
          05 CWS-S0098-SUB                          PIC X(2).
          05 CWS-S0098-QTE-NR                       PIC X(8).
          05 CWS-S0098-ED-NR                        PIC X(2).
          05 CWS-S0098-ACT-NR                       PIC X(3).
          05 CWS-S006T-CIS-POL-ID                   PIC X(14).
          05 CWS-S09M-LSS-PYEE-SEG-SEQ-NO           PIC 9(2).
          05 CWS-S09M-LSS-PYEE-NM                   PIC X(70).
          05 CWS-S09M-LSS-PYEE-STRT-ADDR            PIC X(30).
          05 CWS-S09M-LSS-PYEE-CITY-NM              PIC X(25).
          05 CWS-S09M-LSS-PYEE-PO-ST-CD             PIC X(2).
          05 CWS-S09M-LSS-PYEE-ZIP-CD               PIC X(5).

       01 CWS-AUAISV.
          05 CWS-AUAOIVEH-BS                        PIC X.
          05 CWS-AUAOIVEH-NM                        PIC X(8).
          05 CWS-AUAOIVEH-LENGTH                    PIC 9(5).
          05 CWS-S0098-SUB                          PIC X(2).
          05 CWS-S0098-QTE-NR                       PIC X(8).
          05 CWS-S0098-ED-NR                        PIC X(2).
          05 CWS-S0098-ACT-NR                       PIC X(3).
          05 CWS-S006T-CIS-POL-ID                   PIC X(14).
          05 CWS-S09M-LSS-PYEE-SEG-SEQ-NO           PIC 9(2).
          05 CWS-S09M-LSS-PYEE-VEH-ITM-NO           PIC X(4).

       01 CWS-AUUMCOV.
          05 CWS-AUUMCOV-BS                         PIC X.
          05 CWS-AUUMCOV-NM                         PIC X(8).
          05 CWS-AUUMCOV-LENGTH                     PIC 9(5).
          05 CWS-S0098-SUB                          PIC X(2).
          05 CWS-S0098-QTE-NR                       PIC X(8).
          05 CWS-S0098-ED-NR                        PIC X(2).
          05 CWS-S0098-ACT-NR                       PIC X(3).
          05 CWS-S006T-CIS-POL-ID                   PIC X(14).
          05 CWS-UM-NF-ST-CD                        PIC X(2).
          05 CWS-S9D-UM-CSL-IND                     PIC X.
          05 CWS-S9D-UM-BI-PRSN-LMT                 PIC S9(9).
          05 CWS-S9D-UM-BI-OCCUR-LMT                PIC S9(9).
          05 CWS-S9D-UM-PD-LMT                      PIC S9(9).
          05 CWS-S9D-UIM-CSL-IND                    PIC X.
          05 CWS-S9D-UIM-ABI-PER-LMT                PIC S9(9).
          05 CWS-S9D-UIM-ABI-OCC-LMT                PIC S9(9).
          05 CWS-S9D-UIM-PD-LMT                     PIC S9(9).
          05 CWS-S9D-UM-STACKING-IND                PIC X.
          05 S09D-NF-DED-AMT                        PIC 9(5).
          05 S09D-99-BS-COV-IND                     PIC X.
          05 S09D-NF-OPT-CD                         PIC XX.
          05 S09D-DED-IND-UT-CO                     PIC X.
          05 S09D-NF-PA-MED-IND                     PIC X.
          05 S09D-NF-PA-WL-IND                      PIC X.
          05 S09D-NF-PPI-DED-AMT                    PIC 9(4).
          05 S09D-NF-MED-PAY-DED-AMT                PIC 9(4).
          05 S09D-NF-XCES-MED-PAY-IND               PIC X.
          05 S09D-NF-XCES-MED-AND-WL-MI             PIC X.
          05 S09D-NF-MILITARY-CD-FL                 PIC X.
          05 S09D-NF-XCES-WKLS-IND-FL               PIC X.
          05 S09D-NF-EXT-PIP-MED-CD                 PIC XX.
          05 S09D-NF-LTD-COV-CD                     PIC X.
          05 S09D-NF-MED-AND-WL-CO                  PIC X.
          05 S09D-NF-XCES-WKLS-IND-NY               PIC X.
          05 S09D-NF-NI-IND-NY                      PIC X.
          05 S09D-NF-DEATH-BFT-IND                  PIC X.
          05 S09D-NF-NI-IND                         PIC X.
          05 S09D-MI-MCYC-CHRG-IND                  PIC X.
          05 S09D-ARK-NF-SECT2-IND                  PIC X.
          05 S09D-ARK-NF-SECT3-IND                  PIC X.
          05 S09D-A2-PIP-WL-BEN-IND                 PIC X.
          05 S09D-A2-PIP-COPAY                      PIC X(2).
          05 S09D-A2-PIP-WAGE-LOSS                  PIC X(1).
          05 S09D-A2-PIP-DTH-BEN-OPT                PIC X(1).
          05 S09D-A2-PIP-FNL-EXPN                   PIC X(1).
          05 S09D-A2-PIP-ALT-EXPN                   PIC X(1).
          05 FIL001                                 PIC X(5).

       01 CWS-M1-TABLE.
          05 CWS-M1-ID                              PIC X(9).
          05 M1-YR-IN-BUS                           PIC 9(3).
          05 M1-NON-GRND-Q                          PIC X(001).
          05 M1-WATER-WORK-Q                        PIC X(001).
          05 M1-OTH-BUS-Q                           PIC X(001).
          05 M1-SUB-CNTRCT-Q                        PIC X(001).
          05 M1-NO-CERT-Q                           PIC X(001).
          05 M1-SFTY-PGM-Q                          PIC X(001).
          05 M1-GRP-TRANSP-Q                        PIC X(001).
          05 M1-AGE-Q                               PIC X(001).
          05 M1-OVR-60-Q                            PIC X(001).
          05 M1-PART-TM-Q                           PIC X(001).
          05 M1-VOL-Q                               PIC X(001).
          05 M1-OUT-ST-Q                            PIC X(001).
          05 M1-EMP-PHYS-Q                          PIC X(001).
          05 M1-DECL-Q                              PIC X(001).
          05 M1-WC-GEN-RMKS                         PIC X(0150).
          05 M1-CRAFT-Q                             PIC X(001).
          05 M1-PHYS-HAND-Q                         PIC X(001).
          05 M1-JOCK-Q                              PIC X(001).
          05 M1-HAZ-MAT-Q                           PIC X(001).
          05 M1-VOL-CMPN-IND                        PIC X(001).
          05 M1-BUS-OPER-DESC                       PIC X(030).
          05 M1-EMPL-WRK-HOME-Q                     PIC X(001).
          05 M1-LABOR-INTER-Q                       PIC X(001).
          05 M1-EMPL-LEASE-Q                        PIC X(001).
          05 M1-OTH-INS-Q                           PIC X(001).
          05 M1-HEALTH-PLAN-Q                       PIC X(001).
          05 M1-AUD-ADJ-FREQ                        PIC X(1).

       01 CWS-M3-TABLE.
          05 CWS-M3-ID                              PIC X(9).
          05 M3-ST-CD                               PIC X(002).
          05 M3-INSD-NO                             PIC 9(003).
          05 M3-CLS-CD-LOC-NO                       PIC 9(003).
          05 M3-CLS-CD-SEQ-NO                       PIC 9(003).
          05 M3-APP-CLS-DESC                        PIC X(050).
          05 M3-EMP-CNT                             PIC 9(005).

       01 CWS-M4-TABLE.
          05 CWS-M4-ID                              PIC X(9).
          05 M4-OTH-EMP-SEQ                         PIC 9(5).
          05 M4-JOB-CLS-CD                          PIC X(005).
          05 M4-OTH-EMP-DTY                         PIC X(030).
          05 M4-OE-NM                               PIC X(030).
          05 M4-OE-OWNER-PCT                        PIC 9(3).
          05 M4-OE-REMUN                            PIC 9(11).
          05 M4-OE-TITLE                            PIC X(030).
          05 M4-OE-DOB-YR                           PIC X(004).
          05 M4-OE-DOB-MO                           PIC X(002).
          05 M4-OE-DOB-DA                           PIC X(002).
          05 M4-OE-TYPE                             PIC X(001).
          05 M4-OE-IN-JOB-CLS                       PIC X(001).

       01 CWS-WCCOV.
          05 CWS-WCCOV-BS                           PIC X.
          05 CWS-WCCOV-NM                           PIC X(8).
          05 CWS-WCCOV-LENGTH                       PIC 9(5).
          05 CWS-S069-SUB                           PIC X(2).
          05 CWS-S069-QTE-NR                        PIC X(8).
          05 CWS-S069-ED-NR                         PIC X(2).
          05 CWS-S069-ACT-NR                        PIC X(3).
          05 CWS-S006T-CIS-POL-ID                   PIC X(14).
          05 CWS-S069-WC-DVND-PLAN                  PIC X(3).
          05 CWS-S069-ADJ-FRQ                       PIC X.
          05 CWS-S069-WC-COV-B-ACCT-LMT             PIC S9(9).
          05 CWS-S069-WC-COV-B-EMPLY-LMT            PIC S9(9).
          05 CWS-S069-WC-DEP-INSTL-PCT              PIC S99V99.
          05 CWS-S069-WC-EMPLR-LIAB-POL-LM          PIC S9(9).
          05 CWS-S069-NORM-ANN-RT-DY                PIC XX.
          05 CWS-S069-NORM-ANN-RT-MO                PIC XX.
          05 CWS-S069-PAR-IND                       PIC X.
          05 CWS-S069-WC-FEIN-NUMBER                PIC X(9).
          05 CWS-S069-WC-EFF-YR                     PIC X(4).
          05 CWS-S069-WC-EFF-MO                     PIC X(2).
          05 CWS-S069-WC-EFF-DA                     PIC X(2).
          05 CWS-S069-WC-EXP-YR                     PIC X(4).
          05 CWS-S069-WC-EXP-MO                     PIC X(2).
          05 CWS-S069-WC-EXP-DA                     PIC X(2).
          05 CWS-S069-WC-POL-FNL-PREM               PIC S9(7).
          05 CWS-S069-WC-INSD-EXPER-ID-NBR          PIC X(13).
          05 CWS-S069-WC-NBR-OF-INSTL               PIC 99.
          05 CWS-S069-WC-POL-EXP-MOD-FCTR           PIC S99V9(3).
          05 CWS-S069-WC-COST-ACCT-IND              PIC X.
          05 CWS-S069-WC-DEP-INSTL-INPT-AMT         PIC S9(7).
          05 CWS-S069-WC-RETRO-RT-IND               PIC X.
          05 CWS-S069-WC-ST-ID-1                    PIC XX.
          05 CWS-S069-WC-INTRA-ST-NO-1              PIC X(9).
          05 CWS-S069-WC-ST-ID-2                    PIC XX.
          05 CWS-S069-WC-INTRA-ST-NO-2              PIC X(9).
          05 CWS-S069-WC-ST-ID-3                    PIC XX.
          05 CWS-S069-WC-INTRA-ST-NO-3              PIC X(9).
          05 CWS-S069-WC-ST-ID-4                    PIC XX.
          05 CWS-S069-WC-INTRA-ST-NO-4              PIC X(9).
          05 CWS-S069-WC-NEG-COMM-IND               PIC X.
          05 CWS-S069-WC-LVL-COMM-RT                PIC S9(2)V9.
          05 CWS-S069-WC-COMM-1                     PIC S9(2)V9.
          05 CWS-S069-WC-COMM-2                     PIC S9(2)V9.
          05 CWS-S069-WC-COMM-3                     PIC S9(2)V9.
          05 CWS-S069-INSD-LGL-ENT-CD               PIC XX.
          05 CWS-S069-WC-BUS-DESC                   PIC X(30).
          05 CWS-S069-WC-COV-B-LMTS-RATE            PIC 9(3).
          05 CWS-S069-WC-COV-B-MIN-PREM             PIC 9(3).
          05 CWS-S069-PRLMRY-IND                    PIC X.
          05 CWS-S069-CTNGT-IND                     PIC X.
          05 CWS-S069-EMP-LSING-TYPE                PIC X.
          05 CWS-S069-RTN-RIS-POL-NO                PIC X(14).
          05 CWS-S069-WC-SAS-IND                    PIC X.
          05 CWS-S069-FILLER                        PIC X(3).

       01 CWS-WCOPLOC.
          05 CWS-WCOPLOC-BS                         PIC X.
          05 CWS-WCOPLOC-NM                         PIC X(8).
          05 CWS-WCOPLOC-LENGTH                     PIC 9(5).
          05 CWS-S069-SUB                           PIC X(2).
          05 CWS-S069-QTE-NR                        PIC X(8).
          05 CWS-S069-ED-NR                         PIC X(2).
          05 CWS-S069-ACT-NR                        PIC X(3).
          05 CWS-S006T-CIS-POL-ID                   PIC X(14).
          05 CWS-2F7-LOCATION                       PIC 999.
          05 CWS-S06F-WC-LOC-STREET                 PIC X(38).
          05 CWS-2F7-CITY                           PIC X(30).
          05 CWS-2F7-STATE                          PIC X(2).
          05 CWS-2F7-ZIP-CODE-5                     PIC X(5).
          05 CWS-2F7-ZIP-CODE-4                     PIC 9(04).
          05 CWS-2F7-ST-NUMBER                      PIC 9(10).
          05 CWS-2F7-PRE-DIRECT                     PIC X(02).
          05 CWS-2F7-ST-NAME                        PIC X(28).
          05 CWS-2F7-ST-SUFFIX                      PIC X(04).
          05 CWS-2F7-ST-POST-DIRECT                 PIC X(02).
          05 CWS-2F7-ST-FILLER                      PIC X(15).
          05 CWS-2F7-LOC-DESC                       PIC X(60).
          05 CWS-2F7-SIC-CD                         PIC X(05).
          05 CWS-2F7-NUM-OF-FULL-EMPLYE             PIC 9(05).
          05 CWS-2F7-NUM-OF-PART-EMPLYE             PIC 9(05).

       01 CWS-WCPOLXST.
          05 CWS-WCPOLXST-BS                        PIC X.
          05 CWS-WCPOLXST-NM                        PIC X(8).
          05 CWS-WCPOLXST-LENGTH                    PIC 9(5).
          05 CWS-S069-SUB                           PIC X(2).
          05 CWS-S069-QTE-NR                        PIC X(8).
          05 CWS-S069-ED-NR                         PIC X(2).
          05 CWS-S069-ACT-NR                        PIC X(3).
          05 CWS-S006T-CIS-POL-ID                   PIC X(14).
          05 CWS-S06B-WC-ST-EXP-MOD-FCTR            PIC S99V9(3).
          05 CWS-S06B-WC-ST-INSD-ID-NBR             PIC X(12).
          05 CWS-S06B-WC-RT-ST-CD                   PIC XX.
          05 CWS-S06B-WC-ST-PRELIM-EAP              PIC S9(7).
          05 CWS-S06B-WC-ST-FNL-PREM                PIC S9(7).
          05 CWS-S06B-WC-ST-SCHED-RATE-FCTR         PIC S99.9(3).
          05 CWS-S06B-WC-SMRY-DSCNT-FCTR            PIC S9(3).
          05 CWS-S06B-WC-SMRY-DSCNT-AMT             PIC S9(7).
          05 CWS-S06B-WC-ST-MOD-PREM                PIC S9(7).
          05 CWS-S06B-WC-ST-SCHED-RATE-AMT          PIC S9(7).
          05 CWS-S06B-WC-SIF-ONLY-ST-IND            PIC X.
          05 CWS-S069-WC-SIF-DI-SCRN-90-IND         PIC X.
          05 CWS-S069-WC-SIF-DI-SCRN-96-IND         PIC X.
          05 CWS-S06B-WC-ST-CO-NO                   PIC XX.
          05 CWS-S06B-WC-ST-TAX-LOCN                PIC X(5).
          05 CWS-S06B-WC-ST-PRM-PKG-MOD             PIC S9(5)V99.
          05 CWS-S06B-WC-ST-SCARE-IND               PIC X.
          05 CWS-S06B-WC-ST-SCARE-PLUS-IND          PIC X.
          05 CWS-S06B-WC-ST-SMTCR-PCT               PIC S9(5)V99.
          05 CWS-S06B-FILLER1                       PIC S9(5)V99.
          05 CWS-S06B-FILLER2                       PIC S9(5)V99.
          05 CWS-S06B-FILLER3                       PIC S9(4)V999.
          05 CWS-S06B-WC-ST-RTW-CR-PCT              PIC S9(4)V99.
          05 CWS-S06B-WC-ST-SFTY-CR-PCT-NY          PIC S9(5)V99.

       01 CWS-WCEMPGRP.
          05 CWS-WCEMPGRP-BS                        PIC X.
          05 CWS-WCEMPGRP-NM                        PIC X(8).
          05 CWS-WCEMPGRP-LENGTH                    PIC 9(5).
          05 CWS-S069-SUB                           PIC X(2).
          05 CWS-S069-QTE-NR                        PIC X(8).
          05 CWS-S069-ED-NR                         PIC X(2).
          05 CWS-S069-ACT-NR                        PIC X(3).
          05 CWS-S006T-CIS-POL-ID                   PIC X(14).
          05 CWS-S06B-WC-RT-ST-CD                   PIC XX.
          05 CWS-S06C-WC-CLSS                       PIC X(5).
          05 CWS-S069-WC-SCRN-LOC                   PIC X(3).
          05 CWS-S06C-WC-MAN-RT                     PIC S9(4)V99.
          05 CWS-S06C-WC-XPO-UN                     PIC S9(9).
          05 CWS-S06C-WC-INCD-USLH-IND              PIC X.
          05 CWS-S06C-WC-CLSS-EAP                   PIC S9(7).
          05 CWS-S06C-WC-INSD-NO                    PIC X(3).
          05 CWS-S06C-WC-CLSS-LOC-NO                PIC X(3).
          05 CWS-S06C-WC-CLSS-SYSGEN-KEY            PIC 9(3).
          05 CWS-S06C-WC-CLSS-PHRS-CD               PIC XX.
          05 CWS-S06C-WC-SIF-CLASS-IND              PIC X.

       01 CWS-WCSIN.
          05 CWS-WCSIN-BS                           PIC X.
          05 CWS-WCSIN-NM                           PIC X(8).
          05 CWS-WCSIN-LENGTH                       PIC 9(5).
          05 CWS-S069-SUB                           PIC X(2).
          05 CWS-S069-QTE-NR                        PIC X(8).
          05 CWS-S069-ED-NR                         PIC X(2).
          05 CWS-S069-ACT-NR                        PIC X(3).
          05 CWS-S006T-CIS-POL-ID                   PIC X(14).
          05 CWS-2F7-INSURED-NO                     PIC X(3).
          05 CWS-2F9-ST-EMPLR-ST                    PIC X(2).
          05 CWS-2F9-ST-EMPLR-NUM                   PIC X(15).
          05 WCSIN-FILLER                           PIC X(3).

       01 CWS-WCFEIN.
          05 CWS-WCFEIN-BS                          PIC X.
          05 CWS-WCFEIN-NM                          PIC X(8).
          05 CWS-WCFEIN-LENGTH                      PIC 9(5).
          05 CWS-S069-SUB                           PIC X(2).
          05 CWS-S069-QTE-NR                        PIC X(8).
          05 CWS-S069-ED-NR                         PIC X(2).
          05 CWS-S069-ACT-NR                        PIC X(3).
          05 CWS-S006T-CIS-POL-ID                   PIC X(14).
          05 CWS-06F-INSD-NUM                       PIC X(3).
          05 CWS-06F-FEIN-NUM                       PIC X(9).
          05 CWS-2F7-INSD-LOCATION                  PIC 9(4).

       01 CWS-WCEXL.
          05 CWS-WCEXL-BS                           PIC X.
          05 CWS-WCEXL-NM                           PIC X(8).
          05 CWS-WCEXL-LENGTH                       PIC 9(5).
          05 CWS-S069-SUB                           PIC X(2).
          05 CWS-S069-QTE-NR                        PIC X(8).
          05 CWS-S069-ED-NR                         PIC X(2).
          05 CWS-S069-ACT-NR                        PIC X(3).
          05 CWS-S006T-CIS-POL-ID                   PIC X(14).
          05 CWS-6B-RT-ST-CD                        PIC X(2).

       01 CWS-WCDED.
          05 CWS-WCDED-BS                           PIC X.
          05 CWS-WCDED-NM                           PIC X(8).
          05 CWS-WCDED-LENGTH                       PIC 9(5).
          05 CWS-S069-SUB                           PIC X(2).
          05 CWS-S069-QTE-NR                        PIC X(8).
          05 CWS-S069-ED-NR                         PIC X(2).
          05 CWS-S069-ACT-NR                        PIC X(3).
          05 CWS-S006T-CIS-POL-ID                   PIC X(14).
          05 CWS-6B-RT-ST-CD                        PIC X(2).
          05 CWS-2F1-LRG-DED-IND                    PIC X.
          05 CWS-2F1-DED-AMT                        PIC S9(9).
          05 CWS-2F1-DED-CR-FCTR                    PIC S9(9).
          05 CWS-2F1-DED-AGG-AMT                    PIC S9(9).
          05 CWS-2F1-DED-COIN-AMT                   PIC S9(9).
          05 CWS-2F1-DED-PTR-TYPE                   PIC X(2).
          05 CWS-2F1-XPR-RTNG-IND                   PIC X.
          05 CWS-2F1-PREM-DISC-IND                  PIC X.

       01 CWS-DPL1.
          05 CWS-DPL1-BS                            PIC X.
          05 CWS-DPL1-NM                            PIC X(8).
          05 CWS-DPL1-LENGTH                        PIC 9(5).
          05 CWS-S00X2-SUB                          PIC X(2).
          05 CWS-S00X2-QTE-NR                       PIC X(8).
          05 CWS-S00X2-ED-NR                        PIC X(2).
          05 CWS-S00X2-ACT-NR                       PIC X(3).
          05 CWS-S00X2-DPL-LIAB-LMT-CD              PIC X.
          05 CWS-S00X2-DPL-DEDUCT-CD                PIC X.
          05 CWS-S00X2-DPL-EPL-LMT                  PIC S9(7).
          05 CWS-S00X2-DPL-ERISA-FID-LIMIT          PIC S9(7).
          05 CWS-S00X2-DPL-CORP-RETRO-DATE          PIC X(10).
          05 CWS-S00X2-DPL-EPL-RETRO-DATE           PIC X(10).
          05 CWS-S00X2-DPL-MED-WST-RETRO-DT         PIC X(10).
          05 CWS-S00X2-GL-COMM                      PIC S99.99.
          05 CWS-S00X2-GL-SIF-COMM                  PIC S99.99.

       01 CWS-DPL2.
          05 CWS-DPL2-BS                            PIC X.
          05 CWS-DPL2-NM                            PIC X(8).
          05 CWS-DPL2-LENGTH                        PIC 9(5).
          05 CWS-S00X2-SUB                          PIC X(2).
          05 CWS-S00X2-QTE-NR                       PIC X(8).
          05 CWS-S00X2-ED-NR                        PIC X(2).
          05 CWS-S00X2-ACT-NR                       PIC X(3).
          05 CWS-S02H8-DPL-DNTST-NO                 PIC 9(3).
          05 CWS-S02H8-DPL-DNTST-NAME               PIC X(60).
          05 CWS-S02H8-DPL-STATE                    PIC X(2).
          05 CWS-S02H8-DPL-TERR                     PIC X(3).
          05 CWS-S02H8-DPL-RT-CLASS-IND             PIC X.
          05 CWS-S02H8-DPL-DNTST-RETRO-DATE         PIC X(10).
          05 CWS-S02H8-DPL-PROG-DISC                PIC 9(2).
          05 CWS-S02H8-DPL-IRPM-MOD                 PIC S9.99.
          05 CWS-S02H8-DPL-EXPER-MOD                PIC S9.99.
          05 CWS-S02H8-DPL-CLMS-FREE-DISC           PIC X(1).
          05 CWS-S02H8-DPL-AI-CONTNGT               PIC X(1).
          05 CWS-S02H8-DPL-AI-OPERS                 PIC X(1).
          05 CWS-S02H8-DPL-AI-WAIVER                PIC X(1).
          05 CWS-S02H8-DPL-AI-IND-CONTR             PIC X(1).
          05 CWS-S02H8-DPL-SPEC-CLASS               PIC X(2).
          05 CWS-S02H8-DPL-ANESTH-CLASS             PIC X(1).
          05 CWS-S02H8-DPL-MISC-CR                  PIC S9.99.
          05 CWS-S02H8-DPL-RISK-MGMT                PIC X.
          05 CWS-S02H8-DPL-YRS-IN-CLM-MADE          PIC X(2).

       01 CWS-DPL3.
          05 CWS-DPL3-BS                            PIC X.
          05 CWS-DPL3-NM                            PIC X(8).
          05 CWS-DPL3-LENGTH                        PIC 9(5).
          05 CWS-S00X2-SUB                          PIC X(2).
          05 CWS-S00X2-QTE-NR                       PIC X(8).
          05 CWS-S00X2-ED-NR                        PIC X(2).
          05 CWS-S00X2-ACT-NR                       PIC X(3).
          05 CWS-S02H8-DPL-DNTST-NO                 PIC 9(3).
          05 CWS-S02H8-DPL-TOT-DENT-PREM            PIC S9(7).

       01 CWS-DPL4.
          05 CWS-DPL4-BS                            PIC X.
          05 CWS-DPL4-NM                            PIC X(8).
          05 CWS-DPL4-LENGTH                        PIC 9(5).
          05 CWS-S00X2-SUB                          PIC X(2).
          05 CWS-S00X2-QTE-NR                       PIC X(8).
          05 CWS-S00X2-ED-NR                        PIC X(2).
          05 CWS-S00X2-DPL-IND                      PIC X.

       01 CWS-DL1-TABLE.
          05 CWS-DL1-ID                             PIC X(9).
          05 CWS-DL-DENTIST-NO                      PIC 9(002).
          05 CWS-DL-DENTIST-LIC-NO                  PIC X(010).
          05 CWS-DL-DENTIST-SSN-ID                  PIC X(009).
          05 CWS-DL-DENTIST-DOB                     PIC X(10).
          05 CWS-DL-DENTIST-SCHOOL                  PIC X(030).
          05 CWS-DL-ADDL-FORMS                      PIC X(040).
          05 CWS-DL-IRPM-FTR-DESC                   PIC X(040).
          05 CWS-DL-EXPR-FTR-DESC                   PIC X(040).
          05 CWS-DL-DENTIST-GRAD-DT                 PIC X(10).

       01 CWS-DP1-TABLE.
          05 CWS-DP1-ID                             PIC X(9).
          05 CWS-DP-EPLI-PREM                       PIC 9(007).
          05 CWS-DP-ERISA-PREM                      PIC 9(007).

       01 CWS-ABCSFCOV.
          05 CWS-FILLER-1                           PIC X(140).

       01 CWS-ABCSFITM.
          05 CWS-FILLER-2                           PIC X(260).

       01 CWSHDR.
          05 CWSHDR-FILLER                          PIC X(24).

       01 CWS-FILL.
          05 FILLER                                 PIC X(143).

       01 CWS-WCINEXCL.
          05 CWS-WCINEXCL-FILLER                    PIC X(131).

       01 CWS-ABCCV2.
          05 CWS-ABCCV2-FILLER                      PIC X(53).

       01 CWS-ABCSFEND.
          05 CWS-ABCSFEND-FILLER                    PIC X(257).

       01 CWS-ABCSFENT.
          05 CWS-ABCSFENT-FILLER                    PIC X(138).

       01 CWS-DUMMY.
          05 CWS-DUMMY-FLD                          PIC X(910).

       01 CWS-ABCSFFF.
          05 CWS-FFF-FILLER                         PIC X(174).

       01 CWS-ABCSFFFD.
          05 CWS-FFFD-FILLER                        PIC X(121).

       01 CWS-FORMAB.
          05 CWS-FORM-BS                            PIC X.
          05 CWS-FORM-NM                            PIC X(8).
          05 CWS-FORM-LENGTH                        PIC 9(5).
          05 CWS-S014C-SUB                          PIC X(2).
          05 CWS-S014C-QTE-NR                       PIC X(8).
          05 CWS-S014C-ED-NR                        PIC X(2).
          05 CWS-S014C-ACT-NR                       PIC X(3).
          05 CWS-S006T-CIS-POL-UND-GRP              PIC X.
          05 CWS-S006T-CIS-POL-BR                   PIC X(2).
          05 CWS-S006T-CIS-POL-NO                   PIC X(11).
          05 CWS-KB-COV-CD                          PIC X(2).
          05 CWS-KB-ST-CD                           PIC X(2).
          05 CWS-KB-FORM-NO                         PIC X(20).
          05 CWS-KB-FORM-ED-NO                      PIC X(4).
          05 CWS-KB-FORM-REV-NO                     PIC X(2).
          05 CWS-KB-INPUT-REQ                       PIC X(1).
          05 CWS-KB-SYSTEM-GEN                      PIC X(1).
          05 FIL001                                 PIC X.

       01 CWS-FORMAU.
          05 CWS-FORM-BS                            PIC X.
          05 CWS-FORM-NM                            PIC X(8).
          05 CWS-FORM-LENGTH                        PIC 9(5).
          05 CWS-S014C-SUB                          PIC X(2).
          05 CWS-S014C-QTE-NR                       PIC X(8).
          05 CWS-S014C-ED-NR                        PIC X(2).
          05 CWS-S014C-ACT-NR                       PIC X(3).
          05 CWS-S006T-CIS-POL-UND-GRP              PIC X.
          05 CWS-S006T-CIS-POL-BR                   PIC X(2).
          05 CWS-S006T-CIS-POL-NO                   PIC X(11).
          05 CWS-KB-COV-CD                          PIC X(2).
          05 CWS-KB-ST-CD                           PIC X(2).
          05 CWS-KB-FORM-NO                         PIC X(20).
          05 CWS-KB-FORM-ED-NO                      PIC X(4).
          05 CWS-KB-FORM-REV-NO                     PIC X(2).
          05 CWS-KB-INPUT-REQ                       PIC X(1).
          05 CWS-KB-SYSTEM-GEN                      PIC X(1).
          05 FIL002                                 PIC X.

       01 CWS-FORMGL.
          05 CWS-FORM-BS                            PIC X.
          05 CWS-FORM-NM                            PIC X(8).
          05 CWS-FORM-LENGTH                        PIC 9(5).
          05 CWS-S014C-SUB                          PIC X(2).
          05 CWS-S014C-QTE-NR                       PIC X(8).
          05 CWS-S014C-ED-NR                        PIC X(2).
          05 CWS-S014C-ACT-NR                       PIC X(3).
          05 CWS-S006T-CIS-POL-UND-GRP              PIC X.
          05 CWS-S006T-CIS-POL-BR                   PIC X(2).
          05 CWS-S006T-CIS-POL-NO                   PIC X(11).
          05 CWS-KB-COV-CD                          PIC X(2).
          05 CWS-KB-ST-CD                           PIC X(2).
          05 CWS-KB-FORM-NO                         PIC X(20).
          05 CWS-KB-FORM-ED-NO                      PIC X(4).
          05 CWS-KB-FORM-REV-NO                     PIC X(2).
          05 CWS-KB-INPUT-REQ                       PIC X(1).
          05 CWS-KB-SYSTEM-GEN                      PIC X(1).
          05 FIL003                                 PIC X.

       01 CWS-FORMPL.
          05 CWS-FORM-BS                            PIC X.
          05 CWS-FORM-NM                            PIC X(8).
          05 CWS-FORM-LENGTH                        PIC 9(5).
          05 CWS-S014C-SUB                          PIC X(2).
          05 CWS-S014C-QTE-NR                       PIC X(8).
          05 CWS-S014C-ED-NR                        PIC X(2).
          05 CWS-S014C-ACT-NR                       PIC X(3).
          05 CWS-S006T-CIS-POL-UND-GRP              PIC X.
          05 CWS-S006T-CIS-POL-BR                   PIC X(2).
          05 CWS-S006T-CIS-POL-NO                   PIC X(11).
          05 CWS-KB-COV-CD                          PIC X(2).
          05 CWS-KB-ST-CD                           PIC X(2).
          05 CWS-KB-FORM-NO                         PIC X(20).
          05 CWS-KB-FORM-ED-NO                      PIC X(4).
          05 CWS-KB-FORM-REV-NO                     PIC X(2).
          05 CWS-KB-INPUT-REQ                       PIC X(1).
          05 CWS-KB-SYSTEM-GEN                      PIC X(1).
          05 FIL004                                 PIC X.

       01 CWS-FORMWC.
          05 CWS-FORM-BS                            PIC X.
          05 CWS-FORM-NM                            PIC X(8).
          05 CWS-FORM-LENGTH                        PIC 9(5).
          05 CWS-S014C-SUB                          PIC X(2).
          05 CWS-S014C-QTE-NR                       PIC X(8).
          05 CWS-S014C-ED-NR                        PIC X(2).
          05 CWS-S014C-ACT-NR                       PIC X(3).
          05 CWS-S006T-CIS-POL-UND-GRP              PIC X.
          05 CWS-S006T-CIS-POL-BR                   PIC X(2).
          05 CWS-S006T-CIS-POL-NO                   PIC X(11).
          05 CWS-KB-COV-CD                          PIC X(2).
          05 CWS-KB-ST-CD                           PIC X(2).
          05 CWS-KB-FORM-NO                         PIC X(20).
          05 CWS-KB-FORM-ED-NO                      PIC X(4).
          05 CWS-KB-FORM-REV-NO                     PIC X(2).
          05 CWS-KB-INPUT-REQ                       PIC X(1).
          05 CWS-KB-SYSTEM-GEN                      PIC X(1).
          05 FIL005                                 PIC X.

       01 CWS-GL01FOR.
          05 CWS-GL01FOR-BS                         PIC X.
          05 CWS-GL01FOR-NM                         PIC X(8).
          05 CWS-GL01FOR-LENGTH                     PIC 9(4).
          05 CWS-S00X2-SUB                          PIC X(2).
          05 CWS-S00X2-QTE-NR                       PIC X(8).
          05 CWS-S00X2-ED-NR                        PIC X(2).
          05 CWS-S00X2-ACT-NR                       PIC X(3).
          05 CWS-S006T-CIS-POL-ID                   PIC X(14).
          05 CWS-S01B1-FORM-STATE                   PIC X(2).
          05 CWS-S01B1-FORM-CAT-CODE                PIC X(4).
          05 CWS-S01B1-FORM-NR                      PIC X(20).
          05 CWS-S01B1-FORM-EDITION                 PIC X(4).
          05 CWS-S01B1-FORM-REV-NR                  PIC X(2).
          05 CWS-S01B2-NAME-ADDR-LN1                PIC X(60).
          05 CWS-S01B2-NAME-ADDR-LN2                PIC X(60).
          05 CWS-S01B2-NAME-ADDR-LN3                PIC X(60).
          05 CWS-S01B2-NAME-ADDR-LN4                PIC X(60).
          05 CWS-S01B2-DESC-OPER-1                  PIC X(60).
          05 CWS-S01B2-DESC-OPER-2                  PIC X(60).
          05 CWS-S01B2-DESG-LOC-R OCCURS 10 TIMES
                       INDEXED BY S01B2-SUBSCRIPT.
             10 CWS-S01B2-DESG-LOC                  PIC X(3).

       01 CWS-PL01FOR.
          05 CWS-PL01FOR-BS                            PIC X.
          05 CWS-PL01FOR-NM                            PIC X(8).
          05 CWS-PL01FOR-LENGTH                        PIC 9(4).
          05 CWS-S006S-SUB                             PIC X(2).
          05 CWS-S006S-QTE-NR                          PIC X(8).
          05 CWS-S006S-ED-NR                           PIC X(2).
          05 CWS-S006S-ACT-NR                          PIC X(3).
          05 CWS-S006T-CIS-POL-ID                      PIC X(14).
          05 CWS-S00XS-FORM-STATE                      PIC X(2).
          05 CWS-S00XS-FORM-CAT-CODE                   PIC X(4).
          05 CWS-S00XS-FORM-NR                         PIC X(20).
          05 CWS-S00XS-FORM-EDITION                    PIC X(4).
          05 CWS-S00XS-FORM-REV-NR                     PIC X(2).

       01 CWS-AUSEND.
          05 CWS-AUSEND-ID                             PIC X(0).
          05 CWS-AUSEND-BS                             PIC X.
          05 CWS-AUSEND-NM                             PIC X(8).
          05 CWS-AUSEND-LENGTH                         PIC 9(5).
          05 CWS-AUSEND-DATA                           PIC X(0).
          05 CWS-S0098-SUB                             PIC X(2).
          05 CWS-S0098-QTE-NR                          PIC X(8).
          05 CWS-S0098-ED-NR                           PIC X(2).
          05 CWS-S0098-ACT-NR                          PIC X(3).
          05 CWS-S006T-CIS-POL-ID                      PIC X(14).
          05 CWS-S00M3-AU-SIF-ENDT-KEY                 PIC X(9).
          05 CWS-S00M3-AU-SIF-ENDT-DESC-1              PIC X(60).
          05 CWS-S00M3-AU-SIF-ENDT-DESC-2              PIC X(60).
          05 CWS-S00M3-AU-SIF-ENDT-DESC-3              PIC X(60).
          05 CWS-S00M3-AU-SIF-ENDT-TXT-ID              PIC X(8).

       01 CWS-AUSENDTX.
          05 CWS-AUSENDTX-FILLER                       PIC X(127).

       01 CWS-AUSFCOV.
          05 CWS-AUSFCOV-FILLER                        PIC X(154).

       01 CWS-AUSFCVTX.
          05 CWS-AUSFCVTX-FILLER                       PIC X(121).

       01 CWS-AUSRAT.
          05 CWS-AUSRAT-FILLER                         PIC X(481).

       01 CWS-DL1-TABLE.
          05 CWS-DL1-FILLER                            PIC X(101).

       01 CWS-DL2-TABLE.
          05 CWS-DL2-FILLER                            PIC X(193).

       01 CWS-EXTRACT-ERROR.
          05 CWS-EXTRACT-ERROR-FILLER                  PIC X(34).

       01 CWS-GLSIFRAT.
          05 CWS-GLSIFRAT-FILLER                       PIC X(56).

       01 CWS-GLSIFTXT.
          05 CWS-GLSIFTXT-FILLER                       PIC X(134).

       01 CWS-WCSFCLSS.
          05 CWS-WCSFCLSS-FILLER                       PIC X(126).

       01 CWS-WCSFEND.
          05 CWS-WCSFEND-FILLER                        PIC X(245).

       01 CWS-WCSFENT.
          05 CWS-WCSFENT-FILLER                        PIC X(132).

       01 CWS-WCSFST.
          05 CWS-WCSFST-FILLER                         PIC X(72).

       01 CWS-WCSFPHRS.
          05 CWS-WCSPHRS-FILLER                        PIC X(87).

       01 CWS-ABCCV3.
          05 CWS-ABCCV3-BS                             PIC X.
          05 CWS-ABCCV3-NM                             PIC X(8).
          05 CWS-ABCCV3-LENGTH                         PIC 9(5).
          05 CWS-ABCCV3-SUB                            PIC X(2).
          05 CWS-ABCCV3-QTE-NR                         PIC X(8).
          05 CWS-ABCCV3-ED-NR                          PIC X(2).
          05 CWS-ABCCV3-ACT-NR                         PIC X(3).
          05 CWS-ABCCV3-CIS-POL-ID                     PIC X(14).
          05 CWS-S14C-TOTAL-NO-EMPL                    PIC 9(5).

       01 CWS-ABCLC2.
          05 CWS-ABCLC2-FILLER                         PIC X(50).

       01 CWS-ABCLC3.
          05 CWS-ABCLC3-FILLER                         PIC X(49).

       01 CWS-ABCLC4.
          05 CWS-ABCLC4-FILLER                         PIC X(49).

       01 CWS-ABCLC5.
          05 CWS-ABCLC5-FILLER                         PIC X(50).

       01 CWS-ABCLC6.
          05 CWS-ABCLC6-FILLER                         PIC X(56).

       01 CWS-PL02LC2.
          05 CWS-PL02LC2-FILLER                        PIC X(47).

       01 CWS-PL02LC3.
          05 CWS-PL02LC3-FILLER                        PIC X(86).

       01 CWS-PL02LC4.
          05 CWS-PL02LC4-FILLER                        PIC X(86).

       01 CWS-PL02LC5.
          05 CWS-PL02LC5-FILLER                        PIC X(52).

       01 CWS-PL02LC6.
          05 CWS-PL02LC6-FILLER                        PIC X(58).

       01 CWS-PL02LC7.
          05 CWS-PL02LC7-FILLER                        PIC X(47).

       01 CWS-WCFORMK.
          05 CWS-WCFORMK-FILLER                        PIC X(45).

       01 CWS-COR1PL01.
          05 CWS-COR1PL01-FILLER                       PIC X(51).

       01 CWS-AUDRVR.
          05 CWS-AUDRVR-FILLER                         PIC X(100).

       01 CWS-AOEXPL.
          05 CWS-AOEXPL-FILLER                         PIC X(44).

       01 CWS-AUVEHOWN.
          05 CWS-AUVEHOWN-FILLER                       PIC X(274).

       01 CWS-PL02.
          05 CWS-PL02-FILLER                           PIC X(53).

       01 CWS-COR2PL01.
          05 CWS-COR2PL01-FILLER                       PIC X(59).
