      ******************************************************************
      *  DESC INFORMATION FILE LAYOUT                                  *
      ******************************************************************
      *COPYBOOK: PCB07UPLD
      *FILENAME: PDKUPLOAD
      *RECSIZE:  250

       01  DESC-RECORD.
           05  A15-M7-REC-ID           PIC X(02).
           05  A05-M7-SDS-LINE         PIC X(02).
           05  B94-M7-LOCATION         PIC 9(03).
           05  B79-M7-ITEM-SEQ         PIC 9(03).
           05  C87-M7-COV-END-CODE     PIC X(10).
           05  DESCRIPTION-AREA.
               10  C86-M7-DESCR-LINE1  PIC X(42).
               10  C86-M7-DESCR-LINE2  PIC X(42).
               10  C86-M7-DESCR-LINE3  PIC X(42).
               10  C86-M7-DESCR-LINE4  PIC X(42).
               10  C86-M7-DESCR-LINE5  PIC X(42).
           05  ADDRESS-AREA REDEFINES DESCRIPTION-AREA.
               10  C86-M7-ADDR-LINE1   PIC X(42).
               10  C86-M7-ADDR-LINE2   PIC X(42).
               10  C86-M7-STREET-LINE  PIC X(42).
               10  C86-CITY-ST-ZIP.
                   15  C86-CITY        PIC X(34).
                   15  C86-ST          PIC X(02).
                   15  C86-ZIP         PIC X(05).
                   15  FILLER          PIC X(01).
               10  FILLER              PIC X(42).
           05  FILLER                  PIC X(20).
