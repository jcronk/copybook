        01 PMDUYC1-RISK-UNIT-RATE-RECORD.
           03 PMDUYC1-RISK-UNIT-RATE-SEG.                               
            05 PMDUYC1-SEGMENT-KEY.                                     
              07 PMDUYC1-SEGMENT-ID                   PIC X(02).        
                 88 PMDUYC1-SEGMENT-43                 VALUE '43'.      
              07 PMDUYC1-SEGMENT-STATUS               PIC X(01).        
                 88 PMDUYC1-ACTIVE-STATUS              VALUE 'A'.       
                 88 PMDUYC1-DELETED-STATUS             VALUE 'D'.       
                 88 PMDUYC1-HISTORY-STATUS             VALUE 'H'.       
                 88 PMDUYC1-NOT-ACTIVE-STATUS          VALUE 'H' 'D'.   
                 88 PMDUYC1-RENEWAL-FS-STATUS          VALUE 'R' 'F'.   
              07 PMDUYC1-TRANSACTION-DATE          PIC 9(08).
              07 PMDUYC1-SEGMENT-ID-KEY.                                
                 09 PMDUYC1-SEGMENT-LEVEL-CODE        PIC X(01).        
                    88 PMDUYC1-SEG-LEVEL-U             VALUE 'U'.       
                 09 PMDUYC1-SEGMENT-PART-CODE         PIC X(01).        
                    88 PMDUYC1-SEG-PART-Y              VALUE 'Y'.       
                 09 PMDUYC1-SUB-PART-CODE             PIC X(01).        
                 09 PMDUYC1-INSURANCE-LINE            PIC X(02).        
                    88 PMDUYC1-COMMERCIAL-CRIME       VALUE 'CR'.       
              07 PMDUYC1-LEVEL-KEY.                                     
                 09 PMDUYC1-LOCATION-NUMBER-A.                          
                    11 PMDUYC1-LOCATION-NUMBER        PIC 9(04).        
                 09 PMDUYC1-SUB-LOCATION-NUMBER-A.                      
                    11 PMDUYC1-SUB-LOCATION-NUMBER    PIC 9(03).        
                 09 PMDUYC1-RISK-UNIT-GROUP-KEY       PIC X(06).        
                 09 PMDUYC1-RISK-UNIT.                                  
                    11 PMDUYC1-COVERAGE               PIC X(03).        
                       88 PMDUYC1-A-EMPLOY-DISH-BLKT     VALUE '530'.   
                       88 PMDUYC1-A-EMPLOY-DISH-SCHED    VALUE '531'.   
                       88 PMDUYC1-B-FORG-OR-ALT          VALUE '532'.   
                       88 PMDUYC1-CP-T-D-D-PREM          VALUE '534'.   
                       88 PMDUYC1-CM-T-D-D-MESS          VALUE '535'.   
                       88 PMDUYC1-DR-ROB-CUST            VALUE '536'.   
                       88 PMDUYC1-DB-SAFE-BURG           VALUE '537'.   
                       88 PMDUYC1-DM-ROB-MESS            VALUE '538'.   
                       88 PMDUYC1-E-PREM-BURG            VALUE '542'.   
                       88 PMDUYC1-F-COMPUTE-FRD          VALUE '544'.   
                       88 PMDUYC1-G-EXTORTION            VALUE '546'.   
                       88 PMDUYC1-HP-PREM-THEFT          VALUE '548'.   
                       88 PMDUYC1-HM-ROB-OUTSID          VALUE '549'.   
                       88 PMDUYC1-IS-LESSEE-SEC          VALUE '550'.   
                       88 PMDUYC1-IP-LESSEE-PRO          VALUE '551'.   
                       88 PMDUYC1-J-SECUR-W-OTH          VALUE '552'.   
                       88 PMDUYC1-RP6-K-GST-SAFE-DP      VALUE '554'.   
                       88 PMDUYC1-RP7-L-GUEST-PREM       VALUE '556'.   
                       88 PMDUYC1-RP8-M-SAFE-LIAB        VALUE '558'.   
                       88 PMDUYC1-RP8-NR-SAFE-DIRECT     VALUE '560'.   
                       88 PMDUYC1-RP8-NB-SAFE-DIRECT     VALUE '565'.   
                       88 PMDUYC1-QR-ROB-CS              VALUE '566'.   
                       88 PMDUYC1-QB-SF-BRG              VALUE '567'.   
                       88 PMDUYC1-QM-ROB-MES             VALUE '568'.   
                       88 PMDUYC1-RP3-STOREKEEP-BF       VALUE '570'.   
                       88 PMDUYC1-RP4-STOREKEEP-BURG     VALUE '580'.   
                       88 PMDUYC1-RP5-OFFICE-BURG-ROB    VALUE '590'.   
                       88 PMDUYC1-RP9-EXCESS-BANK-ROB    VALUE '600'.   
                       88 PMDUYC1-RP9-EXCESS-BANK-BURG   VALUE '605'.   
                       88 PMDUYC1-RP10-BANK-EXCESS-SEC   VALUE '610'.   
                       88 PMDUYC1-MISCELLANEOUS          VALUE '999'.   
                    11 PMDUYC1-COVERAGE-AMENDMENT     PIC X(03).        
                       88 PMDUYC1-MISCELLANEOUS          VALUE 'MSC'.   
                       88 PMDUYC1-BASE-COVERAGE          VALUE '   '.   
                 09 PMDUYC1-SEQUENCE-RISK-UNIT.                         
                    11 PMDUYC1-MESSENGER              PIC 9(01).        
                    11 FILLER                         PIC X(01).        
              07 PMDUYC1-ITEM-EFFECTIVE-DATE          PIC 9(08).
              07 PMDUYC1-VARIABLE-KEY                 PIC X(06).        
              07 PMDUYC1-PROCESS-DATE          PIC 9(08).
            05 PMDUYC1-SEGMENT-DATA.                                    
                 09 PMDUYC1-SEGMENT-MG-IND            PIC X(01).        
                 09 PMDUYC1-PREMIUM-A.                                  
                    11 PMDUYC1-PREMIUM                PIC 9(09).        
                 09 PMDUYC1-PREM-MG-IND               PIC X(01).        
                 09 PMDUYC1-DEDUCT-MULTIPLIER-A.                        
                    11 PMDUYC1-DEDUCTIBLE-MULTIPLIER  PIC 9(01)V9(03).  
                 09 PMDUYC1-DED-MG-IND                PIC X(01).        
                 09 PMDUYC1-RDF-A.                                      
                    11 PMDUYC1-RDF                    PIC 9(01)V9(03).  
                 09 PMDUYC1-RDF-MG-IND                PIC X(01).        
                 09 PMDUYC1-COMMISSION-RATE-A         PIC X(06).        
                 09 PMDUYC1-COMM-MG-IND               PIC X(01).        
                 09 PMDUYC1-PACKAGE-MOD-A.                              
                    11 PMDUYC1-PACKAGE-MOD            PIC 9(01)V9(03).  
                 09 PMDUYC1-PACKAGE-MG-IND            PIC X(01).        
                 09 PMDUYC1-COMPANY-DEVIATION         PIC 9(01)V9(03).  
                 09 PMDUYC1-INIT-PREM-MESS-CHRG       PIC 9(07)V9(03).  
                 09 PMDUYC1-RATE-BOOK-ID              PIC X(01).        
                 09 FILLER                            PIC X(01).        
                 09 PMDUYC1-RMF-A.                                      
                    11 PMDUYC1-RMF                    PIC 9(01)V9(03).  
                 09 PMDUYC1-TOTAL-DISCOUNT-RATE-A.                      
                    11 PMDUYC1-TOTAL-DISCOUNT-RATE    PIC 9(01)V9(03).  
                 09 PMDUYC1-DISC-MG-IND               PIC X(01).        
                 09 PMDUYC1-AMENDMENT-RATE-A.                           
                    11 PMDUYC1-AMENDMENT-RATE         PIC 9(01)V9(03).  
                 09 PMDUYC1-AMENDMENT-MG-IND          PIC X(01).        
                 09 PMDUYC1-CALC-PREM-FCTR-A.                           
                    11 PMDUYC1-CALC-PREM-FCTR         PIC 9(01)V9(03).  
                 09 PMDUYC1-FORM-CODE                 PIC X(02).        
                 09 PMDUYC1-PROTECTIVE-DEVICE         PIC X(02).        
                 09 PMDUYC1-TERRITORY-MULTIPLIER-A.                     
                    11 PMDUYC1-TERRITORY-MULTIPLIER   PIC 9(01)V9(03).  
                 09 PMDUYC1-TERR-MG-IND               PIC X(01).        
                 09 PMDUYC1-DED-CREDIT                PIC 9(04)V9(03).  
                 09 PMDUYC1-NUM-MESSENGERS            PIC 9(03).        
                 09 PMDUYC1-BASE-RATE-AREA.                             
                    11 PMDUYC1-1ST-BASE-RATE-A.                         
                       13 PMDUYC1-1ST-BASE-RATE       PIC 9(03)V9(04).  
                    11 PMDUYC1-1ST-RATE-MG-IND        PIC X(01).        
                    11 PMDUYC1-2ND-BASE-RATE-A.                         
                       13 PMDUYC1-2ND-BASE-RATE       PIC 9(03)V9(04).  
                    11 PMDUYC1-2ND-RATE-MG-IND        PIC X(01).        
                    11 PMDUYC1-3RD-BASE-RATE-A.                         
                       13 PMDUYC1-3RD-BASE-RATE       PIC 9(03)V9(04).  
                    11 PMDUYC1-3RD-RATE-MG-IND        PIC X(01).        
                    11 PMDUYC1-4TH-BASE-RATE-A.                         
                       13 PMDUYC1-4TH-BASE-RATE       PIC 9(03)V9(04).  
                    11 PMDUYC1-4TH-RATE-MG-IND        PIC X(01).        
                    11 PMDUYC1-5TH-BASE-RATE-A.                         
                       13 PMDUYC1-5TH-BASE-RATE       PIC 9(03)V9(04).  
                    11 PMDUYC1-5TH-RATE-MG-IND        PIC X(01).        
                 09 PMDUYC1-CLASS-OF-INSURED          PIC X(03).        
                 09 PMDUYC1-RATE1-A.                                    
                    11 PMDUYC1-RATE1                  PIC 9(07)V9(03).  
                 09 PMDUYC1-RATE2-A.                                    
                    11 PMDUYC1-RATE2                  PIC 9(07)V9(03).  
                 09 PMDUYC1-NUMBER-OF-EMPL-1-A.                         
                    11 PMDUYC1-NUMBER-OF-EMPLOYEE-1   PIC 9(06).        
                 09 PMDUYC1-NUMBER-OF-EMPL-2-A.                         
                    11 PMDUYC1-NUMBER-OF-EMPLOYEE-2   PIC 9(06).        
                 09 PMDUYC1-TOTAL-EMPLOYEES-A.                          
                    11 PMDUYC1-TOTAL-EMPLOYEES        PIC 9(07).        
                 09 PMDUYC1-NUMBER-EMPLOYEE-CODE      PIC X(01).        
                 09 PMDUYC1-DEC-CHANGE-FLAG           PIC X(01).        
                    88 PMDUYC1-DEC-CHANGE-NEEDED      VALUE 'A'.        
                    88 PMDUYC1-DEC-CHANGE-NOTED       VALUE 'B'.        
                 09 PMDUYC1-1ST-25-LOC-FCTR           PIC 9V9(03).      
                 09 PMDUYC1-2ND-25-LOC-FCTR           PIC 9V9(03).      
                 09 PMDUYC1-OVR-50-LOC-FCTR           PIC 9V9(03).      
                 09 PMDUYC1-TOT-ADD-LOC-CHG           PIC 9(10)V9(03).  
                 09 PMDUYC1-TERM-FACTOR-A.                              
                    11 PMDUYC1-TERM-FACTOR            PIC 9(01)V9(03).  
                 09 PMDUYC1-ADDL-LOC-A.                                 
                    11 PMDUYC1-ADDL-LOC-CHG           PIC 9(04)V9(03).  
                 09 PMDUYC1-PERS-ACCTS-FACTOR         PIC 9(03)V9(02).  
                 09 PMDUYC1-COV-MIN-PREM-IND          PIC X(01).        
                 09 PMDUYC1-FORM-FACTOR               PIC V9(02).       
                 09 PMDUYC1-RMF-MG-IND                PIC X(01).        
                 09 PMDUYC1-RATE1-MG-IND              PIC X(01).        
                 09 PMDUYC1-RATE2-MG-IND              PIC X(01).        
                 09 PMDUYC1-MO-CPC-PREM               PIC 9(05)V9(02).  
                 09 PMDUYC1-1RST-CPC-RTE              PIC 9(01)V9(03).  
                 09 PMDUYC1-2ND-CPC-RTE               PIC 9(01)V9(03).  
                 09 PMDUYC1-REPLACE-CST-FACT          PIC 9(03)V9(03).  
                 09 PMDUYC1-BASE-LOSS-COST-FACTOR     PIC 9(03)V9(03).  
                 09 PMDUYC1-FORM-CODE-THREE           PIC X(03).        
                 09 PMDUYC1-MOD-BASE-LOSS-FACT        PIC 9(03)V9(03).  
                 09 PMDUYC1-NEW-LOSS-COST-MULT        PIC 9(03)V9(03).  
                 09 PMDUYC1-PMS-FUTURE-USE            PIC X(04).        
                 09 PMDUYC1-CUSTOMER-USE              PIC X(01).        
                 09 PMDUYC1-TERRCOV                   PIC X(29).        
                 09 PMDUYC1-YR2000-CUST-USE           PIC X(100).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).