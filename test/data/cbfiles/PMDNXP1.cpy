        01 PMDNXP1-SUB-LOC-RATING-RECORD.
           03 PMDNXP1-SUB-LOC-RATING-SEG.                               
            05 PMDNXP1-SEGMENT-KEY.                                     
              07 PMDNXP1-SEGMENT-ID                   PIC X(02).        
                 88 PMDNXP1-SEGMENT-43                 VALUE '43'.      
              07 PMDNXP1-SEGMENT-STATUS               PIC X(01).        
                 88 PMDNXP1-ACTIVE-STATUS              VALUE 'A'.       
                 88 PMDNXP1-DELETED-STATUS             VALUE 'D'.       
                 88 PMDNXP1-HISTORY-STATUS             VALUE 'H'.       
                 88 PMDNXP1-NOT-ACTIVE-STATUS          VALUE 'H' 'D'.   
                 88 PMDNXP1-RENEWAL-FS-STATUS          VALUE 'R' 'F'.   
              07 PMDNXP1-TRANSACTION-DATE             PIC 9(08).
              07 PMDNXP1-SEGMENT-ID-KEY.                                
                 09 PMDNXP1-SEGMENT-LEVEL-CODE        PIC X(01).        
                    88 PMDNXP1-SEG-LEVEL-N             VALUE 'N'.       
                 09 PMDNXP1-SEGMENT-PART-CODE         PIC X(01).        
                    88 PMDNXP1-SEG-PART-X              VALUE 'X'.       
                 09 PMDNXP1-SUB-PART-CODE             PIC X(01).        
                 09 PMDNXP1-INSURANCE-LINE            PIC X(02).        
                    88  PMDNXP1-COMMERCIAL-FIRE       VALUE 'CF'.       
              07 PMDNXP1-LEVEL-KEY.                                     
                 09 PMDNXP1-LOCATION-NUMBER-A.                          
                    11 PMDNXP1-LOCATION-NUMBER        PIC 9(04).        
                       88 PMDNXP1-TERRORISM-COV-LOC   VALUE 0911.       
                 09 PMDNXP1-SUB-LOCATION-NUMBER-A.                      
                    11 PMDNXP1-SUB-LOCATION-NUMBER    PIC 9(03).        
              07 PMDNXP1-ITEM-EFFECTIVE-DATE          PIC 9(08).
              07 PMDNXP1-VARIABLE-KEY.                                  
                 09 PMDNXP1-PEAK-SEASON-INDICATOR     PIC X(01).        
                    88 PMDNXP1-PEAK-SEASON-NO           VALUE '0'.      
                    88 PMDNXP1-PEAK-SEASON-YES          VALUE 'P'.      
                 09 FILLER                            PIC X(05).        
              07 PMDNXP1-PROCESS-DATE                 PIC 9(08).
              05 PMDNXP1-SEGMENT-DATA.                                  
                 09 PMDNXP1-ZONE-CODE.                                  
                    88 PMDNXP1-SEACOAST-AREA-1     VALUE 'S1'.          
                    88 PMDNXP1-SEACOAST-AREA-2     VALUE 'S2'.          
                    88 PMDNXP1-SEACOAST-AREA-3     VALUE 'S3'.          
                    88 PMDNXP1-SEACOAST-AREA-4     VALUE 'S4'.          
                    11 PMDNXP1-ZONE                   PIC X(01).        
                       88 PMDNXP1-SEACOAST         VALUE 'S'.           
                       88 PMDNXP1-BEACH            VALUE 'B'.           
                       88 PMDNXP1-INLAND           VALUE 'I'.           
                       88 PMDNXP1-MIDDLE           VALUE 'M'.           
                    11 PMDNXP1-ZONE-AREA              PIC X(01).        
                 09 PMDNXP1-COMPANY-NUMBER-A.                           
                    11 PMDNXP1-COMPANY-NUMBER         PIC 9(02).        
                 09 PMDNXP1-AGENTS-COMM-RATE-A.                         
                    11 PMDNXP1-AGENTS-COMM-RATE       PIC 9(01)V9(05).  
                 09 FILLER                            PIC X(04).        
                 09 PMDNXP1-SCHEDULE-MOD-A.                             
                    11 PMDNXP1-SCHEDULE-MOD           PIC 9(01)V9(03).  
                 09 PMDNXP1-COMM-RED-MOD-A.                             
                    11 PMDNXP1-COMM-RED-MOD           PIC 9(01)V9(03).  
                 09 PMDNXP1-OTHER-MOD-A.                                
                    11 PMDNXP1-OTHER-MOD              PIC 9(01)V9(03).  
                 09 PMDNXP1-EXPENSE-MOD-A.                              
                    11 PMDNXP1-EXPENSE-MOD            PIC 9(01)V9(03).  
                 09 PMDNXP1-IRPM-A.                                     
                    11 PMDNXP1-IRPM                   PIC 9(01)V9(03).  
                 09 PMDNXP1-CSP-CONSTR-COD-A.                           
                    11 PMDNXP1-CSP-CONSTR-COD         PIC 9(01).        
                 09 PMDNXP1-MIXED-CONSTR-IND          PIC X(01).        
                 09 PMDNXP1-PROTECTION-CLASS.                           
                    11 PMDNXP1-PROT-CLASS-PART1-A.                      
                       13 PMDNXP1-PROTECTION-CLASS-PART1 PIC X(02).     
                    11 PMDNXP1-PROTECTION-CLASS-PART2 PIC X(02).        
                 09 PMDNXP1-NUMBER-OF-UNITS-A.                          
                    11 PMDNXP1-NUMBER-OF-UNITS        PIC 9(04).        
                 09 PMDNXP1-SEASONAL-INDICATOR        PIC X(01).        
                 09 PMDNXP1-VACANCY-IND               PIC X(01).        
                    88  PMDNXP1-VACANCY-INCLUDED      VALUE 'Y'.        
                 09 PMDNXP1-VAC-CVG-EFF-DAT           PIC 9(08).
                 09 PMDNXP1-VAC-CVG-EXPIR-DAT         PIC 9(08).
                 09 PMDNXP1-CHRG-ADDIT-INS-IND        PIC X(01).        
                 09 PMDNXP1-NUMBER-WAREHOUSES         PIC X(03).        
                 09 PMDNXP1-WTCHMN-MOD-FACT-A.                          
                    11 PMDNXP1-WTCHMN-MOD-FACT        PIC V9(02).       
                 09 PMDNXP1-PRM-BRG-ALM-MOD-FCT-A.                      
                    11 PMDNXP1-PREM-BURG-ALM-MOD-FACT PIC V9(02).       
                 09 PMDNXP1-BLDG-SPECIFIC-RATE-A.                       
                    11 PMDNXP1-BLDG-SPECIFIC-RATE     PIC 9(03)V9(03).  
                 09 PMDNXP1-CONT-SPECIFIC-RATE-A.                       
                    11 PMDNXP1-CONT-SPECIFIC-RATE     PIC 9(03)V9(03).  
                 09 PMDNXP1-M-E-SPECIFIC-RATE-A.                        
                    11 PMDNXP1-M-E-SPECIFIC-RATE      PIC 9(03)V9(03).  
                 09 PMDNXP1-STOCK-SPECIFIC-RATE-A.                      
                    11 PMDNXP1-STOCK-SPECIFIC-RATE    PIC 9(03)V9(03).  
                 09 PMDNXP1-GRPII-SPECIFIC-RATE-A.                      
                    11 PMDNXP1-GRPII-SPECIFIC-RATE    PIC 9(03)V9(03).  
                 09 PMDNXP1-GRPII-SYMBOL.                               
                    11 PMDNXP1-GRPII-EC-SYMBOL        PIC X(02).        
                    11 PMDNXP1-GRPII-NUM-PREFIX-A.                      
                    13 PMDNXP1-GRPII-NUM-PREFIX    PIC 9(01)V9(01).     
                       88 PMDNXP1-PREF-IS-ONE-HALF  VALUE 0.5.          
                 09 PMDNXP1-BLDG-PUB-COINS-PCT-A.                       
                    11 PMDNXP1-BLDG-PUB-COINS-PCT     PIC 9(03).        
                 09 PMDNXP1-CONT-PUB-COINS-PCT-A.                       
                    11 PMDNXP1-CONT-PUB-COINS-PCT     PIC 9(03).        
                 09 PMDNXP1-M-E-PUB-COINS-PCT-A.                        
                    11 PMDNXP1-M-E-PUB-COINS-PCT      PIC 9(03).        
                 09 PMDNXP1-STOCK-PUB-COINS-PCT-A.                      
                    11 PMDNXP1-STOCK-PUB-COINS-PCT    PIC 9(03).        
                 09 PMDNXP1-SPECIFIC-EFF-DATE         PIC 9(08).
                 09 PMDNXP1-SPECIAL-CLS-RATE-ITEM.                      
                    11 PMDNXP1-SPL-CLS-RATE-ITEM-1    PIC X(02).        
                    11 PMDNXP1-SPL-CLS-RATE-ITEM-2    PIC X(01).        
                    11 PMDNXP1-SPL-CLS-RATE-ITEM-3    PIC X(01).        
                 09 PMDNXP1-OPEN-SIDES-IND            PIC X(01).        
                 09 PMDNXP1-SPRINKLER-TYPE            PIC X(01).        
                    88 PMDNXP1-FULL-SPRINKLER          VALUE 'F'.       
                    88 PMDNXP1-PARTIAL-SPRINKLER       VALUE 'P'.       
                 09 PMDNXP1-ALARM-TYPES               PIC X(01).        
                    88 PMDNXP1-CENT-STN-WITH-KEYS      VALUE 'C'.       
                    88 PMDNXP1-LOCAL-GONG              VALUE 'G'.       
                 09 PMDNXP1-ALARM-INSTALLATION-A.                       
                    11 PMDNXP1-ALARM-INSTALLATION     PIC 9(01).        
                 09 PMDNXP1-ALARM-CLASS               PIC X(01).        
                 09 PMDNXP1-AGREED-EXPIR-DATE         PIC 9(08).
                 09 PMDNXP1-MERCANTILE-AREA-A.                          
                    11 PMDNXP1-MERCANTILE-AREA        PIC 9(07).        
                 09 PMDNXP1-MANUFACTURING-AREA-A.                       
                    11 PMDNXP1-MANUFACTURING-AREA     PIC 9(07).        
                 09 PMDNXP1-VARIABLE-AREA-BY-STATE    PIC X(10).        
                 09 PMDNXP1-RENTAL-AREA-A.                              
                    11 PMDNXP1-RENTAL-AREA            PIC 9(07).        
                 09 PMDNXP1-EQ-RERATE-IND             PIC X(01).        
                 09 PMDNXP1-TXGRPII-NUM-PREFIX-A.                       
                    11 PMDNXP1-TXGRPII-NUM-PREFIX  PIC 9(02)V9(01).     
                    88 PMDNXP1-TXPREF-IS-ONE-HALF     VALUE 0.5.        
                 09 FILLER                            PIC X(09).        
                 09 PMDNXP1-GRPII-TYPE                PIC X(01).        
                    88 PMDNXP1-HABITATIONAL           VALUE 'H'.        
                    88 PMDNXP1-NON-HABITATIONAL       VALUE 'N'.        
                 09 PMDNXP1-PUBLIC-PROPERTY-IND       PIC X(01).        
                    88  PMDNXP1-PUBLIC-PROP-INCLUDED  VALUE 'Y'.        
                 09 PMDNXP1-STORAGE-IND               PIC X(01).        
                    88  PMDNXP1-STORAGE-INCLUDED      VALUE 'Y'.        
                 09 PMDNXP1-CITY-CODE                 PIC X(03).        
                 09 PMDNXP1-OCC-CLS-SYM.                                
                    11 PMDNXP1-OCC-CLS                PIC X(01).        
                    11 PMDNXP1-OCC-SYM                PIC X(03).        
                 09 PMDNXP1-GRPII-CODE-A.                               
                    11 PMDNXP1-GRPII-CODE             PIC 9(03).        
                       88  PMDNXP1-ALL-OTH-RISKS-NO-BR     VALUE 013.   
                       88  PMDNXP1-ALL-OTH-RISKS-BLD-RISK  VALUE 015.   
                       88  PMDNXP1-TRAILERS                VALUE 026.   
                       88  PMDNXP1-VALID-NY-GRPII-CODES                 
                                                     VALUE 051 052.     
                       88  PMDNXP1-VALID-NY-GRPI-CODES                  
                                                     VALUE 053 054 055. 
                       88  PMDNXP1-NY-GRPI-APT       VALUE 053.         
                       88  PMDNXP1-NY-GRPI-OFFICE    VALUE 054.         
                       88  PMDNXP1-NY-GRPI-APT-AND-OFFICE               
                                                     VALUE 055.         
                 09 PMDNXP1-GRPII-CONT-SP-RT-A.                         
                    11 PMDNXP1-GRPII-CONT-SPEC-RATE   PIC 9(03)V9(03).  
                 09 PMDNXP1-BROAD-BLDG-BL-RT-A.                         
                    11 PMDNXP1-BROAD-BLDG-BLKT-RATE   PIC 9(03)V9(03).  
                 09 PMDNXP1-BROAD-CONT-BL-RT-A.                         
                    11 PMDNXP1-BROAD-CONT-BLKT-RATE   PIC 9(03)V9(03).  
                 09 PMDNXP1-SPECIAL-BLDG-BL-RT-A.                       
                    11 PMDNXP1-SPECIAL-BLDG-BLKT-RATE PIC 9(03)V9(03).  
                 09 PMDNXP1-SPECIAL-CONT-BL-RT-A.                       
                    11 PMDNXP1-SPECIAL-CONT-BLKT-RATE PIC 9(03)V9(03).  
                 09 PMDNXP1-STATEMENT-OF-VALUE-IND    PIC X(01).        
                    88 PMDNXP1-STATEMENT-OF-VALUE     VALUE 'Y'.        
                 09 PMDNXP1-SUB-STD-CONVERSION-IND    PIC X(01).        
                 09 PMDNXP1-SUB-STD-EXPOSURE-IND      PIC X(01).        
                 09 PMDNXP1-SUB-STD-HEAT-COOK-IND     PIC X(01).        
                 09 PMDNXP1-SUB-STD-PHYS-COND-IND     PIC X(01).        
                 09 PMDNXP1-SUB-STD-WIRING-IND        PIC X(01).        
                 09 PMDNXP1-GENL-CLS-RT-GRP           PIC X(04).        
                 09 PMDNXP1-VARIABLE-AREA-STATE       PIC X(02).        
                 09 PMDNXP1-DEC-CHANGE-FLAG           PIC X(01).        
                    88 PMDNXP1-DEC-CHANGE-NEEDED      VALUE 'A'.        
                    88 PMDNXP1-DEC-CHANGE-NOTED       VALUE 'B'.        
                 09 PMDNXP1-RERATE-IM-IND             PIC X(01).        
                    88 PMDNXP1-RERATE-IM              VALUE 'Y'.        
                 09 PMDNXP1-WAIVE-CONTRACTOR-IND      PIC X(01).        
                 09 PMDNXP1-BLKT-MISC-LOC-EXCL        PIC X(01).        
                    88 PMDNXP1-BLKT-MISC-LOC-EXCLUDED VALUE 'Y'.        
                    88 PMDNXP1-BLKT-MISC-LOC          VALUE ' '.        
                    88 PMDNXP1-BLKT-MISC-LOC-NOT-EXCL VALUE 'N'.        
                 09 PMDNXP1-PMSC-FUTURE-USE           PIC X(38).        
                 09 PMDNXP1-NEW-CSP-CONSTR-CODE       PIC X(02).        
                 09 PMDNXP1-YR-BUILT                  PIC X(04).        
                 09 PMDNXP1-NO-STORIES                PIC X(02).        
                 09 PMDNXP1-SQ-FOOTAGE                PIC X(06).        
                 09 PMDNXP1-CUSTOMER-FUTURE-USE       PIC X(11).        
                 09 PMDNXP1-YR2000-CUST-USE           PIC X(100).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).