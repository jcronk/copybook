        01 PMDUN-UNIT-PRINT-VERB-RECORD.
           03 PMDUN-UNIT-PRINT-VERB-SEG.                                
            05 PMDUN-SEGMENT-KEY.                                       
              07 PMDUN-SEGMENT-ID                   PIC X(02).          
                 88 PMDUN-SEGMENT-43                 VALUE '43'.        
              07 PMDUN-SEGMENT-STATUS               PIC X(01).          
                 88 PMDUN-ACTIVE-STATUS              VALUE 'A'.         
                 88 PMDUN-DELETED-STATUS             VALUE 'D'.         
                 88 PMDUN-HISTORY-STATUS             VALUE 'H'.         
                 88 PMDUN-NOT-ACTIVE-STATUS          VALUE 'H' 'D'.     
                 88 PMDUN-RENEWAL-FS-STATUS          VALUE 'R' 'F'.     
              07 PMDUN-TRANSACTION-DATE             PIC 9(08).
              07 PMDUN-SEGMENT-ID-KEY.                                  
                 09 PMDUN-SEGMENT-LEVEL-CODE        PIC X(01).          
                    88 PMDUN-SEG-LEVEL-U             VALUE 'U'.         
                 09 PMDUN-SEGMENT-PART-CODE         PIC X(01).          
                    88 PMDUN-SEG-PART-N              VALUE 'N'.         
                 09 PMDUN-SUB-PART-CODE             PIC X(01).          
                 09 PMDUN-INSURANCE-LINE            PIC X(02).          
              07 PMDUN-LEVEL-KEY-GL.                                
                 09 PMDUN-LEVEL-KEY.                                
                    11 PMDUN-LOCATION-NUMBER           PIC 9(04).       
                    11 PMDUN-RISK-UNIT-GROUP-KEY.                   
                       13 PMDUN-RISK-UNIT-GROUP        PIC 9(03).       
                       13 PMDUN-SEQ-RSK-UNT-GRP        PIC 9(03).       
                    11 PMDUN-RISK-UNIT.                             
                       13 PMDUN-CLASS-CODE             PIC X(05).       
                       13 FILLER                       PIC X(01).       
                    11 PMDUN-SEQUENCE-RISK-UNIT.                    
                       13 PMDUN-RISK-SEQUENCE          PIC 9(01).       
                       13 PMDUN-RISK-TYPE-IND          PIC X(01).       
                          88 PMDUN-PREMISES-OPERATIONS  VALUE 'O'.  
                          88 PMDUN-PRODUCTS             VALUE 'P'.  
                 09 PMDUN-ITEM-EFFECTIVE-DATE          PIC 9(08).
                 09 PMDUN-VARIABLE-KEY.                             
                    11 PMDUN-ADDIT-INT-TYP.                      
                      13 PMDUN-ADD-INT-TYPE            PIC X(01).
                      13 PMDUN-ADD-INT-SEQ-A.                    
                        15 PMDUN-ADD-INT-SEQ           PIC 9(02).
                    11 PMDUN-AI-SEQ-USE-CODE           PIC X(03).       
                 09 PMDUN-PROCESS-DATE                 PIC 9(08).
                 09 PMDUN-SUB-LOCATION-NUMBER          PIC 9(03).
              07 PMDUN-SEGMENT-DATA.                                    
                 09 PMDUN-OVERRIDE-CLASS-DESC       PIC X(60).          
                 09 PMDUN-PRODUCTS-INDICATOR        PIC X(01).          
                    88 PMDUN-PRODUCTS-INCLUDED        VALUE 'I'.        
                 09 PMDUN-DEC-CHANGE-FLAG           PIC X(01).          
                    88 PMDUN-DEC-CHANGE-NEEDED      VALUE 'A'.          
                    88 PMDUN-DEC-CHANGE-NOTED       VALUE 'B'.          
                 09 PMDUN-PMS-FUTURE-USE            PIC X(201).         
                 09 PMDUN-CUSTOMER-USE              PIC X(30).          
                 09 PMDUN-YR2000-CUST-USE           PIC X(100).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).