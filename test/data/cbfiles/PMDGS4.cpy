        01 PMDGS1-PREMIUM-SUMMARY-RECORD.
           03 PMDGS4-SUR-TAX-FEE-SUMMARY-SEG.                  
              07 PMDGS4-SEGMENT-ID                   PIC X(02).
                 88 PMDGS4-SEGMENT-43                 VALUE '43'.
              07 PMDGS4-SEGMENT-STATUS               PIC X(01).  
                 88 PMDGS4-ACTIVE-STATUS              VALUE 'A'. 
                 88 PMDGS4-DELETED-STATUS             VALUE 'D'. 
                 88 PMDGS4-HISTORY-STATUS             VALUE 'H'. 
                 88 PMDGS4-NOT-ACTIVE-STATUS          VALUE 'H' 'D'.
                 88 PMDGS4-RENEWAL-FS-STATUS          VALUE 'R' 'F'.
              07 PMDGS4-TRANSACTION-DATE             PIC 9(08).
              07 PMDGS4-SEGMENT-ID-KEY.                        
                 09 PMDGS4-SEGMENT-LEVEL-CODE        PIC X(01).
                    88 PMDGS4-SEG-LEVEL-G             VALUE 'G'.
                 09 PMDGS4-SEGMENT-PART-CODE         PIC X(01). 
                    88 PMDGS4-SEG-PART-S              VALUE 'S'.
                 09 PMDGS4-SUB-PART-CODE             PIC X(01). 
              07 PMDGS4-PROCESS-DATE                 PIC 9(08).
              07 PMDGS4-SEGMENT-DATA.                          
                 09 PMDGS4-ST-TOTAL-TAX-SUR-FEE.     
                    11 PMDGS4-STATE                  PIC X(02).
                    11 PMDGS4-ST-TAX-SUR-FEE         PIC S9(07)V99.
                    11 PMDGS4-TAX-SUR-FEE-IND        PIC X(01).    
                       88 PMDGS4-ST-TAX               VALUE 'T'.   
                       88 PMDGS4-ST-SURCHARGE         VALUE 'S'.   
                       88 PMDGS4-ST-FEE               VALUE 'F'.   
                 09 PMDGS4-ST-TOTAL-TAX-SUR-FEE.     
                    11 PMDGS4-STATE                  PIC X(02).
                    11 PMDGS4-ST-TAX-SUR-FEE         PIC S9(07)V99.
                    11 PMDGS4-TAX-SUR-FEE-IND        PIC X(01).    
                 09 PMDGS4-ST-TOTAL-TAX-SUR-FEE.     
                    11 PMDGS4-STATE                  PIC X(02).
                    11 PMDGS4-ST-TAX-SUR-FEE         PIC S9(07)V99.
                    11 PMDGS4-TAX-SUR-FEE-IND        PIC X(01).    
                 09 PMDGS4-ST-TOTAL-TAX-SUR-FEE.     
                    11 PMDGS4-STATE                  PIC X(02).
                    11 PMDGS4-ST-TAX-SUR-FEE         PIC S9(07)V99.
                    11 PMDGS4-TAX-SUR-FEE-IND        PIC X(01).    
                 09 PMDGS4-ST-TOTAL-TAX-SUR-FEE.     
                    11 PMDGS4-STATE                  PIC X(02).
                    11 PMDGS4-ST-TAX-SUR-FEE         PIC S9(07)V99.
                    11 PMDGS4-TAX-SUR-FEE-IND        PIC X(01).    
                 09 PMDGS4-ST-TOTAL-TAX-SUR-FEE.     
                    11 PMDGS4-STATE                  PIC X(02).
                    11 PMDGS4-ST-TAX-SUR-FEE         PIC S9(07)V99.
                    11 PMDGS4-TAX-SUR-FEE-IND        PIC X(01).    
                 09 PMDGS4-ST-TOTAL-TAX-SUR-FEE.     
                    11 PMDGS4-STATE                  PIC X(02).
                    11 PMDGS4-ST-TAX-SUR-FEE         PIC S9(07)V99.
                    11 PMDGS4-TAX-SUR-FEE-IND        PIC X(01).    
                 09 PMDGS4-INS-LN-TOT-TAX-SUR-FEE.    
                    11 PMDGS4-INSURANCE-LINE         PIC X(02).   
                    11 PMDGS4-INS-LN-TAX-SUR-FEE     PIC S9(07)V99.
                    11 PMDGS4-INS-LN-TAX-SUR-FEE-IND PIC X(01).    
                       88 PMDGS4-IL-TAX               VALUE 'T'.   
                       88 PMDGS4-IL-SURCHARGE         VALUE 'S'.   
                       88 PMDGS4-IL-FEE               VALUE 'F'.   
                 09 PMDGS4-INS-LN-TOT-TAX-SUR-FEE.    
                    11 PMDGS4-INSURANCE-LINE         PIC X(02).   
                    11 PMDGS4-INS-LN-TAX-SUR-FEE     PIC S9(07)V99.
                    11 PMDGS4-INS-LN-TAX-SUR-FEE-IND PIC X(01).    
                 09 PMDGS4-INS-LN-TOT-TAX-SUR-FEE.    
                    11 PMDGS4-INSURANCE-LINE         PIC X(02).   
                    11 PMDGS4-INS-LN-TAX-SUR-FEE     PIC S9(07)V99.
                    11 PMDGS4-INS-LN-TAX-SUR-FEE-IND PIC X(01).    
                 09 PMDGS4-INS-LN-TOT-TAX-SUR-FEE.    
                    11 PMDGS4-INSURANCE-LINE         PIC X(02).   
                    11 PMDGS4-INS-LN-TAX-SUR-FEE     PIC S9(07)V99.
                    11 PMDGS4-INS-LN-TAX-SUR-FEE-IND PIC X(01).    
                 09 PMDGS4-INS-LN-TOT-TAX-SUR-FEE.    
                    11 PMDGS4-INSURANCE-LINE         PIC X(02).   
                    11 PMDGS4-INS-LN-TAX-SUR-FEE     PIC S9(07)V99.
                    11 PMDGS4-INS-LN-TAX-SUR-FEE-IND PIC X(01).    
                 09 PMDGS4-INS-LN-TOT-TAX-SUR-FEE.    
                    11 PMDGS4-INSURANCE-LINE         PIC X(02).   
                    11 PMDGS4-INS-LN-TAX-SUR-FEE     PIC S9(07)V99.
                    11 PMDGS4-INS-LN-TAX-SUR-FEE-IND PIC X(01).    
                 09 PMDGS4-INS-LN-TOT-TAX-SUR-FEE.    
                    11 PMDGS4-INSURANCE-LINE         PIC X(02).   
                    11 PMDGS4-INS-LN-TAX-SUR-FEE     PIC S9(07)V99.
                    11 PMDGS4-INS-LN-TAX-SUR-FEE-IND PIC X(01).    
                 09 PMDGS4-INS-LN-TOT-TAX-SUR-FEE.    
                    11 PMDGS4-INSURANCE-LINE         PIC X(02).   
                    11 PMDGS4-INS-LN-TAX-SUR-FEE     PIC S9(07)V99.
                    11 PMDGS4-INS-LN-TAX-SUR-FEE-IND PIC X(01).    
                 09 PMDGS4-INS-LN-TOT-TAX-SUR-FEE.    
                    11 PMDGS4-INSURANCE-LINE         PIC X(02).   
                    11 PMDGS4-INS-LN-TAX-SUR-FEE     PIC S9(07)V99.
                    11 PMDGS4-INS-LN-TAX-SUR-FEE-IND PIC X(01).    
                 09 PMDGS4-INS-LN-TOT-TAX-SUR-FEE.    
                    11 PMDGS4-INSURANCE-LINE         PIC X(02).   
                    11 PMDGS4-INS-LN-TAX-SUR-FEE     PIC S9(07)V99.
                    11 PMDGS4-INS-LN-TAX-SUR-FEE-IND PIC X(01).    
                 09 PMDGS4-INS-LN-TOT-TAX-SUR-FEE.    
                    11 PMDGS4-INSURANCE-LINE         PIC X(02).   
                    11 PMDGS4-INS-LN-TAX-SUR-FEE     PIC S9(07)V99.
                    11 PMDGS4-INS-LN-TAX-SUR-FEE-IND PIC X(01).    
                 09 PMDGS4-INS-LN-TOT-TAX-SUR-FEE.    
                    11 PMDGS4-INSURANCE-LINE         PIC X(02).   
                    11 PMDGS4-INS-LN-TAX-SUR-FEE     PIC S9(07)V99.
                    11 PMDGS4-INS-LN-TAX-SUR-FEE-IND PIC X(01).    
                 09 PMDGS4-TOTAL-TAX-SURCHARGE-CHG   PIC S9(07)V99.
                 09 PMDGS4-PMS-FUTURE-USE            PIC X(69).    
                 09 PMDGS4-CUSTOMER-USE              PIC X(24).    
                 09 PMDGS4-YR2000-CUST-USE           PIC X(100).   
