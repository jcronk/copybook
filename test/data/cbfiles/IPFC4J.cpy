       01 IPFC4J-NAME-ADDR-RECORD.                           
        05 IPFC4J-NAME-ADDR-SEG.                           
           07 IPFC4J-SEGMENT-ID-KEY.                               
              09 IPFC4J-SEGMENT-ID                PIC X(02).       
              09 IPFC4J-SEGMENT-LEVEL-CODE        PIC X(01).       
              09 IPFC4J-SEGMENT-PART-CODE         PIC X(01).       
              09 IPFC4J-SUB-PART-CODE             PIC X(01).       
           07 IPFC4J-4J-LEVEL-KEY-AREA.                            
              09 IPFC4J-INSURANCE-LINE            PIC X(02).       
              09 IPFC4J-LOCATION-NUMBER-A.                         
                 11 IPFC4J-LOCATION-NUMBER        PIC 9(04).       
              09 IPFC4J-SUB-LOCATION-NUMBER       PIC 9(03).       
              09 IPFC4J-RISK-UNIT-GROUP-KEY.                       
                 11 IPFC4J-RISK-UNIT-GROUP        PIC 9(03).       
                 11 IPFC4J-SEQ-RSK-UNT-GRP.                        
                    13 IPFC4J-CLASS-CODE-GROUP    PIC 9(02).       
                    13 IPFC4J-CLASS-CODE-MEMBER   PIC 9(01).       
              09 IPFC4J-RISK-UNIT                 PIC X(06).       
              09 IPFC4J-SEQUENCE-RISK-UNIT.                        
                 11 IPFC4J-RISK-SEQUENCE          PIC 9(01).       
                 11 IPFC4J-RISK-TYPE-IND          PIC X(01).       
           07 IPFC4J-ITEM-EFFECTIVE-DATE          PIC X(08).
           07 IPFC4J-TRANSACTION-DATE             PIC X(08).
           07 IPFC4J-4J-VARIABLE-KEY.                       
              09 IPFC4J-LOSS-OCCURENCE            PIC 9(03).
              09 IPFC4J-LOSS-CLAIMANT             PIC 9(03).  
              09 IPFC4J-USE-CODE                  PIC X(03).  
                 88 IPFC4J-CLAIMANT-INFO          VALUE 'CMT'.
              09 IPFC4J-SEQUENCE-USE-CODE         PIC 9(03).  
              09 IPFC4J-AI-SEQ-USE-CODE           PIC 9(03).  
           07 IPFC4J-PROCESS-DATE                 PIC 9(08).
           07 IPFC4J-CHANGE-ENTRY-GROUP.                     
              09 IPFC4J-CHANGE-ENTRY-DATE         PIC 9(08).
              09 IPFC4J-SEQUENCE-CHANGE-ENTRY     PIC 9(02).   
           07 IPFC4J-4J-SEGMENT-DATA.                     
              09 IPFC4J-SEGMENT-STATUS            PIC X(01).   
              09 IPFC4J-ENTRY-OPERATOR            PIC X(03).   
              09 IPFC4J-USE-CODE-DATA             PIC X(01).   
                 88 IPFC4J-JOB-ADDRESS VALUE 'J'.              
                 88 IPFC4J-LOCATION-ADDRESS VALUE 'L'.         
              09 IPFC4J-NAME-GROUP.                            
                 11 IPFC4J-SORT-NAME-AREA.                     
                    13 IPFC4J-SORT-NAME           PIC X(04).   
                    13 FILLER                     PIC X(06).   
                 11 IPFC4J-NAME-TYPE-IND         PIC X(01).    
                 11 IPFC4J-ADDRESS-LINE-1-2.                   
                    13 IPFC4J-ADDRESS-LINE-1      PIC X(30).   
                    13 IPFC4J-ADDRESS-LINE-2.                  
                       15 IPFC4J-ADDR-LIN-2-POS-1     PIC X(01).   
                          88 IPFC4J-INSR-NAM-HAS-TWO-LIN VALUE '&'.
                       15 IPFC4J-ADDR-LIN-2-POS-2-30    PIC X(29).
                  11 IPFC4J-ADDRESS-LINE-3          PIC X(30).    
                  11 IPFC4J-ADDRESS-LINE-4          PIC X(30).    
                  11 IPFC4J-ID-NUMBER               PIC X(13).    
                  11 IPFC4J-ZIP-POSTAL-CODE.                      
                     13 IPFC4J-ZIP-USA.                           
                        15 IPFC4J-ZIP-BASIC          PIC X(05).   
                        15 FILLER                    PIC X(01).   
                        15 IPFC4J-ZIP-EXPANDED       PIC 9(04).   
              09 IPFC4J-PHONE.                                    
                   11 IPFC4J-PHONE-AREA               PIC 9(03).  
                   11 IPFC4J-PHONE-EXCHANGE           PIC 9(03).  
                   11 IPFC4J-PHONE-NUMBER             PIC 9(04).  
                   11 IPFC4J-PHONE-EXTENSION          PIC 9(04).  
              09 IPFC4J-INTEREST-ITEM                 PIC X(30).  
              09 IPFC4J-LOCATION-STATE                PIC X(02).  
              09 IPFC4J-OFFSET-ONSET-IND              PIC X(01).  
                 88 IPFC4J-OFFSET-RECORD    VALUE 'O'.            
              09 IPFC4J-PMS-FUTURE-USE-4J-1           PIC X(20).  
              09 IPFC4J-CUST-SPL-USE-4J-1             PIC X(05).  
           07 IPFC4J-PMS-FUTURE-USE                    PIC X(44). 
           07 IPFC4J-YR2000-CUST-USE                   PIC X(100).
        05 NEEDED-SORT-DATA          PIC X(179).
        05 BLANK-OUT                 PIC X(44).
