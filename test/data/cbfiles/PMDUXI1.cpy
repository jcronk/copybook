        01 PMDUXI1-UNIT-RATING-RECORD.
           03 PMDUXI1-UNIT-RATING-SEG.                                  
            05 PMDUXI1-SEGMENT-KEY.                                     
              07 PMDUXI1-SEGMENT-ID                   PIC X(02).        
                 88 PMDUXI1-SEGMENT-43                 VALUE '43'.      
              07 PMDUXI1-SEGMENT-STATUS               PIC X(01).        
                 88 PMDUXI1-ACTIVE-STATUS              VALUE 'A'.       
                 88 PMDUXI1-DELETED-STATUS             VALUE 'D'.       
                 88 PMDUXI1-HISTORY-STATUS             VALUE 'H'.       
                 88 PMDUXI1-NOT-ACTIVE-STATUS          VALUE 'H' 'D'.   
                 88 PMDUXI1-RENEWAL-FS-STATUS          VALUE 'R' 'F'.   
              07 PMDUXI1-TRANSACTION-DATE             PIC 9(08).
              07 PMDUXI1-SEGMENT-ID-KEY.                                
                 09 PMDUXI1-SEGMENT-LEVEL-CODE        PIC X(01).        
                    88 PMDUXI1-SEG-LEVEL-U             VALUE 'U'.       
                 09 PMDUXI1-SEGMENT-PART-CODE         PIC X(01).        
                    88 PMDUXI1-SEG-PART-X              VALUE 'X'.       
                 09 PMDUXI1-SUB-PART-CODE             PIC X(01).        
                 09 PMDUXI1-INSURANCE-LINE            PIC X(02).        
                    88 PMDUXI1-INLAND-MARINE           VALUE 'IM'.      
              07 PMDUXI1-LEVEL-KEY.                                     
                 09 PMDUXI1-LOCATION-NUMBER-ALPHA.                      
                    11 PMDUXI1-LOCATION-NUMBER        PIC 9(04).        
                 09 PMDUXI1-SUB-LOCATION-NUMBER-A.                      
                    11 PMDUXI1-SUB-LOCATION-NUMBER    PIC 9(03).        
                 09 PMDUXI1-RISK-UNIT-GROUP-KEY.                        
                    11 PMDUXI1-RISK-UNIT-GROUP-A.                       
                       13 PMDUXI1-RISK-UNIT-GROUP     PIC 9(03).        
                    11 PMDUXI1-RISK-UNIT-GROUP-SEQ-A.                   
                       13 PMDUXI1-RISK-UNIT-GROUP-SEQ PIC 9(03).        
                 09 PMDUXI1-RISK-UNIT.                                  
                    11 PMDUXI1-ITEM-NUMBER            PIC X(06).        
                 09 PMDUXI1-SEQUENCE-RISK-UNIT.                         
                    11 PMDUXI1-RISK-SEQUENCE          PIC 9(01).        
                    11 PMDUXI1-RISK-TYPE-IND          PIC X(01).        
              07 PMDUXI1-ITEM-EFFECTIVE-DATE          PIC 9(08).
              07 PMDUXI1-VARIABLE-KEY                 PIC X(06).        
              07 PMDUXI1-PROCESS-DATE                 PIC 9(08).
             05 PMDUXI1-SEGMENT-DATA.                                   
                 09 PMDUXI1-RATING-STATE              PIC X(02).        
                 09 PMDUXI1-ITEM-DESC-LINE1           PIC X(40).        
                 09 PMDUXI1-ITEM-DESC-LINE2.                            
                    11 FILLER                         PIC X(01).        
                       88 PMDUXI1-DESC-HAS-TWO-LINES   VALUE '&'.       
                    11 FILLER                         PIC X(39).        
                 09 PMDUXI1-LIMIT.                                      
                    11 PMDUXI1-LIMIT-N                PIC 9(10).        
                 09 PMDUXI1-SIGNS-POSITION            PIC X(01).        
                    88 PMDUXI1-INSIDE-SIGNS           VALUE 'I'.        
                 09 PMDUXI1-DEC-CHANGE-FLAG           PIC X(01).        
                    88 PMDUXI1-DEC-CHANGE-NEEDED      VALUE 'A'.        
                    88 PMDUXI1-DEC-CHANGE-NOTED       VALUE 'B'.        
                 09 PMDUXI1-PMSC-FUTURE-USE           PIC X(169).       
                 09 PMDUXI1-CUST-FUTURE-USE           PIC X(30).        
                 09 PMDUXI1-YR2000-CUST-USE           PIC X(100).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).