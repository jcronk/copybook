	       01 VEHICLE-DESCRIPTION-RECORD.
           03 VEHICLE-DESCRIPTION-SEG.                                  
           05 USVD-ID                    PIC X(02).                     
              88 USVD-VEHICLE-REC        VALUE '35'.                    
           05 USVD-KEY.                                                 
              10 USVD-UNIT-NUMBER        PIC X(03).                                      
              10 USVD-SEGMENT-NUMBER     PIC X(01).                     
                 88 USVD-DESCRIPTION-SEGMENT VALUE '0'.                 
              10 USVD-AMENDMENT-NUMBER   PIC X(01).                     
           05 USVD-STATE                 PIC X(02).                     
              88 USVD-COLORADO           VALUE '05'.                    
              88 USVD-GEORGIA            VALUE '10'.                    
              88 USVD-KENTUCKY           VALUE '16'.                    
              88 USVD-LOUISIANA          VALUE '17'.                    
              88 USVD-MARYLAND           VALUE '19'.                    
              88 USVD-MICHIGAN           VALUE '21'.                    
              88 USVD-MISSISSIPPI        VALUE '23'.                    
              88 USVD-NEBRASKA           VALUE '26'.                    
              88 USVD-NEVADA             VALUE '27'.                    
              88 USVD-FLORIDA            VALUE '09'.                    
              88 USVD-ARIZONA            VALUE '02'.                    
              88 USVD-NEW-JERSEY         VALUE '29'.                    
              88 USVD-PENNSYLVANIA       VALUE '37'.                    
              88 USVD-SOUTH-CAROLINA     VALUE '39'.                    
              88 USVD-TEXAS              VALUE '42'.                    
              88 USVD-WEST-VIRGINIA      VALUE '47'.                    
              88 USVD-VIRGINIA           VALUE '45'.                    
              88 USVD-NEWYORK            VALUE '31'.                    
              88 USVD-MINNESOTA          VALUE '22'.                    
           05 USVD-TERRITORY             PIC X(03).                     
           05 USVD-YEAR-MAKE             PIC X(04).                                           
           05 USVD-MAKE-DESC             PIC X(17).                     
           05 USVD-SERIAL-NUMBER         PIC X(17).                     
           05 USVD-TYPE-VEHICLE          PIC X(02).                     
              88 USVD-MASSACHUSETTS-TYPE-VEH VALUE 'PP' 'CO' 'PU' 'VN'  
                    'MC' 'MH' 'MS'.                                     
              88 USVD-MISCELLANEOUS-TYPES VALUE 'AN' 'EL' 'MC' 'GO'     
                    'MC' 'MH' 'MS' 'SN' 'TR' 'RT' 'DB' 'RD' 'AM' 'MP'   
                    'MB' 'GC' 'AT'.                                     
              88 USVD-MISC-VEH-TYPE      VALUE 'SN' 'DB' 'TR' 'GC'.     
              88 USVD-COMM-PRI-PASS      VALUE 'PT'.                    
              88 USVD-COMM-TRUCK         VALUE 'TO'.                    
              88 USVD-PICKUP             VALUE 'PU'.                    
              88 USVD-VAN                VALUE 'VN'.                    
              88 USVD-ANTIQUE            VALUE 'AN'.                    
              88 USVD-SNOWMOBILE         VALUE 'SN'.                    
              88 USVD-MOBILE-TRAILER     VALUE 'MT'.                    
              88 USVD-PUBLIC-AUTO        VALUE 'PA'.                    
              88 USVD-MOTORCYCLE         VALUE 'MC'.                    
              88 USVD-MOTOR-HOME         VALUE 'MH'.                    
              88 USVD-UMS-EXCLUDED       VALUE 'TR' 'MT'.               
           05 USVD-PERFORMANCE           PIC X(01).                     
           05 USVD-BODY                  PIC X(01).                     
           05 USVD-COST-NEW              PIC X(05).                     
           05 USVD-SYMBOL                PIC X(02).                     
           05 USVD-DRIVER-ASSIGNED       PIC X(02).                     
           05 USVD-USE-CODE              PIC X(01).                     
              88 USE-WORK                VALUE 'W'.                     
              88 USE-PLEASURE            VALUE 'P'.                     
           05 USVD-MILES-TO-WORK         PIC X(02).                     
              88 MILES-LESS-THAN-10      VALUE '00' THRU '09'.          
           05 USVD-DAYS-PER-WEEK         PIC X(01).                     
           05 USVD-ANNUAL-MILES          PIC X(02).                     
           05 USVD-VEH-CLASS-CODE        PIC X(06).                     
           05 USVD-DRIVER-CLASS          PIC X(05).                     
           05 USVD-MULTICAR-OVERRIDE     PIC X(01).                     
              88 NO-MULTI-CAR-OVERIDE    VALUE ' '.                     
           05 USVD-ANTILOCK-DISC PIC X(01).                             
              88 NO-ANTILOCK-BRAKE-DISC  VALUE ' '.                     
              88 ANTILOCK-DISCOUNT-APPLIES  VALUE 'Y' '1' '2' '6'.      
              88 TRACTION-DISCOUNT-APPLIES  VALUE '2' '3' '4' '6'.      
              88 BUMPER-DISCOUNT-APPLIES    VALUE '1' '4' '5' '6'.      
           05 USVD-PIP-RATING-BASIS      PIC X(01).                     
              88 MAUTO-PRE-INSPECTED     VALUE '1'.                     
           05 USVD-ANTITHEFT-DEVICE      PIC X(01).                     
              88 ANTITHEFT-DISCOUNT-APPLIES      VALUE '1' '2' '3' '4'  
                                                       '6' '7'.         
           05 USVD-PASSIVE-RESTRAINT     PIC X(01).                     
              88 PASSIVE-REST-DISCOUNT-APPLIES   VALUE 'Y' 'A' 'B'      
                                                       'C' 'D'.         
           05 USVD-SPECIAL-USE           PIC X(01).                     
           05 USVD-ZIP-POSTAL-CODE       PIC X(06).                     
           05 USVD-TAX-LOCATION          PIC X(06).                     
           05 USVD-CONTROL.                                             
              10 VEHICLE-RATING-AGE      PIC 9(01).                     
              10 VEHICLE-ACTUAL-AGE      PIC 9(01).                     
              10 USVD-DATE-LAST-CHANGE   PIC X(08).                                 
           05 USVD-PURCHASE-DATE         PIC X(08).                                       
           05 USVD-VEH-WEIGHT            PIC X(05).                     
           05 USVD-CUBIC-CENTIMETERS     PIC X(04).                     
           05 USVD-COMMUTER-DISCOUNT     PIC X(02).                     
           05 USVD-STATED-AMOUNT         PIC X(05).                     
           05 USVD-VIN                   PIC X(10).                     
           05 USVD-SAFE-DRIVER-DISCOUNT  PIC X(01).                     
           05 USVD-HAP-MOD-INDICATOR     PIC X(01).                     
              88 USVD-HAP-MOD-YES        VALUE 'Y'.                     
              88 USVD-HAP-MOD-NO         VALUE 'N'.                     
           05 USVD-COL-CLASS             PIC X(06).                     
           05 USVD-FARTHEST-ZONE         PIC X(05).                     
           05 USVD-SIZE                  PIC X(01).                     
           05 USVD-COMM-USE              PIC X(01).                     
           05 USVD-RADIUS                PIC X(01).                     
           05 USVD-SPECIAL-INDUSTRY      PIC X(03).                     
           05 USVD-SCHOOL-BUS-POL-SUBDIV    PIC X(01).                  
           05 USVD-SCHOOL-CHART-REG-PLATE   PIC X(01).                  
           05 USVD-NON-PROFIT-RELIG-ORG     PIC X(01).                  
           05 USVD-RIDE-SHARING             PIC X(01).                  
           05 USVD-MUN-LIAB-VOL-WORKER      PIC X(01).                  
           05 USVD-DRIVE-TRAIN-DUAL-CONTROL PIC X(01).                  
           05 USVD-SNOWMOBILE-PASS-HAZARD   PIC X(01).                  
           05 USVD-RATE-BOOK-ID-STORE       PIC X(01).                  
           05 USVD-LEASED-AUTO-INDICATOR    PIC X(01).                  
           05 USVD-FLEET-INDICATOR          PIC X(01).                  
              88 USVD-FLEET              VALUE 'Y'.                     
              88 USVD-NON-FLEET          VALUE 'N'.                     
           05 USVD-CEDED-INDICATOR          PIC X(01).                  
              88 USVD-CEDED              VALUE 'Y'.                     
              88 USVD-VOLUNTARY          VALUE 'N'.                     
           05 USVD-REG-TERRITORY            PIC X(03).                  
           05 USVD-STAT-TERRITORY           PIC X(03).                  
           05 USVD-CPP-PACK-MOD-ASSIGN      PIC X(02).                  
           05 USVD-POINT-DRIVER             PIC X(02).                  
           05 USVD-PASS-OR-FAIL             PIC X(01).                  
              88 USVD-PASS                  VALUE 'P'.                  
              88 USVD-FAIL                  VALUE 'F'.                  
           05 USVD-MIN-RETAINED-SW          PIC X(01).                  
              88 USVD-MIN-RETAINED-ON       VALUE '1'.                  
           05 USVD-SUBSTITUTE               PIC X(01).                  
           05 USVD-DDC-FACTOR               PIC 9(04)V99.               
           05 USVD-DDC-INDICATOR            PIC X(01).                  
              88 DDC-YES                    VALUE 'Y'.                  
              88 ADC-YES                    VALUE '2'.                  
              88 BDC-YES                    VALUE '3'.                  
              88 MDT-YES                    VALUE 'M'.                  
           05 USVD-ANTI-THEFT-FACTOR        PIC 9V99.                   
           05 USVD-PASS-RESTRAINT-FACT      PIC 9V99.                   
           05 USVD-GST-INDICATOR            PIC X(01).                  
              88 GST-YES                    VALUE 'Y'.                  
           05 USVD-GDD-INDICATOR            PIC X(01).                  
              88 GDD-YES                    VALUE 'Y'.                  
           05 USVD-DTD-INDICATOR            PIC X(01).                  
              88 DTD-YES                    VALUE 'Y'.                  
           05 USVD-GDD-FACTOR               PIC 9(01)V99.               
           05 USVD-DGD-FACTOR               PIC 9(01)V99.               
           05 USVD-DGD-INDICATOR            PIC X(01).                  
              88 DGD-YES                    VALUE 'Y'.                  
           05 USVD-TOTAL-OPERATORS          PIC 9(01).                  
           05 USVD-YOUTHFUL-OPERATORS       PIC 9(01).                  
           05 USVD-POLICY-OPERATORS         PIC 9(01).                  
           05 USVD-TOTAL-VEHICLES           PIC 9(01).                  
           05 USVD-ANTILOCK-FACTOR          PIC 9V999.                  
           05 USVD-NH-SDIP-PERCENT          PIC V99.                    
           05 FILLER                        PIC X(13).                  
           05 USVD-APC-FACTOR               PIC 9V99.                   
           05 USVD-APC-INDICATOR            PIC X(01).                  
              88 APC-YES                    VALUE '1'.                  
           05 USVD-WLW-FACTOR               PIC 9(01)V99.               
           05 USVD-WLW-INDICATOR            PIC X(01).                  
              88 WLW-YES                    VALUE '1'.                  
           05 USVD-CDD-INDICATOR            PIC X(01).                  
              88 CDD-YES                    VALUE '1'.                  
           05 USVD-VEHICLE-ADD-DATE         PIC X(08).                  
           05 USVD-XX-RATE-BOOK-ID-TABLE.                               
              10 USVD-XX-RATE-BOOK-ID-DATA OCCURS 14 TIMES              
                         INDEXED BY USVD-XX-RBID-I.                     
                 15  USVD-XX-COV-RATE-BOOK-ID  PIC X(01).               
           05 USVD-PMS-FUTURE-USE           PIC X(54).                  
           05 USVD-CUST-FUTURE-USE.                                     
              10 USVD-VEH-MS-MC-CLASS.                                  
                 15 USVD-VEH-MS-MC-PRI-CLASS  PIC X(4).                 
                 15 USVD-VEH-MS-MC-SEC-CLASS  PIC X(2).                 
              10 USVD-ANTI-THEFT-STAT-HOLD  PIC X(01).                  
              10 USVD-TERRCOV               PIC X(01).                  
              10 USVD-PCO                   PIC X(02).                  
              10 USVD-GVW                   PIC X(05).                  
              10 PREV-PCO                   PIC X(02).
              10 USVD-MILITARY-DISC         PIC X(01).
              10 USVD-CREDIT-SCORE          PIC 9(05).
              10 USVD-NYO-CREDIT            PIC X(01).
              10 USVD-NYO-FACTOR            PIC 9V99.
              10 USVD-PREV-CS-FACT          PIC 9V99.
              10 USVD-CURR-CS-FACT          PIC 9V99.
           05 USVD-PREV-CR-SCORE         PIC 9(05).
           05 USVD-RATING-MODE           PIC X(01).
           05 USVD-HYBRID                PIC X(01).
           05 USVD-LIAB-SYMBOL.                                         
              10  USVD-LIAB-SYMBOL-1-3   PIC X(03).
              10  USVD-LIAB-SYMBOL-4-6   PIC X(03).
           05 USVD-DRIVER-AGE            PIC X(02).                     
           05 USVD-COLL-SYMBOL           PIC X(02).                     
           05 USVD-ORIG-OWNER            PIC X(01).
           05 USVD-YR2000-CUST-USE       PIC X(82).                     
           05 BLANK-OUT       PIC X(03).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).
