        01 PMDGS1-PREMIUM-SUMMARY-RECORD.
           03 PMDGS1-PREMIUM-SUMMARY-SEG.                               
              07 PMDGS1-SEGMENT-ID                   PIC X(02).         
                 88 PMDGS1-SEGMENT-43                 VALUE '43'.       
              07 PMDGS1-SEGMENT-STATUS               PIC X(01).         
                 88 PMDGS1-ACTIVE-STATUS              VALUE 'A'.        
                 88 PMDGS1-DELETED-STATUS             VALUE 'D'.        
                 88 PMDGS1-HISTORY-STATUS             VALUE 'H'.        
                 88 PMDGS1-NOT-ACTIVE-STATUS          VALUE 'H' 'D'.    
                 88 PMDGS1-RENEWAL-FS-STATUS          VALUE 'R' 'F'.    
              07 PMDGS1-TRANSACTION-DATE             PIC 9(08).                               
              07 PMDGS1-SEGMENT-ID-KEY.                                 
                 09 PMDGS1-SEGMENT-LEVEL-CODE        PIC X(01).         
                    88 PMDGS1-SEG-LEVEL-G             VALUE 'G'.        
                 09 PMDGS1-SEGMENT-PART-CODE         PIC X(01).         
                    88 PMDGS1-SEG-PART-S              VALUE 'S'.        
                 09 PMDGS1-SUB-PART-CODE             PIC X(01).         
              07 PMDGS1-PROCESS-DATE                 PIC 9(08).
              07 PMDGS1-SEGMENT-DATA.                                   
                 09 PMDGS1-POLICY-DEPOSIT-PREM  PIC S9(10)V9(02).       
                 09 PMDGS1-POLICY-TERM-PREM          PIC S9(10).        
                 09 PMDGS1-POLICY-PREM-CHANGE        PIC S9(10).        
                 09 PMDGS1-INS-LINE-TOTAL-PREM   OCCURS 3 TIMES         
                                     INDEXED BY PMDGS1-INS-LINE.        
                    11 PMDGS1-INSURANCE-LINE         PIC X(02).         
                       88 PMDGS1-CF                   VALUE 'CF'.       
                       88 PMDGS1-CP                   VALUE 'CP'.       
                       88 PMDGS1-LI                   VALUE 'LI'.       
                       88 PMDGS1-CP                   VALUE 'CP'.       
                       88 PMDGS1-GARAGE               VALUE 'GA'.       
                       88 PMDGS1-LI                   VALUE 'LI'.       
                       88 PMDGS1-GLASS                VALUE 'CG'.       
                       88 PMDGS1-GL                   VALUE 'GL'.       
                       88 PMDGS1-INLAND-MARINE        VALUE 'IM'.       
                       88 PMDGS1-BURGLARY-THEFT       VALUE 'BT'.       
                       88 PMDGS1-PERSONAL-AUTO        VALUE 'PA'.       
                       88 PMDGS1-HOMEOWNERS           VALUE 'HP'.       
                       88 PMDGS1-BUSINESS-OWNERS      VALUE 'BP'.       
                       88 PMDGS1-WORKERS-COMP         VALUE 'WC'.       
                       88 PMDGS1-ELECTRONIC-EQUIP     VALUE 'EE'.       
                    11 PMDGS1-INS-LINE-DEPOSIT-PREM  PIC S9(10).        
                    11 PMDGS1-INS-LINE-TERM-PREM     PIC S9(10).        
                    11 PMDGS1-INS-LINE-PREM-CHANGE   PIC S9(10).        
                    11 PMDGS1-INS-LINE-MINIMUM-AP    PIC S9(10).        
                 09 PMDGS1-SUBLINE-TOTAL-PREM   OCCURS 2 TIMES          
                                     INDEXED BY PMDGS1-SUBLINE.         
                    11 PMDGS1-PMS-DEF-SUBLINE        PIC X(03).         
                    11 PMDGS1-RISK-TYPE-IND          PIC X(01).         
                    11 PMDGS1-SUBLINE-TERM-PREM      PIC S9(10).        
                    11 PMDGS1-SUBLINE-PREM-CHANGE    PIC S9(10).        
                    11 PMDGS1-SUBLINE-MINIMUM-AP     PIC S9(10).        
                 09 PMDGS1-FIRE-DED-BUY-BACK         PIC S9(10).        
                 09 PMDGS1-SERVICE-CHARGE      PIC S9(09)V9(02).        
                 09 PMDGS1-TEXAS-DISC-PREMIUM        PIC S9(10)V9(02).  
                 09 PMDGS1-GAR-TEXAS-DISC-PREMIUM    PIC S9(10)V9(02).  
                 09 PMDGS1-PMS-FUTURE-USE            PIC X(48).         
                 09 PMDGS1-CUSTOMER-USE              PIC X(11).         
                 09 PMDGS1-INS-LINE-TOTAL-COMM   OCCURS 3 TIMES         
                                     INDEXED BY PMDGS1-COMM-LINE.       
                    11 PMDGS1-COMM-INSURANCE-LINE    PIC X(02).         
                    11 PMDGS1-INS-LINE-COMM-RATE     PIC S9(01)V9(5).   
                    11 PMDGS1-INS-LINE-COMM-TRAN     PIC S9(08)V9(2).   
                 09 PMDGS1-YR2000-CUST-USE           PIC X(46).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).