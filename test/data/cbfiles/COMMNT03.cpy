      01 COMMENTS-REC.
           03 COMMENTS-SEG.                                            
           05 COMMENTS-ID                PIC X(02).                    
              88 COMMENTS-RECORD         VALUE '03'.                   
           05 COMMENTS-SEGMENT-PART-CODE PIC X(01).                    
           05 COMMENTS-REASON-SUSPENDED  PIC X(02).                    
              88 COMMENTS-RECS-W-20991231       VALUE 'LH' 'ER' 'AG'   
                  'CK' 'CL' 'IN' 'MB' 'MV' 'P#' 'PX' 'LH' 'RI' 'UC' 'UD
                  'UR' 'ZW' 'RF' 'RT'.                                 
              88 COMMENTS-LOSS-HISTORY          VALUE 'LH'.            
              88 COMMENTS-RATING-ROOF           VALUE 'RF'.            
              88 COMMENTS-REASON-AMENDED        VALUE 'ZR'.            
              88 COMMENTS-PROP-SCORING          VALUE 'ZP'.            
              88 COMMENTS-CLIENT-ID             VALUE 'ZU'.            
              88 COMMENTS-RPT-FREQ              VALUE 'ZF'.            
              88 COMMENTS-OTHER-COMPANION       VALUE 'OC'.            
              88 COMMENTS-ASSIGNED-UPLOAD-OPT   VALUE 'AU'.            
              88 COMMENTS-INCLUDED-PREMIUM  VALUE 'IP'.                
              88 COMMENTS-INCLUDED-ALL-PREMIUMS VALUE 'IA'.            
              88 COMMENTS-RSN-AUDIT-REQUEST VALUE 'AR'.                
              88 COMMENTS-HOLD-FRA-FIN-AUDIT VALUE 'HF'.               
              88 COMMENTS-HOLD-FRA-INT-AUDIT VALUE 'HI'.               
              88 COMMENTS-COUNTERSIGNING-AGENT VALUE 'CS'.             
              88 COMMENTS-CANCELLATION   VALUE 'CN'.                   
              88 COMMENTS-DEC-PRINT      VALUE 'DP'.                   
              88 COMMENTS-MINOR-BIRTHDAY   VALUE 'MB'.                 
              88 COMMENTS-DEL-VEHICLE    VALUE 'DV'.                   
              88 MASS-MR-DRIV-INQ        VALUE 'AB'.                   
              88 MASS-MR-RENEWAL-INQ     VALUE 'AA'.                   
              88 MASS-MR-REINQUIRY       VALUE 'AF'.                   
              88 COMMENTS-RENEWAL-QUESTIONNAIRE VALUE 'RQ'.            
              88 COMMENTS-MASS-RESPONSE-ERROR VALUE 'AC'.              
              88 COMMENTS-MASS-RESPONSE-VALID VALUE 'AD'.              
              88 COMMENTS-MASS-RESP-REINQUIRY VALUE 'AF'.              
              88 COMMENTS-FUTURE-STATUS VALUE 'FS'.                    
              88 COMMENTS-RESEQ-VEHICLE  VALUE 'RV'.                   
              88 COMMENTS-COMPUTER-GENERATED VALUE 'CG'.               
              88 COMMENTS-RECISION       VALUE 'RE'.                   
              88 COMMENTS-AGENTS-QUOTE   VALUE 'AQ'.                   
              88 COMMENTS-HOLD-RENEWAL   VALUE 'HR'.                   
              88 COMMENTS-HOLD-CANCELLATION VALUE 'HC'.                
              88 COMMENTS-HOLD-PURGE     VALUE 'HP'.                   
              88 COMMENTS-STATUS-REQUESTED VALUE 'ST'.                 
              88 COMMENTS-STATUS           VALUE 'ST' 'SL' 'SC' 'SP'.  
              88 MASS-AUTO-LOB-CHANGE    VALUE 'AE'.                   
              88 MASS-AUTO-CEDED-PERM-COMMENT  VALUE 'MC'.             
              88 COMMENTS-RATE-CHANGE    VALUE 'RC' 'AE'.              
              88 COMMENTS-CLASS-PUBLIC      VALUE 'CL'.                
              88 COMMENTS-HOLD-TRANSACTION VALUE 'HT'.                 
              88 COMMENTS-CANCELLATION-PENDING VALUE 'CP'.             
              88 COMMENTS-PERMANENT-COMMENT VALUE 'PC'.                
              88 COMMENTS-NON-RENEWAL    VALUE 'NR'.                   
              88 COMMENTS-SELF-DESTRUCT-POLICY VALUE 'SD'.             
              88 COMMENTS-WAIVE-MIN-RET-PREM VALUE 'WM'.               
              88 COMMENTS-MRP-OVERRIDE VALUE 'MP'.                     
              88 COMMENTS-SET-MOE-CODES  VALUE 'CP' 'CN' 'NR'.         
              88 COMMENTS-NEW-SOFT-ERROR VALUE 'S0' 'S1' 'S2' 'S3'     
                    'S4' 'S5' 'S6' 'S7' 'S8' 'S9'.                     
              88 COMMENTS-OLD-SOFT-ERROR VALUE 'E0' 'E1' 'E2' 'E3'     
                    'E4' 'E5' 'E6' 'E7' 'E8' 'E9'.                     
              88 COMMENTS-MANUAL-AUDIT   VALUE 'WK' 'WJ'.              
              88 ODD-TERM-CMNTS-REC      VALUE 'OT'.                   
              88 SC-CARRIER-TYPE-03-REC  VALUE 'CT'.                   
              88 COMMENTS-COVERAGE-CHG   VALUE 'CC'.                   
              88 RECLASS-DRIVER-MIDTERM  VALUE 'RM'.                   
              88 MASS-AUTO-DRIV-RECLASS  VALUE 'RM'.                   
              88 COMMENTS-EARTH-QUAKE    VALUE 'EQ'.                   
              88 VALID-COMMENTS          VALUE 'DP' 'EQ'.              
              88 CLAIMS-MADE             VALUE 'CM'.                   
              88 COVERAGE-EXCLUSIONS     VALUE 'EX'.                   
              88 COMMENTS-UK-DIRECT-DEBIT VALUE 'DD'.                  
              88 COMMENTS-FS1-LIABILITY  VALUE 'F1'.                   
              88 COMMENTS-FS4-LIABILITY  VALUE 'F4'.                   
              88 COMMENTS-OLD-PREMIUM    VALUE 'OP'.                   
              88 COMMENTS-ADDITION-PACKAGE VALUE 'AP'.                 
              88 COMMENTS-ACA-FIXED-ID   VALUE 'FI'.                   
              88 COMMENTS-LAST-UPDATE    VALUE 'XX'.                   
              88 COMMENTS-DEC-DEST-OVERRIDE VALUE 'ZA'.                
              88 COMMENTS-CUST-USE-ONLY-ZA-ZZ                          
                 VALUE 'ZA' 'ZB' 'ZC' 'ZD' 'ZE' 'ZF' 'ZG' 'ZH' 'ZI'    
                       'ZJ' 'ZK' 'ZL' 'ZM' 'ZN' 'ZO' 'ZP' 'ZQ' 'ZR'    
                       'ZS' 'ZT' 'ZU' 'ZV' 'ZW' 'ZX' 'ZY' 'ZZ'.        
              88 COMMENTS-RATE-CHANGE-REP  VALUE 'RC'.                 
           05 COMMENTS-TYPE-REQUEST      PIC X(01).                    
           05 COMMENTS-DEST-BRANCH       PIC X(02).                    
           05 COMMENTS-DEST              PIC X(05).                    
              88 COMMENTS-DEST-CHRGD            VALUE 'CHRGD'.         
              88 COMMENTS-DEPOSIT-DES           VALUE 'DEPOS'.         
              88 COMMENTS-ANV-RATING            VALUE 'RATED'.         
           05 COMMENTS-REQUESTED-BY      PIC X(03).                    
              88 COMMENTS-REQUESTED-BY-ACA      VALUE 'ACA'.           
              88 COMMENTS-REQUESTED-BY-TAX      VALUE 'TAX'.           
              88 COMMENTS-REQUESTED-BY-ANV      VALUE 'ANV'.           
           05 COMMENTS-SUSPENSE-DATE     PIC 9(08).                    
              88 COMMENTS-SUSPENSE-DATE-991231  VALUE 19991231.        
              88 COMMENTS-SUSPENSE-DTE-20991231  VALUE 20991231.       
           05 COMMENTS-SEQ               PIC X(02).                    
              88 COMMENTS-SEQ-ACA-UPF-RECORD    VALUE '90'.            
              88 COMMENTS-SEQ-ANV-RATING        VALUE '01'.            
           05 COMMENTS-LOSS-DATE         PIC X(08).                    
           05 COMMENTS-LOSS-OCCURANCE-X.                               
              10 COMMENTS-LOSS-OCCURANCE PIC 9(03).                    
           05 COMMENTS-LOSS-CLAIMANT-X.                                
              10 COMMENTS-LOSS-CLAIMANT  PIC 9(03).                    
           05 COMMENTS-FREQUENCY         PIC X(01).                    
              88 COMMENTS-FREQUENCY-IS-BIWEEKLY  VALUE 'B'.            
              88 COMMENTS-FREQUENCY-IS-MOLY-CAL  VALUE 'C'.            
              88 COMMENTS-FREQUENCY-IS-MOLY-28   VALUE 'M'.            
              88 COMMENTS-FREQUENCY-IS-WEEKLY    VALUE 'W'.            
          04 COMMENTS-REST-OF-RECORD.                                  
           05 COMMENTS-AREA              PIC X(51).                    
           05 COMMENTS-AREA-2             PIC X(51).                   
           05 FILLER                      PIC X(10).                   
           05 COMMENTS-PMS-FUTURE-USE     PIC X(153).                  
           05 COMMENTS-ENTERED-DATE       PIC X(08).                   
           05  COMMENTS-CUST-FUTURE-USE.                               
           10  COMMENTS-RC-OVERRIDE        PIC X(1).                   
           10  FILLER                      PIC X(34).                  
           05 COMMENTS-YR2000-CUST-USE    PIC X(100).                  
           05 COMMENTS-DUP-KEY-SEQ-NUM    PIC X(03).
           05 NEEDED-SORT-DATA          PIC X(179).
           05 BLANK-OUT                 PIC X(44).
