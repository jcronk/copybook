        01 PMDRXA1-RECORD.
           03 PMDRXA1-UNIT-GRP-RATE-SEG.
           05 PMDRXA1-SEGMENT-KEY.                                      
              07 PMDRXA1-SEGMENT-ID                     PIC X(02).      
                 88 PMDRXA1-SEGMENT-43                  VALUE '43'.     
              07 PMDRXA1-SEGMENT-STATUS                 PIC X(01).      
                 88 PMDRXA1-ACTIVE-STATUS               VALUE 'A'.      
                 88 PMDRXA1-DELETED-STATUS              VALUE 'D'.      
                 88 PMDRXA1-HISTORY-STATUS              VALUE 'H'.      
                 88 PMDRXA1-NOT-ACTIVE-STATUS           VALUE 'H' 'D'.  
                 88 PMDRXA1-RENEWAL-FS-STATUS           VALUE 'R' 'F'.  
              07 PMDRXA1-TRANSACTION-DATE               PIC 9(08).
              07 PMDRXA1-SEGMENT-ID-KEY.                                
                 09 PMDRXA1-SEGMENT-LEVEL-CODE          PIC X(01).      
                    88 PMDRXA1-SEG-LEVEL-R              VALUE 'R'.      
                 09 PMDRXA1-SEGMENT-PART-CODE           PIC X(01).      
                    88 PMDRXA1-SEG-PART-X               VALUE 'X'.      
                 09 PMDRXA1-SUB-PART-CODE               PIC X(01).      
                 09 PMDRXA1-INSURANCE-LINE              PIC X(02).      
                    88 PMDRXA1-LIABILITY                VALUE 'GA'.     
                    88 PMDRXA1-GARAGE                   VALUE 'GA'.     
              07 PMDRXA1-LEVEL-KEY.                                     
                 09 PMDRXA1-LOCATION-NUMBER-A.                          
                    11 PMDRXA1-LOCATION-NUMBER          PIC 9(04).      
                 09 FILLER                              PIC X(03).      
                 09 PMDRXA1-RISK-UNIT-GROUP-KEY.                        
                    11 PMDRXA1-COVERAGE                 PIC X(03).      
                       88 PMDRXA1-STATE-REC             VALUE '000'.    
                       88 PMDRXA1-SL                    VALUE '100'.    
                       88 PMDRXA1-AGG                   VALUE '101'.    
                       88 PMDRXA1-PIP                   VALUE '102'.    
                       88 PMDRXA1-PIC                   VALUE '103'.    
                       88 PMDRXA1-PIE                   VALUE '104'.    
                       88 PMDRXA1-EPI                   VALUE '105'.    
                       88 PMDRXA1-PIN                   VALUE '106'.    
                       88 PMDRXA1-MP                    VALUE '107'.    
                       88 PMDRXA1-UMB                   VALUE '108'.    
                       88 PMDRXA1-UMP                   VALUE '109'.    
                       88 PMDRXA1-UNB                   VALUE '110'.    
                       88 PMDRXA1-UNP                   VALUE '111'.    
                       88 PMDRXA1-UMS                   VALUE '112'.    
                       88 PMDRXA1-BI                    VALUE '113'.    
                       88 PMDRXA1-PD                    VALUE '114'.    
                       88 PMDRXA1-GKL                   VALUE '115'.    
                       88 PMDRXA1-DPD                   VALUE '116'.    
                       88 PMDRXA1-UNS                   VALUE '117'.    
                       88 PMDRXA1-FPB                   VALUE '118'.    
                       88 PMDRXA1-MEB                   VALUE '119'.    
                       88 PMDRXA1-WLB                   VALUE '120'.    
                       88 PMDRXA1-FEB                   VALUE '121'.    
                       88 PMDRXA1-ADB                   VALUE '122'.    
                       88 PMDRXA1-CPB                   VALUE '123'.    
                       88 PMDRXA1-FBI                   VALUE '124'.    
                       88 PMDRXA1-MBI                   VALUE '125'.    
                       88 PMDRXA1-WBI                   VALUE '126'.    
                       88 PMDRXA1-FEI                   VALUE '127'.    
                       88 PMDRXA1-ABI                   VALUE '128'.    
                       88 PMDRXA1-CBI                   VALUE '129'.    
                       88 PMDRXA1-EMB                   VALUE '130'.    
                       88 PMDRXA1-PIA                   VALUE '131'.    
                       88 PMDRXA1-UMV                   VALUE '132'.    
                       88 PMDRXA1-UNV                   VALUE '133'.    
                       88 PMDRXA1-PP                    VALUE '140'.    
                       88 PMDRXA1-PDB                   VALUE '141'.    
                       88 PMDRXA1-FLL                   VALUE '142'.    
                       88 PMDRXA1-CA                    VALUE '143'.    
                       88 PMDRXA1-CAB                   VALUE '144'.    
                       88 PMDRXA1-AWA                   VALUE '145'.    
                       88 PMDRXA1-MI                    VALUE '146'.    
                       88 PMDRXA1-OBE                   VALUE '167'.    
                       88 PMDRXA1-SUS                   VALUE '135'.    
                       88 PMDRXA1-SUB                   VALUE '235'.    
                    11 PMDRXA1-SEQUENCE                 PIC X(03).      
              07 PMDRXA1-ITEM-EFFECTIVE-DATE            PIC 9(08).
              07 PMDRXA1-PROCESS-DATE                   PIC 9(08).
           05 PMDRXA1-SEGMENT-DATA.                                     
              10 PMDRXA1-PMS-USAGE                      PIC X(211).     
              10 PMDRXA1-CUSTOMER-FUTURE-USE            PIC X(96).      
              10 PMDRXA1-YR2000-CUST-USE                PIC X(100).     
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).