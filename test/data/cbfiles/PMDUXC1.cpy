        01 PMDUXC1-RISK-UNIT-RATE-RECORD.
           03 PMDUXC1-RISK-UNIT-RATE-SEG.                               
            05 PMDUXC1-SEGMENT-KEY.                                     
              07 PMDUXC1-SEGMENT-ID                   PIC X(02).        
                 88 PMDUXC1-SEGMENT-43                 VALUE '43'.      
              07 PMDUXC1-SEGMENT-STATUS               PIC X(01).        
                 88 PMDUXC1-ACTIVE-STATUS              VALUE 'A'.       
                 88 PMDUXC1-DELETED-STATUS             VALUE 'D'.       
                 88 PMDUXC1-HISTORY-STATUS             VALUE 'H'.       
                 88 PMDUXC1-NOT-ACTIVE-STATUS          VALUE 'H' 'D'.   
                 88 PMDUXC1-RENEWAL-FS-STATUS          VALUE 'R' 'F'.   
              07 PMDUXC1-TRANSACTION-DATE             PIC 9(08).
              07 PMDUXC1-SEGMENT-ID-KEY.                                
                 09 PMDUXC1-SEGMENT-LEVEL-CODE        PIC X(01).        
                    88 PMDUXC1-SEG-LEVEL-U             VALUE 'U'.       
                 09 PMDUXC1-SEGMENT-PART-CODE         PIC X(01).        
                    88 PMDUXC1-SEG-PART-X              VALUE 'X'.       
                 09 PMDUXC1-SUB-PART-CODE             PIC X(01).        
                 09 PMDUXC1-INSURANCE-LINE            PIC X(02).        
                    88 PMDUXC1-COMMERCIAL-CRIME       VALUE 'CR'.       
              07 PMDUXC1-LEVEL-KEY.                                     
                 09 PMDUXC1-LOCATION-SLOC.                              
                    11 PMDUXC1-LOCATION-NUMBER-A.                       
                       13 PMDUXC1-LOCATION-NUMBER     PIC 9(04).        
                    11 PMDUXC1-SUB-LOCATION-NUMBER-A.                   
                       13 PMDUXC1-SUB-LOCATION-NUMBER PIC 9(03).        
                 09 PMDUXC1-RISK-UNIT-GROUP-KEY       PIC X(06).        
                 09 PMDUXC1-RISK-UNIT.                                  
                    11 PMDUXC1-COVERAGE               PIC X(03).        
                       88 PMDUXC1-A-EMPLOY-DISH-BLKT     VALUE '530'.   
                       88 PMDUXC1-A-EMPLOY-DISH-SCHED    VALUE '531'.   
                       88 PMDUXC1-B-FORG-OR-ALT          VALUE '532'.   
                       88 PMDUXC1-CP-T-D-D-PREM          VALUE '534'.   
                       88 PMDUXC1-CM-T-D-D-MESS          VALUE '535'.   
                       88 PMDUXC1-DR-ROB-CUST            VALUE '536'.   
                       88 PMDUXC1-DB-SAFE-BURG           VALUE '537'.   
                       88 PMDUXC1-DM-ROB-MESS            VALUE '538'.   
                       88 PMDUXC1-E-PREM-BURG            VALUE '542'.   
                       88 PMDUXC1-F-COMPUTE-FRD          VALUE '544'.   
                       88 PMDUXC1-G-EXTORTION            VALUE '546'.   
                       88 PMDUXC1-HP-PREM-THEFT          VALUE '548'.   
                       88 PMDUXC1-HM-ROB-OUTSID          VALUE '549'.   
                       88 PMDUXC1-IS-LESSEE-SEC          VALUE '550'.   
                       88 PMDUXC1-IP-LESSEE-PRO          VALUE '551'.   
                       88 PMDUXC1-J-SECUR-W-OTH                         
                          VALUES '552' '700' '701' '702'.               
                       88 PMDUXC1-RP6-K-GST-SAFE-DP      VALUE '554'.   
                       88 PMDUXC1-RP7-L-GUEST-PREM       VALUE '556'.   
                       88 PMDUXC1-RP8-M-SAFE-LIAB        VALUE '558'.   
                       88 PMDUXC1-RP8-NR-SAFE-DIRECT     VALUE '560'.   
                       88 PMDUXC1-RP8-NB-SAFE-DIRECT     VALUE '565'.   
                       88 PMDUXC1-QR-ROB-CS              VALUE '566'.   
                       88 PMDUXC1-QB-SF-BRG              VALUE '567'.   
                       88 PMDUXC1-QM-ROB-MES             VALUE '568'.   
                       88 PMDUXC1-RP3-STOREKEEP-BF       VALUE '570'.   
                       88 PMDUXC1-RP4-STOREKEEP-BURG     VALUE '580'.   
                       88 PMDUXC1-RP5-OFFICE-BURG-ROB    VALUE '590'.   
                       88 PMDUXC1-RP9-EXCESS-BANK-ROB    VALUE '600'.   
                       88 PMDUXC1-RP9-EXCESS-BANK-BURG   VALUE '605'.   
                       88 PMDUXC1-RP10-BANK-EXCESS-SEC   VALUE '610'.   
                       88 PMDUXC1-MISCELLANEOUS          VALUE '999'.   
                    11 PMDUXC1-COVERAGE-AMENDMENT     PIC X(03).        
                       88 PMDUXC1-BASE-COVERAGE          VALUE '   '.   
                       88 PMDUXC1-OFFICE-EQUIP           VALUE 'OE '.   
                       88 PMDUXC1-FIXTURES-FIT           VALUE 'FF '.   
                       88 PMDUXC1-PUBLIC-ENTR            VALUE 'PE '.   
                       88 PMDUXC1-DRO-ROB-CS             VALUE 'DRO'.   
                       88 PMDUXC1-DBO-SF-BURG            VALUE 'DBO'.   
                       88 PMDUXC1-DMO-ROB-MES            VALUE 'DMO'.   
                       88 PMDUXC1-HMO-ROB-OUT            VALUE 'HMO'.   
                       88 PMDUXC1-AE                     VALUE 'AE '.   
                       88 PMDUXC1-GS                     VALUE 'GS '.   
                 09 PMDUXC1-SEQUENCE-RISK-UNIT.                         
                    11 PMDUXC1-MESSENGER              PIC 9(01).        
                    11 FILLER                         PIC X(01).        
              07 PMDUXC1-ITEM-EFFECTIVE-DATE          PIC 9(08).
              07 PMDUXC1-VARIABLE-KEY                 PIC X(06).        
              07 PMDUXC1-PROCESS-DATE                 PIC 9(08).
             05 PMDUXC1-SEGMENT-DATA.                                   
                 09 PMDUXC1-RATE-GROUP             PIC 9(02).           
                 09 PMDUXC1-CRIME-LIMIT            PIC 9(10).           
                 09 PMDUXC1-DEDUCTIBLE             PIC 9(08).           
                 09 PMDUXC1-AMT-INS-MONEY          PIC 9(10).           
                 09 PMDUXC1-AMT-INS-SECURITIES     PIC 9(10).           
                 09 PMDUXC1-AMT-INS-CHECKS         PIC 9(10).           
                 09 PMDUXC1-AMT-INS-OTHER          PIC 9(10).           
                 09 PMDUXC1-AMT-INS-PAY-BANK       PIC 9(10).           
                 09 PMDUXC1-NUMBER-OF-GUARDS       PIC 9(01).           
                 09 PMDUXC1-ARMOR-MOTOR-VEHICLE    PIC X(01).           
                    88 PMDUXC1-VEHICLE-YES             VALUE 'Y'.       
                    88 PMDUXC1-VEHICLE-NO              VALUE ' '.       
                 09 PMDUXC1-PRIVATE-CONVEY-IND     PIC X(01).           
                    88 PMDUXC1-PRIVATE-CONVEY-YES      VALUE 'Y'.       
                    88 PMDUXC1-PRIVATE-CONVEY-NO       VALUE ' '.       
                 09 PMDUXC1-BAG-SAFE-CHEST-IND     PIC X(01).           
                    88 PMDUXC1-BAG-SAFE-CHEST-YES      VALUE 'Y'.       
                    88 PMDUXC1-BAG-SAFE-CHEST-NO       VALUE ' '.       
                 09 PMDUXC1-HOLDUP-ALARM-IND       PIC X(01).           
                    88 PMDUXC1-HOLDUP-ALARM-YES        VALUE 'Y'.       
                    88 PMDUXC1-HOLDUP-ALARM-NO         VALUE ' '.       
                 09 PMDUXC1-BULLET-RESIST-IND      PIC X(01).           
                    88 PMDUXC1-BULLET-RESIST-YES       VALUE 'Y'.       
                    88 PMDUXC1-BULLET-RESIST-NO        VALUE ' '.       
                 09 PMDUXC1-BULLET-RESIST-ALRM-IND PIC X(01).           
                    88 PMDUXC1-BULLET-RESIST-ALRM-YES  VALUE 'Y'.       
                    88 PMDUXC1-BULLET-RESIST-ALRM-NO   VALUE ' '.       
                 09 PMDUXC1-GUARD-IND              PIC X(01).           
                    88 PMDUXC1-GUARD-YES               VALUE 'Y'.       
                    88 PMDUXC1-GUARD-NO                VALUE ' '.       
                 09 PMDUXC1-BURG-RESIST-GLAZE-IND  PIC X(01).           
                    88 PMDUXC1-BURG-RESIST-GLAZE-YES   VALUE 'Y'.       
                    88 PMDUXC1-BURG-RESIST-GLAZE-NO    VALUE ' '.       
                 09 PMDUXC1-RELOCKING-DEVICE-IND   PIC X(01).           
                    88 PMDUXC1-RELOCKING-DEVICE-YES    VALUE 'Y'.       
                    88 PMDUXC1-RELOCKING-DEVICE-NO     VALUE ' '.       
                 09 PMDUXC1-CENTRAL-STATION-NUMBER PIC 9(01).           
                 09 PMDUXC1-REGIST-AT-CLOCK-NUMBER PIC 9(01).           
                 09 PMDUXC1-DO-NOT-SIGNAL-NUMBER   PIC 9(01).           
                 09 PMDUXC1-OPEN24-HOUR-IND        PIC X(01).           
                    88 PMDUXC1-OPEN24-HOUR-YES         VALUE 'Y'.       
                    88 PMDUXC1-OPEN24-HOUR-NO          VALUE ' '.       
                 09 PMDUXC1-OTHER-DISCOUNT-RATE    PIC 9(01)V9(03).     
                 09 PMDUXC1-DEC-CHANGE-FLAG        PIC X(01).           
                    88 PMDUXC1-DEC-CHANGE-NEEDED   VALUE 'A'.           
                    88 PMDUXC1-DEC-CHANGE-NOTED    VALUE 'B'.           
                 09 PMDUXC1-FENCED-IND             PIC X(01).           
                 09 PMDUXC1-VANDALISM-IND          PIC X(01).           
                 09 PMDUXC1-ROB-JANITOR-IND        PIC X(01).           
                 09 PMDUXC1-BLKT-BOND-EXCESS-IND   PIC X(01).           
                 09 PMDUXC1-LOSS-DURING-FIRE-IND   PIC X(01).           
                 09 PMDUXC1-INIT-MESSENGER-IND     PIC X(01).           
                 09 PMDUXC1-MULT-PREM-MESS-IND     PIC X(01).           
                 09 PMDUXC1-SPEC-PROP-LIMIT-A.                          
                    11 PMDUXC1-SPEC-PROP-LIMIT     PIC 9(10).           
                 09 PMDUXC1-PREM-ALARM-FACTOR      PIC V9(03).          
                 09 PMDUXC1-SAFE-VAULT-FACTOR      PIC V9(03).          
                 09 PMDUXC1-MULT-PREM-FACTOR       PIC V9(03).          
                 09 PMDUXC1-HOLDUP-ALARM-FACTOR    PIC V9(03).          
                 09 PMDUXC1-BRE-FACTOR             PIC V9(03).          
                 09 PMDUXC1-BRE-ALARM-FACTOR       PIC V9(03).          
                 09 PMDUXC1-GUARD-FACTOR           PIC V9(03).          
                 09 PMDUXC1-WATCHPERSON-FACTOR     PIC V9(03).          
                 09 PMDUXC1-MULT-MESS-FACTOR       PIC V9(03).          
                 09 PMDUXC1-MESS-BAG-SAFE-FCTR     PIC V9(03).          
                 09 PMDUXC1-PRIV-CONVEY-FACTOR     PIC V9(03).          
                 09 PMDUXC1-EXCESS-OVER-FACTOR     PIC V9(03).          
                 09 PMDUXC1-PREM-OPEN-24-FCTR      PIC V9(03).          
                 09 PMDUXC1-GLAZE-MATERIAL-FCTR    PIC V9(03).          
                 09 PMDUXC1-GROSS-PREMIUM          PIC 9(08)V9(03).     
                 09 PMDUXC1-NET-PREMIUM            PIC 9(08)V9(03).     
                 09 PMDUXC1-MESSENGER-NUM          PIC X(01).           
                 09 PMDUXC1-JANITOR-SURCHARGE      PIC 9(01)V9(03).     
                 09 PMDUXC1-INCR-LIMIT-SPEC-PROP   PIC 9(04).           
                 09 PMDUXC1-EXTEND-DEF-PREM        PIC 9(01)V9(03).     
                 09 PMDUXC1-INCL-VANDALISM         PIC 9(01)V9(03).     
                 09 PMDUXC1-SERV-AUTO-AMEND        PIC 9(03)V9(03).     
                 09 PMDUXC1-INCR-LIMIT-GUEST       PIC 9(01)V9(03).     
                 09 PMDUXC1-COV-L-MULTIPLES        PIC 9(03).           
                 09 PMDUXC1-INCL-LOSS-FIRE         PIC 9(03)V9(03).     
                 09 PMDUXC1-LOSS-COST-USED-SW      PIC X(01).           
                    88 PMDUXC1-LOSS-COST-USED      VALUE 'Y'.           
                 09 PMDUXC1-LOSS-COST-MULT         PIC 9(03)V9(02).     
                 09 PMDUXC1-FID-EXPERIENCE         PIC 9(01)V9(03).     
                 09 PMDUXC1-REPLACE-CST-IND        PIC X(01).           
                 09 PMDUXC1-EXTENDED-PREMISES      PIC X(01).           
                 09 PMDUXC1-PROTECTIVE-DEVICES     PIC X(01).           
                 09 PMDUXC1-SELLING-PRICE          PIC X(01).           
                 09 PMDUXC1-OUTSIDE-ROBBERY        PIC X(01).           
                 09 PMDUXC1-RECORD-OF-CHECKS       PIC X(01).           
                 09 PMDUXC1-RATABLE-EMPLOYEES-A.                        
                    11 PMDUXC1-RATABLE-EMPLOYEES   PIC 9(05).           
                 09 PMDUXC1-NUMBER-EMPLOYEES-A.                         
                    11 PMDUXC1-NUMBER-EMPLOYEES    PIC 9(05).           
                 09 PMDUXC1-EXPOSURE-UNITS         PIC 9(07)V9(03).     
                 09 PMDUXC1-EXP-DED-UNITS          PIC 9(07)V9(03).     
                 09 PMDUXC1-REC-OF-CHECKS-FACTOR   PIC 9(01)V9(03).     
                 09 PMDUXC1-SELLING-PRICE-FACTOR   PIC 9(01)V9(03).     
                 09 PMDUXC1-PMS-FUTURE-USE         PIC X(03).           
                 09 PMDUXC1-CUSTOMER-USE           PIC X(30).           
                 09 PMDUXC1-YR2000-CUST-USE        PIC X(100).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).