        01 PMDLXA1-SEGMENT-INFO-RECORD.
           03 PMDLXA1-INS-LINE-RATE-SEG.
           05 PMDLXA1-SEGMENT-KEY.                                      
              10 PMDLXA1-SEGMENT-ID                     PIC X(02).      
                 88 PMDLXA1-SEGMENT-43                  VALUE '43'.     
              10 PMDLXA1-SEGMENT-STATUS                 PIC X(01).      
                 88 PMDLXA1-ACTIVE-STATUS               VALUE 'A'.      
                 88 PMDLXA1-DELETED-STATUS              VALUE 'D'.      
                 88 PMDLXA1-HISTORY-STATUS              VALUE 'H'.      
                 88 PMDLXA1-NOT-ACTIVE-STATUS           VALUE 'H' 'D'.  
                 88 PMDLXA1-RENEWAL-FS-STATUS           VALUE 'R' 'F'.  
              10 PMDLXA1-TRANSACTION-DATE               PIC 9(08).                              
              10 PMDLXA1-SEGMENT-ID-KEY.                                
                 15 PMDLXA1-SEGMENT-LEVEL-CODE          PIC X(01).      
                    88 PMDLXA1-SEG-LEVEL-L              VALUE 'L'.      
                 15 PMDLXA1-SEGMENT-PART-CODE           PIC X(01).      
                    88 PMDLXA1-SEG-PART-X               VALUE 'X'.      
                 15 PMDLXA1-SUB-PART-CODE               PIC X(01).      
                 15 PMDLXA1-INSURANCE-LINE              PIC X(02).      
                    88 PMDLXA1-GARAGE                   VALUE 'GA'.     
                    88 PMDLXA1-GARAGE-LIABILITY         VALUE 'GA'.     
              10 PMDLXA1-LEVEL-KEY.                                     
                 15 PMDLXA1-LOCATION-NUMBER-A.                          
                    20 PMDLXA1-LOCATION-NUMBER          PIC 9(04).      
              10 PMDLXA1-ITEM-EFFECTIVE-DATE            PIC 9(08). 
              10 PMDLXA1-VARIABLE-KEY.                                  
                 15 FILLER                              PIC X(06).      
              10 PMDLXA1-PROCESS-DATE                   PIC 9(08).
           05 PMDLXA1-SEGMENT-DATA.                                     
              10 PMDLXA1-ORIGINAL-PROCESS-DATE          PIC 9(08).
              10 PMDLXA1-VARYING-DATA.                                  
                 15 PMDLXA1-POLICY-COMPANY-A.                           
                    20 PMDLXA1-POLICY-COMPANY           PIC 9(02).      
                 15 PMDLXA1-LIABILITY-AUDIT             PIC X(01).      
                 15 PMDLXA1-PHYSICAL-DAMAGE             PIC X(01).      
                 15 PMDLXA1-BROADENED-COVERAGE          PIC X(01).      
                 15 PMDLXA1-PI-GROUP.                                   
                    20 PMDLXA1-PERS-INJ-LI-GROUP.                       
                       25 PMDLXA1-PILG1                 PIC X(01).      
                          88 PMDLXA1-VALID-PILG1 VALUE 'A' 'B' 'C' ' '. 
                       25 PMDLXA1-PILG2                 PIC X(01).      
                          88 PMDLXA1-VALID-PILG2 VALUE 'A' 'B' 'C' ' '. 
                       25 PMDLXA1-PILG3                 PIC X(01).      
                          88 PMDLXA1-VALID-PILG3 VALUE 'A' 'B' 'C' ' '. 
                    20 PMDLXA1-DEL-EXC-C                PIC X(01).      
                 15 PMDLXA1-BROAD-FORM-PRODUCTS         PIC X(01).      
                 15 PMDLXA1-DEL-COMP.                                   
                    20 PMDLXA1-DELETE-100-COMP          PIC X(01).      
                 15 PMDLXA1-COMPANY-DEVIATION-A.                        
                    20 PMDLXA1-COMPANY-DEVIATION        PIC 9V9(03).    
                 15 PMDLXA1-BLK-COL-ADJ-FAC-A.                          
                    20 PMDLXA1-BLK-COL-ADJ-FAC          PIC 9V9(03).    
                 15 PMDLXA1-TERM-FACTOR                 PIC 9(02)V999.  
                 15 PMDLXA1-BLK-COL-VALU-FAC-A.                         
                    20 PMDLXA1-BLK-COL-VALU-FAC         PIC 9(04)V99.   
              10 PMDLXA1-SAVE-LIAB-UX-AC                PIC X(01).      
              10 PMDLXA1-LIAB-UX-TRANS-DATE             PIC X(08).      
              10 PMDLXA1-SAVE-LIAB-RX-AC                PIC X(01).      
              10 PMDLXA1-LIAB-RX-TRANS-DATE             PIC X(08).      
              10 PMDLXA1-AGENTS-COMM-RATE-A.                            
                 15 PMDLXA1-AGENTS-COMM-RATE            PIC 9V9(05).    
              10 PMDLXA1-FIRE-LEGAL-LIAB                PIC X(01).      
                 88 PMDLXA1-FLL-COV                     VALUE 'Y'.      
              10 PMDLXA1-PMA-CODE                       PIC X(02).      
                 88 PMDLXA1-MOTEL-HOTEL                 VALUE 'MH'.     
                 88 PMDLXA1-APARTMENT                   VALUE 'A '.     
                 88 PMDLXA1-OFFICE                      VALUE 'O '.     
                 88 PMDLXA1-MERCANTILE                  VALUE 'M '.     
                 88 PMDLXA1-INSTITUTIONAL               VALUE 'I '.     
                 88 PMDLXA1-SERVICES                    VALUE 'S '.     
                 88 PMDLXA1-INDUS-PROCESS               VALUE 'IP'.     
                 88 PMDLXA1-CONTRACTOR                  VALUE 'C '.     
                 88 PMDLXA1-MONOLINE-CPP                VALUE '  '.     
                 88 PMDLXA1-VALID-PMA-CODE    VALUE '  ' 'MH' 'A '      
                                               'O ' 'M ' 'I ' 'S '      
                                               'IP' 'C '.               
              10 PMDLXA1-RATE-LEVEL-DATE                PIC X(08).      
              10 PMDLXA1-WORK-SHEET-IND                 PIC X(01).      
              10 PMDLXA1-PMS-FUTURE-USE                 PIC X(136).     
              10 PMDLXA1-CUSTOMER-USE                   PIC X(100).     
              10 PMDLXA1-YR2000-CUST-USE                PIC X(100).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).