        01 PMDRXP1-UNIT-GRP-RATING-RECORD.
           03 PMDRXP1-UNIT-GRP-RATING-SEG.                              
             05 PMDRXP1-SEGMENT-KEY.                                    
              07 PMDRXP1-SEGMENT-ID                   PIC X(02).        
                 88 PMDRXP1-SEGMENT-43                 VALUE '43'.      
              07 PMDRXP1-SEGMENT-STATUS               PIC X(01).        
                 88 PMDRXP1-ACTIVE-STATUS              VALUE 'A'.       
                 88 PMDRXP1-DELETED-STATUS             VALUE 'D'.       
                 88 PMDRXP1-HISTORY-STATUS             VALUE 'H'.       
                 88 PMDRXP1-NOT-ACTIVE-STATUS          VALUE 'H' 'D'.   
                 88 PMDRXP1-RENEWAL-FS-STATUS          VALUE 'R' 'F'.   
              07 PMDRXP1-TRANSACTION-DATE             PIC 9(08).
              07 PMDRXP1-SEGMENT-ID-KEY.                                
                 09 PMDRXP1-SEGMENT-LEVEL-CODE        PIC X(01).        
                    88 PMDRXP1-SEG-LEVEL-R             VALUE 'R'.       
                 09 PMDRXP1-SEGMENT-PART-CODE         PIC X(01).        
                    88 PMDRXP1-SEG-PART-X              VALUE 'X'.       
                 09 PMDRXP1-SUB-PART-CODE             PIC X(01).        
                 09 PMDRXP1-INSURANCE-LINE            PIC X(02).        
              07 PMDRXP1-LEVEL-KEY.                                     
                 09 PMDRXP1-LOCATION-NUMBER-A.                          
                    11 PMDRXP1-LOCATION-NUMBER           PIC 9(04).     
                       88 PMDRXP1-TERRORISM-COV-LOC      VALUE 0911.    
                 09 PMDRXP1-SUB-LOCATION-NUMBER-A.                      
                    11 PMDRXP1-SUB-LOCATION-NUMBER       PIC 9(03).     
                 09 PMDRXP1-RISK-UNIT-GROUP-KEY.                        
                    11 PMDRXP1-PMS-DEF-SUBJ-OF-INS-A.                   
                       13 PMDRXP1-PMS-DEF-SUBJ-OF-INS    PIC 9(03).     
                          88 PMDRXP1-BUILDING             VALUE 100.    
                          88 PMDRXP1-FIRE-LEGAL-BLDG      VALUE 101.    
                          88 PMDRXP1-BUILDERS-RISK        VALUE 102.    
                          88 PMDRXP1-BLKT-BLDG            VALUE 103.    
                          88 PMDRXP1-TRAILER              VALUE 104.    
                          88 PMDRXP1-CONTENTS             VALUE 110.    
                          88 PMDRXP1-FIRE-LEGAL-CONTENTS  VALUE 111.    
                          88 PMDRXP1-FURN-FIX             VALUE 112.    
                          88 PMDRXP1-BLKT-CONTS           VALUE 113.    
                          88 PMDRXP1-MACH-EQUIP           VALUE 114.    
                          88 PMDRXP1-BLKT-BLDG-CONTS      VALUE 115.    
                          88 PMDRXP1-PERS-PROP-EXC-STOCK  VALUE 116.    
                          88 PMDRXP1-BLKT-P-PR-EXC-STOCK  VALUE 117.    
                          88 PMDRXP1-PERS-PROP-OTHERS     VALUE 118.    
                          88 PMDRXP1-TRAILER-CONTS        VALUE 119.    
                          88 PMDRXP1-STOCK                VALUE 120.    
                          88 PMDRXP1-BLKT-STOCK           VALUE 123.    
                          88 PMDRXP1-COMMODITIES          VALUE 121.    
                          88 PMDRXP1-TREES-PLANTS-SHRUBS  VALUE 122.    
                          88 PMDRXP1-TENANTS-IMPROVEMENTS VALUE 124.    
                          88 PMDRXP1-BLKT-P-PR-INC-STOCK  VALUE 125.    
                          88 PMDRXP1-CONTS-EXC-PLANTS     VALUE 126.    
                          88 PMDRXP1-RADIO-TV-TOWERS      VALUE 128.    
                          88 PMDRXP1-SIGN                 VALUE 129.    
                          88 PMDRXP1-FENCE                VALUE 131.    
                          88 PMDRXP1-OFFICE-CONTENTS      VALUE 133.    
                          88 PMDRXP1-YARD                 VALUE 135.    
                          88 PMDRXP1-ALS                  VALUE 165.    
                          88 PMDRXP1-BLKT-BUS-INC         VALUE 173.    
                          88 PMDRXP1-BLKT-RENTAL-BUS-INC  VALUE 175.    
                          88 PMDRXP1-CLASS-DESC-NOT-REQ   VALUE 104     
                             122 128 129 131 190 192 194.               
                          88 PMDRXP1-BUSINESS-INCOME      VALUE 172     
                             173 175 180 182                            
                             174 176 178.                               
                          88 PMDRXP1-BUILDING-RUG         VALUE 100     
                             101 102 103 104 115 124 128 129 131.       
                          88 PMDRXP1-CONTENTS-RUG         VALUE 110     
                             111 112 113 114 116 118 119 120 121        
                             117 123 125                                
                             122 126 133 135.                           
                          88 PMDRXP1-BLKT-SUBJ-OF-INS     VALUE 103     
                             117 123 125 173 175                        
                             113 115.                                   
              07 PMDRXP1-ITEM-EFFECTIVE-DATE          PIC 9(08).
              07 PMDRXP1-VARIABLE-KEY.                                  
                 09 PMDRXP1-PEAK-SEASON-INDICATOR     PIC X(01).        
                    88 PMDRXP1-PEAK-SEASON-NO           VALUE '0'.      
                    88 PMDRXP1-PEAK-SEASON-YES          VALUE 'P'.      
                 09 FILLER                            PIC X(05).        
              07 PMDRXP1-PROCESS-DATE                 PIC 9(08).
             05 PMDRXP1-SEGMENT-DATA.                                   
                 09 PMDRXP1-RATING-STATE              PIC X(02).        
                 09 PMDRXP1-TERRITORY-A.                                
                    11 PMDRXP1-TERRITORY              PIC 9(03).        
                       88 PMDRXP1-NY-SPECIAL-TERRITORIES                
                          VALUE 030 240 310 410 430.                    
                       88 PMDRXP1-FLA-TERRITORY-WH                      
                          VALUE 060 310 315 430 500 560 565.            
                       88 PMDRXP1-FLA-TERRITORY-WHB-C-D  VALUE 130.     
                       88 PMDRXP1-MONROE-COUNTY    VALUE 440 445.       
                       88 PMDRXP1-KEY-WEST         VALUE 440.           
                       88 PMDRXP1-GA-TERRITORY-WH                       
                          VALUE 080 110 140 335 470 505.                
                 09 PMDRXP1-REPORTING-FREQUENCY       PIC X(01).        
                 09 PMDRXP1-CONTENT-REPORT-PCT-A.                       
                    11 PMDRXP1-CONTENT-REPORTING-PCT  PIC 9(01)V9(02).  
                 09 PMDRXP1-AMOUNT-OF-INSURANCE-A.                      
                    11 PMDRXP1-AMOUNT-OF-INSURANCE    PIC 9(10).        
                 09 PMDRXP1-PMA-CODE                  PIC X(02).        
                 09 PMDRXP1-CSP-CLS-CODE              PIC X(04).        
                    88 PMDRXP1-CSP-CLS-TENANT        VALUE '0321'       
                        '0322' '0323' '0581' '0582' '0701' '0702'.      
                    88 PMDRXP1-TENANT-A-CSP-CODE      VALUE '0511'      
                       '0701' '0702' '0745' '0746' '0747' '0851'        
                       '0852' '0900' '0921' '0923' '0931' '1000'        
                       '1052' '1070'.                                   
                    88 PMDRXP1-TENANT-B-CSP-CODE      VALUE '0520'      
                       '0541' '0562' '0564' '0570' '0580' '0832'        
                       '0940' '1051'.                                   
                    88 PMDRXP1-VACANCY-EXP-CLS-CODE   VALUE '0580'      
                       '0745' '0746' '0747' '0833' '0841' '0842'        
                       '0843' '0844' '0900' '1051' '1052'.              
                    88 PMDRXP1-STORAGE-CLS-CODE       VALUE '0511'      
                       '0512' '0520' '0531' '0532' '0541' '0561'        
                       '0562' '0563' '0564' '0565' '0566' '0567'        
                       '0570' '0581' '0582'.                            
                    88 PMDRXP1-GA-W-AND-H-EXCL-CODES  VALUE '0074'      
                       '0075' '0076' '0077' '0078' '0079' '0311'        
                       '0312' '0313' '0321' '0322' '0323'.              
                    88 PMDRXP1-BOSTON-SPECIAL-CODES   VALUE '0074'      
                       '0075' '0076' '0745' '0746' '0747' '0311'        
                       '0312' '0313'.                                   
                    88 PMDRXP1-APARTMENT-CLASS                          
                         VALUE '0311' '0312' '0313'                     
                               '0321' '0322' '0323'                     
                               '0074' '0075' '0076' '0196' '0197'       
                               '0310' '0320' '0330' '0360' '0370'       
                               '0380' '0390' '0070'                     
                               '0198' '0199'.                           
                    88 PMDRXP1-OFFICE-CLASS     VALUE '0701' '0702'     
                                                      '0700'.           
                    88 PMDRXP1-APARTMENT-OFFICE-CLASS   VALUE           
                       '0311' '0312' '0313' '0321' '0322' '0323'        
                       '0701' '0702' '0196' '0197' '0198' '0199'        
                       '0700' '0310' '0320' '0330' '0360' '0370'        
                       '0380' '0390' '0070'.                            
                    88 PMDRXP1-CSP-SPECIAL-CLASS  VALUE                 
                         '1190' '1200' '0833'.                          
                    88 PMDRXP1-CONDOMINIUMS       VALUE  '0300'.        
                    88 PMDRXP1-RESTAURANTS        VALUE  '0532'         
                         '0542'.                                        
                 09 PMDRXP1-CLS-DESC-CODE             PIC X(06).        
                 09 PMDRXP1-CLASS-DESC-SOURCE         PIC X(01).        
                    88 PMDRXP1-CLASS-TABLE          VALUE 'C'.          
                    88 PMDRXP1-CLASS-DESC-TABLE     VALUE 'D'.          
                    88 PMDRXP1-CLASS-ON-SEGMENT     VALUE 'S'.          
                    88 PMDRXP1-CLASS-BLANK          VALUE ' '.          
                 09 PMDRXP1-RATE-PLAN-A.                                
                       88 PMDRXP1-CLASS-RATED          VALUE '1'.       
                       88 PMDRXP1-SPECIFIC-RATED       VALUE '2'.       
                       88 PMDRXP1-SPECIAL-RATED        VALUE '3'.       
                       88 PMDRXP1-TENTATIVE-RATED      VALUE '4'.       
                    11 PMDRXP1-RATE-PLAN              PIC 9(01).        
                 09 PMDRXP1-BLDG-CSP-CLS-CODE         PIC X(04).        
                    88 PMDRXP1-TENANT-CSP-CODE       VALUE '0321'       
                        '0322' '0323' '0581' '0582' '0701' '0702'.      
                    88 PMDRXP1-B-CSP-SPECIAL-CLASS  VALUE               
                         '1190' '1200' '0833'.                          
                    88 PMDRXP1-TEX-BLDG-MISC-CLASS  VALUE               
                         '1185' '1190'.                                 
                 09 PMDRXP1-DEDUCTIBLE                PIC X(07).        
                 09 PMDRXP1-COINSURANCE-PERCENT       PIC X(03).        
                 09 PMDRXP1-CVG-EXPIR-DAT             PIC 9(08).
                 09 PMDRXP1-UM-CVG-TYPE-IND-A.                          
                    11 PMDRXP1-UM-CVG-TYPE-IND        PIC 9(01).        
                 09 PMDRXP1-RNTL-VAL-MONTHLY-LIM      PIC 9(03).        
                 09 PMDRXP1-RNTL-VAL-SEAS-INCEPT.                       
                    11 PMDRXP1-MONTH-SEAS-INCEPT      PIC X(02).        
                    11 PMDRXP1-DAY-SEAS-INCEPT        PIC X(02).        
                 09 PMDRXP1-RNTL-VAL-SEAS-EXPIR.                        
                    11 PMDRXP1-MONTH-SEAS-EXPIR       PIC X(02).        
                    11 PMDRXP1-DAY-SEAS-EXPIR         PIC X(02).        
                 09 PMDRXP1-GROUPI-EXCLUSIONS.                          
                    11 PMDRXP1-SPRINKLER-LEAKAGE      PIC X(01).        
                       88 PMDRXP1-SPRINKLER-EXCLUDED  VALUE 'Y'.        
                       88 PMDRXP1-SPRINKLER-INCLUDED  VALUE ' '.        
                    11 PMDRXP1-VANDALISM              PIC X(01).        
                       88 PMDRXP1-VANDALISM-EXCLUDED  VALUE 'Y'.        
                       88 PMDRXP1-VANDALISM-INCLUDED  VALUE ' ',        
                                             '1', '2', '3', '4'.        
                    11 FILLER                         PIC X(01).        
                 09 PMDRXP1-GROUPII-EXCLUSIONS.                         
                    11 PMDRXP1-WIND-AND-HAIL          PIC X(01).        
                       88 PMDRXP1-W-AND-H-EXCLUDED    VALUE 'Y'.        
                    11 FILLER                         PIC X(04).        
                 09 PMDRXP1-AGREED-VALUE-IND          PIC X(01).        
                    88 PMDRXP1-AGREED-VALUE           VALUE 'Y'.        
                 09 PMDRXP1-COVERAGE-CODES            PIC X(01).        
                    88 PMDRXP1-BROAD-FORM             VALUE 'B'.        
                    88 PMDRXP1-SPECIAL-FORM           VALUE 'S'.        
                 09 PMDRXP1-THEFT-EXCLUSION-IND       PIC X(01).        
                    88 PMDRXP1-THEFT-EXCLUDED         VALUE 'Y'.        
                 09 PMDRXP1-RATE-GROUP                PIC X(02).        
                 09 PMDRXP1-INFO-VARIABLE-BY-STATE    PIC X(21).        
                 09 PMDRXP1-STATEMENT-VALUES          PIC 9(10).        
                 09 PMDRXP1-ACQ-LOC-DOL-LIM           PIC 9(10).        
                 09 PMDRXP1-INFL-GD-PCT-ANN-INC-A.                      
                    11 PMDRXP1-INFL-GD-PCT-ANN-INC    PIC 9(02).        
                 09 PMDRXP1-TOT-CONTR-INS             PIC 9(10).        
                 09 PMDRXP1-REPL-COST-IND             PIC X(01).        
                    88 PMDRXP1-REPLACEMENT-COST       VALUE 'Y'.        
                 09 PMDRXP1-BUSINESS-INCOME-DATA.
                    11 PMDRXP1-ORD-PAYROLL-LIMIT-A.
                       13 PMDRXP1-ORD-PAYROLL-LIMITATION PIC 9(03).
                    11 PMDRXP1-EXT-EXP-COV-CODE       PIC X(01).
                    11 PMDRXP1-TYPE-OF-BUS-IND        PIC X(01).
                       88 PMDRXP1-MERCANTILE            VALUE '1'.
                       88 PMDRXP1-MANUFACTURING         VALUE '2'.
                       88 PMDRXP1-RENTAL-PROPERTIES     VALUE '3'.
                       88 PMDRXP1-MINING                VALUE '4'.
                       88 PMDRXP1-COMB-MERC-MFG         VALUE '5'.
                       88 PMDRXP1-COMB-MERC-RENT        VALUE '6'.
                       88 PMDRXP1-COMB-MFG-RENT         VALUE '7'.
                    11 PMDRXP1-MAX-PERIOD-INDEMNITY   PIC X(01).  
                       88 PMDRXP1-MAX-PER-IND-INCLUDED  VALUE 'Y'.
                    11 PMDRXP1-ORD-PAYROLL-EXCLUSION  PIC X(01).  
                       88 PMDRXP1-ORD-PAY-EXCL-INCLUDED VALUE 'Y'.
                    11 PMDRXP1-MONTHLY-LIMITATION     PIC X(03).  
                    11 PMDRXP1-EXT-PERIOD-INDEMNITY-A.            
                       13 PMDRXP1-EXT-PERIOD-INDEMNITY   PIC 9(03).
                    11 PMDRXP1-BLD-RISK-IND           PIC X(01).   
                       88 PMDRXP1-BLD-RISK-INCLUDED   VALUE 'Y'.   
                    11 PMDRXP1-DEPENDENT-PROP-IND     PIC X(01).   
                       88 PMDRXP1-BUSINC-DEP-PROP     VALUE 'Y'.   
                    11 FILLER                         PIC X(35).   
                 09 PMDRXP1-DEDUCTIBLE-CODE           PIC X(03).        
                    88  PMDRXP1-WH-PROCESSING         VALUE 'WH '.      
                    88  PMDRXP1-WHA-PROCESSING        VALUE 'WHA'.      
                    88  PMDRXP1-WHB-PROCESSING        VALUE 'WHB'.      
                    88  PMDRXP1-WHC-PROCESSING        VALUE 'WHC'.      
                    88  PMDRXP1-WHD-PROCESSING        VALUE 'WHD'.      
                    88  PMDRXP1-WHE-PROCESSING        VALUE 'WHE'.      
                    88  PMDRXP1-WH1-PROCESSING        VALUE 'WH1'.      
                    88  PMDRXP1-WH2-PROCESSING        VALUE 'WH2'.      
                    88  PMDRXP1-WH5-PROCESSING        VALUE 'WH5'.      
                    88  PMDRXP1-KS-WH-STAT-DED-CODE   VALUE 'FV2'.      
                    88  PMDRXP1-WH-STAT-DED-CODE      VALUE 'WH '       
                        'WH5'                                           
                        'WHA' 'WHB' 'WHC' 'WHD' 'WHE' 'WH1' 'WH2'.      
                    88  PMDRXP1-MINE-SUB-CODES        VALUE 'MS1'       
                        'MS2' 'MS3'.                                    
                 09 PMDRXP1-OTHER-DEDUCTIBLE          PIC X(07).        
                 09 PMDRXP1-TRAILER-CNTS-ADDL-PREM    PIC 9(03)V9(02).  
                 09 PMDRXP1-WH-DEDUCTIBLE-FACTOR      PIC 9(01)V9(03).  
                 09 PMDRXP1-DEC-CHANGE-FLAG           PIC X(01).        
                    88 PMDRXP1-DEC-CHANGE-NEEDED      VALUE 'A'.        
                    88 PMDRXP1-DEC-CHANGE-NOTED       VALUE 'B'.        
                 09 PMDRXP1-SPEC-FM-CLASS-LIMIT-A.                      
                    11 PMDRXP1-SPEC-FM-CLASS-LIMIT    PIC 9(06).        
                 09 PMDRXP1-SPEC-FM-CLS-IND           PIC X(01).        
                 09 PMDRXP1-RATE-GROUP-IND            PIC X(01).        
                 09 PMDRXP1-PMA-IND                   PIC X(01).        
                 09 PMDRXP1-RERATE-IM-IND             PIC X(01).        
                    88 PMDRXP1-RERATE-IM              VALUE 'Y'.        
                 09 PMDRXP1-BUSNC-EXC-EXP-IND         PIC X(01).        
                 09 PMDRXP1-BLRSK-EXC-COLLP-IND       PIC X(01).        
                 09 PMDRXP1-SUB-OF-INS                PIC X(06).        
                    88 PMDRXP1-BLDG-SUB-OF-INS        VALUE 'BLDG  '.   
                    88 PMDRXP1-CONTS-SUB-OF-INS       VALUE 'CONTS '.   
                    88 PMDRXP1-STOCK-SUB-OF-INS       VALUE 'STOCK '.   
                    88 PMDRXP1-BLRSK-SUB-OF-INS       VALUE 'BLRSK '.   
                 09 PMDRXP1-MINE-SUB-LIMIT            PIC 9(10).        
                 09 PMDRXP1-POLICY-EXEMPT-IND         PIC X(01).        
                    88 PMDRXP1-POLICY-EXEMPT          VALUE 'Y'.        
                    88 PMDRXP1-POLICY-NOT-EXEMPT      VALUE 'N'.        
                 09 PMDRXP1-TENANT-IMPRVMNT-IND       PIC X(01).        
                    88 PMDRXP1-TEN-IMPR-CONTS         VALUE 'Y'.        
                 09 PMDRXP1-SPRINK-LEAK-EXC-FCTR      PIC 9(01)V9(04).  
                 09 PMDRXP1-PMSC-FUTURE-USE           PIC X(44).        
                 09 PMDRXP1-TX-EC-TERR                PIC X(03).        
                 09 PMDRXP1-TX-PLACE-CODE             PIC X(05).        
                 09 PMDRXP1-KEY-CONT-CLASS            PIC X(04).        
                    88 PMDRXP1-TX-APARTMENT-CLASS                       
                         VALUE '0311' '0312' '0313'                     
                               '0321' '0322' '0323'                     
                               '0074' '0075' '0076' '0196' '0197'       
                               '0310' '0320' '0330' '0360' '0370'       
                               '0380' '0390' '0070'                     
                               '0198' '0199'.                           
                    88 PMDRXP1-TX-OFFICE-CLASS     VALUE '0701' '0702'  
                                                      '0700'.           
                    88 PMDRXP1-TEX-CONT-MISC-CLASS  VALUE               
                         '1185' '1190'.                                 
                 09 PMDRXP1-TX-COUNTY                 PIC X(03).        
                 09 PMDRXP1-STAT-CLASS-CODE           PIC 9(05).        
                 09 PMDRXP1-CUSTOMER-FUTURE-USE       PIC X(01).        
                 09 PMDRXP1-TEXAS-TOWN.                           
                    11 PMDRXP1-TX-TOWN                PIC X(4).   
                 09 PMDRXP1-YR2000-CUST-USE           PIC X(096). 
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).