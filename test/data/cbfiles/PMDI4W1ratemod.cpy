        01 PMDI4W1-STATE-RATING-RECORD.
        03 PMDI4W1-STATE-RATING-REC.                                    
           05  PMDI4W1-SEGMENT-KEY.                                     
               07  PMDI4W1-SEGMENT-ID                   PIC X(02).      
                   88  PMDI4W1-SEGMENT-43                 VALUE '43'.   
               07  PMDI4W1-SEGMENT-STATUS               PIC X(01).      
                   88  PMDI4W1-ACTIVE-STATUS              VALUE 'A'.    
                   88  PMDI4W1-DELETED-STATUS             VALUE 'D'.    
                   88  PMDI4W1-HISTORY-STATUS             VALUE 'H'.    
                   88  PMDI4W1-PRORATA-DELETED            VALUE 'P'.    
               07  PMDI4W1-TRANSACTION-DATE             PIC 9(08).                            
               07  PMDI4W1-SEGMENT-ID-KEY.                              
                   09  PMDI4W1-SEGMENT-LEVEL-CODE       PIC X(01).      
                       88  PMDI4W1-SEGMENT-LEVEL-I        VALUE 'I'.    
                   09  PMDI4W1-SEGMENT-PART-CODE        PIC X(01).      
                       88  PMDI4W1-RATING-RECORD          VALUE 'X'.    
                       88  PMDI4W1-AUDIT-RECORD           VALUE 'Z'.    
                   09  PMDI4W1-SUB-PART-CODE            PIC X(01).      
                   09  PMDI4W1-INSURANCE-LINE           PIC X(02).      
                       88  PMDI4W1-WORKERS-COMPENSATION   VALUE 'WC'.   
               07  PMDI4W1-LEVEL-KEY.                                   
                   09  PMDI4W1-WC-RATING-STATE          PIC X(02).      
                   09  PMDI4W1-LOCATION-NUMBER          PIC X(04).      
                       88  PMDI4W1-STATE-LEVEL-RECORD     VALUE '0000'. 
                   09  FILLER                           PIC X(03).      
                   09  PMDI4W1-RISK-UNIT-GROUP.                         
                       11  PMDI4W1-CLASS-ORDER-CODE     PIC X(04).      
                           88  PMDI4W1-NON-STAT-INFO-REC  VALUE '0000'. 
                           88  PMDI4W1-EXPERIENCE-MOD-REC VALUE '0010'. 
                           88  PMDI4W1-SCHEDULE-MOD-REC   VALUE '0020'  
                                                                '0380'. 
                           88  PMDI4W1-MED-DEDUCT-REC     VALUE '0030'. 
                           88  PMDI4W1-COINS-CRED-REC     VALUE '0040'. 
                           88  PMDI4W1-CONTRCT-CRED-REC   VALUE '0050'. 
                           88  PMDI4W1-MERIT-RATE-REC     VALUE '0060'. 
                           88  PMDI4W1-ADDNL-MED-REC      VALUE '0070'. 
                           88  PMDI4W1-COVII-MISC-REC     VALUE '0080'. 
                           88  PMDI4W1-RATE-DEV-REC       VALUE '0090'. 
                           88  PMDI4W1-INC-COVII-STD-REC  VALUE '0100'. 
                           88  PMDI4W1-COVII-STD-MP-REC   VALUE '0110'. 
                           88  PMDI4W1-LOSS-CONST-REC     VALUE '0120'. 
                           88  PMDI4W1-EXPENSE-CONST-REC  VALUE '0130'. 
                           88  PMDI4W1-WAIVER-REC         VALUE '0140'. 
                           88  PMDI4W1-SURCHARGE-REC      VALUE '0150'. 
                           88  PMDI4W1-PREM-DISC-REC      VALUE '0160'. 
                           88  PMDI4W1-SHORT-RATE-PENALTY VALUE '0170'. 
                           88  PMDI4W1-BAL-TO-MP-REC      VALUE '0180'. 
                           88  PMDI4W1-FRA-REC            VALUE '0190'  
                                                                '0191'  
                                                                '0192'  
                                                                '0193'  
                                                                '0194'. 
                           88  PMDI4W1-FRA-1-REC          VALUE '0190'. 
                           88  PMDI4W1-FRA-2-REC          VALUE '0191'. 
                           88  PMDI4W1-FRA-3-REC          VALUE '0192'. 
                           88  PMDI4W1-FRA-4-REC          VALUE '0193'. 
                           88  PMDI4W1-FRA-5-REC          VALUE '0194'. 
                           88  PMDI4W1-INC-COVII-ADM-REC  VALUE '0200'. 
                           88  PMDI4W1-COVII-ADM-MP-REC   VALUE '0210'. 
                           88  PMDI4W1-INC-COVII-VOL-REC  VALUE '0220'. 
                           88  PMDI4W1-AR-SURCHARGE-REC   VALUE '0230'. 
                           88  PMDI4W1-UNINS-SUB-REC      VALUE '0240'. 
                           88  PMDI4W1-TAX-ASSESS-REC-1   VALUE '0250'. 
                           88  PMDI4W1-TAX-ASSESS-REC-3   VALUE '0255'. 
                           88  PMDI4W1-TAX-ASSESS-REC-2   VALUE '0260'. 
                           88  PMDI4W1-TAX-ASSESS-RECORD                
                                   VALUE '0250' '0255' '0260'.          
                           88  PMDI4W1-USLH-MIN-PREM      VALUE '0270'. 
                           88  PMDI4W1-MISC-CLASS-REC     VALUE '0280'. 
                           88  PMDI4W1-SAFE-PROG-REC      VALUE '0290'  
                                                                '0400'. 
                           88  PMDI4W1-FLA-FLAT-FEE-REC   VALUE '0300'. 
                           88  PMDI4W1-FS-CLASS-ORDR-CDE  VALUE '0320'. 
                           88  PMDI4W1-DED-COINS-REC      VALUE '0330'. 
                           88  PMDI4W1-ASS-RSK-ADJUST-REC VALUE '0340'. 
                           88  PMDI4W1-DRUG-FREE-CRED-REC VALUE '0350'. 
                           88  PMDI4W1-VAR-DEDUCT-REC     VALUE '0360'. 
                           88  PMDI4W1-RISK-MGMT-REC      VALUE '0370'. 
                           88  PMDI4W1-DES-MED-PROV-REC   VALUE '0380'. 
                           88  PMDI4W1-MCO-CR-REC         VALUE '0390'. 
                           88  PMDI4W1-SAFETY-DEBIT-REC   VALUE '0400'. 
                           88  PMDI4W1-VALID-CLSS-ORDR-PERIOD  VALUE    
                               '0110' '0120' '0130' '0160' '0170'       
                               '0300'                                   
                               '0320'                                   
                               '0180' '0210' '0230'.                    
                           88  PMDI4W1-VALID-CLASS-ORDER-CODE VALUE     
                               '0000' '0010' '0020' '0030' '0040'       
                               '0050' '0060' '0070' '0080' '0090'       
                               '0100' '0110' '0120' '0130' '0140'       
                               '0150' '0160' '0170' '0180' '0190'       
                               '0200' '0210' '0220' '0230' '0240'       
                               '0250' '0260' '0270' '0280' '0290'       
                               '0330' '0340' '0350' '0360' '0370'       
                               '0380' '0390' '0400' '0255'.             
                           88  PMDI4W1-VALID-CLASS-ORDER-ST   VALUE     
                               '0000' '0010' '0020' '0030' '0040'       
                               '0050' '0060' '0070' '0080' '0090'       
                               '0100' '0140' '0150' '0190' '0200'       
                               '0220' '0240' '0250' '0260' '0270'       
                               '0280' '0290' '0340' '0360' '0370'       
                               '0380' '0390' '0400' '0255'.             
                           88  PMDI4W1-ENTER-REPRTNG-CLSS-CDE VALUE     
                               '0010' '0050' '0070' '0110' '0120'       
                               '0130' '0140' '0150' '0160' '0170'       
                               '0180' '0210' '0230' '0240' '0250'       
                               '0260' '0270' '0340' '0350' '0400'       
                               '0255'.                                  
                           88  PMDI4W1-COVII-MODIFIER         VALUE     
                               '0080' '0100' '0200' '0220'.             
                       11  PMDI4W1-CLASS-ORDER-SEQ      PIC X(02).      

                   09  PMDI4W1-RISK-UNIT.                               
                       11  PMDI4W1-REPORTING-CLASS-CODE PIC X(04).      
                       11  PMDI4W1-REPORTING-CLASS-SEQ  PIC X(02).      
                   09  PMDI4W1-SPLIT-RATE-SEQ           PIC X(02).      
                       88  PMDI4W1-SPLIT-RATE-SEQ-00      VALUE '00'.   
                       88  PMDI4W1-SPLIT-RATE-SEQ-01      VALUE '01'.   
                       88  PMDI4W1-SPLIT-RATE-SEQ-02      VALUE '02'.   
               07  PMDI4W1-ITEM-EFFECTIVE-DATE          PIC 9(08).                         
               07  PMDI4W1-VARIABLE-KEY.                                
                   09  PMDI4W1-AUDIT-NUMBER             PIC 9(02).      
                   09  PMDI4W1-AUDIT-NUM-SEQ            PIC 9(01).      
                   09  FILLER                           PIC X(03).      
               07  PMDI4W1-PROCESS-DATE                 PIC 9(08).                                
           05  PMDI4W1-SEGMENT-DATA.                                    
               07  PMDI4W1-ITEM-EXPIRE-DATE             PIC 9(08).                            
               07  PMDI4W1-RATING-MODIFIER-DATA.                                
                   09  PMDI4W1-GENERATED-SEG-IND        PIC X(01).              
                       88 PMDI4W1-SEG-IND-APPLICABLE     VALUE 'M' 'G'.         
                       88 PMDI4W1-SEG-IND-NOT-APPLICABLE VALUE ' '.             
                       88 PMDI4W1-SEG-GENERATED          VALUE 'G'.             
                       88 PMDI4W1-SEG-MANUALLY-ENTERED   VALUE 'M'.             
                   09  PMDI4W1-MODIFIER-DESC            PIC X(10).              
                       88 PMDI4W1-MN-INDEMNITY-DED       VALUE                  
                                                         'IDEDUCT   '.          
                       88 PMDI4W1-MN-MEDICAL-DED         VALUE                  
                                                         'MDEDUCT   '.          
                       88 PMDI4W1-NH-PER-ACC-DIS-DED     VALUE                  
                                                         'ADEDUCT   '.          
                   09  PMDI4W1-MODIFIER-RATE          PIC 9(01)V9(04).          
                   09  PMDI4W1-MOD-FACTOR-MG-IND        PIC X(01).              
                       88  PMDI4W1-MOD-FCTR-NOT-ENTERED   VALUE ' '.            
                       88  PMDI4W1-MOD-FCTR-MANUAL-ENTRD  VALUE 'M'.            
                       88  PMDI4W1-MOD-FCTR-GENERATED     VALUE 'G'.            
                   09  PMDI4W1-MODIFIER-PREMIUM         PIC S9(09).             
                   09  PMDI4W1-MOD-PREM-MG-IND          PIC X(01).              
                       88  PMDI4W1-MOD-PREM-NOT-ENTERED   VALUE ' '.            
                       88  PMDI4W1-MOD-PREM-MANUAL-ENTRD  VALUE 'M'.            
                       88  PMDI4W1-MOD-PREM-GENERATED     VALUE 'G'.            
                   09  PMDI4W1-MODIFIER-TYPE.                                   
                       11  PMDI4W1-MODIFIER-TYPE-1      PIC X(01).              
                           88  PMDI4W1-MOD-TYPE-MISC-C    VALUE 'C'.            
                           88  PMDI4W1-MOD-TYPE-MISC-D    VALUE 'D'.            
                           88  PMDI4W1-MOD-TYPE-AMPF-U    VALUE 'U'.            
                           88  PMDI4W1-MOD-TYPE-AMPS-S    VALUE 'S'.            
                           88  PMDI4W1-MOD-TYPE-EMOD-F    VALUE 'F'.            
                           88  PMDI4W1-MOD-TYPE-EMOD-T    VALUE 'T'.            
                           88  PMDI4W1-MOD-TYPE-ARAP-F    VALUE 'F'.            
                           88  PMDI4W1-MOD-TYPE-ARAP-T    VALUE 'T'.            
                           88  PMDI4W1-MOD-TYPE-FRA-S     VALUE 'S'.            
                           88  PMDI4W1-MOD-TYPE-FRA-F     VALUE 'F'.            
                           88  PMDI4W1-MOD-TYPE-FRA-B     VALUE 'B'.            
                           88  PMDI4W1-MOD-TYPE-MDP-A     VALUE 'A'.            
                           88  PMDI4W1-MOD-TYPE-MDP-B     VALUE 'B'.            
                           88  PMDI4W1-MOD-TYPE-MRP-C     VALUE 'C'.            
                           88  PMDI4W1-MOD-TYPE-MRP-D     VALUE 'D'.            
                           88  PMDI4W1-MOD-TYPE-AMPB      VALUE 'B'.            
                           88  PMDI4W1-MOD-TYPE-SCP       VALUE ' '.            
                           88  PMDI4W1-MOD-TYPE-RDAP-A    VALUE 'A'.            
                           88  PMDI4W1-MOD-TYPE-RDBP-B    VALUE 'B'.            
                           88  PMDI4W1-MOD-TYPE-SRCH-SPECIFIC                   
                                                          VALUE 'S'.            
                           88  PMDI4W1-MOD-TYPE-SRCH-BLANKET                    
                                                          VALUE 'B'.            
                           88  PMDI4W1-MOD-TYPE-WVP-B     VALUE 'B'.            
                           88  PMDI4W1-MOD-TYPE-WVP-S     VALUE 'S'.            
                           88  PMDI4W1-MOD-TYPE-PD-DEFER  VALUE 'D'.            
                           88  PMDI4W1-MOD-TYPE-RSK-MGT-Y VALUE 'Y'.            
                           88  PMDI4W1-MOD-TYPE-RSK-MGT-N VALUE 'N'.            
                       11  PMDI4W1-MODIFIER-TYPE-2      PIC X(01).              
                           88  PMDI4W1-MOD-TYPE-EMOD-M    VALUE 'M'.            
                           88  PMDI4W1-MOD-TYPE-FRA       VALUE ' '.            
                           88  PMDI4W1-MOD-TYPE-FRA-HOLD-FA                     
                                                          VALUE 'F'.            
                           88  PMDI4W1-MOD-TYPE-WVP-A-Z   VALUE 'A'             
                                                           THRU 'Z'.            
                           88  PMDI4W1-MOD-TYPE-MISC-A    VALUE 'A'.            
                           88  PMDI4W1-MOD-TYPE-MISC-B    VALUE 'B'.            
                           88  PMDI4W1-MOD-TYPE-NH-DED-A  VALUE 'A'.            
                           88  PMDI4W1-MOD-TYPE-MN-DED-C  VALUE ' '.            
                           88  PMDI4W1-MOD-TYPE-MN-DED-I  VALUE 'I'.            
                           88  PMDI4W1-MOD-TYPE-MN-DED-M  VALUE 'M'.            
                           88  PMDI4W1-MOD-TYPE-VAR-DED-N VALUE 'N'.            
                           88  PMDI4W1-MOD-TYPE-LRG-DED-L VALUE 'L'.            
                   09  PMDI4W1-MODIFIER-PREM-BASIS      PIC 9(06).              
                   09  PMDI4W1-MOD-BASIS-MG-IND         PIC X(01).              
                       88  PMDI4W1-MOD-BASIS-NOT-ENTERED   VALUE ' '.           
                       88  PMDI4W1-MOD-BASIS-MANUAL-ENTRD  VALUE 'M'.           
                       88  PMDI4W1-MOD-BASIS-GENERATED     VALUE 'G'.           
                   09  PMDI4W1-MODIFIER-MIN-PREM        PIC 9(05).              
                   09  PMDI4W1-MOD-MIN-PREM-MG-IND      PIC X(01).              
                       88  PMDI4W1-MOD-MIN-PRM-NOT-ENTRD  VALUE ' '.            
                       88  PMDI4W1-MOD-MIN-PRM-MAN-ENTRD  VALUE 'M'.            
                       88  PMDI4W1-MOD-MIN-PRM-GENERATED  VALUE 'G'.            
                   09  PMDI4W1-DEPOSIT-PREMIUM          PIC S9(10).             
                   09  PMDI4W1-MODIFIER-USLH-FACTOR     PIC 9(01)V9(04).
                   09  PMDI4W1-AUDIT-SEG-BUILT-IND      PIC X(01).              
                       88 PMDI4W1-AUDIT-SEGMENT-BUILT   VALUE 'B'.              
                       88 PMDI4W1-AUDIT-SEGMENT-ENTERED VALUE 'E'.              
                   09  PMDI4W1-RATING-EXPIRE-DATE       PIC 9(08).                              
                   09  PMDI4W1-COVII-LIMIT-ID           PIC X(02).              
                   09  PMDI4W1-MOD-ANNIV-RATE-DATE      PIC 9(08).                             
                   09  PMDI4W1-VAR-DEDUCTIBLE-AMT       PIC 9(07).              
                   09  PMDI4W1-VAR-AGGR-DEDUCT-AMT      PIC 9(06).              
                   09  PMDI4W1-CA-EMOD-AT-INCEPTION    PIC 9(01)V9(04).         
                   09  PMDI4W1-PMS-FUTURE-USE-2         PIC X(161).             
                   09  PMDI4W1-CUSTOMER-FUTURE-USE-2    PIC X(28).              
                   09  PMDI4W1-YR2000-CUST-USE-2        PIC X(100).             
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).