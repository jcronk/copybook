        01 LOSS-RECORD.
           03 LOSS-TRANSACTION-SEG.                                    
           05 LOSS-ID                 PIC X(02).                       
              88  45-94-RECORD   VALUE '45' THRU '94'.                 
           05 LOSS-MAJOR-KEY.                                          
              07 LOSS-INSURANCE-LINE     PIC X(02).                    
              07 LOSS-LOCATION-NUMBER-A.                               
                 09 LOSS-LOCATION-NUMBER  PIC 9(04).                   
              07 LOSS-SUB-LOCATION-NUMBER-A.                           
                 09 LOSS-SUB-LOCATION-NUMBER  PIC 9(03).               
              07 LOSS-RISK-UNIT-GROUP-KEY.                             
                 09 LOSS-RISK-UNIT-GROUP   PIC X(03).                  
                 09 LOSS-SEQ-RSK-UNT-GRP.                              
                    11 LOSS-CLASS-CODE-GROUP  PIC 9(02).               
                    11 LOSS-CLASS-CODE-MEMBER PIC 9(01).               
              07 LOSS-CHARGEABLE-KEY.                                  
                 09 LOSS-RISK-UNIT-KEY.                                
                    11 LOSS-RISK-UNIT.                                 
                       13 LOSS-UNIT            PIC X(03).              
                       13 FILLER               PIC X(03).              
                    11 LOSS-SEQUENCE-RISK-UNIT PIC X(02).              
                 09 LOSS-TYPE-EXPOSURE   PIC X(03).                    
                 09 LOSS-MAJOR-PERIL     PIC X(03).                    
                    88 LOSS-BODILY-INJURY VALUE '084'.                 
                    88 LOSS-PROPERTY-DAMAGE VALUE '085'.               
                    88 LOSS-SERV-CHARG-MAJ-PERIL VALUE '088'.          
                    88 LOSS-TAXES-MAJ-PERIL VALUE '089'.               
                    88 LOSS-SERV-CHG-OR-TAXES-MP VALUE '088' '089'.    
                    88 LOSS-SINGLE-LIMITS VALUE '100' '115' '118'.     
                    88 QUEBEC-CLMS-TRACKING-MP VALUE '605' '606' '607'.
                 09 LOSS-MAJOR-PERIL-SEQ PIC X(02).                    
                 09 LOSS-COV-EFF-DATE    PIC X(08).                    
                 09 LOSS-PART            PIC X(01).                    
                    88 PART7-8           VALUE '7' '8'.                
                    88 LOSS-CLAIMS       VALUE '7' '8'.                
                    88 LOSS-NON-REINSURANCE VALUE '7'.                 
                    88 LOSS-REINS-ONLY   VALUE '8'.                    
                 09 LOSS-DATE            PIC 9(08).                                         
              07 LOSS-VARIABLE-KEY.                                    
                 09 LOSS-OCCURENCE       PIC 9(03).                    
                 09 LOSS-CLAIMANT           PIC 9(03).                 
                 09 LOSS-DETAIL-KEY.                                   
                    11 LOSS-MEMBER             PIC X(02).              
                    11 LOSS-DISABILITY         PIC X(02).              
                    11 LOSS-RESERVE-CATEGORY   PIC X(01).              
                 09 LOSS-REINS-KEY.                                    
                    11 LOSS-LAYER        PIC 9(02).                    
                    11 LOSS-REINS-KEY-ID       PIC X(01).              
                    11 LOSS-REINS-TREATY.                              
                       13 LOSS-REINS-CO-NO PIC X(04).                  
                             88 LOSS-NO-REINS-COMPANY VALUE SPACES.    
                       13 LOSS-REINS-BROKER    PIC 9(04).              
                 09 LOSS-BASE-TRANSACTION   PIC 9(02).                 
                 09 LOSS-TRANSACTION  PIC X(02).                       
                       88 NON-REQUIRED-TRANS VALUE '43' '65' '66' '91'.
                       88 FINAL-PAYMENT  VALUE '22' '27' '40' '41' '42'
                             '83' '84'.                                
                       88 SUBROGATION-DISTRIBUTED VALUE '86'.          
                       88 MRB-REPORT-LOSS-TRANSACTION VALUE '21' '22'  
                          '23' '24' '26' '27' '28' '29' '81' '82' '83' 
                          '84' '88' '89'.                              
                       88 SUBROGATION-DISTRIB-REVERSAL VALUE '87'.     
                       88 NON-REGULAR-FINAL-PAYMENT VALUE '27' '40'    
                                                          '83' '84'.   
                       88 FINAL-SUBROGATION VALUE '84'.                
                       88 FINAL-SALVAGE  VALUE '83'.                   
                       88 LOSS-RECOVERY-REVERSAL VALUE '26'.           
                       88 CLOSE-EXPENSE  VALUE '40'.                   
                       88 FINAL-RECOVERY VALUE '27'.                   
                       88 FINAL-REGULAR  VALUE '22' '41' '42'.         
                       88 RESERVE-TRANSACTION VALUE '90' '92' '95'     
                             '97' '98' '99'.                           
                       88 RESERVE-CHANGE VALUE '65'.                   
                       88 RECOVERY-RESERVE VALUE '97'.                 
                       88 EXPENSE-RECOVERY-IND VALUE '74'.             
                       88 EXPENSE-RECOVERY-LEGAL VALUE '75'.           
                       88 EXPENSE-RECOVERY-REVERSAL VALUE '76'.        
                       88 EXPENSE-RESERVE VALUE '95'.                  
                       88 SALVAGE-RESERVE VALUE '98'.                  
                       88 SUBROGATION-RESERVE VALUE '99'.              
                       88 REGULAR-RESERVE VALUE '90' '92'.             
                       88 LOSS-FINAL-PAYMENT VALUE '22'.               
                       88 OPEN-CLAIM     VALUE '90'.                   
                       88 REOPEN-HISTORY VALUE '92'.                   
                       88 RESERVE-REOPEN VALUE '66'.                   
                       88 NON-REGULAR-RESERVE VALUE '95' '97' '98' '99'
                       88 PARTIAL-PYMT-OPEN-CLAIM VALUE '21'.          
                       88 PARTIAL-PAYMENT VALUE '21' '25' '37' '28'    
                             '29' '71' '72' '73' '77' '78' '79' '81'   
                             '82' '88' '89'.                           
                       88 PARTIAL-SALVAGE VALUE '81' '88'.             
                       88 PARTIAL-SUBROGATION VALUE '82' '89'.         
                       88 PARTIAL-REGULAR VALUE '21' '25' '28' '71'    
                             '72' '73' '77' '78' '79'.                 
                       88 PARTIAL-EXPENSE VALUE '71' '72' '73' '77'    
                             '78' '79'.                                
                       88 EXPENSE-TRANSACTION VALUE '71' '72' '73'     
                             '74' '75' '76' '77' '78' '79'.            
                       88 PAYMENT-NO-RESERVE VALUE '23' '24'.          
                       88 LOSS-PAYMENT   VALUE '21' '22' '23' '24'     
                             '25' '28' '29'.                           
                       88 PARTIAL-LOSS-PAYMENT VALUE '29'.             
                       88 PARTIAL-RECOVERY VALUE '37' '26'.            
                       88 CLOSED-CLAIM   VALUE '91'.                   
                       88 NOTICE-ONLY    VALUE '43'.                   
                       88 CLOSING-NO-CLAIM VALUE '41'.                 
                       88 RESERVE-REQUIRED VALUE '21' '22' '25' '28'   
                             '41' '42'.                                
                       88 RESERVE-REQ-EXCL-41 VALUE '21' '22' '25' '28'
                             '42'.                                     
                       88 LOSS-SINGLE-PMT-NO-PREV-RESV VALUE '23'.     
                       88 REGULAR-PAYMENT-TRANS VALUE '21' '22' '23'   
                             '24' '25' '26' '27' '28'.                 
                       88 LOSS-FIRST-FINAL VALUE '23'.                 
                       88 LOSS-SUPPLEMENTAL VALUE '24'.                
                       88 FIRST-PARTIAL-CANADA VALUE '25'.             
                       88 LOSS-CLOSE-NO-CLAIM VALUE '41'.              
                       88 LOSS-CLOSE-WITH-CLAIM VALUE '42'.            
                       88 LOSS-UPDATE-VALID VALUE '22' '27' '28' '41'  
                             '42' '83' '84' '90' '92' '97' '98' '99'.  
                       88 CF-PAYMENT-RECORDS-ALL VALUE '21' '22' '23'  
                        '24' '25' '26' '27' '28' '29' '37' '40' '41'   
                        '42' '71' '72' '73' '74' '75' '76' '77' '78'   
                        '79' '81' '82' '83'                            
                             '84' '86' '87' '88' '89'.                 
                       88 CF-PAYMENT-ADJ-RESERVE VALUE '21' '22' '25'  
                        '28' '41' '42'.                                
                       88 DUPLICATE-ADD-LOSS-TRN-ALLOWED VALUE '21' '24
                             '26' '27' '28' '29' '37' '40' '71' '72'   
                             '73' '74' '75' '76' '77' '78' '79'        
                             '81' '82' '86' '87' '88' '89'.            
                     88 TRANSACTION-FOR-EXCESS-CLAIMS VALUE '21' '22'  
                        '23' '24' '25' '28' '29' '90' '92'.            
                     88 UPDATE-CLAIM-TRANS-ALLOWED VALUE '21' '22' '95'
                      '23' '24' '27' '28' '29' '43' '83' '84' '97' '98'
                      '99'.                                            
                     88 CANADA-CX-RESERVES VALUE '40' '41' '65' '66'   
                            '90' '95' '97' '98' '99'.                  
                     88 QUEBEC-CX-FINAL-PAY VALUE '22' '23' '42'.      
                     88 LOSS-RESERVE        VALUE '90' '91' '92'.      
                     88 LOSS-PAY      VALUE '21' '22' '23' '24' '25'   
                        '28' '29' '86' '87'.                           
                     88 LOSS-SALVAGE      VALUE '81' '83' '88'.        
                     88 LOSS-SUBROGATION  VALUE '82' '84' '89'.        
                     88 LOSS-RECOVERY     VALUE '26' '27' '37'.        
                     88 EXPENSE-RECOVERY  VALUE '74' '75' '76'.        
                 09 LOSS-DRAFT-CONTROL-SEQ  PIC 9(02).                 
              07 LOSS-SUB-PART-CODE   PIC X(01).                       
           05 LOSS-SEGMENT-DATA.                                       
              07 LOSS-SEGMENT-STATUS     PIC X(01).                    
              07 LOSS-ENTRY-OPERATOR     PIC X(03).                    
              07 LOSS-TRANSACTION-CATEGORY PIC X(02).                  
                 88 WORKERS-COMP-DEDUCTIBLE VALUE 'WD'.                
              07 LOSS-DATE-REPORTED         PIC X(08).                                   
              07 LOSS-CAUSE              PIC X(02).                    
                 88 CANADA-CAUSE-OF-LOSS-BI VALUE '03' '09' '10' '55'  
                    '59' '61' '64' '67' '74' '90'.                     
                 88 CANADA-CAUSE-OF-LOSS-PD VALUE '04' '10' '12' '39'  
                    '52' '53' '56' '57' '60' '62' '63' '71' '72' '76'  
                    '90' '91' '94' '97'.                               
                 88 CANADA-CAUSE-OF-LOSS-COLL VALUE '15' '57' '59' '96'
              07 LOSS-ADJUSTOR-NUMBER.                                 
                 09 LOSS-ADJUSTOR-NO        PIC X(03).                 
                 09 FILLER                  PIC X(01).                 
              07 LOSS-EXAMINER           PIC X(04).                    
              07 LOSS-OTHER-ASSIGNMENT   PIC X(04).                    
              07 LOSS-PAID-OR-RESV-AMT   PIC S9(10)V9(02) COMP-3.      
              07 LOSS-BANK-NUMBER        PIC X(02).                    
              07 LOSS-DRAFT-AMOUNT       PIC S9(10)V9(02) COMP-3.      
              07 LOSS-DRAFT-NO           PIC X(10).                    
              07 LOSS-DRAFT-CHECK-IND    PIC X(01).                    
              07 LOSS-DATE-OF-DRAFT      PIC X(08).                                   
                 88 NO-DATE-OF-DRAFT     VALUE '00000000' '        '   
                       '********'.                                     
              07 LOSS-DRAFT-PAY-TO.                                    
                 09 LOSS-DRAFT-PAY-TO-1   PIC X(01).                   
                 09 LOSS-DRAFT-PAY-TO-2   PIC X(01).                   
                 09 LOSS-DRAFT-PAY-TO-3   PIC X(01).                   
              07 LOSS-DRAFT-MAIL-TO      PIC X(01).                    
              07 LOSS-ORIGINAL-RESERVE   PIC S9(10)V9(02) COMP-3.      
              07 LOSS-ACCOUNT-ENTERED-DATE PIC X(06).                  
              07 LOSS-AVERAGE-RESERVE-CODE PIC X(01).                  
                 88 LOSS-AVERAGE-RESERVE VALUE '1'.                    
                 88 LOSS-CASE-RESERVE    VALUE '0'.                    
              07 LOSS-BRANCH             PIC X(03).                    
              07 LOSS-WORKERS-COMP.                                    
                 88 LOSS-NO-INDEMNITY-INFO VALUE SPACES.               
                 10 LOSS-IND-DRAFT-DATE  PIC X(08).                               
                    88 LOSS-NO-IND-DRAFT-DATE VALUE '        '         
                                                    '00000000'.        
                 10 LOSS-WEEKLY-WAGE     PIC X(06).                    
                 10 LOSS-PAYMENT-RATE    PIC X(06).                    
                    88 LOSS-NO-WEEKLY-RATE VALUE '      ' '000000'.    
                 10 LOSS-FREQUENCY       PIC X(01).                    
                    88 LOSS-FREQ-IS-MONTHLY-CAL VALUE 'C'.             
                    88 LOSS-FREQ-IS-MONTHLY-28 VALUE 'M'.              
                    88 LOSS-FREQ-IS-BIWEEKLY VALUE 'B'.                
                    88 LOSS-FREQ-IS-WEEKLY VALUE 'W'.                  
                 10 LOSS-PERIOD-PAY      PIC X(03).                    
                    88 LOSS-NEXT-PAYMENTS-IS-FINAL VALUE '001'.        
                    88 LOSS-NO-PAYMENTS-LEFT VALUE '000' '   '.        
                 10 LOSS-INDUSTRIAL-COMM PIC X(10).                    
                 10 LOSS-PAYEE-PHRASE    PIC X(02).                    
                 10 LOSS-MEMO-PHRASE     PIC X(02).                    
                 10 IWS-ORIGIN-INDICATOR PIC X(01).                    
              07 LOSS-AIA-CODES.                                       
                 10 LOSS-AIA-CODES-1-2   PIC X(02).                    
                 10 LOSS-AIA-CODES-3-4   PIC X(02).                    
                 10 LOSS-AIA-CODES-5-6   PIC X(02).                    
              07 LOSS-AIA-SUB-CODE       PIC X(02).                    
              07 LOSS-ACCIDENT-STATE     PIC X(02).                    
              07 LOSS-HANDLING-BRANCH    PIC X(02).                    
              07 LOSS-1099-NUMBER        PIC X(10).                    
              07 LOSS-CLAIM-PAYEE        PIC 9(03).                    
              07 LOSS-CLAIM-PAYEE-NAME   PIC X(30).                    
              07 LOSS-CESSION-GROUP.                                   
                 09 LOSS-CESSION         PIC X(10).                    
                 09 FILLER               PIC X(08).                    
              07 LOSS-CLAIM-GROUP.                                     
                 09 LOSS-CLAIM-NUMBER       PIC X(12).                 
                 09 LOSS-TYPE-CLAIM-PAYEE   PIC X(02).                 
              07 LOSS-SPECIAL-USE-1      PIC X(08).                    
              07 LOSS-SPECIAL-USE-2      PIC X(02).                    
              07 LOSS-REP-CLMNT          PIC X(01).                    
              07 LOSS-TIME.                                            
                 09 LOSS-2-POS           PIC X(02).                    
                 09 FILLER               PIC X(02).                    
              07 LOSS-TYPE-DISABILITY    PIC X(02).                    
              07 LOSS-CLAIMS-MADE-IND    PIC X(01).                    
              07 LOSS-MISC-ADJUSTOR-IND  PIC X(01).                    
              07 LOSS-PMS-FUTURE-USE     PIC X(40).                    
              07 LOSS-OFFSET-ONSET-IND   PIC X(01).                    
                 88  LOSS-OFFSET-ONSET-RECORD VALUE 'O' 'N'.           
                 88  LOSS-OFFSET-RECORD       VALUE 'O'.               
                 88  LOSS-ONSET-RECORD        VALUE 'N'.               
              07 LOSS-PIF-DATE           PIC X(08).                    
              07 LOSS-S3P-CLAIM-NBR      PIC X(20).                    
              07 LOSS-YR2000-CUST-USE    PIC X(72).                    
              07 BLANK-OUT               PIC X(03).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).