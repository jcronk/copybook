        01 STAT-ACCOUNT-RECORD.                                         
           03 STAT-ACCOUNT-RECORD-SEG.                                 
           05 SAR-SEQUENCE-KEY.                                        
              10 SAR-ID                  PIC X(02).                    
                 88 STAT-ACCT-LOSS-REC   VALUE '45' THRU '94'.         
                 88 SAR-COMBINED-AC-PREMIUMS VALUE '91'.               
                 88 SAR-UPRATE-SEGMENT   VALUE '93'.                   
                 88 SERVICE-CHARGE-SEGMENT VALUE '94'.                 
                 88 SAR-AUTO-STAT-ACCT-REC VALUE '45'.                 
                 88 SAR-WC-TAX-RECORD      VALUE '47'.                 
                 88 SAR-WC-TAX-AND-SURCH-REC  VALUE '47' '94'.         
                 88 SAR-WCP-RATE-STAT      VALUE '45' THRU '50'.       
                 88 SAR-WCP-AUDIT-STAT     VALUE '45' THRU '47'.       
                 88 SAR-MIN-RETAIN-PREM-SEGMENT VALUE '92'.            
                 88 SAR-HO-SEGMENT                                     
                       VALUE '46' '47' '48' '49' '50' '51' '52' '53'.  
                 88 SAR-WCP-MODIFIER        VALUE '46'.                
                 88 SAR-50-SEGMENT          VALUE '50'.                
                 88 SAR-45-SEGMENT          VALUE '45'.                
              10 SAR-KEY.                                              
                 15 SAR-INSURANCE-LINE   PIC X(02).                    
                    88  SAR-BOILER-MACHINERY  VALUE 'BM'.              
                    88  SAR-BUSINESSOWNERS    VALUE 'BP'.              
                    88  SAR-COMMERCIAL-FIRE   VALUE 'CF'.              
                    88  SAR-GENERAL-LIAB      VALUE 'GL'.              
                    88  SAR-CAN-COMM-PROP     VALUE 'CP'.              
                    88  SAR-CAN-GEN-LIAB      VALUE 'LI'.              
                    88  SAR-COMMERCIAL-CRIME  VALUE 'CR'.              
                    88  SAR-COMMERCIAL-GLASS  VALUE 'CG'.              
                    88  SAR-CPP-INLAND-MARINE VALUE 'IM'.              
                    88  SAR-NOT-ON-TM523X     VALUE 'FF' 'BM'          
                                                    'CG' 'BT'.         
                    88  SAR-GARAGE            VALUE 'GA'.              
                    88  SAR-EMPTY-INS-LINE    VALUE SPACES.            
                    88  SAR-WC-INS-LINE       VALUE 'WC'.              
                 15 SAR-LOCATION-NUMBER-A.                             
                    20 SAR-LOCATION-NUMBER   PIC 9(04).                
                 15 SAR-SUB-LOCATION-NUMBER-A.                         
                    20 SAR-SUB-LOCATION-NUMBER PIC 9(03).              
                 15 SAR-RISK-UNIT-GROUP-KEY.                           
                    20 SAR-RISK-UNIT-GROUP     PIC X(03).              
                       88 SAR-ACCT-REC         VALUE '404'.            
                    20 SAR-SEQ-RSK-UNT-GRP.                            
                       25 SAR-CLASS-CODE-GROUP  PIC 9(02).             
                       25 SAR-CLASS-CODE-MEMBER PIC 9(01).             
                 15 SAR-RISK-UNIT.                                     
                    20 SAR-UNIT-GROUP.                                 
                       25 SAR-UNIT             PIC X(03).              
                          88 SAR-PRIMARY-UNIT  VALUE '000'.            
                          88 SAR-VALID-TX-HO-PI-UNIT VALUE             
                          '011' '012' '013' '014'                      
                          '015' '016' '017' '018'                      
                          '023' '019' '020' '021' '022'                
                          '019' '020' '021' '022'.                     
                       25 SAR-RISK-UNIT-CONTINUED  PIC X(03).          
                    20 SAR-SEQUENCE-RISK-UNIT.                         
                       25 SAR-RISK-SEQUENCE    PIC 9(01).              
                       25 SAR-RISK-TYPE-IND    PIC X(01).              
                          88 SAR-PREMISES-OPERATIONS  VALUE 'O'.       
                          88 SAR-PRODUCTS             VALUE 'P'.       
                 15 SAR-TYPE-EXPOSURE    PIC X(03).                    
                 15 SAR-MAJOR-PERIL      PIC X(03).                    
                    88 SAR-IDENTITY-THEFT        VALUE '950'.          
                    88 SAR-ADDITIONAL-STAT-CODES VALUE '000'.          
                    88 SAR-GARAGE-MAJ-PERIL   VALUE '269' '270'        
                                        '271' '272' '273' '274'.       
                    88 SAR-TEXAS-PREM-DISC    VALUE '325'.             
                    88 SAR-BONDS              VALUE '016' '031'.       
                    88 SAR-UMBRELLA           VALUE '105'.             
                    88 SAR-HOMEOWNERS         VALUE '002'.             
                    88 SAR-CALI-EQ-SURCHARGE  VALUE '040'.             
                    88 SAR-MP-NC-RECOUP-EXEMP VALUE '145' '170' '149'  
                          '146' '147' '148'.                           
                    88 SAR-MAJOR-PERIL-AUTO-LIAB VALUE '101' '110'.    
                    88 SAR-MP-NC-RECOUP-AUTO-LIAB VALUE '101' '110'    
                          '114' '116' '119' '120' '121' '100' '115'    
                          '117' '118' '123'.                           
                    88 SAR-NC-RECOUP-MAJ-PERIL VALUE '077' '078'       
                           '079' '135' '244' '245'.                    
                    88 SAR-OCCURRENCE-POLICY VALUE '517' '530' '534'   
                           '535' '017' '067' '075' '084' '085'.        
                    88 SAR-CLAIMS-MADE-POLICY VALUE '518' '540'        
                           '542' '544'.                                
                    88 SAR-CLAIMS-MADE-SERP VALUE '519' '541'          
                           '543' '545'.                                
                    88 SAR-MAJOR-PERIL-PHYS-DAM VALUE '170'.           
                    88 SAR-AUTO-BI       VALUE '101'.                  
                    88 SAR-AUTO-PIP      VALUE '130'.                  
                    88 SAR-AUTO-BIPD     VALUE '100'.                  
                    88 SAR-UMBI          VALUE '116'.                  
                    88 SAR-UMPD          VALUE '120'.                  
                    88 SAR-UNDER-MTRST   VALUE '118' '119' '121' '123'.
                    88 SAR-AUTO-PD       VALUE '110'.                  
                    88 SAR-AUTO-COLL     VALUE '170'.                  
                    88 SAR-PIP           VALUE '130'.                  
                    88 SAR-MED           VALUE '114'.                  
                    88 SAR-MED-PAY-ALL   VALUE '114' '131' '140' '141' 
                                               '142' '143'.            
                    88 SAR-NC-RECOUP-CLEAN-RISK-MP VALUE '077'.        
                    88 SAR-NC-RECOUP-CLEAN-ALLOC-MP VALUE '077' '244'. 
                    88 SAR-NC-RECOUP-LOSS-ALLOC-MP VALUE '079' '245'.  
                    88 SAR-NC-RECOUP-CLEAN-LOSS-MP                     
                                 VALUE '077' '079' '244' '245' '135'.  
                    88 SAR-NC-RECOUP-LOSS-MP VALUE '078'.              
                    88 SAR-NC-RECOUP-ASSESSMENT-MP VALUE '079' '135'.  
                    88 SAR-SC-ROAD-TAX   VALUE '184'.                  
                    88 SAR-FARMOWNERS    VALUE '005' '010'.            
                    88 SAR-FARM-PROPERTY VALUE '005'.                  
                    88 SAR-FARM-LIAB     VALUE '010'.                  
                    88 SAR-FARM-USER-END VALUE '300' THRU '320'.       
                    88 SAR-GENLIAB-BI    VALUE '084' '017' '107' '530' 
                                         '534' '540' '542'.            
                    88 SAR-GENLIAB-PD    VALUE '085' '535' '544'.      
                    88 SAR-SERVICE-CHARGE-MAJ-PERIL VALUE '088'.       
                    88 SAR-MARYLAND-RECOUP-MP VALUE '098'.             
                    88 SAR-NEWFOUNDLAND-MP VALUE '095'.                
                    88 SAR-CAN-PROV-TAX-MP VALUE '095'.                
                    88 SAR-ASSESSMENT-MP VALUE '078'.                  
                    88 SAR-CALIFORNIA-MP VALUE '079' '096' '098'.      
                    88 SAR-MAJOR-PERIL-TAXES VALUE '089' '127' '184'.  
                    88 SAR-ORE-MANDAY-MP VALUE '128'.                  
                    88 SAR-MOTOR-LAND-MAJOR-PERIL VALUE '205'.         
                    88 SAR-SERV-CHG-OR-TAXES-MP VALUE '088' '089'      
                          '127' '184' '128' '188'.                     
                    88 SAR-MASS-COLL-SURCHRG VALUE '175'.              
                    88 SAR-MASS-PD-SURCHRG VALUE '176'.                
                    88 SAR-MASS-PD-SURCH-CRED VALUE '180'.             
                    88 SAR-MASS-COLL-SURCH-OVD VALUE '181'.            
                    88 SAR-MASS-PD-SURCH-OVD VALUE '182'.              
                    88 SAR-TX-BUSS-OWNER-PROP VALUE '251'.             
                    88 SAR-TX-BUSS-OWNER-LIAB VALUE '252'.             
                    88 SAR-TX-BUSS-OWNER-OPT  VALUE '253'.             
                    88 SAR-FLA-CERT-FINANCL-RESPON VALUE '188'.        
                    88 SAR-MASS-SURCHARGES VALUE '175' '176' '181'     
                          '182'.                                       
                    88 SAR-UK-TEMP-DRIVER-VEHICLE  VALUE '811' '812'.  
                    88 SAR-UK-GREEN-CARD           VALUE '803'.        
                    88 SAR-UMB-PERSONAL-INJURY  VALUE '105'.           
                    88 SAR-UMB-MEDICAL-PAYMENTS  VALUE '106'.          
                    88 SAR-CREDIT-FOR-EXIST-INS  VALUE '129'.          
                    88 SAR-WATER-BACK-UP VALUE '097'.                  
                    88 SAR-INLAND-MARINE VALUE '201'.                  
                    88 SAR-MINIMUM-PREM  VALUE '599'.                  
                    88 SAR-BUY-BACK      VALUE '498'.                  
                    88 SAR-CLAIMS-MADE-GL VALUE '540' '542' '544' '518'
                    88 SAR-SERP-COVG      VALUE '541' '543' '545' '519'
                    88 SAR-LC-EXT-COV-MP-850  VALUE '850'.             
                    88 SAR-MAIF-SURCHARGE VALUE '183'.                 
                    88 SAR-PERIL-TAX-SURCHRG                           
                           VALUE '089' '489' '499' '599'.              
                    88 SAR-KY-COLLECTION-FEE VALUE '899'.              
                 15 SAR-SEQ-NO           PIC X(02).                    
                    88 SAR-WC-NO-SPLIT-RATING   VALUE '00'.            
           05  SAR-END-SEQ-KEY.                                        
                 15 SAR-COV-EFFECTIVE-DATE   PIC 9(08).                            
                 15 SAR-PART-CODE        PIC X(01).                    
                    88 SAR-REINSURANCE-ONLY-STAT-ACCT VALUE '4'.       
                    88 SAR-PART-ONE      VALUE '1'.                    
                    88 SAR-REINSURANCE   VALUE '4'.                    
                    88 STAT-ACCT-RECORD  VALUE '1' '4'.                
                    88 STAT-ACCT-FOR-RATING VALUE '1'.                 
                 15 SAR-TRANS-EFFECTIVE-DATE PIC 9(08).                          
                 15 SAR-VARIABLE-KEY.                                  
                    20 SAR-ADDIT-INT-TYP  PIC X(03).                   
                    20 SAR-SEQ-ADDIT-INT-TYP PIC 9(03).                
                    20 SAR-PEAK-SEASON-INDICATOR PIC X(01).            
                       88 SAR-MINIMUM-PREMIUM-STAT  VALUE 'M'.         
                       88 SAR-PEAK-SEASON-NO        VALUE 'O'.         
                       88 SAR-PEAK-SEASON-YES       VALUE 'P'.         
                    20 SAR-LAYER                 PIC 9(02).            
                    20 SAR-REINS-KEY-ID          PIC X(01).            
                    20 SAR-REINS-TREATY.                               
                       25 SAR-REINSURANCE-COMPANY-NO  PIC X(04).       
                       25 SAR-REINS-BROKER            PIC 9(04).       
                    20 SAR-REINS-SEQUENCE             PIC 9(02).       
                 15 SAR-ENTRY-DATE       PIC 9(08).                                    
           05 SAR-EXPIRATION-DATE        PIC 9(08).                                     
           05 SAR-TRANSACTION            PIC X(02).                    
              88 SAR-REINSTATED-POLICY   VALUE '15'.                   
              88 SAR-CANCELLED-POLICY    VALUE '20' '21' '23' '25'     
                    '29' '40' '41' '43' '45'.                          
              88 SAR-CANCEL-REINST-POLICY VALUE '15' '20' '21' '23'    
                    '25' '29'.                                         
              88 SAR-NEW-BUSINESS        VALUE '10'.                   
              88 SAR-RENEWAL-BUSINESS    VALUE '11'.                   
              88 SAR-NO-WCP-TAX-TRANSACTIONS VALUE '16' '17' '26'      
                                                   '27' '59' '69'.     
              88 SAR-DIVIDEND-TRANSACTIONS   VALUE '16' '17' '26' '27'.
              88 SAR-OFFSET-ONSET-QUALIFIER VALUE '10' '11' '12' '14'  
                    '15' '18' '19' '20' '21' '22' '23' '24' '25' '29'  
                    '40' '41' '43' '45' '13' '28'.                     
              88 SAR-PAYROLL-AUDIT-PREMIUMS VALUE '14' '24'.           
              88 SAR-REINSURANCE-PREMIUMS VALUE '53' '63'.             
              88 SAR-ACCT-CURRENT-PREMIUMS VALUE '59' '69'.            
              88 SAR-ACCT-CURR-BILL      VALUE '59'.                   
              88 SAR-ACCT-CURR-DOWN-PAY  VALUE '69'.                   
              88 SAR-OFFSET-ONSET-TRANSACTION VALUE '12' '22' '13' '28'
              88 SAR-ALL-NEW-BUSINESS    VALUE '10' '18'.              
              88 SAR-ALL-RENEWAL-BUSINESS VALUE '11' '19'.             
              88 SAR-RENEW-TRANS-POS     VALUE '10' '11' '12'.         
              88 SAR-DEPOSIT-PREMIUM     VALUE '55'.                   
              88 SAR-DEPOSIT-PREMIUM-REVERSED VALUE '65'.              
              88 SAR-NON-RENEW-TRANS                                   
                     VALUE '14' '24' '28' '83' '84' '93' '94'.         
              88 SAR-PREPAYMENT          VALUE '95'.                   
              88 SAR-RETURN-PREM         VALUE '22'.                   
              88 SAR-ADDITIONAL-PREMIUM  VALUE '12'.                   
              88 SAR-MIDTERM-DELETION    VALUE '28'.                   
              88 SAR-ADDIT-AUDITABLE-PREM VALUE '13'.                  
              88 SAR-FLAT-CANCELLATION   VALUE '20' '21'.              
              88 SAR-NON-FLAT-CANCELLATION VALUE '23' '25'.            
              88 SAR-NEGATIVE-TRANS      VALUE '63' '69' '84' '86'.    
              88 SAR-ASSUMED-BUSINESS    VALUE '30' '31' '40' '41'.    
              88 SAR-ADD-PREM-DUE        VALUE '14'.                   
              88 SAR-DIRECT-BUS-TRX-DB   VALUE '12' '14' '15' '16'     
                    '17' '18' '19' '56' '57' '54' '13'.                
              88 SAR-ASSUMED-BUS-TRX-DB  VALUE '30' '31' '32' '34'.    
              88 SAR-DIRECT-BUS-TRX-CR   VALUE '21' '22' '23' '24'     
                    '25' '26' '27' '64' '66' '67' '28'.                
              88 SAR-ADDITIONAL-OR-RETURN-PREM VALUE '12' '22' '13'    
                                                     '28'.             
              88 SAR-AUDITABLE-COVERAGE VALUE '13' '18' '19' '28'.     
              88 SAR-DIRECT-BUS-TRX      VALUE '29'.                   
              88 SAR-ASSUMED-BUS-TRX-CR  VALUE '40' '41' '42' '43'     
                    '45'.                                              
              88 SAR-NEW-OR-REN-TRANS    VALUE '10' '11'.              
              88 SAR-REINS-OR-ACCT-CURRENT VALUE '53' '59'.            
              88 SAR-RETRO-ADJ-ACTUAL-CREDIT VALUE '67'.               
              88 SAR-REIN-OR-ACCT-CUR-CANCEL VALUE '63' '69'.          
              88 SAR-NEW-RENEWAL-DEP-PREMIUMS VALUE '18' '19'.         
              88 SAR-SERP-PREMIUM-REVERSED VALUE '83'.                 
              88 SAR-SERP-PREMIUM          VALUE '84'.                 
              88 SAR-PICS-DEPOSIT-PREM-TRANS VALUE '10' '11' '12' '15' 
                    '18' '19' '20' '21' '22' '23' '25'.                
              88 SAR-RETRO-PREM              VALUE '67'.               
           05 SAR-PREMIUM                PIC S9(09)V9(02).             
           05 SAR-SUBPAY-AMT             PIC S9(09)V9(02).             
           05 SAR-ORIGINAL-PREM          PIC S9(09)V9(02).             
           05 SAR-PREMIUM-ESTIM-TM       PIC S9(09)V9(02).             
           05 SAR-AGENTS-COMM            PIC X(06).                    
              88 SAR-COMMISSION-NOT-ASSIGNED VALUE '999999'.           
           05 SAR-TOTAL-COMM             PIC X(06).                    
           05 SAR-TOTAL-COMM-AMOUNT      PIC S9(09)V9(02).             
           05 SAR-ACCOUNT-ENTERED-DATE   PIC 9(06).                                
              88 VB-ACCT-ENT-DATE-NINES  VALUE '999999'.               
           05 SAR-ANNUAL-STATEMENT-LINE  PIC 9(03).                    
              88 SAR-NON-BUREAU-ACCTG    VALUE 880.                    
           05 SAR-REINS-TYPE             PIC X(01).                    
              88 SAR-AGENCY-REINSURANCE  VALUE '1'.                    
              88 SAR-FACULTATIVE-REINSURANCE VALUE '2'.                
              88 SAR-TREATY-REINSURANCE   VALUE '3'.                   
           05 SAR-REINS-CATEGORY         PIC X(02).                    
           05 SAR-FACULTA-COMM-RATE      PIC S9(01)V9(05).             
           05 SAR-CESSION-NUMBER         PIC X(10).                    
           05 SAR-STATE                  PIC X(02).                    
              88 SAR-STATE-CALIF         VALUE '04'.                   
              88 SAR-STATE-CONNECTICUT   VALUE '06'.                   
              88 SAR-STATE-KENTUCKY      VALUE '16'.                   
              88 SAR-STATE-MARYLAND      VALUE '19'.                   
              88 SAR-STATE-MASS          VALUE '20'.                   
              88 SAR-STATE-NC            VALUE '32'.                   
              88 SAR-STATE-NC-SC         VALUE '32' '39'.              
              88 SAR-STATE-NEW-JERSEY    VALUE '29'.                   
              88 SAR-STATE-WASHINGTON    VALUE '46'.                   
              88 SAR-STATE-NEWFOUNDLAND  VALUE '60'.                   
              88 SAR-STATE-OREGON        VALUE '36'.                   
              88 SAR-STATE-UK            VALUE '99'.                   
              88 SAR-STATE-TEXAS         VALUE '42'.                   
              88 SAR-PROVINCE-QUEBEC     VALUE '68' '78'.              
           05 SAR-TERRITORY.                                           
              10 FILLER                  PIC X(01).                    
              10 SAR-LOC-PROV-TERRITORY  PIC X(03).                    
                 88 SAR-CALIF-TERR-CODE     VALUE '001' '002' '003'.   
           05 SAR-COMPANY-NUMBER         PIC 9(02).                    
           05 SAR-TAX-LOC.                                             
              07  SAR-COUNTY.                                          
                  15  SAR-COUNTY-FIRST-TWO  PIC X(02).                 
                  15  SAR-COUNTY-LAST-ONE   PIC X(01).                 
              07  SAR-CITY                  PIC X(03).                 
           05 SAR-REASON-AMEND.                                        
              10 SAR-RSN-AMEND-ONE       PIC X(01).                    
                 88 SAR-RSN-AMEND-P-S    VALUE 'P' 'S'.                
                 88 SAR-RSN-AMEND-P      VALUE 'P'.                    
                 88 SAR-RSN-AMEND-S      VALUE 'S'.                    
              10 SAR-RSN-AMEND-TWO       PIC X(01).                    
                 88 SAR-BILL-NOW2        VALUE 'Q'.                    
                 88 SAR-SPREAD-REINSURANCE2 VALUE 'O'.                 
                 88 SAR-SERP-REAS-AMD2   VALUE '0'.                    
              10 SAR-RSN-AMEND-THREE     PIC X(01).                    
           05 SAR-SPECIAL-USE            PIC X(08).                    
           05 SAR-PRODUCT-LINE.                                        
              88 SAR-PROD-LINE-NOT-ASSIGNED VALUE SPACES.              
              10 SAR-STAT-BREAKDOWN-LINE PIC X(02).                    
                 88 SAR-COMMERCIAL-AUTO  VALUE '05' '06' '55' '56'.    
                 88 SAR-PRIVATE-PASSENGER VALUE '01' THRU '04' '51'    
                       THRU '54'.                                      
                 88 SAR-NC-REINS-FACILITY-CODE VALUE '01' THRU '06'.   
                 88 SAR-NON-REPORTING-SB-LINE VALUE '87' '88' '89'     
                       '90' THRU '97'.                                 
              10 SAR-USER-LINE           PIC X(02).                    
           05 SAR-SECTION                PIC X(01).                    
              88 SAR-ALL-SECTIONS        VALUE '0'.                    
           05 FUTURE-USE                 PIC X(01).                    
           05 SAR-TYPE-BUREAU            PIC X(02).                    
              88 SAR-FULL-COMM-AUTO-LIAB VALUE 'AL'.                   
              88 SAR-FULL-COMM-AUTO-LIAB-NF VALUE 'AN'.                
              88 SAR-FULL-COMM-AUTO-PHYS-DAMG VALUE 'AP'.              
              88 SAR-FULL-BUSINESS-BLDG-CONT VALUE 'BB'.               
              88 SAR-FULL-BUS-BLDG-CONTENTS VALUE 'BC'.                
              88 SAR-FULL-BUSINESS-OTHER-CONT VALUE 'BE'.              
              88 SAR-FULL-BOILER-MACHINERY VALUE 'BM'.                 
              88 SAR-FULL-BURGLARY-THEFT VALUE 'BT'.                   
              88 SAR-FULL-COMM-FIRE-ALLIED-LINE VALUE 'CF'.            
              88 SAR-FULL-EARTHQUAKE     VALUE 'EQ'.                   
              88 SAR-FULL-FIDELITY-FORGERY VALUE 'FF'.                 
              88 SAR-FULL-FIDELITY-SURETY-CSP  VALUE 'BF'.             
              88 SAR-FULL-FARM-RANCH-OWNERS VALUE 'FO'.                
              88 SAR-FULL-GENERAL-LIABILITY VALUE 'GL'.                
              88 SAR-FULL-GLASS          VALUE 'GS'.                   
              88 SAR-FULL-INLAND-MARINE  VALUE 'IM'.                   
              88 SAR-FULL-PRIV-PASS-LIABILITY VALUE 'LP'.              
              88 SAR-FULL-PRIV-PASS-NO-FAULT VALUE 'NF'.               
              88 SAR-FULL-PLSP-IM-PREM VALUE 'PI'.                     
              88 SAR-FULL-PLSP-PL-PREM VALUE 'PL'.                     
              88 SAR-FULL-PLSP-THEFT VALUE 'PT'.                       
              88 SAR-FULL-CRIME-2000 VALUE 'CX'.                       
              88 SAR-FULL-PRIV-PASS-PHYS-DAMG VALUE 'PP'.              
              88 SAR-FULL-MED-PROF-LIABILITY  VALUE 'ML'.              
              88 SAR-FULL-TEXAS-GEN-LIABILITY VALUE 'LT'.              
              88 SAR-FULL-TEXAS-COMM-PROPERTY VALUE 'FX'.              
              88 SAR-FULL-TEXAS-AUTO-AND-GARA VALUE 'XA'.              
              88 SAR-FULL-TEXAS-BUSINESS-OWN  VALUE 'XB'.              
              88 SAR-FULL-TEXAS-MISCELLANEOUS VALUE 'XM'.              
              88 SAR-FULL-TEXAS-FIDEL-SURETY  VALUE 'XS'.              
              88 SAR-HOUSEHOLD-PERS-ACCID     VALUE 'HA'.              
              88 SAR-HOUSEHOLD-BUILDING       VALUE 'HB'.              
              88 SAR-HOUSEHOLD-CONTENTS       VALUE 'HC'.              
              88 SAR-HOUSEHOLD-PERS-LIAB      VALUE 'HL'.              
              88 SAR-HOUSEHOLD-MORTAGE-PROT   VALUE 'HM'.              
              88 SAR-HOUSEHOLD-ALL-RISKS      VALUE 'HR'.              
              88 SAR-HOUSEHOLD-SMALL-CRAFTS   VALUE 'HS'.              
              88 SAR-HOUSEHOLD-CARAVAN        VALUE 'HV'.              
              88 SAR-HOUSEHOLD-BLOCK-BUILDING VALUE 'HZ'.              
              88 SAR-ALL-UK-HOUSEHOLD         VALUE 'HA' 'HB' 'HC' 'HL'
                                                    'HM' 'HR' 'HS' 'HV'
                                                    'HZ'.              
              88 SAR-INTER-COMM-AUTO-LIABILITY VALUE 'AI'.             
              88 SAR-INTER-COMM-AUTO-PHYS-DAMG VALUE 'A2'.             
              88 SAR-INTER-COMM-AUTO-NO-FAULT VALUE 'A3'.              
              88 SAR-INTER-BUSINESSOWNERS VALUE 'B2'.                  
              88 SAR-INTER-BURGLARY-THEFT VALUE 'BI'.                  
              88 SAR-INTER-COMM-FIRE-ALLIED-LN VALUE 'CI'.             
              88 SAR-INTER-EARTHQUAKE    VALUE 'EI'.                   
              88 SAR-INTER-FIDELITY-FORGERY VALUE 'FM'.                
              88 SAR-INTER-FIDELITY-SUR-NON-CSP VALUE 'BP'.            
              88 SAR-INTER-FARM-RANCH-OWNERS VALUE 'FI'.               
              88 SAR-INTER-GENERAL-LIABILITY VALUE 'GI'.               
              88 SAR-INTER-GLASS         VALUE 'GM'.                   
              88 SAR-INTER-INLAND-MARINE VALUE 'II'.                   
              88 SAR-INTER-PRIV-PASS-LIABILITY VALUE 'LI'.             
              88 SAR-INTER-BOILER-MACHINERY VALUE 'M2'.                
              88 SAR-INTER-MED-PROF-LIABILITY VALUE 'MI'.              
              88 SAR-INTER-PRIV-PASS-NO-FAULT VALUE 'NI'.              
              88 SAR-MINI-BUSINESSOWNERS VALUE 'BL'.                   
              88 SAR-MINI-GENERAL-LIABILITY VALUE 'GP'.                
              88 SAR-MINI-PRIV-PASS-NO-FAULT VALUE 'NL'.               
              88 SAR-MINI-COMM-AUTO-PHYS-DAMAGE VALUE 'CP'.            
              88 SAR-MINI-COMM-AUTO-LIABILITY VALUE 'CL'.              
              88 SAR-MINI-COMM-AUTO-NO-FAULT VALUE 'CN'.               
              88 SAR-MINI-ALL-OTHER      VALUE 'MP'.                   
              88 SAR-UK-MOTOR-VEHICLE    VALUE 'MV'.                   
              88 MASS-AUTO-TL            VALUE 'TL'.                   
              88 MASS-AUTO-TN            VALUE 'TN'.                   
              88 MASS-AUTO-TP            VALUE 'TP'.                   
              88 SAR-RUN-OFF-HOME-FIRE-DWELL VALUE 'HO'.               
              88 SAR-RUN-OFF-LIAB-FIRE-DWELL VALUE 'ND'.               
              88 SAR-RUN-OFF-MOBILHOMEOWNERS VALUE 'MH'.               
              88 SAR-RUN-OFF-HOMEOWNER-PREMIUM VALUE 'PH'.             
              88 SAR-RUN-OFF-MOBILE-HO-PREMIUM VALUE 'PM'.             
              88 SAR-RUN-OFF-INLAND-MARINE VALUE 'RI'.                 
              88 SAR-RUN-OFF-LIABILITY-PREMIUM VALUE 'PL'.             
              88 SAR-RUN-OFF-MILL-ELEVATOR VALUE 'ME'.                 
              88 SAR-RUN-OFF-RANCH-FARM-OWNWERS VALUE 'TH'.            
              88 SAR-RUN-OFF-TEXAS-HOMEOWNERS VALUE 'TH'.              
              88 SAR-RUN-OFF-TEXAS-DWELL-FIRE VALUE 'JR'.              
              88 SAR-RUN-OFF-TEXAS-COMM-FIRE VALUE 'JP'.               
              88 SAR-RUN-OFF-WATERCRAFT-LIAB VALUE 'WL'.               
              88 SAR-RUN-OFF-EARTHQUAKE      VALUE 'NE'.               
              88 SAR-RUN-OFF-WORKERS-COMP VALUE 'WC'.                  
              88 SAR-RUN-OFF-SMP         VALUE 'SM'.                   
              88 SAR-RUN-OFF-FIRE-SCH-COMM VALUE 'FC'.                 
              88 SAR-RUN-OFF-GENERAL-LIABILITY VALUE 'RQ'.             
              88 SAR-RUN-OFF-BONDS       VALUE 'BD'.                   
              88 SAR-RUN-OFF-GLASS       VALUE 'RG'.                   
              88 SAR-RUN-OFF-BURGLARY    VALUE 'RB'.                   
              88 SAR-RUN-OFF-BOILER-MACHINERY VALUE 'RM'.              
              88 SAR-RUN-OFF-PHYS-DAMAGE VALUE 'RP'.                   
              88 SAR-RUN-OFF-CANADA-PERS-LINES VALUE 'PC'.             
              88 SAR-RUN-OFF-CANADA-COMM-LINES VALUE 'CC'.             
              88 SAR-RUN-OFF-CANADA-AUTO VALUE 'AC'.                   
              88 SAR-RUN-OFF-AUTO-LIABILITY VALUE 'RL'.                
              88 SAR-RUN-OFF-AUTO-LIAB-NO-FAULT  VALUE 'RN'.           
              88 SAR-TEXAS-MULTI-PERIL-BURGLARY  VALUE 'JB'.           
              88 SAR-TEXAS-MULTI-PERIL-FID-FORG  VALUE 'JF'.           
              88 SAR-TEXAS-MULTI-PERIL-GLASS     VALUE 'JG'.           
              88 SAR-TEXAS-MULTI-PERIL-LIAB      VALUE 'JL'.           
              88 SAR-TEXAS-MULTI-PERIL-IN-MAR    VALUE 'JO'.           
              88 SAR-TEXAS-MULTI-PERIL-COMM-PR   VALUE 'JP'.           
              88 SAR-TEXAS-BUSS-OWNER            VALUE 'JT'.           
              88 SAR-AUTO-LIAB-NO-FAULT          VALUE 'FN'.           
              88 SAR-AUTO-LIAB-EXCEPT-NO-FAULT   VALUE 'LN'.           
              88 SAR-FULL-FIDELITY-CSP   VALUE 'FT'.                   
              88 SAR-INTER-FIDELITY-CSP  VALUE 'FP'.                   
              88 SAR-NON-BUREAU-TYPE-1   VALUE 'NN'.                   
              88 SAR-NON-BUREAU-TYPE-2   VALUE 'NB'.                   
              88 SAR-NON-BUREAU-TYPE     VALUE 'NN' 'NB'.              
              88 SAR-GENERAL-LIABILITY   VALUE 'GI' 'GL'.              
              88 SAR-PASP-PHYS-DAMAGE    VALUE 'VP'.                   
              88 SAR-PASP-AUTO-LIABILITY VALUE 'VL'.                   
              88 SAR-PASP-AUTO-LIAB-NO-FAULT  VALUE 'VN'.              
              88 SAR-PASP-VEH-COMMON-DATA VALUE 'VC'.                  
              88 SAR-PASP                VALUE 'VN' 'VL' 'VP' 'VC'.    
              88 SAR-FULL-CSP-PLAN       VALUE 'AL' 'BT' 'AP' 'CF'     
                    'AN' 'GS' 'LP' 'BM' 'PP' 'GL' 'NP' 'ML' 'BB' 'IM'  
                    'BE' 'EQ' 'FO' 'FF' 'FT' 'BF' 'IN' 'NF' 'BC'.      
              88 SAR-AAIS-FARMOWNERS     VALUE 'SO'.                   
              88 SAR-INTERMEDIATE-CSP-PLAN VALUE 'AI' 'BI' 'A2' 'CI'   
                    'A3' 'GM' 'LI' 'M2' 'GI' 'NI' 'MI' 'B2' 'II' 'FI'  
                    'EI' 'FM' 'FP'.                                    
              88 SAR-MINI-CSP-PLAN       VALUE 'GN' 'BL' 'MP'.         
              88 SAR-MINI-CSP-PLAN-GROUP-1 VALUE 'GP' 'CP' 'CL' 'CN'   
                    'BL'.                                              
              88 SAR-MINI-CSP-PLAN-GROUP-2 VALUE 'MP'.                 
              88 SAR-RUN-OFF-CSP-PLAN    VALUE 'RL' 'RQ' 'RP' 'RG'     
                    'RN' 'RB' 'AC' 'FC' 'HO' 'SM' 'MH' 'ME' 'TH' 'WC'  
                    'JP' 'JR' 'RI' 'BD' 'WL' 'RM' 'PC' 'CC' 'BP'       
                    'FP' 'NE'.                                         
              88 SAR-NAII-TYPE-BUREAU                                  
                                 VALUE 'HO' 'MH' 'WL' 'NE'.            
              88 SAR-ALL-GENERAL-LIABILITY VALUE 'GL' 'GI' 'GN' 'RQ'.  
              88 SAR-ALL-AUTO-BI-PD      VALUE 'AL' 'A3' 'AN' 'AP'     
                    'AI' 'LP' 'NF' 'PP' 'AC' 'A2' 'RL' 'RN'.           
              88 SAR-ALL-AUTO-BI-PD-STAT-RPT VALUE 'AL' 'LP' 'AI' 'LI' 
                    'RL'.                                              
              88 SAR-GLASS               VALUE 'GS' 'GM' 'RG'.         
              88 SAR-FIDELITY-FORGERY-STAT-RPT VALUE 'FF' 'FM' 'BF'    
                    'BP' 'FT' 'FP'.                                    
              88 SAR-BURGLARY-THEFT-STAT-RPT VALUE 'BI' 'BT' 'RB'.     
              88 SAR-COMM-LINES-CAS-LIABILITY VALUE 'LC'.              
              88 SAR-WORKERS-COMP-PLUS        VALUE 'WP'.              
           05 SAR-CLASS.                                               
              10 SAR-CLASS-1-4               PIC X(04).                
                 88 SAR-PREM-DISCOUNT-REC    VALUE '7040' '9989'.      
                 88 SAR-GARAGE-POLICY        VALUE '7070' '7072'       
                       '7301' '7302' '7311' '7312' '7321' '7322'       
                       '7331' '7332' '7341' '7342' '7351' '7352'       
                       '7802' '7804' '7808' '7810' '7812' '7815'       
                       '7800' '7829' '7929'                            
                       '7820' '7822' '7830' '7877' '7878'.             
              10 SAR-CLASS-5-6               PIC X(02).                
           05 SAR-EXPOSURE               PIC S9(10).                   
              88 IF-ANY-CLASS            VALUE +009999999.             
           05 SAR-SUB-LINE               PIC X(03).                    
           05 SAR-CODES.                                               
              10 SAR-CODE-1              PIC X(01).                    
              10 SAR-CODE-2              PIC X(01).                    
              10 SAR-CODE-3              PIC X(01).                    
              10 SAR-CODE-4              PIC X(02).                    
              10 SAR-CODE-5              PIC X(02).                    
              10 SAR-CODE-6              PIC X(03).                    
              10 SAR-CODE-7              PIC X(03).                    
              10 SAR-CODE-8              PIC X(03).                    
              10 SAR-CODE-9              PIC X(03).                    
              10 SAR-CODE-10             PIC X(03).                    
              10 SAR-CODE-11             PIC X(03).                    
              10 SAR-CODE-12             PIC X(03).                    
              10 SAR-CODE-13             PIC X(03).                    
              10 SAR-CODE-14             PIC X(03).                    
              10 SAR-CODE-15             PIC X(04).                    
           05 SAR-ZIP-POSTAL-CODE        PIC X(10).                    
           05 SAR-ARS-OFFSET-PREMIUM     PIC S9(09)V9(02).             
           05 SAR-ARS-OFFSET-COMMISSION  PIC S9(01)V9(05).             
           05 SAR-AUDIT-REINST-IND       PIC X(01).                    
              88  REINSTATING-29-TRANS   VALUE 'R'.                    
           05 SAR-REIN-PROPOR            PIC X(01).                    
           05 SAR-PMS-FUTURE-USE         PIC X(34).                    
           05 SAR-LAST-UPDATE-SEQ        PIC X(03).                    
           05 SAR-TX-PI-HO0460-IND       PIC X(01).                    
           05 SAR-CL-AUTO-WOS            PIC X(01).                    
           05 SAR-TX-TOWN-CODE           PIC X(04).                    
           05 SAR-JR-ROOF-COV-YR-INST-POS1-3 PIC X(03).                
           05 SAR-FL-EMPATF-MIN-RET-IND  PIC X(01).                    
           05 SAR-JR-ROOF-COVER-EX       PIC X(001).                   
           05 SAR-JR-TENURE-DISC         PIC X(001).                   
           88 SAR-JR-TENURE-DISC-0       VALUE '0'.                    
           88 SAR-JR-TENURE-DISC-1       VALUE '1'.                    
           88 SAR-JR-TENURE-DISC-2       VALUE '2'.                    
           88 SAR-JR-TENURE-DISC-3       VALUE '3'.                    
           88 SAR-JR-TENURE-DISC-4       VALUE '4'.                    
           88 SAR-JR-TENURE-DISC-5       VALUE '5'.                    
           88 SAR-JR-TENURE-DISC-GT5     VALUE '6'.                    
           05 SAR-JR-TENURE-DISC-AMT     PIC X(002).                   
           05 SAR-RM-RATING-TIER         PIC X(002).                   
           05 SAR-RM-RATING-TIER-POINTS  PIC X(003).                   
           05 SAR-RM-RATING-FULL-COV     PIC X(001).                   
           05 SAR-RM-RATING-EXC-DRIVER   PIC X(001).                   
           05 SAR-RM-RATING-HHG          PIC X(002).                   
           05 SAR-CL-MANUAL-ENTRY        PIC X(001).                   
           05 SAR-VIN                    PIC X(017).                   
           05 SAR-RM-RATING-YEARS-FREE   PIC X(001).                   
           05 SAR-YR2000-CUST-USE        PIC X(068).                   
           05 BLANK-OUT PIC X(03).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).