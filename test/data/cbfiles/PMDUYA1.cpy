	      01 GARAGE-KEEPERS-LIAB-UY-RECORD.                                         
           03 PMDUYA1-UNIT-RATING-SEG.            
           05 PMDUYA1-SEGMENT-KEY.                                      
             10 PMDUYA1-SEGMENT-ID                      PIC X(02).      
                88 PMDUYA1-SEGMENT-43                   VALUE '43'.     
             10 PMDUYA1-SEGMENT-STATUS                  PIC X(01).      
                88 PMDUYA1-ACTIVE-STATUS                VALUE 'A'.      
                88 PMDUYA1-DELETED-STATUS               VALUE 'D'.      
                88 PMDUYA1-HISTORY-STATUS               VALUE 'H'.      
                88 PMDUYA1-NOT-ACTIVE-STATUS            VALUE 'H' 'D'.  
                88 PMDUYA1-RENEWAL-FS-STATUS            VALUE 'R' 'F'.  
             10 PMDUYA1-TRANSACTION-DATE                PIC 9(08).
             10 PMDUYA1-SEGMENT-ID-KEY.                                 
                15 PMDUYA1-SEGMENT-LEVEL-CODE           PIC X(01).      
                   88 PMDUYA1-SEG-LEVEL-U               VALUE 'U'.      
                15 PMDUYA1-SEGMENT-PART-CODE            PIC X(01).      
                   88 PMDUYA1-SEG-PART-Y                VALUE 'Y'.      
                15 PMDUYA1-SUB-PART-CODE                PIC X(01).      
                15 PMDUYA1-INSURANCE-LINE               PIC X(02).      
                   88 PMDUYA1-GARAGE                    VALUE 'GA'.     
             10 PMDUYA1-LEVEL-KEY.                                      
                15 PMDUYA1-LOCATION-NUMBER-A.                           
                   20 PMDUYA1-LOCATION-NUMBER           PIC 9(04).      
                15 PMDUYA1-SUB-LOCATION-NUMBER          PIC X(03).      
               13 PMDUYA1-RUG-RU-KEY.                                   
                15 PMDUYA1-RISK-UNIT-GROUP-KEY.                         
                   20 PMDUYA1-RISK-UNIT-GROUP.                          
                      25 PMDUYA1-COVERAGE            PIC X(06).         
                         88 PMDUYA1-SL               VALUE '100   '.    
                         88 PMDUYA1-AGG              VALUE '101   '.    
                         88 PMDUYA1-PIP              VALUE '102   '.    
                         88 PMDUYA1-PIC              VALUE '103   '.    
                         88 PMDUYA1-PIE              VALUE '104   '.    
                         88 PMDUYA1-EPI              VALUE '105   '.    
                         88 PMDUYA1-PIN              VALUE '106   '.    
                         88 PMDUYA1-MP               VALUE '107   '.    
                         88 PMDUYA1-UMB              VALUE '108   '.    
                         88 PMDUYA1-UMP              VALUE '109   '.    
                         88 PMDUYA1-UNB              VALUE '110   '.    
                         88 PMDUYA1-UNP              VALUE '111   '.    
                         88 PMDUYA1-UMS              VALUE '112   '.    
                         88 PMDUYA1-BI               VALUE '113   '.    
                         88 PMDUYA1-PD               VALUE '114   '.    
                         88 PMDUYA1-GKL              VALUE '115   '.    
                         88 PMDUYA1-DPD              VALUE '116   '.    
                         88 PMDUYA1-UNS              VALUE '117   '.    
                         88 PMDUYA1-FPB              VALUE '118   '.    
                         88 PMDUYA1-MEB              VALUE '119   '.    
                         88 PMDUYA1-WLB              VALUE '120   '.    
                         88 PMDUYA1-FEB              VALUE '121   '.    
                         88 PMDUYA1-ADB              VALUE '122   '.    
                         88 PMDUYA1-CPB              VALUE '123   '.    
                         88 PMDUYA1-FBI              VALUE '124   '.    
                         88 PMDUYA1-MBI              VALUE '125   '.    
                         88 PMDUYA1-WBI              VALUE '126   '.    
                         88 PMDUYA1-FEI              VALUE '127   '.    
                         88 PMDUYA1-ADI              VALUE '128   '.    
                         88 PMDUYA1-ABI              VALUE '128   '.    
                         88 PMDUYA1-CBI              VALUE '129   '.    
                         88 PMDUYA1-EMB              VALUE '130   '.    
                         88 PMDUYA1-PIA              VALUE '131   '.    
                         88 PMDUYA1-UMV              VALUE '132   '.    
                         88 PMDUYA1-UNV              VALUE '133   '.    
                         88 PMDUYA1-SUS              VALUE '135   '.    
                         88 PMDUYA1-SUB              VALUE '235   '.    
                         88 PMDUYA1-PDB              VALUE '141   '.    
                         88 PMDUYA1-CA               VALUE '143   '.    
                         88 PMDUYA1-CAB              VALUE '144   '.    
                         88 PMDUYA1-AWA              VALUE '145   '.    
                         88 PMDUYA1-MI               VALUE '146   '.    
                         88 PMDUYA1-OBE              VALUE '167   '.    
                         88 PMDUYA1-RMEC             VALUE '197   '.    
                         88 PMDUYA1-POLCON           VALUE '198   '.    
                         88 PMDUYA1-FPC              VALUE '199   '.    
                         88 PMDUYA1-HA               VALUE '450   '.    
                         88 PMDUYA1-RP               VALUE '600   '.    
                         88 PMDUYA1-DOC              VALUE '650   '.    
                         88 PMDUYA1-RPS              VALUE '750   '.    
                         88 PMDUYA1-UM-UNDER-CODES   VALUE '108   '     
                            '109   ' '110   ' '111   ' '112   '         
                            '117   '.                                   
                         88 PMDUYA1-SPECIAL-COVERAGES VALUE '650   '    
                            '102   ' '103   ' '104   ' '105   '         
                            '106   ' '107   '.                          
                         88 PMDUYA1-RUG-D-S-FORMS    VALUE              
                            '650   '.                                   
                15 PMDUYA1-RISK-UNIT.                                   
                   20 PMDUYA1-RU.                                       
                      25 PMDUYA1-PERIL                  PIC X(03).      
                         88 PMDUYA1-DEALER              VALUE '100'.    
                         88 PMDUYA1-NON-DEALER          VALUE '100'.    
                         88 PMDUYA1-SINGLE-LIAB         VALUE '100'.    
                         88 PMDUYA1-AGGREGATE           VALUE '101'.    
                         88 PMDUYA1-REG-PLATES-PIP      VALUE '102'.    
                         88 PMDUYA1-UNINS-MOT-BI        VALUE '108'.    
                         88 PMDUYA1-UNDER-MOT-BI        VALUE '110'.    
                         88 PMDUYA1-UNDER-MOT-PD        VALUE '111'.    
                         88 PMDUYA1-UNINS-MOT-SL        VALUE '112'.    
                         88 PMDUYA1-BI-LIAB             VALUE '113'.    
                         88 PMDUYA1-UNDER-MOT-SL        VALUE '117'.    
                         88 PMDUYA1-REG-PLATES-FPB      VALUE '118'.    
                         88 PMDUYA1-REG-PLATES-EMB      VALUE '130'.    
                         88 PMDUYA1-BROAD-COVERAGE      VALUE '132'.    
                         88 PMDUYA1-SUS-COVERAGE        VALUE '135'.    
                         88 PMDUYA1-SUB-COVERAGE        VALUE '235'.    
                         88 PMDUYA1-PERSONAL-INJURY     VALUE '134'.    
                         88 PMDUYA1-USE-OTHER-AUTOS     VALUE '135'.    
                         88 PMDUYA1-BROAD-FORM-PROD     VALUE '136'.    
                         88 PMDUYA1-COMPL-OPERATIONS    VALUE '138'.    
                         88 PMDUYA1-PP                  VALUE '140'.    
                         88 PMDUYA1-REG-PLATES-PDB      VALUE '141'.    
                         88 PMDUYA1-FLL                 VALUE '142'.    
                         88 PMDUYA1-CA-CA               VALUE '143'.    
                         88 PMDUYA1-CAB-CAB             VALUE '144'.    
                         88 PMDUYA1-REG-PLATES-AWA      VALUE '145'.    
                         88 PMDUYA1-REG-PLATES-MI       VALUE '146'.    
                         88 PMDUYA1-RMEC-VALUES         VALUE           
                            '177' '178' '179'.                          
                         88 PMDUYA1-OWNS-PREMISES       VALUE '200'.    
                         88 PMDUYA1-POLCON-VALUES       VALUE           
                            '246' '247' '248'.                          
                         88 PMDUYA1-DEALER-PLATES       VALUE '250'.    
                         88 PMDUYA1-MED-PAY             VALUE '400'.    
                         88 PMDUYA1-PKP-DELIV           VALUE '500'.    
                         88 PMDUYA1-NAMED-DRIVER        VALUE '525'.    
                         88 PMDUYA1-DRIVE-AWAY-COLL     VALUE '550'.    
                         88 PMDUYA1-FALSE-PRETENSE      VALUE '700'.    
                         88 PMDUYA1-FIRE                VALUE '936'.    
                         88 PMDUYA1-FIRE-THEFT          VALUE '937'.    
                         88 PMDUYA1-COMP                VALUE '946'.    
                         88 PMDUYA1-SPEC-PERIL          VALUE '960'.    
                         88 PMDUYA1-LTD-SPEC-PERIL      VALUE '961'.    
                         88 PMDUYA1-COLLISION           VALUE '970'.    
                         88 PMDUYA1-BLANKET-COLL        VALUE '971'.    
                         88 PMDUYA1-RU-D-S-FORMS        VALUE           
                            '138'.                                      
                         88 PMDUYA1-RU-UM-UNDER-CODES   VALUE '108'     
                            '109' '110' '111' '112' '117'.              
                   20 PMDUYA1-RU-CONTINUED.                             
                      25 FILLER                         PIC X(02).      
                      25 PMDUYA1-DPD-TYPE-VALUE         PIC X(01).      
                         88 PMDUYA1-DEA-TYPE            VALUE '1'.      
                         88 PMDUYA1-NON-TYPE            VALUE '2'.      
                         88 PMDUYA1-DPD-BUILDING        VALUE '1'.      
                         88 PMDUYA1-DPD-STD-OPN-LOT     VALUE '2'.      
                         88 PMDUYA1-DPD-NON-STD-OPN-LOT VALUE '3'.      
                         88 PMDUYA1-DPD-BLDG-MISC-TYPE  VALUE '4'.      
                         88 PMDUYA1-DPD-OPEN-LOT-MISC   VALUE '5'.      
                         88 PMDUYA1-DPD-OTHER           VALUE '6'.      
                   20 PMDUYA1-SEQUENCE-RISK-UNIT        PIC X(02).      
             10 PMDUYA1-ITEM-EFFECTIVE-DATE             PIC 9(08).
             10 PMDUYA1-VARIABLE-KEY.                                   
                15 PMDUYA1-ADD-INTEREST-CODE.                           
                   20 PMDUYA1-ADD-INTEREST-TYPE         PIC X(01).      
                   20 PMDUYA1-ADD-INTEREST-SEQ-A.                       
                      25 PMDUYA1-ADD-INTEREST-SEQ       PIC 9(02).      
                15 FILLER                               PIC X(03).      
             10 PMDUYA1-PROCESS-DATE                    PIC 9(08).
           05 PMDUYA1-SEGMENT-DATA.                                     
              10 PMDUYA1-EXPIRATION                  PIC 9(08).
              10 PMDUYA1-BASE-RATE-A.                                   
                 15 PMDUYA1-BASE-RATE                PIC 9(05)V9(04).   
              10 PMDUYA1-BASE-RATE-MGIND             PIC X.             
              10 PMDUYA1-DED-A.                                         
                 15 PMDUYA1-DED                      PIC 9(01)V9(03).   
              10 PMDUYA1-DED-MGIND                   PIC X.             
              10 PMDUYA1-ILF-A.                                         
                 15 PMDUYA1-ILF                      PIC 9(01)V9(03).   
              10 PMDUYA1-ILF-MGIND                   PIC X.             
              10 PMDUYA1-RMF-A.                                         
                 15 PMDUYA1-RMF                      PIC 9(01)V9(03).   
              10 PMDUYA1-RMF-MGIND                   PIC X.             
              10 PMDUYA1-MOD-RATE-A.                                    
                 15 PMDUYA1-MOD-RATE                 PIC 9(05)V9(03).   
              10 PMDUYA1-MOD-RATE-MGIND              PIC X.             
              10 PMDUYA1-EST-TM-PREM-A.                                 
                 15 PMDUYA1-EST-TM-PREM              PIC S9(09)V9(02).  
              10 PMDUYA1-EST-TM-PREM-MGIND           PIC X.             
              10 PMDUYA1-DEP-PREM-A.                                    
                 15 PMDUYA1-DEP-PREM                 PIC 9(09)V9(02).   
              10 PMDUYA1-DEP-PREM-MGIND              PIC X.             
              10 PMDUYA1-RATED-STATE                 PIC X(02).         
              10 PMDUYA1-RATED-TERRITORY             PIC X(03).         
              10 PMDUYA1-NON-FRANCHISE-FAC-A.                           
                 15 PMDUYA1-NON-FRANCHISE-FACTOR     PIC 9(01)V9(02).   
              10 PMDUYA1-BASE-RATE-2-A.                                 
                 15 PMDUYA1-BASE-RATE-2              PIC 9(05)V9(04).   
              10 PMDUYA1-BASE-RATE-3-A.                                 
                 15 PMDUYA1-BASE-RATE-3              PIC 9(05)V9(04).   
              10 PMDUYA1-MOD-RATE-2-A.                                  
                 15 PMDUYA1-MOD-RATE-2               PIC 9(05)V9(03).   
              10 PMDUYA1-MOD-RATE-3-A.                                  
                 15 PMDUYA1-MOD-RATE-3               PIC 9(05)V9(03).   
              10 PMDUYA1-BLANKET-ADJ-FAC-A.                             
                 15 PMDUYA1-BLANKET-ADJ-FACTOR       PIC 9(01)V9(03).   
              10 PMDUYA1-GKLL-EXC-FAC-A.                                
                 15 PMDUYA1-GKLL-EXC-FAC             PIC 9(01)V9(03).   
              10 PMDUYA1-RATING-ID-CODE              PIC X(01).         
              10 PMDUYA1-VALUE-PER-UNIT              PIC X(01).         
              10 PMDUYA1-RATING-CLASS-CODE           PIC X(04).         
              10 PMDUYA1-AGGREGATE-FAC-A.                               
                 15 PMDUYA1-AGGREGATE-FACTOR         PIC 9(01)V9(03).   
              10 PMDUYA1-EXPOS-BASIS-A.                                 
                 15 PMDUYA1-EXPOS-UNITS              PIC 9(08)V9(02).   
              10 PMDUYA1-CALC-PREM-FCTR-A.                              
                 15 PMDUYA1-CALC-PREM-FCTR           PIC 9(01)V9(03).   
              10 PMDUYA1-COMP-DEV-A.                                    
                 15 PMDUYA1-COMP-DEV                 PIC 9(01)V9(03).   
              10 PMDUYA1-REG-PLATES-FAC-A.                              
                 15 PMDUYA1-REG-PLATES-FAC           PIC 9(02)V9(02).   
              10 PMDUYA1-REPORT-BASIS-VALS-A.                           
                 15 PMDUYA1-REPORTING-BASIS-IND      PIC X(01).         
                    88 PMDUYA1-REPORTING-BASIS       VALUE 'Y'.         
                 15 PMDUYA1-COVERAGE-VALUE-A.                           
                    20 PMDUYA1-COVERAGE-VALUE        PIC 9(10).         
                 15 PMDUYA1-COVERAGE-VALUE-MGIND     PIC X(01).         
                 15 PMDUYA1-ACTUAL-VALUE-A.                             
                    20 PMDUYA1-ACTUAL-VALUE          PIC 9(10).         
                 15 PMDUYA1-ACTUAL-VALUE-MGIND       PIC X(01).         
              10 PMDUYA1-LOSS-COST-A.                                   
                 15 PMDUYA1-LOSS-COST                PIC 9(01)V9(03).   
              10 PMDUYA1-ISO-RDF-A.                                     
                 15 PMDUYA1-ISO-RDF                  PIC 9(01)V9(03).   
              10 PMDUYA1-ISO-RDF-MGIND               PIC X.             
              10 PMDUYA1-SERV-ADDL-MIN-PREM-A.                          
                 15 PMDUYA1-SERV-ADDL-MIN-PREM       PIC 9(04).         
              10 PMDUYA1-COVERAGE-RATE-BOOK-ID       PIC X(01).         
              10 PMDUYA1-NYF-FEE                     PIC 9(03)V9(02).   
              10 PMDUYA1-PMS-FUTURE-USE              PIC X(69).         
              10 PMDUYA1-CUSTOMER-USE                PIC X(34).         
              10 PMDUYA1-YR2000-CUST-USE             PIC X(100).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).
