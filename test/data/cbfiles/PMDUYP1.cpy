        01 PMDUYP1-RATE-OVERRIDE-RECORD.
           03 PMDUYP1-RATE-OVERRIDE-SEG.                                
            05 PMDUYP1-SEGMENT-KEY.                                     
              07 PMDUYP1-SEGMENT-ID                   PIC X(02).        
                 88 PMDUYP1-SEGMENT-43                 VALUE '43'.      
              07 PMDUYP1-SEGMENT-STATUS               PIC X(01).        
                 88 PMDUYP1-ACTIVE-STATUS              VALUE 'A'.       
                 88 PMDUYP1-DELETED-STATUS             VALUE 'D'.       
                 88 PMDUYP1-HISTORY-STATUS             VALUE 'H'.       
                 88 PMDUYP1-NOT-ACTIVE-STATUS          VALUE 'H' 'D'.   
                 88 PMDUYP1-RENEWAL-FS-STATUS          VALUE 'R' 'F'.   
              07 PMDUYP1-TRANSACTION-DATE          PIC 9(08).
              07 PMDUYP1-SEGMENT-ID-KEY.                                
                 09 PMDUYP1-SEGMENT-LEVEL-CODE        PIC X(01).        
                    88 PMDUYP1-SEG-LEVEL-U             VALUE 'U'.       
                 09 PMDUYP1-SEGMENT-PART-CODE         PIC X(01).        
                    88 PMDUYP1-SEG-PART-Y              VALUE 'Y'.       
                 09 PMDUYP1-SUB-PART-CODE             PIC X(01).        
                 09 PMDUYP1-INSURANCE-LINE            PIC X(02).        
              07 PMDUYP1-LEVEL-KEY.                                     
                 09 PMDUYP1-LOCATION-NUMBER-A.                          
                    11 PMDUYP1-LOCATION-NUMBER        PIC 9(04).        
                       88 PMDUYP1-TERRORISM-COV-LOC   VALUE 0911.       
                 09 PMDUYP1-SUB-LOCATION-NUMBER-A.                      
                    11 PMDUYP1-SUB-LOCATION-NUMBER    PIC 9(03).        
                 09 PMDUYP1-RISK-UNIT-GROUP-KEY.                        
                    11 PMDUYP1-PMS-DEF-SUBJ-OF-INS-A.                   
                       13 PMDUYP1-PMS-DEF-SUBJ-OF-INS PIC 9(03).        
                          88 PMDUYP1-BUILDING             VALUE 100.    
                          88 PMDUYP1-FIRE-LEGAL-BLDG      VALUE 101.    
                          88 PMDUYP1-BUILDERS-RISK        VALUE 102.    
                          88 PMDUYP1-BLKT-BLDG            VALUE 103.    
                          88 PMDUYP1-TRAILER              VALUE 104.    
                          88 PMDUYP1-CONTENTS             VALUE 110.    
                          88 PMDUYP1-FIRE-LEGAL-CONTENTS  VALUE 111.    
                          88 PMDUYP1-FURN-FIX             VALUE 112.    
                          88 PMDUYP1-BLKT-CONTS           VALUE 113.    
                          88 PMDUYP1-MACH-EQUIP           VALUE 114.    
                          88 PMDUYP1-BLKT-BLDG-CONTS      VALUE 115.    
                          88 PMDUYP1-PERS-PROP-EXC-STOCK  VALUE 116.    
                          88 PMDUYP1-BLKT-P-PR-EXC-STOCK  VALUE 117.    
                          88 PMDUYP1-PERS-PROP-OTHERS     VALUE 118.    
                          88 PMDUYP1-TRAILER-CONTS        VALUE 119.    
                          88 PMDUYP1-STOCK                VALUE 120.    
                          88 PMDUYP1-BLKT-STOCK           VALUE 123.    
                          88 PMDUYP1-COMMODITIES          VALUE 121.    
                          88 PMDUYP1-TREES-PLANTS-SHRUBS  VALUE 122.    
                          88 PMDUYP1-TENANTS-IMPROVEMENTS VALUE 124.    
                          88 PMDUYP1-BLKT-P-PR-INC-STOCK  VALUE 125.    
                          88 PMDUYP1-CONTS-EXC-PLANTS     VALUE 126.    
                          88 PMDUYP1-RADIO-TV-TOWERS      VALUE 128.    
                          88 PMDUYP1-OFFICE-CONTENTS      VALUE 133.    
                          88 PMDUYP1-YARD                 VALUE 135.    
                          88 PMDUYP1-ENH-ENDORSEMENT      VALUE 140.    
                          88 PMDUYP1-REPUBLIC-PLUS        VALUE 141.    
                          88 PMDUYP1-EQ-BUILDING          VALUE 145.
                          88 PMDUYP1-EQ-CONTENTS          VALUE 146.
                          88 PMDUYP1-EQ-TIME-ELEM         VALUE 147.
                          88 PMDUYP1-EQ-BUS-INC           VALUE 148.
                          88 PMDUYP1-EQ-EXTRA-EXP         VALUE 149.
                          88 PMDUYP1-EQ-RENTAL-VAL        VALUE 150.
                          88 PMDUYP1-EQ-STOCK             VALUE 151.
                          88 PMDUYP1-EQ-LLB               VALUE 152.
                          88 PMDUYP1-EQ-LLC               VALUE 153.
                          88 PMDUYP1-EQ-PPO               VALUE 154.
                          88 PMDUYP1-REPUBLIC-ENDSMNTS    VALUE 140     
                             141
                             180
                             181.
                          88 PMDUYP1-EQ-COV               VALUE 145
                              146 147 148 149 150 151 152 153 154.
                          88 PMDUYP1-BED-AND-BRKFAST      VALUE 180.
                          88 PMDUYP1-RESTAURANT-PROGRAM   VALUE 181.
                          88 PMDUYP1-BLKT-BUS-INC         VALUE 173.    
                          88 PMDUYP1-BLKT-RENTAL-BUS-INC  VALUE 175.    
                          88 PMDUYP1-CLASS-DESC-NOT-REQ   VALUE 122     
                             128 129 190 192 194.                       
                          88 PMDUYP1-BLKT-SUBJ-OF-INS     VALUE 103     
                             117 123 125 173 175                        
                             113 115.                                   
                 09 PMDUYP1-RISK-UNIT.                                  
                    11 PMDUYP1-PMS-DEF-COV-CODE-A.                      
                       13 PMDUYP1-PMS-DEF-COVERAGE-CODE  PIC 9(03).     
                    11 FILLER                         PIC 9(03).        
              07 PMDUYP1-ITEM-EFFECTIVE-DATE          PIC 9(08).
              07 PMDUYP1-VARIABLE-KEY.                                  
                 09 PMDUYP1-PEAK-SEASON-INDICATOR     PIC X(01).        
                    88 PMDUYP1-PEAK-SEASON-NO           VALUE '0'.      
                    88 PMDUYP1-PEAK-SEASON-YES          VALUE 'P'.      
                 09 FILLER                            PIC X(05).        
              07 PMDUYP1-PROCESS-DATE          PIC 9(08).
             05 PMDUYP1-SEGMENT-DATA.                                   
                 09 PMDUYP1-RATE-BOOK-ID              PIC X(01).        
                 09 PMDUYP1-VACANCY-FACTOR            PIC V99.          
                 09 PMDUYP1-INCIDENTAL-FACTOR         PIC 9V999.        
                 09 PMDUYP1-EXPIRATION-DATE          PIC 9(08).
                 09 PMDUYP1-MANUAL-GENERATE-IND       PIC X(01).        
                 09 PMDUYP1-BASE-RATE-A.                                
                    11 PMDUYP1-BASE-RATE              PIC 9(03)V9(03).  
                 09 PMDUYP1-MOD-RAT-MG-IND            PIC X(01).        
                 09 PMDUYP1-MODIFIED-RATE-A.                            
                    11 PMDUYP1-MODIFIED-RATE          PIC 9(03)V9(03).  
                 09 PMDUYP1-PKG-MOD-MG-IND            PIC X(01).        
                 09 PMDUYP1-PKG-MOD-FACTOR-A.                           
                    11 PMDUYP1-PKG-MOD-FACTOR         PIC 9(01)V9(03).  
                 09 PMDUYP1-BLDG-RLAF-MG-IND          PIC X(01).        
                 09 PMDUYP1-BLDG-RLAF-A.                                
                    11 PMDUYP1-BLDG-RLAF              PIC 9(01)V9(03).  
                 09 PMDUYP1-DED-FCTR-MG-IND           PIC X(01).        
                 09 PMDUYP1-DEDUCTIBLE-FACTOR-A.                        
                    11 PMDUYP1-DEDUCTIBLE-FACTOR      PIC 9(01)V9(03).  
                 09 PMDUYP1-WIND-HAIL-FTR-A.                            
                    11 PMDUYP1-WIND-HAIL-FTR          PIC 9(01)V9(03).  
                 09 PMDUYP1-CONTS-RLAF-MG-IND         PIC X(01).        
                 09 PMDUYP1-CONTS-RLAF-A.                               
                    11 PMDUYP1-CONTS-RLAF             PIC 9(01)V9(03).  
                 09 PMDUYP1-CALC-PREM-FCTR-A.                           
                    11 PMDUYP1-CALC-PREM-FCTR         PIC 9(01)V9(03).  
                 09 PMDUYP1-TERM-FACTOR-A.                              
                    11 PMDUYP1-TERM-FACTOR            PIC 9(01)V9(03).  
                 09 PMDUYP1-PREM-TM-MG-IND            PIC X(01).        
                 09 PMDUYP1-PREMIUM-TERM-A.                             
                    11 PMDUYP1-PREMIUM-TERM           PIC 9(09).        
                 09 PMDUYP1-SPECIAL-FORM-PREMIUM-A.                     
                    11 PMDUYP1-SPECIAL-FORM-PREMIUM   PIC 9(09).        
                 09 PMDUYP1-TERR-MULT-MG-IND          PIC X(01).        
                 09 PMDUYP1-TERR-MULTIPLIER-A.                          
                    11 PMDUYP1-TERR-MULTIPLIER        PIC 9(01)V9(03).  
                 09 PMDUYP1-COINSURANCE-FACTOR-A.                       
                    11 PMDUYP1-COINSURANCE-FACTOR     PIC 9(01)V9(03).  
                 09 PMDUYP1-INFLATION-GUARD-FAC-A.                      
                    11 PMDUYP1-INFLATION-GUARD-FACTOR PIC 9(01)V9(03).  
                 09 PMDUYP1-AGREED-VALUE-FACTOR-A.                      
                    11 PMDUYP1-AGREED-VALUE-FACTOR    PIC 9(01)V9(03).  
                 09 PMDUYP1-LEGAL-LIAB-FACTOR-A.                        
                    11 PMDUYP1-LEGAL-LIABILITY-FACTOR PIC 9(01)V9(03).  
                 09 PMDUYP1-BUS-INC-DATA.                               
                    11 PMDUYP1-MONTHLY-LIMIT-FACTOR-A.                  
                      13 PMDUYP1-MONTHLY-LIMIT-FACTOR   PIC 9(01)V9(03).
                    11 PMDUYP1-EXT-PER-INDEM-FCTR-A.                    
                      13 PMDUYP1-EXT-PER-INDEMNITY-FCTR PIC 9(01)V9(03).
                    11 PMDUYP1-PAYROLL-EXCL-FCTR-A.                     
                      13 PMDUYP1-PAYROLL-EXCLUSION-FCTR PIC 9(01)V9(03).
                 09 PMDUYP1-COMPANY-DEVIATION-A.                        
                    11 PMDUYP1-COMPANY-DEVIATION      PIC 9(01)V9(03).  
                 09 PMDUYP1-MUNICIPAL-TAX-FCTR-A.                       
                    11 PMDUYP1-MUNICIPAL-TAX-FCTR     PIC 9(01)V9(03).  
                 09 PMDUYP1-COMPLETED-VALUE-FCTR-A.                     
                    11 PMDUYP1-COMPLETED-VALUE-FCTR   PIC 9(01)V9(03).  
                 09 PMDUYP1-SPRINK-LEAK-EXC-FCTR-A.                     
                    11 PMDUYP1-SPRINK-LEAK-EXC-FCTR   PIC 9(01)V9(03).  
                 09 PMDUYP1-VANDALISM-EXC-RATE-A.                       
                    11 PMDUYP1-VANDALISM-EXC-RATE     PIC V9(03).       
                 09 PMDUYP1-RDF-A.                                      
                    11 PMDUYP1-RDF                    PIC 9(01)V9(03).  
                 09 PMDUYP1-RMF-A.                                      
                    11 PMDUYP1-RMF                    PIC 9(01)V9(03).  
                 09 PMDUYP1-MODIFIED-THEFT-INC-A.                       
                    11 PMDUYP1-MODIFIED-THEFT-INCRMNT PIC 9(10)V9(02).  
                 09 PMDUYP1-THEFT-INCREMENT           PIC 9(10)V9(02).  
                 09 PMDUYP1-PROT-CLASS-MULT           PIC 9(03)V9(03).  
                 09 PMDUYP1-AVERAGE-RATE-IND          PIC X(01).        
                    88 PMDUYP1-BLANKET-AVERAGE-RATE   VALUE 'B'.        
                    88 PMDUYP1-HIGHEST-AVERAGE-RATE   VALUE 'H'.        
                    88 PMDUYP1-BLANKET-TENTATIVE-RATE VALUE 'T'.        
                 09 PMDUYP1-P-PROP-RATE               PIC 9(03)V9(03).  
                 09 PMDUYP1-P-PROP-MIN-IND            PIC X(01).        
                 09 PMDUYP1-RDF-MG-IND                PIC X(01).        
                 09 PMDUYP1-BUS-INC-FTR               PIC 9(01)V9(03).  
                 09 PMDUYP1-MIN-RATE                  PIC 9(03)V9(03).  
                 09 PMDUYP1-DEC-CHANGE-FLAG           PIC X(01).        
                    88 PMDUYP1-DEC-CHANGE-NEEDED      VALUE 'A'.        
                    88 PMDUYP1-DEC-CHANGE-NOTED       VALUE 'B'.        
                 09 PMDUYP1-MASS-DIST-RATE            PIC 9(01)V9(03).  
                 09 PMDUYP1-MASS-TEN-RELOCATE.                          
                    11 PMDUYP1-MASS-TEN-RELO-PREM     PIC 9(05).        
                    11 PMDUYP1-MASS-TEN-RELO-RATE     PIC 9(03)V9(03).  
                    11 PMDUYP1-MASS-TEN-RELO-RDF-A.                     
                       13 PMDUYP1-MASS-TEN-RELO-RDF   PIC 9(01)V9(03).  
                 09 PMDUYP1-BLRSK-RATE-FACTOR         PIC 9(03)V9(03).  
                 09 PMDUYP1-LOSS-COST-MULT            PIC 9(03)V9(03).  
                 09 PMDUYP1-MOD-THFT-INC-MG-IND       PIC X(01).        
                 09 PMDUYP1-RX-INDICATOR              PIC X(01).        
                    88  RX-IND-SW                          VALUE 'Y'.   
                 09 PMDUYP1-PMS-FUTURE-USE            PIC X(45).        
                 09 PMDUYP1-KEY-RATE                  PIC 9V99.         
                 09 PMDUYP1-KEY-RATE-ADJ              PIC 9V999.        
                 09 PMDUYP1-BASIC-GRPI-FCT            PIC 9V999.        
                 09 PMDUYP1-TERRCOV                   PIC X(01).        
                 09 PMDUYP1-CUSTOMER-USE              PIC X(18).        
                 05 PMDUYP1-EQ-COVERAGE-TYPE          PIC X(01).
                 05 PMDUYP1-EQ-ATTACH-IND             PIC X(01).
                 05 PMDUYP1-EQ-COVERAGE               PIC X(01).
                 05 PMDUYP1-EQ-RISK-ID                PIC X(01).
                 05 PMDUYP1-EQ-CONSTR                 PIC X(02).
                 05 PMDUYP1-EQ-DEDUCTIBLE             PIC X(02).
                 05 PMDUYP1-EQ-RATE-GRADE             PIC X(01).
                 05 PMDUYP1-EQ-SUBLIMIT-EXP           PIC X(07).
                 05 PMDUYP1-EQ-EXPOS-BASE-IND         PIC X(01).
                 05 PMDUYP1-EQ-BCEG-CLASS             PIC X(02).
                 05 PMDUYP1-EQ-SIC                    PIC X(04).
                 05 PMDUYP1-EQ-TERR-ZONE              PIC X(03).
                 05 PMDUYP1-YR2000-CUST-USE           PIC X(74).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).