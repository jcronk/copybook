        01 PMD4J-NAME-ADDR-RECORD.
           03 PMD4J-NAME-ADDR-SEG.                                      
            05 PMD4J-SEGMENT-KEY.                                       
              07 PMD4J-SEGMENT-ID                   PIC X(02).          
                 88 PMD4J-SEGMENT-43                 VALUE '43'.        
              07 PMD4J-SEGMENT-STATUS               PIC X(01).          
                 88 PMD4J-ACTIVE-STATUS              VALUE 'A'.         
                 88 PMD4J-DELETED-STATUS             VALUE 'D'.         
                 88 PMD4J-PRORATA-DELETED            VALUE 'P'.         
                 88 PMD4J-HISTORY-STATUS             VALUE 'H'.         
                 88 PMD4J-NOT-ACTIVE-STATUS          VALUE 'H' 'D'.     
                 88 PMD4J-RENEWAL-FS-STATUS          VALUE 'R' 'F'.     
              07 PMD4J-TRANSACTION-DATE             PIC 9(08).                                
              07 PMD4J-SEGMENT-ID-KEY.                                  
                 09 PMD4J-SEGMENT-LEVEL-CODE        PIC X(01).          
                    88 PMD4J-SEG-LEVEL-G             VALUE 'G'.         
                    88 PMD4J-SEG-LEVEL-J             VALUE 'J'.         
                    88 PMD4J-SEG-LEVEL-L             VALUE 'L'.         
                 09 PMD4J-SEGMENT-PART-CODE         PIC X(01).          
                    88 PMD4J-SEG-PART-J              VALUE 'J'.         
                 09 PMD4J-SUB-PART-CODE             PIC X(01).          
                 09 PMD4J-INSURANCE-LINE            PIC X(02).          
                    88 PMD4J-WC-OR-COM-LINE          VALUE 'WC' '  '.   
              07 PMD4J-LEVEL-KEY.                                       
                 09 PMD4J-LOCATION-NUMBER           PIC X(04).          
                 09 PMD4J-SUB-LOCATION-NUMBER       PIC X(03).          
                 09 PMD4J-RISK-UNIT-GROUP-KEY.                          
                    11 PMD4J-RISK-UNIT-GROUP        PIC 9(03).          
                    11 PMD4J-SEQ-RSK-UNT-GRP        PIC 9(03).          
                 09 PMD4J-RISK-UNIT                 PIC X(06).          
                 09 PMD4J-SEQUENCE-RISK-UNIT.                           
                    11 PMD4J-RISK-SEQUENCE          PIC 9(01).          
                    11 PMD4J-RISK-TYPE-IND          PIC X(01).          
              07 PMD4J-ITEM-EFFECTIVE-DATE          PIC 9(08).                             
              07 PMD4J-VARIABLE-KEY.                                    
                 09 PMD4J-USE-CODE                  PIC X(03).          
                    88 PMD4J-LOCATION-ADDR          VALUE 'LOC'.        
                    88 PMD4J-DEC-CHANGE-SEGMENT     VALUE 'END'.        
                    88 PMD4J-ADDL-INSURED             VALUE 'INS'.      
                    88 PMD4J-EXT-ADDL-INSURED         VALUE 'EI '.      
                    88 PMD4J-NAMED-INSURED            VALUE 'NI '.      
                    88 PMD4J-UI-SUBCONTR              VALUE 'UI '.      
                    88 PMD4J-AUDIT-CONTACT            VALUE 'ADC'.      
                    88 PMD4J-LIMITED-INSURED          VALUE 'LI '.      
                    88 PMD4J-CERT-HOLDER              VALUE 'CH '.      
                    88 PMD4J-REWRITE                  VALUE 'RW '.      
                    88 PMD4J-WC-ENT-INCLUDED          VALUE 'INC'.
                    88 PMD4J-WC-ENT-EXCLUDED          VALUE 'EXC'.
                    88 PMD4J-NON-PAY-LET-RECIP       VALUES 'LLP'       
                                                            'LLE'       
                                                            'LP '       
                                                            'MT '       
                                                            'OLC'       
                                                            'OMO'       
                                                            'SM '       
                                                            'TM '       
                                                            'CIN'       
                                                            'COL'       
                                                            'EST'.      
                 09 PMD4J-SEQUENCE-USE-CODE-A.                          
                    11 PMD4J-SEQUENCE-USE-CODE      PIC 9(03).          
              07 PMD4J-PROCESS-DATE                 PIC 9(08).                                    
              05 PMD4J-SEGMENT-DATA.                                    
                 09 PMD4J-USE-CODE-DATA             PIC X(01).          
                    88 PMD4J-JOB-ADDRESS              VALUE 'J'.        
                    88 PMD4J-LOCATION-ADDRESS         VALUE 'L'.        
                 09 PMD4J-NAME-GROUP.                                   
                    11 PMD4J-SORT-NAME-AREA.                            
                       13 PMD4J-SORT-NAME           PIC X(04).          
                       13 PMD4J-TAX-LOC             PIC X(06).          
                    11 PMD4J-NAME-TYPE-IND          PIC X(01).          
                    11 PMD4J-ADDRESS-LINE-1-2.                          
                       13 PMD4J-ADDRESS-LINE-1      PIC X(30).          
                       13 PMD4J-ADDRESS-LINE-2      PIC X(30).                         
                     11 PMD4J-ADDRESS-LINE-3          PIC X(30).        
                     11 PMD4J-ADDRESS-LINE-4          PIC X(30).        
                     11 PMD4J-ID-NUMBER               PIC X(13).        
                     11 PMD4J-ZIP-POSTAL-CODE.                          
                        13 PMD4J-ZIP-USA.                               
                           15 PMD4J-ZIP-BASIC         PIC X(05).        
                           15 PMD4J-ZIP-DASH          PIC X(01).        
                           15 PMD4J-ZIP-EXPANDED      PIC 9(04).        
                 09 PMD4J-PHONE.                                        
                      11 PMD4J-PHONE-AREA-A.                            
                         13 PMD4J-PHONE-AREA           PIC X(03).       
                      11 PMD4J-PHONE-EXCHANGE-A.                        
                         13 PMD4J-PHONE-EXCHANGE       PIC X(03).       
                      11 PMD4J-PHONE-NUMBER-A.                          
                         13 PMD4J-PHONE-NUMBER         PIC X(04).       
                      11 PMD4J-PHONE-EXTENSION         PIC 9(04).       
                 09 PMD4J-INTEREST-ITEM                PIC X(30).
                 09 PMD4J-LOC-LINE-EXCLUSION           PIC X(01).       
                    88 PMD4J-GL-EXCLUDED               VALUE 'Y'.       
                 09 PMD4J-SURCHARGE                    PIC X(01).       
                    88 PMD4J-SURCHARGE-APPLIES         VALUE 'Y'.       
                 09 PMD4J-MUNICIPAL-TAX                PIC X(01).       
                    88 PMD4J-MUN-TAX-APPLIES           VALUE 'Y'.       
                 09 PMD4J-02-AUDIT-CODE                PIC X(01).       
                 09 PMD4J-02-LEGAL-ENTITY              PIC X(01).       
                 09 PMD4J-DEC-CHANGE-FLAG              PIC X(01).       
                    88 PMD4J-DEC-CHANGE-NEEDED         VALUE 'A'.       
                    88 PMD4J-DEC-CHANGE-NOTED          VALUE 'B'.       
                 09 PMD4J-BLKT-MISC-LOC-EXCLUSION      PIC X(01).       
                    88 PMD4J-BLKT-MISC-LOC-EXCLUDED    VALUE 'Y'.       
                    88 PMD4J-BLKT-MISC-LOC-NOT-EXCL    VALUES 'N' ' '.  
                 09 PMD4J-LOCATION-STATE               PIC X(02).       
                 09 PMD4J-MATCHING-LOC-SW              PIC X(01).       
                    88 PRINT-THIS-ADDRESS              VALUE 'Y'.       
                 09 PMD4J-ITEM-EXPIRE-DATE             PIC X(08).                             
                 09 PMD4J-PMS-FUTURE-USE               PIC X(52).       
                 09 PMD4J-NUM-OF-EMPLOYEES             PIC X(03).       
                 09 PMD4J-STREET-ADR-LOCATION          PIC X(01).       
                    88 PMD4J-STREET-ADR-NEW-FORMAT     VALUE 'N'.       
                 09 PMD4J-O-NUM-OF-EMPLOYEES             PIC X(03).     
                 09 PMD4J-CUST-SPL-USE                 PIC X(17).       
                 09 PMD4J-YR2000-CUST-USE              PIC X(100).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).