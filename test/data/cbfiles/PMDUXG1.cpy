        01 PMDUXG1-UNIT-RATING-RECORD.
           03 PMDUXG1-UNIT-RATING-SEG.                                  
            05 PMDUXG1-SEGMENT-KEY.                                     
              07 PMDUXG1-SEGMENT-ID                   PIC X(02).        
                 88 PMDUXG1-SEGMENT-43                 VALUE '43'.      
              07 PMDUXG1-SEGMENT-STATUS               PIC X(01).        
                 88 PMDUXG1-ACTIVE-STATUS              VALUE 'A'.       
                 88 PMDUXG1-DELETED-STATUS             VALUE 'D'.       
                 88 PMDUXG1-HISTORY-STATUS             VALUE 'H'.       
                 88 PMDUXG1-NOT-ACTIVE-STATUS          VALUE 'H' 'D'.   
                 88 PMDUXG1-RENEWAL-FS-STATUS          VALUE 'R' 'F'.   
              07 PMDUXG1-TRANSACTION-DATE             PIC 9(08).
              07 PMDUXG1-SEGMENT-ID-KEY.                                
                 09 PMDUXG1-SEGMENT-LEVEL-CODE        PIC X(01).        
                    88 PMDUXG1-SEG-LEVEL-U             VALUE 'U'.       
                 09 PMDUXG1-SEGMENT-PART-CODE         PIC X(01).        
                    88 PMDUXG1-SEG-PART-X              VALUE 'X'.       
                 09 PMDUXG1-SUB-PART-CODE             PIC X(01).        
                 09 PMDUXG1-INSURANCE-LINE            PIC X(02).        
              07 PMDUXG1-LEVEL-KEY.                                     
                 09 PMDUXG1-LOCATION-NUMBER-ALPHA.                      
                    11 PMDUXG1-LOCATION-NUMBER        PIC 9(04).        
                 09 PMDUXG1-RISK-UNIT-GROUP-KEY.                        
                    11 PMDUXG1-PMS-DEF-GL-SUBLINE     PIC 9(03).        
                       88 PMDUXG1-PREMOPS-PRODUCTS     VALUE '340'.     
                       88 PMDUXG1-OWNERS-CONTRACT-PROT VALUE '345'.     
                       88 PMDUXG1-RAILROAD-PROTECTIVE  VALUE '346'.     
                       88 PMDUXG1-PRINCIPAL-PROTECTIVE VALUE '347'.     
                       88 PMDUXG1-LIQUOR-LIABILITY     VALUE '355'.     
                       88 PMDUXG1-POLLUTION-LIABILITY  VALUE '360'.     
                    11 PMDUXG1-SEQ-RSK-UNT-GRP.                         
                       13 PMDUXG1-CLASS-CODE-GROUP    PIC 9(02).        
                       13 PMDUXG1-CLASS-CODE-MEMBER   PIC 9(01).        
                 09 PMDUXG1-RISK-UNIT.                                  
                    11 PMDUXG1-CLASS-CODE             PIC X(05).        
                       88 PMDUXG1-POLLUTION-EXCLUSION  VALUE '90110'.   
                       88 PMDUXG1-LIQUOR-EXCLUSION     VALUE '70416'.   
                       88 PMDUXG1-CONDOMINIUMS         VALUE '62000'    
                                             '62001' '62002' '62003'.   
                       88 PMDUXG1-RESTAURANTS          VALUE '16813'    
                             '16814' '16815' '16816' '16817' '16818'    
                             '16819' '16820' '16821' '16822' '16823'    
                             '16824'.                                   
                    11 FILLER                         PIC X(01).        
                 09 PMDUXG1-SEQUENCE-RISK-UNIT.                         
                    11 PMDUXG1-RISK-SEQUENCE          PIC 9(01).        
                    11 PMDUXG1-RISK-TYPE-IND          PIC X(01).        
                       88 PMDUXG1-PREMISES-OPERATIONS  VALUE 'O'.       
                       88 PMDUXG1-PRODUCTS             VALUE 'P'.       
              07 PMDUXG1-ITEM-EFFECTIVE-DATE          PIC 9(08).
              07 PMDUXG1-VARIABLE-KEY.                                  
                 09 PMDUXG1-ADD-INTEREST-CODE.                          
                    11 PMDUXG1-ADD-INTEREST-TYPE      PIC X(01).        
                    11 PMDUXG1-ADD-INTEREST-SEQ-A.                      
                       13 PMDUXG1-ADD-INTEREST-SEQ    PIC 9(02).        
                 09 FILLER                            PIC X(03).        
              07 PMDUXG1-PROCESS-DATE                 PIC 9(08).
             05 PMDUXG1-SEGMENT-DATA.                                   
                 09 PMDUXG1-EXPOSURE-AMOUNT-ALPHA.                      
                    11 PMDUXG1-EXPOSURE-AMOUNT        PIC 9(10).        
                 09 PMDUXG1-RATING-CLASS              PIC X(05).        
                 09 PMDUXG1-INC-LIMIT-TABLE-IND       PIC X(01).        
                 09 PMDUXG1-CLASS-DESC-SOURCE         PIC X(01).        
                    88 PMDUXG1-CLASS-TABLE          VALUE 'C'.          
                    88 PMDUXG1-CLASS-DESC-TABLE     VALUE 'D'.          
                    88 PMDUXG1-CLASS-ON-SEGMENT     VALUE 'S'.          
                 09 PMDUXG1-YEAR-IN-TRANSITION        PIC 9(01).        
                    88 PMDUXG1-TRANSITION-MATURE    VALUE 9.            
                 09 FILLER                            PIC X(07).        
                 09 PMDUXG1-EXPOSURE-INDICATOR        PIC X(01).        
                    88 PMDUXG1-AT-AUDIT             VALUE 'A'.          
                    88 PMDUXG1-FLAT-CHARGE          VALUE 'F'.          
                    88 PMDUXG1-IF-ANY               VALUE 'I'.          
                    88 PMDUXG1-PRO-RATA-DELETE      VALUE 'P'.          
                 09 PMDUXG1-RATING-STATE              PIC X(02).        
                 09 PMDUXG1-TERRITORY-ALPHA.                            
                    11 PMDUXG1-TERRITORY              PIC 9(03).        
                 09 PMDUXG1-CO-OWN-RSP-LIAB-IND       PIC X(01).        
                 09 PMDUXG1-MTGEE-ASSGN-LIAB-IND      PIC X(01).        
                 09 PMDUXG1-CNTL-INT-IND              PIC X(01).        
                 09 PMDUXG1-OWN-LAND-LESD-IND         PIC X(01).        
                 09 PMDUXG1-OWN-LESE-LIAB-IND         PIC X(01).        
                 09 PMDUXG1-EXECT-ADMIN-TRST-IND      PIC X(01).        
                 09 PMDUXG1-ELECT-OFIC-CORP-IND       PIC X(01).        
                 09 PMDUXG1-GOVT-UNTS-OWN-IND         PIC X(01).        
                 09 PMDUXG1-FIDUC-COV-FIDUC-IND       PIC X(01).        
                 09 PMDUXG1-MBR-OF-CHRCH-IND          PIC X(01).        
                 09 PMDUXG1-OIL-GAS-OPS-OWN-IND       PIC X(01).        
                 09 PMDUXG1-TRST-CLRGY-IND            PIC X(01).        
                 09 PMDUXG1-SPSE-OF-PARTN-IND         PIC X(01).        
                 09 PMDUXG1-PRIV-RES-BI-IND           PIC X(01).        
                 09 PMDUXG1-PRIV-RES-APT-NUM-FAM      PIC 9(01).        
                 09 PMDUXG1-FCLTY-MBR-SCHL-IND        PIC X(01).        
                 09 PMDUXG1-SCHL-GOVT-IMM-IND         PIC X(01).        
                 09 PMDUXG1-PREM-BASIS                PIC X(02).        
                 09 PMDUXG1-EXPOSURE-BASIS            PIC X(01).        
                 09 PMDUXG1-CVG-EXPIR-DAT             PIC 9(08).
                 09 PMDUXG1-SPLIT-COMBINED-DED-IND    PIC X(01).        
                    88 PMDUXG1-SPLIT-DED                VALUE 'S'.      
                    88 PMDUXG1-COMBINED-DED             VALUE 'C'.      
                 09 PMDUXG1-DED-AMOUNT                PIC X(07).        
                 09 PMDUXG1-DEDUCTIBLE-PER            PIC X(01).        
                    88 PMDUXG1-DED-PER-CLAIM              VALUE 'C'.    
                    88 PMDUXG1-DED-PER-OCCUR              VALUE 'O'.    
                 09 PMDUXG1-DED-AMT-PD                PIC X(07).        
                 09 PMDUXG1-DEDUCTIBLE-PER-PD         PIC X(01).        
                    88 PMDUXG1-DED-PER-CLAIM-PD           VALUE 'C'.    
                    88 PMDUXG1-DED-PER-OCCUR-PD           VALUE 'O'.    
                 09 PMDUXG1-PMA-CODE                  PIC X(02).        
                    88 PMDUXG1-PMA-NOT-APPLICABLE         VALUE 'NA'.   
                    88 PMDUXG1-PMA-NOT-ENTERED            VALUE SPACES. 
                 09 PMDUXG1-A-RATES-IND               PIC X(01).        
                    88 PMDUXG1-NO-A-RATE                  VALUE 'A'.    
                    88 PMDUXG1-A-RATE                     VALUE 'G'.    
                    88 PMDUXG1-A-RATE-RANGE               VALUE 'R'.    
                 09 PMDUXG1-FRINGE-TABLE.                               
                    11 PMDUXG1-FRINGE-DATA OCCURS 03 TIMES              
                       INDEXED BY GLFRINGC.                             
                       13 PMDUXG1-FRINGE-CODE         PIC X(01).        
                 09 PMDUXG1-SPECIAL-MED-PAY-IND       PIC X(01).        
                 09 PMDUXG1-SKL-MIN-PREM-IND          PIC X(01).        
                    88 PMDUXG1-STORE-KEEPERS-MINIMUM  VALUE 'Y'.        
                 09 PMDUXG1-PRODUCTS-IND              PIC X(01).        
                    88 PMDUXG1-INCLUDED-PRODUCTS      VALUE 'I'.        
                 09 PMDUXG1-EXT-REPRT-PRD-MONTHS      PIC 9(03).        
                    88 PMDUXG1-PERIOD-ON-GOING          VALUE 999.      
                 09 PMDUXG1-ERP-PERCENT               PIC 9(01)V9(03).  
                 09 PMDUXG1-OLD-BASE-EXPOSURE         PIC 9(10).        
                 09 PMDUXG1-OLD-PREMIUM-BASIS         PIC X(02).        
                    88 PMDUXG1-CLASS-NOT-SUB-TRANS    VALUE SPACES.     
                 09 PMDUXG1-DEC-CHANGE-FLAG           PIC X(01).        
                    88 PMDUXG1-DEC-CHANGE-NEEDED      VALUE 'A'.        
                    88 PMDUXG1-DEC-CHANGE-NOTED       VALUE 'B'.        
                 09 PMDUXG1-EXPO-IND                  PIC X(01).        
                    88 PMDUXG1-EXPO-VALID-DEC         VALUE 'Y'.        
                    88 PMDUXG1-EXPO-INVALID-DEC       VALUE 'N'.        
                 09 PMDUXG1-NO-DEC-IND                PIC X(01).        
                    88 PMDUXG1-NO-DEC                 VALUE 'Y'.        
                 09 PMDUXG1-CLASS-MIN-PREM-A.                           
                    11 PMDUXG1-CLASS-MIN-PREM         PIC 9(04).        
                 09 PMDUXG1-MUN-LIAB-IND              PIC X(01).        
                    88 PMDUXG1-MUN-LIAB-RATE          VALUE 'Y'.        
                 09 PMDUXG1-SKL-MIN-PREM-A.                             
                    11 PMDUXG1-SKL-MIN-PREM           PIC 9(04).        
                 09 PMDUXG1-STAT-TERRITORY-ALPHA.                       
                    11 PMDUXG1-STAT-TERRITORY         PIC 9(03).        
                 09 PMDUXG1-PMSC-FUTURE-USE           PIC X(147).       
                 09 PMDUXG1-DED-OCCUR-MAXIMUM         PIC X(07).        
                 09 PMDUXG1-CUST-FUTURE-USE           PIC X(23).        
                 09 PMDUXG1-YR2000-CUST-USE           PIC X(100).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).