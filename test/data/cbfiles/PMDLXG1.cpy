        01 PMDLXG1-INS-LINE-RATE-RECORD. 
           03 PMDLXG1-INS-LINE-RATE-SEG.                                
            05 PMDLXG1-SEGMENT-KEY.                                     
              07 PMDLXG1-SEGMENT-ID                   PIC X(02).        
                 88 PMDLXG1-SEGMENT-43                 VALUE '43'.      
              07 PMDLXG1-SEGMENT-STATUS               PIC X(01).        
                 88 PMDLXG1-ACTIVE-STATUS              VALUE 'A'.       
                 88 PMDLXG1-DELETED-STATUS             VALUE 'D'.       
                 88 PMDLXG1-HISTORY-STATUS             VALUE 'H'.       
                 88 PMDLXG1-NOT-ACTIVE-STATUS          VALUE 'H' 'D'.   
                 88 PMDLXG1-RENEWAL-FS-STATUS          VALUE 'R' 'F'.   
              07 PMDLXG1-TRANSACTION-DATE             PIC 9(08).                              
              07 PMDLXG1-SEGMENT-ID-KEY.                                
                 09 PMDLXG1-SEGMENT-LEVEL-CODE        PIC X(01).        
                    88 PMDLXG1-SEG-LEVEL-L             VALUE 'L'.       
                 09 PMDLXG1-SEGMENT-PART-CODE         PIC X(01).        
                    88 PMDLXG1-SEG-PART-X              VALUE 'X'.       
                 09 PMDLXG1-SUB-PART-CODE             PIC X(01).        
                 09 PMDLXG1-INSURANCE-LINE            PIC X(02).        
                    88 PMDLXG1-GENERAL-LIABILITY       VALUE 'GL'.      
              07 PMDLXG1-LEVEL-KEY.                                     
                 09 PMDLXG1-LOCATION-NUMBER           PIC 9(04).        
              07 PMDLXG1-ITEM-EFFECTIVE-DATE          PIC 9(08).
              07 PMDLXG1-PROCESS-DATE                 PIC 9(08).
             07 PMDLXG1-SEGMENT-DATA.                                   
                 09 PMDLXG1-COMPANY-NUMBER            PIC 9(02).        
                    88 PMDLXG1-ASSIGN-RISK-COMPANY      VALUE 35.       
                    88 PMDLXG1-JUA-COMPANY              VALUE 18.       
                    88 PMDLXG1-TEXAS-JUA-COMPANY        VALUE 46.       
                 09 PMDLXG1-ORIGINAL-PROCESS-DATE     PIC 9(08).
                 09 PMDLXG1-OCCUR-CLAIMS-MADE-IND     PIC X(01).        
                    88 PMDLXG1-OCCURRENCE               VALUE 'O'.      
                    88 PMDLXG1-CLAIMS-MADE              VALUE 'C'.      
                 09 PMDLXG1-CLMS-MADE-ENT-DTE         PIC 9(08).
                 09 PMDLXG1-RETRO-DATE                PIC 9(08).
                 09 FILLER                            PIC 9(10).        
                 09 PMDLXG1-SPLIT-LIM-IND             PIC X(01).        
                    88 PMDLXG1-SPLIT-LIMITS             VALUE 'Y'.      
                    88 PMDLXG1-COMBINED-LIMIT           VALUE ' '.      
                 09 PMDLXG1-FRINGE-TABLE.                               
                    11 PMDLXG1-FRINGE-DATA OCCURS 10 TIMES              
                       INDEXED BY GLFRINGA.                             
                       13 PMDLXG1-FRINGE-CODE         PIC X(01).        
                       13 PMDLXG1-FRINGE-RATE-A.                        
                          15 PMDLXG1-FRINGE-RATE      PIC 9(02)V9(03).  
                       13 PMDLXG1-FRINGE-RATE-MG-IND  PIC X(01).        
                          88 PMDLXG1-FRINGE-GENERATED   VALUE 'G'.      
                 09 FILLER                            PIC 9(14).        
                 09 PMDLXG1-AUDIT-FREQUENCY-CODE      PIC X(01).        
                    88 PMDLXG1-BLANK-AUDIT-FREQUENCY    VALUE ' '.      
                 09 PMDLXG1-AUDIT-TYPE                PIC X(01).        
                    88 PMDLXG1-AUDIT-BYPASS             VALUE 'B'.      
                    88 PMDLXG1-AUDIT-PHYSICAL           VALUE 'P'.      
                    88 PMDLXG1-AUDIT-UNDEFINED          VALUE 'U' ' '.  
                    88 PMDLXG1-AUDIT-VOLUNTARY          VALUE 'V'.      
                 09 FILLER                            PIC X(14).        
                 09 PMDLXG1-TEXAS-DISC-IND            PIC X(01).        
                    88 PMDLXG1-NO-TEXAS-DISC          VALUE 'N'.        
                 09 PMDLXG1-TEXAS-DISC-MG-IND         PIC X(01).        
                    88 PMDLXG1-TEXAS-DISC-ENTERED     VALUE 'M'.        
                    88 PMDLXG1-TEXAS-DISC-GENERATED   VALUE 'G'.        
                 09 PMDLXG1-TEXAS-DISCOUNT-PCNT-A.                      
                    11 PMDLXG1-TEXAS-DISCOUNT-PERCENT PIC 99V9.         
                 09 PMDLXG1-DEC-CHANGE-FLAG           PIC X(01).        
                    88 PMDLXG1-DEC-CHANGE-NEEDED      VALUE 'A'.        
                    88 PMDLXG1-DEC-CHANGE-NOTED       VALUE 'B'.        
                 09 PMDLXG1-PMA-CODE                  PIC X(02).        
                 09 PMDLXG1-IRPM-EXPER-PERCENT-A.                       
                    11 PMDLXG1-IRPM-EXPER-PERCENT     PIC 9(01)V9(03).  
                 09 PMDLXG1-SCHEDULED-PERCENT-A.                        
                    11 PMDLXG1-SCHEDULED-PERCENT      PIC 9(01)V9(03).  
                 09 PMDLXG1-EXPENSE-PERCENT-A.                          
                    11 PMDLXG1-EXPENSE-PERCENT        PIC 9(01)V9(03).  
                 09 PMDLXG1-OTHER-PERCENT-A.                            
                    11 PMDLXG1-OTHER-PERCENT          PIC 9(01)V9(03).  
                 09 PMDLXG1-COMM-REDUCTION-A.                           
                    11 PMDLXG1-COMM-REDUCTION         PIC 9(01)V9(03).  
                 09 PMDLXG1-COMM-RATE-A.                                
                    11 PMDLXG1-COMM-RATE              PIC 9(01)V9(05).  
                 09 PMDLXG1-RATE-LEVEL-DATE           PIC X(08).        
                 09 PMDLXG1-WORK-SHEET-IND            PIC X(01).        
                 09 PMDLXG1-PMS-FUTURE-USE            PIC X(105).       
                 09 PMDLXG1-CUST-SPL-USE              PIC X(30).        
                 09 PMDLXG1-YR2000-CUST-USE           PIC X(100).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).