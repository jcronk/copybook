        01 PMDUYG1-UNIT-RATE-OVERRIDE-RECORD.
           03 PMDUYG1-UNIT-RATE-OVERRIDE-SEG.                           
            05 PMDUYG1-SEGMENT-KEY.                                     
              07 PMDUYG1-SEGMENT-ID                   PIC X(02).        
                 88 PMDUYG1-SEGMENT-43                 VALUE '43'.      
              07 PMDUYG1-SEGMENT-STATUS               PIC X(01).        
                 88 PMDUYG1-ACTIVE-STATUS              VALUE 'A'.       
                 88 PMDUYG1-DELETED-STATUS             VALUE 'D'.       
                 88 PMDUYG1-HISTORY-STATUS             VALUE 'H'.       
                 88 PMDUYG1-NOT-ACTIVE-STATUS          VALUE 'H' 'D'.   
                 88 PMDUYG1-RENEWAL-FS-STATUS          VALUE 'R' 'F'.   
              07 PMDUYG1-TRANSACTION-DATE          PIC 9(08).
              07 PMDUYG1-SEGMENT-ID-KEY.                                
                 09 PMDUYG1-SEGMENT-LEVEL-CODE        PIC X(01).        
                    88 PMDUYG1-SEG-LEVEL-U             VALUE 'U'.       
                 09 PMDUYG1-SEGMENT-PART-CODE         PIC X(01).        
                    88 PMDUYG1-SEG-PART-Y              VALUE 'Y'.       
                 09 PMDUYG1-SUB-PART-CODE             PIC X(01).        
                 09 PMDUYG1-INSURANCE-LINE            PIC X(02).        
              07 PMDUYG1-LEVEL-KEY.                                     
                 09 PMDUYG1-LOCATION-NUMBER           PIC 9(04).        
                 09 PMDUYG1-RISK-UNIT-GROUP-KEY.                        
                    11 PMDUYG1-PMS-DEF-GL-SUBLINE     PIC 9(03).        
                       88 PMDUYG1-PREMOPS-PRODUCTS     VALUE 340.       
                       88 PMDUYG1-OWNERS-CONTRACT-PROT VALUE 345.       
                       88 PMDUYG1-RAILROAD-PROTECTIVE  VALUE 346.       
                       88 PMDUYG1-PRINCIPAL-PROTECTIVE VALUE 347.       
                       88 PMDUYG1-LIQUOR-LIABILITY     VALUE 355.       
                       88 PMDUYG1-POLLUTION-LIABILITY  VALUE 360.       
                    11 PMDUYG1-SEQ-RSK-UNT-GRP.                         
                       13 PMDUYG1-CLASS-CODE-GROUP    PIC 9(02).        
                       13 PMDUYG1-CLASS-CODE-MEMBER   PIC 9(01).        
                 09 PMDUYG1-RISK-UNIT.                                  
                    11 PMDUYG1-CLASS-CODE             PIC X(05).        
                    11 FILLER                         PIC X(01).        
                 09 PMDUYG1-SEQUENCE-RISK-UNIT.                         
                    11 PMDUYG1-RISK-SEQUENCE          PIC 9(01).        
                    11 PMDUYG1-RISK-TYPE-IND          PIC X(01).        
                       88 PMDUYG1-PREMISES-OPERATIONS  VALUE 'O'.       
                       88 PMDUYG1-PRODUCTS             VALUE 'P'.       
              07 PMDUYG1-ITEM-EFFECTIVE-DATE          PIC 9(08).
              07 PMDUYG1-VARIABLE-KEY.                                  
               08 PMDUYG1-ADD-INTEREST-CODE.                            
                 09 PMDUYG1-ADD-INTEREST-TYPE         PIC X(01).        
                 09 PMDUYG1-ADD-INTEREST-SEQ-A.                         
                    11 PMDUYG1-ADD-INTEREST-SEQ       PIC 9(02).        
                 09 FILLER                            PIC X(03).        
              07 PMDUYG1-PROCESS-DATE          PIC 9(08).
             05 PMDUYG1-SEGMENT-DATA.                                   
                 09 PMDUYG1-MANUAL-GENERATE-IND       PIC X(01).        
                 09 PMDUYG1-BASIC-RATE                PIC 9(05)V9(03).  
                 09 PMDUYG1-BASIC-RATE-MG-IND         PIC X(01).        
                    88 PMDUYG1-MANUAL-BASIC-RATE       VALUE 'M'.       
                    88 PMDUYG1-GENERATED-BASIC-RATE    VALUE 'G'.       
                 09 PMDUYG1-MODIFIED-RATE             PIC 9(05)V9(03).  
                 09 PMDUYG1-MODIFIED-RATE-MG-IND      PIC X(01).        
                 09 PMDUYG1-PKG-MOD-MG-IND            PIC X(01).        
                    88 PMDUYG1-MANUAL-PKG-MOD          VALUE 'M'.       
                    88 PMDUYG1-GENERATED-PKG-MOD       VALUE 'G'.       
                 09 PMDUYG1-PKG-MOD-FACTOR-A.                           
                    11 PMDUYG1-PKG-MOD-FACTOR         PIC 9(01)V9(03).  
                 09 FILLER                            PIC X(04).        
                 09 PMDUYG1-DED-CREDIT-ALPHA.                           
                    11 PMDUYG1-DEDUCTIBLE-CREDIT     PIC 9(01)V9(03).   
                 09 PMDUYG1-DED-CRDT-MG-IND           PIC X(01).        
                    88 PMDUYG1-MANUAL-DED-CRDT         VALUE 'M'.       
                    88 PMDUYG1-GENERATED-DED-CRDT      VALUE 'G'.       
                 09 PMDUYG1-DED-CREDIT-PD-A.                            
                    11 PMDUYG1-DEDUCTIBLE-CREDIT-PD   PIC 9(01)V9(03).  
                 09 PMDUYG1-DED-CRDT-MG-IND-PD        PIC X(01).        
                    88 PMDUYG1-MANUAL-DED-CRDT-PD      VALUE 'M'.       
                    88 PMDUYG1-GENERATED-DED-CRDT-PD   VALUE 'G'.       
                 09 PMDUYG1-CALC-PREM-FCTR-A.                           
                    11 PMDUYG1-CALC-PREM-FCTR         PIC 9(01)V9(03).  
                 09 PMDUYG1-TERM-FACTOR-A.                              
                    11 PMDUYG1-TERM-FACTOR            PIC 9(01)V9(03).  
                 09 PMDUYG1-PREM-TM-MG-IND            PIC X(01).        
                 09 PMDUYG1-PREMIUM-TERM              PIC 9(09).        
                 09 PMDUYG1-PREM-DEPOS-MG-IND         PIC X(01).        
                 09 PMDUYG1-PREMIUM-DEPOSIT           PIC 9(09).        
                 09 PMDUYG1-CLAIMS-M-FACTOR-A.                          
                    11 PMDUYG1-CLAIMS-MADE-FACTOR      PIC 9(01)V9(03). 
                 09 PMDUYG1-INCR-LIM-MG-IND           PIC X(01).        
                    88 PMDUYG1-MANUAL-INCR-LIM         VALUE 'M'.       
                    88 PMDUYG1-GENERATED-INCR-LIM      VALUE 'G'.       
                    88 PMDUYG1-BI-MANUAL-INCR-LIM       VALUE 'M'.      
                    88 PMDUYG1-BI-GENERATED-INCR-LIM    VALUE 'G'.      
                 09 PMDUYG1-INCREASE-L-FACTOR-A.                        
                    11 PMDUYG1-INCREASE-LIMITS-FACTOR  PIC 9(02)V9(03). 
                 09 PMDUYG1-PD-INCR-LIM-MG-IND        PIC X(01).        
                    88 PMDUYG1-PD-MANUAL-INCR-LIM       VALUE 'M'.      
                    88 PMDUYG1-PD-GENERATED-INCR-LIM    VALUE 'G'.      
                 09 PMDUYG1-PD-INCR-L-FACTOR-A.                         
                    11 PMDUYG1-PD-INCR-LIMIT-FACTOR    PIC 9(02)V9(03). 
                 09 PMDUYG1-BI-WEIGHT-MG-IND          PIC X(01).        
                 09 PMDUYG1-BI-WEIGHT-FACTOR-A.                         
                    11 PMDUYG1-BI-WEIGHT-FACTOR       PIC 9(01)V9(03).  
                 09 PMDUYG1-PD-WEIGHT-MG-IND          PIC X(01).        
                 09 PMDUYG1-PD-WEIGHT-FACTOR-A.                         
                    11 PMDUYG1-PD-WEIGHT-FACTOR       PIC 9(01)V9(03).  
                 09 FILLER                            PIC X(01).        
                 09 PMDUYG1-FRINGE-TABLE.                               
                    11 PMDUYG1-FRINGE-DATA OCCURS 03 TIMES              
                       INDEXED BY GLFRINGD.                             
                       13 PMDUYG1-FRINGE-RATE-A.                        
                          15 PMDUYG1-FRINGE-RATE      PIC 9(02)V9(03).  
                       13 PMDUYG1-FRINGE-RATE-MG-IND  PIC X(01).        
                 09 PMDUYG1-RDF-A.                                      
                    11 PMDUYG1-RDF                    PIC 9(01)V9(03).  
                 09 PMDUYG1-RMF-A.                                      
                    11 PMDUYG1-RMF                    PIC 9(01)V9(03).  
                 09 PMDUYG1-TRANSITION-FACTOR         PIC 9(05)V9(03).  
                 09 PMDUYG1-CONSTANT-FACTOR-A.                          
                    11 PMDUYG1-CONSTANT-FACTOR        PIC SV9(03).      
                 09 PMDUYG1-CONSTANT-MG-IND           PIC X(01).        
                 09 PMDUYG1-COVERAGE-CHANGE-A.                          
                    11 PMDUYG1-COVERAGE-CHANGE        PIC 9(02)V9(03).  
                 09 PMDUYG1-RATE-AT-LIMITS-A.                           
                    11 PMDUYG1-RATE-AT-LIMITS         PIC 9(05)V9(03).  
                 09 PMDUYG1-COMPANY-DEVIATION-A.                        
                    11 PMDUYG1-COMPANY-DEVIATION      PIC 9(01)V9(03).  
                 09 PMDUYG1-BI-SPLIT-ILF-A.                             
                    11 PMDUYG1-BI-SPLIT-ILF           PIC 9(02)V9(03).  
                 09 PMDUYG1-PD-SPLIT-ILF-A.                             
                    11 PMDUYG1-PD-SPLIT-ILF           PIC 9(02)V9(03).  
                 09 PMDUYG1-TOTAL-SPLIT-ILF-A.                          
                    11 PMDUYG1-TOTAL-SPLIT-ILF        PIC 9(02)V9(03).  
                 09 PMDUYG1-ADD-INTEREST-FACTOR       PIC 9V999.        
                 09 PMDUYG1-ADD-INT-FACTOR-MG-IND     PIC X(01).        
                    88 PMDUYG1-ADD-INT-FACTOR-MAN     VALUE 'M'.        
                    88 PMDUYG1-ADD-INT-FACTOR-GEN     VALUE 'G'.        
                 09 PMDUYG1-CLAIMS-MADE-MG-IND        PIC X(01).        
                 09 PMDUYG1-AUDIT-TRANS-FACTOR        PIC 9(05)V9(03).  
                 09 PMDUYG1-RDF-MG-IND                PIC X(01).        
                 09 PMDUYG1-DEC-CHANGE-FLAG           PIC X(01).        
                    88 PMDUYG1-DEC-CHANGE-NEEDED      VALUE 'A'.        
                    88 PMDUYG1-DEC-CHANGE-NOTED       VALUE 'B'.        
                 09 PMDUYG1-TRANS-FACTOR-NEW          PIC 9(05)V9(05).  
                 09 PMDUYG1-NY-TORT-FACTOR            PIC 9V9(03).      
                 09 PMDUYG1-LOSS-COST-MULT            PIC 9(03)V9(03).  
                 09 PMDUYG1-PMS-FUTURE-USE            PIC X(67).        
                 09 PMDUYG1-TERRCOV                   PIC X(01).        
                 09 PMDUYG1-CUSTOMER-USE              PIC X(29).        
                 05 PMDUYG1-YR2000-CUST-USE           PIC X(100).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).