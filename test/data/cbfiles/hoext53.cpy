        01 PROPERTY-EXTENSION-RECORD.                                   
           03 PROPERTY-EXTENSION-SEG.                                   
           04 PER-ID                     PIC X(02).                     
              88 PROPERTY-EXTENSION      VALUE '30'.                    
              88 MASS-AUTO-MR            VALUE '30'.                    
           04 PROPERTY-EXTENSION-DATA.                                  
           05 PER-USE-SEQUENCE.       
              10 PER-USE                 PIC X(04).                     
                 88 PER-EXCESS-INCIDENT-FARM-LIAB VALUE 'UIFL'.         
                 88 PER-EXCESS-PERMIT-INCIDEN-LIA VALUE 'UPIO'.         
                 88 PER-EXCLUDE-DESIGNATED-WC    VALUE 'UDWC'.          
                 88 PER-EXCLUDE-DESIGNATED-AUTOS VALUE 'UEDA'.          
                 88 PER-EXCLUDE-DESIGNATED-RVS   VALUE 'UDRV'.          
                 88 PER-EXCLUDE-NAMED-DRIVER     VALUE 'UEND'.          
                 88 PER-EXCL-1-OR-MORE-DESIGN-PR VALUE 'UEDP'.          
                 88 PER-EXCL-PERS-LIAB-CYCLE-MOP VALUE 'UEMC'.          
                 88 PER-EXCL-PERS-LIAB-WATERCR   VALUE 'UEWC'.          
                 88 PER-EXCL-PERS-LIAB-RVS       VALUE 'URVE'.          
                 88 PER-EXCL-PERS-LIAB-MOTOR-HM  VALUE 'UMHE'.          
                 88 PER-EXCL-AUTO-LIAB-COVERAGE  VALUE 'UEAL'.          
                 88 PER-OTHER-MEMBERS-OF-YOUR-HH VALUE 'ROTH'.          
                 88 PER-HO-3-FOUNDATION          VALUE 'HFND'.          
                 88 PER-HO-3-WT-BACKUP-AND-SUMP  VALUE 'RWBU'.          
                 88 PER-HO-3-CASH-VALUE-ACTUAL   VALUE 'RCVA'.          
                 88 PER-COMPUTER                 VALUE 'HCOM'.          
                 88 PER-SPECIFIC-STRUCTURE       VALUE 'HSOS'.          
                 88 PER-STUDENT-LIVING-AWAY      VALUE 'RSLA'.          
                 88 PER-GOLF-CART-PHYS-DAMAGE    VALUE 'RGLF'.          
                 88 PER-FUNCTIONAL-REPL-COST     VALUE 'HFRC'.          
                 88 PER-EX-RES-COMM-PROP         VALUE 'HRCE'.          
                 88 PER-INC-COST-OF-CONSTRUCT    VALUE 'RCOC'.          
                 88 PER-LOSS-OF-USE              VALUE 'RLOU'.          
                 88 PER-CONTENTS-REPLACE-KOST    VALUE 'RCRK'.          
                 88 PER-ACV-ON-ROOF-SURFACE      VALUE 'RACV'.          
                 88 PER-BUSINESS-VALUE-PAPERS    VALUE 'HBVP'.          
                 88 PER-JEWEL-WATCH-FUR          VALUE 'HJWF'.          
                 88 PER-IDENTITY-THEFT           VALUE 'RITH'.          
                 88 PER-TX-SPECIAL-PERS-PROPERTY VALUE 'RSPP'.          
                 88 PER-LONE-STAR                VALUE 'STRL'.          
                 88 PER-LONE-STAR-PLUS           VALUE 'STRM'.          
                 88 PER-SPECIAL-COMPUTER-END     VALUE 'RCOM'.          
                 88 PER-REFRIGERATED-PERS-PROP   VALUE 'RREF'.          
                 88 PER-MOLD                     VALUE 'FMLD' 'HMLD'.   
                 88 PER-ADVANTAGE-MOLD           VALUE 'HMLA'.          
                 88 PER-ADVANTAGE-FOUNDATION     VALUE 'RFND'.          
                 88 PER-ADVANTAGE-WBU-SUMP-PUMP  VALUE 'RSMP'.          
                 88 PER-ADVANTAGE-AEC            VALUE 'R110'.          
                 88 PER-ADVANTAGE-AEC-POLIC-LMT  VALUE 'R109'.          
                 88 PER-PARTIAL-LOSS-SETTLE      VALUE 'RPLS'.
                 88 PER-VALID-REP-HOME-PROTECT   VALUE 'RDBE'           
                    'H000'                                              
                    'RPLS' 'RTRE'                                       
                    'RHWD'                                              
                    'RAEE' 'RSWR' 'HARM' 'RULC' 'RMRL'                  
                    'HRGC' 'HRTA' 'HMLD' 'RDOG' 'HARR' 'HAII'.          
                 88 PER-VALID-OK-HOME-PROTECT   VALUE 'H000' 'RLFC'     
                    'RDOG' 'RAEE' 'RDBE' 'RTMP' 'RARC' 'RHWD'           
                    'RMRC' 'RSWS' 'HAII' 'HAIN' 'HARM'.                 
                 88 PER-VALID-OK-LOW-VALUE-DWL  VALUE 'F000'            
                    'RDOG' 'RAEE' 'RDBE' 'RTMP' 'RHWD' 'RMRC'           
                    'FAII' 'FAIN' 'F216'.                               
                 88 PER-VALID-AR-LOW-VALUE-DWL  VALUE 'F000'            
                    'RDOG' 'RAEE' 'RDBE' 'RTMP' 'RHWD' 'RMRC' 'RCSC'    
                'FAII' 'FAIN' 'F216' 'FALE' 'RFWR' 'RMRL' 'RLFC' 'RARC'.
                 88 PER-VALID-MS-LOW-VALUE-DWL  VALUE 'F000'            
                    'RDOG' 'RAEE' 'RDBE' 'RTMP' 'RHWD' 'RMRC'           
                    'RARC' 'RLFC' 'RCSC'                                
                    'FAII' 'FAIN' 'F216' 'MCFD' 'RFWR' 'RMRL' 'FALE'.   
                 88 PER-VALID-MS-HO-8           VALUE 'H000'            
                    'RDOG' 'RAEE' 'RDBE' 'RTMP' 'RHWD' 'RMRC' 'RARC'    
                    'RCSC'                                              
                    'HAII' 'HAIN' 'HARM' 'MCFD' 'RSWR' 'RMRL' 'RLFC'.   
                 88 PER-VALID-AR-HO-8           VALUE 'H000'            
                    'RDOG' 'RAEE' 'RDBE' 'RTMP' 'RHWD' 'RMRC' 'RARC'    
                    'HAII' 'HAIN' 'HARM' 'RSWR' 'RMRL' 'RLFC'.          
                 88 PER-VALID-AR-HO-3           VALUE 'H000'            
                    'HAIN' 'HLAP' 'HCPL' 'HWBU' 'HFRM' 'RV3C' 'RCSC'    
                    'HBUP' 'HAPR' 'HEQC' 'RSLA' 'HWCR' 'RLFC' 'RGCC'    
                    'RCOM' 'HAII' 'ROTH' 'RGLF' 'HPIP' 'RCMP' 'RDBE'    
                    'HOFF' 'HISL' 'HARR' 'RITH' 'HARM' 'RAEE' 'HARI'    
                    'RSWR' 'RFAM'                                       
                    'HSAA' 'HAPP' 'HCRC' 'HBSP' 'RREF' 'RMRC' 'HOEB'.   
                 88 PER-VALID-MS-HO-3           VALUE 'H000'  'HCRC'    
                                                      'HARI'            
                    'RV3C' 'HAIN' 'HBUP' 'RCOM' 'HARM' 'HSAA' 'HLAP'    
                           'HAPR' 'HAII' 'HOFF' 'HAPP' 'HCPL' 'HFRM'    
                    'HEQC' 'ROTH' 'HISL' 'HWBU' 'RSLA' 'RGLF' 'HACV'    
                    'MCFD' 'HARR' 'HBSP' 'HWCR' 'RITH' 'RREF' 'RLFC'    
                    'RCMP' 'RMRC' 'RCSC' 'RDBE' 'RAEE' 'RFAM' 'HPIP'    
                    'HOEB'.                                             
                 88 PER-VALID-MS-DP-3           VALUE 'F000'  'FALE'    
                    'RFWR' 'FAIN' 'FFRV' 'FAWN' 'F216' 'HSAA' 'FLAP'    
                    'FAII' 'HAPR' 'HOFF' 'HAPP' 'HCPL' 'HFRM' 'FACV'    
                    'FANO' 'FWCL' 'FRTE' 'FAWN' 'FSIN' 'SSOT'           
                    'FEQC' 'ROTH' 'HISL' 'HWBU' 'RSLA' 'RGLF'           
                    'MCFD' 'HARR' 'HBSP' 'RITH' 'RREF' 'RLFC'           
                    'RCMP' 'RMRC' 'RCSC' 'RDBE' 'RAEE' 'FTFT' 'FPIP'.   
                 88 PER-VALID-AR-DP-3           VALUE 'F000'  'FALE'    
                    'SSOT' 'FAIN' 'FFRV' 'FAWN' 'F216' 'HSAA' 'FLAP'    
                    'FAII' 'HAPR' 'HOFF' 'HAPP' 'HCPL' 'HFRM'           
                    'FANO' 'FWCL' 'FRTE' 'FAWN' 'FSIN' 'RFWR'           
                    'FEQC' 'ROTH' 'HISL' 'HWBU' 'RSLA' 'RGLF' 'HACV'    
                    'RWBU' 'HARR' 'HBSP' 'RITH' 'RREF' 'RLFC'           
                    'RCMP' 'RMRC' 'RCSC' 'RDBE' 'RAEE' 'FTFT' 'FPIP'.   
                 88 REPUBLIC-LOSS-ASSESS-EQ      VALUE 'RLEQ'.          
                 88 PER-DOG-SURCHARGE            VALUE 'RDOG'.          
                 88 PER-MS-CERTIFIED-FIRE-DIST   VALUE 'MCFD'.          
                 88 PER-REP-HOME-PROT-WATER-DMG  VALUE 'RHWD'.          
                 88 PER-MULTI-ROOF-LAYERS-SURCHG VALUE 'RMRL'.          
                 88 PER-DOG-BITE-EXCLUSION       VALUE 'RDBE'.          
                 88 PER-ROOF-EXCLUSION           VALUE 'RREE'.          
                 88 PER-ANIMAL-EXCLUSION         VALUE 'RAEE'.          
                 88 PER-OTHER-STRUCTURES-EXCLUDE VALUE 'ROSE'.          
                 88 PER-MONEY                    VALUE 'HMON'.          
                 88 PER-ISO-HOMEOWNERS-CHOICE    VALUE 'RIHC'.          
                 88 PER-ISO-ADDL-EXTENDED-COV    VALUE 'HAEC'.          
                 88 PER-REPUBLIC-DWELL-REPL-COST VALUE 'RDRC'.          
                 88 PER-REPUBLIC-REPLC-PERS-PROP VALUE 'RRPP'.          
                 88 PER-REFRIGERATED             VALUE 'HREF'.          
                 88 PER-TEJAS-HOMEOWNERS-CHOICE  VALUE 'RTHC'.          
                 88 PER-HO-ENHANCEMENT-PLUS      VALUE 'RV3C'.          
                 88 PER-DWELLING-REPLACEMENT-COST VALUE 'HHRC'.         
                 88 PER-WOOD-ROOF-SURCHARGE      VALUE 'RSWR'.          
                 88 PER-AGT-RENEWAL-CREDIT       VALUE 'RARC'.          
                 88 PER-RENEWAL-CREDIT           VALUE 'RTRC'.          
                 88 PER-LOSS-FREE-CREDIT         VALUE 'RLFC'.          
                 88 PER-MATURE-CREDIT            VALUE 'RCSC'.          
                 88 PER-REPL-COST-ON-ROOF-SURFAC VALUE 'HRCR'.          
                 88 PER-SPRINKLER-CREDIT VALUE 'HSPR'.                  
                 88 PER-TRAMPOLINE-SURCHARGE     VALUE 'RTMP'.          
                 88 ADDITIONAL-COVC      VALUE 'H050' 'H066'            
                                               'HCAW' 'HCCA'.           
                 88 ADDITIONAL-INSURED   VALUE 'H041' 'HAII'.           
                 88 ADDITIONAL-INTEREST  VALUE 'H310' 'HAIN'.           
                 88 ADDITIONAL-RES-EMPL  VALUE 'HARE'.                  
                 88 ADDITIONAL-RESIDENCE VALUE 'HARI' 'FARI'.           
                 88 ADDL-RESIDENCE-RENTED VALUE 'H070' 'HARR'.          
                 88 APPURTENANT-STRUCTURES VALUE 'H048' 'HAPP'.         
                 88 APPURTENANT-STRUCTURE-RENT VALUE 'H040' 'HAPR'.     
                 88 BLDG-ADD-ALTR        VALUE 'H049' 'H051' 'H192'     
                                               'HBAA' 'HBA1' 'HBA2'.    
                 88 BLDG-ADD-ALTR-PUERTO VALUE 'H051' 'H192'            
                                               'HBA1' 'HBA2'.           
                 88 FELLOW-EMPLOYEE-LIABILITY VALUE 'H309' 'FFEL'       
                                                    'HFEL'.             
                 88 LEAD-POISONING-COVERAGE   VALUE 'FLPC' 'HLPC'.      
                 88 LEAD-POISONING-EXCLUSION  VALUE 'FLPE' 'HLPE'.      
                 88 WATER-BACKUP-SEWER        VALUE 'FWBU'.             
                 88 PER-ACTUAL-CASH-VALUE-DWG VALUE 'FACV'.             
                 88 PER-PERSONAL-INJ-PROT-DWG VALUE 'FPIP'.             
                 88 PER-ADD-INSURED-LOC  VALUE 'FANO' 'ANO1' 'ANO2'.    
                 88 PER-AWNINGS          VALUE 'FAWN'.                  
                 88 PER-BLDG-ADD-ALTR-FORM5 VALUE 'H049' 'HBAA'.        
                 88 PER-BUILDING-ORDINANCE VALUE 'H277' 'HBDO'.         
                 88 PER-BUILDING-ORDINANCE-ON-FIRE VALUE 'FBDO'.        
                 88 PER-BUILDING-ITEMS   VALUE 'FBIT'.                  
                 88 PER-BUSINESS-PROPERTY VALUE 'H312' 'HBUP'.          
                 88 PER-GREENHOUSE       VALUE 'FGRH'.                  
                 88 PER-PERGOLA          VALUE 'FSLH'.                  
                 88 PER-CABANA           VALUE 'FCAB'.                  
                 88 PER-OUTDOOR-EQUIPMENT VALUE 'FODE'.                 
                 88 PER-INCIDENTAL-OCCUPANCIES VALUE 'FINC'.            
                 88 BLDG-ADD-ALTR-HO6-BASIC VALUE 'H031' 'HBAB'.        
                 88 BLDG-ADD-ALTR-HO6-SPEC VALUE 'H032' 'HBA6'.         
                 88 BLDG-ADD-ALTR-TENANTS VALUE 'H051' 'HBA1'.          
                 88 BLDG-CONDO-UNIT-OWNERS VALUE 'H192' 'HBA2'.         
                 88 PER-BOATHOUSES       VALUE 'FBTH'.                  
                 88 BUSINESS-PURSUITS    VALUE 'H071' 'HBSP' 'FBSP'.    
                 88 COVERAGES-THAT-NEED-NO-PARMS VALUE 'H041' 'H082'    
                       'RGLL'                                           
                       'RSPP'                                           
                       'MCFD'                                           
                       'RGCC'                                           
                       'UCMP' 'SCMP'
                       'H037' 'FO02' 'FO03' 'FO04' 'FO06' 'FO08'        
                       'H314' 'H266' 'H320' 'H321' 'H500' 'H400'        
                       'FFEL' 'HCOM' 'HTNC' 'HOPB' 'HRPP' 'HGLB'        
                       'HHRC' 'HWNC' 'HSOP' 'HREF' 'HAII' 'HPIP'        
                       'HEIX' 'IAV ' 'RV61' 'RVCC' 'RVC2' 'RCNS'        
                       'RV3A' 'RV3B' 'RV3C' 'RV3D' 'RSWS' 'RSWR'        
                       'RVRC' 'RVLP' 'RCMP' 'RCMP' 'RFLP' 'HEXI'        
                       'RARC'                                           
                       'RULC'                                           
                       'RLFC'                                           
                       'ROTH'                                           
                       'RRPP'                                           
                       'RREF' 'RCOM' 'STRL' 'STRM'                      
                       'HHCE'                                           
                       'RCPF'                                           
                       'HALL'                                           
                       'HACV'                                           
                       'FACV'                                           
                       'HRCR'                                           
                       'RMRC'                                           
                       'FPIP'                                           
                       'RIHC'                                           
                       'FIML'                                           
                       'RTMP'                                           
                       'RDOG'                                           
                       'RAEE'                                           
                       'RDBE'                                           
                       'RCFP' 'RFWS' 'RFWR' 'RV62' 'RVCC' 'RVC2'.       
                 88 PER-CERTIFIED-FIRE-DEPT VALUE 'H401' 'HCFD'.        
                 88 PER-CK-FRR-UNIT-SEQ VALUE 'FALE' 'FAWN' 'FBAA'      
                       'FBIT' 'FCAB' 'FEQC' 'FFRV' 'FFEN' 'FDSC'        
                       'FINC' 'FLAP' 'FMIS' 'FOST' 'FRTE' 'FPST'        
                       'SSOT'                                           
                       'FSAS' 'FSIN' 'FSKH' 'F216' 'FBDO' 'FGRH'        
                       'FODE' 'FSLH'.                                   
                 88 PER-CK-HRR-UNIT-SEQ          VALUE 'H035' 'HLAP'.   
                 88 PER-COVERAGE-DEPENDS-ON-UNIT VALUE 'FAWN' 'FDSC'    
                       'FEQC' 'FFEN' 'FLTS' 'FMIS' 'FRTV' 'FSAS'        
                       'FSIN' 'FSKH' 'FOST' 'FBAA' 'FALE' 'FAST'        
                       'SSOT'                                           
                       'FPST' 'FFRV' 'FWSA' 'FBDO' 'FGRH' 'FSLH'        
                       'FODE' 'F216' 'FCAB' 'FSWP' 'FSCC' 'FSRC'.       
                 88 PER-COVERAGE-RATES-AS-BEFORE VALUE 'FEQC' 'FSCC'    
                       'FMIN' 'F000' 'FHSB' 'F216' 'FBDO' 'FINC'        
                       'F270'.                                          
                 88 PER-LIABILITY-30-SEGMENT     VALUE 'FANO' 'FARE'    
                       'ANO1' 'ANO2'                                    
                       'FARI' 'FBSP' 'FHDC' 'FLAL' 'FPIO' 'FSNB'        
                       'FWCL' 'FWCP' 'FFEL'.                            
                 88 CREDIT-CARD          VALUE 'H053' 'HCCR'.           
                 88 EARTHQUAKE           VALUE 'H054' 'H055' 'H315'     
                                               'HEQC' 'HEQ5' 'HQID'.    
                 88 EARTHQUAKE-RECOVERY-FUND     VALUE 'CRER'.          
                 88 PER-EARTHQUAKE-COVERAGE VALUE 'FEQC'.               
                 88 PER-EMPLOYER-LIABILITY  VALUE 'FARE'.               
                 88 PER-EXISTING-INS-CREDIT VALUE 'H168' 'HEXI'.        
                 88 FARMERS-CPL          VALUE 'H072' 'H073'            
                                               'HFRM' 'HCPL'.           
                 88 FARMERS-CPL-H073     VALUE 'H073' 'HCPL'.           
                 88 INCIDENTAL-FARM      VALUE 'H072' 'HFRM'.           
                 88 PER-FARM-MISC-ENDORS VALUE 'F100'.                  
                 88 PER-FARM-MONTH-REPORT VALUE 'MR01' 'MR02'           
                       'MR03' 'MR04' 'MR05' 'MR06' 'MR07' 'MR08'        
                       'MR09' 'MR10' 'MR11' 'MR12'.                     
                 88 PER-FARM-MONTH-REPORT-CAN VALUE 'MR98'.             
                 88 PER-FARM-MONTH-REPORT-EST VALUE 'MR99'.             
                 88 PER-FARM-PRE-SELECT  VALUE 'PS01' 'PS02'            
                       'PS03' 'PS04' 'PS05' 'PS06' 'PS07' 'PS08'        
                       'PS09' 'PS10' 'PS11' 'PS12'.                     
                 88 PER-FARM-PRE-SELECT-EST   VALUE 'PS99'.             
                 88 PER-FENCES           VALUE 'FFEN'.                  
                 88 PER-FIRE-DEPT-SERVICE-CHARGE VALUE 'FDSC'.          
                 88 PER-HOUSEBOATS       VALUE 'FHSB'.                  
                 88 PER-INC-LIM-UNSCHED-SILV VALUE 'H301' 'H302'        
                                                   'HIUS' 'HUSG'.       
                 88 PER-JEWELRY-AND-FURS     VALUE 'H267' 'HJFA'.       
                 88 INCREASED-SPECIAL-LIM-LIB VALUE 'H065' 'H211'       
                                                    'HISL' 'HUPP'.      
                 88 INCREASED-UNSCHED-PER-PROP VALUE 'H210' 'H211'      
                                                     'HHWF' 'HUPP'.     
                 88 LOSS-ASSESSMENT      VALUE 'H035' 'HLAP'.           
                 88 PER-MERCHANDISE-IN-STORAGE VALUE 'FMIS'.            
                 88 PER-MINE-SUBSID      VALUE 'H287' 'HMIN'.           
                 88 PER-MISC-PREM-CHGS-FORMS VALUE 'H000' 'HOMC'.       
                 88 PER-MISC-REAL-PROPERTY VALUE 'H038' 'HMBR'.         
                 88 PER-MISCELLANEOUS-CHARGES VALUE 'F000'.             
                 88 TENANTS-RELOCATION        VALUE 'H270' 'HMTR'.      
                 88 TENANTS-RELOCATION-FIRE   VALUE 'F270'.             
                 88 MONEY-SECURITIES     VALUE 'H065' 'HISL'.           
                 88 MOTORIZED-LAND-CONVEYANCES VALUE 'H313' 'HIML'.     
                 88 PER-NO-PARMS-OPTIONAL VALUE 'F000' 'H000' 'H065'    
                       'FCOS'                                           
                       'HRGC'                                           
                       'H090' 'H091' 'H092' 'H164' 'H208' 'H211' 'FFEL' 
                       'H250' 'HARI' 'H099' 'H277' 'H265' 'H072' 'H313' 
                       'H259' 'H304' 'H309' 'H290' 'H299' 'FO01' 'H016' 
                       'HIDW' 'HNBL' 'HFEL' 'HCRC' 'HPPR' 'HROF'        
                       'HIML' 'HSOS' 'HOMC' 'HISL' 'HCMP' 'HWC1' 'HSNB' 
                       'HWBU' 'HUPP' 'HPAS' 'HSKH' 'HBDO' 'HFRM' 'HAIN' 
                       'RCPF'                                           
                       'RARC' 'RLFC' 'ROTH' 'RULC' 'HRCE' 'RRPP' 'RSLA' 
                       'HFRC'                                           
                       'RCFL' 'RCFP' 'RFWR' 'RFLP' 'HBA6'.              
                 88 PER-LOSS-ASSESSMENT  VALUE 'FLAP'.                  
                 88 LOSS-ASSESSMENT-LIAB VALUE 'FLAL'.                  
                 88 PERM-INCIDENTAL-OCC  VALUE 'FPIO'.                  
                 88 PER-ON-OFF-PREMISES-THEFT  VALUE 'FTFT'.            
                 88 PER-LANDLORD-FURNISHING  VALUE 'H308' 'HLAF'.       
                 88 OFFICE-OCCUPANCY     VALUE 'H042' 'H043'            
                                               'HOFF' 'HOFO'.           
                 88 PER-OUTBOARD-MARINE-FLOATER VALUE 'HOBM'.           
                 88 PER-OUTDOOR-RADIO-TV-EQUIPMENT VALUE 'FRTE'.        
                 88 PERSONAL-INJURY      VALUE 'H082' 'HPIP'.           
                 88 PHYSICIANS-AND-SURGEONS VALUE 'H069' 'HPHY'.        
                 88 PER-REPLACEMENT-COST-COVERAGE VALUE 'H290' 'H299'   
                                                        'HCRC' 'HPPR'.  
                 88 PER-H265-REPLACEMENT-COST-COV VALUE 'H265'.         
                 88 PER-INSPECTED-DWELLING VALUE 'H259' 'HIDW'.         
                 88 SECTION-II-LIABILITY VALUE 'HEFC'.                  
                 88 SCHEDULED-GLASS-NON-CONDO VALUE 'H068' 'HGLS'.      
                 88 SCHEDULED-GLASS-CONDO VALUE 'H037' 'HGLB'.          
                 88 PER-SCREENINGS-AND-SUPPORTS VALUE 'FSAS'.           
                 88 PER-ROOF-SURFACING     VALUE 'H016' 'HROF'.         
                 88 PER-ROOFLIKE-COVERINGS VALUE 'FSRC'.                
                 88 PER-SIGNS-COVERAGE   VALUE 'FSIN'.                  
                 88 PER-SINKHOLE-COLLAPSE VALUE 'H099' 'HSKH'.          
                 88 PER-SINKHOLE-COVERAGE VALUE 'FSKH'.                 
                 88 PER-SNOWMOBILE       VALUE 'H164' 'FSNB' 'HSNB'.    
                 88 PER-SWIMMING-POOL    VALUE 'FSWP'.                  
                 88 PER-SUBSTANDARD-COND  VALUE 'FSCC'.                 
                 88 PER-PROTECTIVE-DEVICE VALUE 'H216' 'H250' 'F216'    
                                                'HARM' 'HPAS'.          
                 88 VOLCANIC-ERUPTION    VALUE 'H35A' 'HVOL'.           
                 88 WATERBED             VALUE 'H010' 'HBED'.           
                 88 PER-H400-WATERBED    VALUE 'H400'.                  
                 88 WATERCRAFT           VALUE 'H075' 'FWCL' 'HWCR'.    
                 88 PER-WINDSTORM-DEDUCTIBLE   VALUE 'HDED'.            
                 88 FIRE-WINDSTORM-DEDUCTIBLE   VALUE 'FDED'.           
                 88 PER-MOTOR-LAND-CONV-DWG     VALUE 'FIML'.           
                 88 PER-WINDSTORM-ASSUMPTION VALUE 'HWSA' 'H910'.       
                 88 PER-WINDSTORM-EXCLUSION VALUE 'FWSE'.               
                 88 PER-WORKERS-COMPENSATION VALUE 'H090' 'H091'        
                                            'FWCP' 'HCMP' 'HWC1'.       
                 88 PER-OTHER-STRUCTURES VALUE 'FOST'.                  
                 88 PER-SPEC-STRUCTURE-NRTO VALUE 'SSOT'.               
                 88 PER-FAIR-RENTAL-VALUE VALUE 'FFRV'.                 
                 88 PER-BUILDING-ADD-AND-ALT VALUE 'FBAA'.              
                 88 PER-PLANTS-SHRUBS-TREES VALUE 'FPST'.               
                 88 PER-ADD-LIVING-EXPENSE VALUE 'FALE'.                
                 88 PER-SPECIAL-LOSS-SETTLEMENT VALUE 'H256' 'HLSS'.    
                 88 PER-THREE-FOUR-FAMILY-DWELLING VALUE 'H074' 'HFDW'. 
                 88 PER-WINDSTORM-ON-OUTDOOR-PROP VALUE 'H288' 'HODE'.  
                 88 PER-TRANS-RECEIVING-EQUIP VALUE 'H257' 'HSEQ'.      
                 88 PER-BACK-UP-SEWERS-OR-DRNS VALUE 'H208' 'HWBU'.     
                 88 PER-UNIT-OWNERS      VALUE 'H034' 'HUNO'.           
                 88 PER-PROPERTY-REMOVED-INC-LIMIT  VALUE 'HPRI'.       
                 88 PER-LIENHOLDERS-SINGLE-INTREST  VALUE 'HLSI'.       
                 88 H08-ON-OFF-PREMISES-THEFT VALUE 'H303' 'HPTL'.      
                 88 NEIGHBORHOOD-BETTERMENT-H304 VALUE 'H304' 'HNBL'.   
                 88 PER-MINE-SUBSIDENCE-FMIN   VALUE 'FMIN'.            
                 88 PER-MINE-SUBSIDENCE-FI86   VALUE 'FI86'.            
                 88 VANDALISM-MALICIOUS-MISCHIEF VALUE 'HVAN'.          
                 88 PROPERTY-LOSS-ASSESSMENT VALUE 'HPLA'.              
                 88 LIABILITY-LOSS-ASSESSMENT VALUE 'HLLA'.             
                 88 UNIT-IMPROVEMENT     VALUE 'HIMP'.                  
                 88 MASS-EVACUATION      VALUE 'HMAS'.                  
                 88 UNSCHEDULED-SILVERWARE VALUE 'HUSW'.                
                 88 THEFT-EXTENSION-SEASONAL VALUE 'HTES'.              
                 88 CANADIAN-DISCOUNT    VALUE 'HDIS'.                  
                 88 CANADIAN-SURCHARGE   VALUE 'HSUR'.                  
                 88 FIRE-EXTINGUISHERS   VALUE 'FCFE'.                  
                 88 HOME-DAY-CARE        VALUE 'H323' 'FHDC' 'HHDC'.    
                 88 FELLOW-EMP-LIAB      VALUE 'FFEL'.                  
                 88 TEXAS-FIRE-VACANCY-CLAUSE  VALUE 'FVAC'.            
                 88 CAL-EARTHQUAKE-RECOVERY-FUND VALUE 'CRER'.          
                 88 LEAD-POISONING-COV   VALUE 'HLPC' 'FLPC'.           
                 88 LEAD-POISONING-EXC   VALUE 'HLPE' 'FLPE'.           
                 88 WATER-BACKUP-COV     VALUE 'FWBU'.                  
                 88 REPUBLIC-LIMITED-ACCESS                             
                                         VALUE 'RCLA'.                  
                 88 REPUBLIC-MODERNIZATION                              
                                         VALUE 'RCMC'.                  
                 88 REPUBLIC-MODERNIZATION-FIRE                         
                                         VALUE 'RCFM'.                  
                 88 REPUBLIC-WATER-SUMP-PUMP                            
                                         VALUE 'RVWS'.                  
                 88 REPUBLIC-OFF-PREMISES                               
                                         VALUE 'RVOP'.                  
                 88 REPUBLIC-MULTI-POLICY-DISCOUNT                      
                                         VALUE 'RCMP'.                  
                 88 REPUBLIC-NEW-BUYERS-DISCOUNT                        
                                         VALUE 'RCNB'.                  
                 88 REPUBLIC-HOME-BUS-LIABILITY                         
                                         VALUE 'RVHL'.                  
                 88 REPUBLIC-HOME-BUS-COVERAGE                          
                                         VALUE 'RVHB'.                  
                 88 REPUBLIC-RENEWAL-DISCOUNT                           
                                         VALUE 'RCRC'.                  
                 88 REPUBLIC-FIRE-RENEWAL-DISCOUNT                      
                                         VALUE 'RCFR'.                  
                 88 REPUBLIC-WATER-DAMAGE                               
                                         VALUE 'HOWD'.                  
                 88 PER-SPECIFIED-ADDITIONAL-AMT                        
                                         VALUE 'HSAA'.                  
                 88 PER-ADDITIONAL-LIMITS-LIAB                          
                                         VALUE 'HALL'.                  
                 88 PER-ACTUAL-CASH-VALUE-LS                            
                                         VALUE 'HACV'.                  
                 88 PER-FIRE-DEPARTMENT-SVC-CHARGE                      
                                         VALUE 'HFDS'.                  
                 88 PER-ACCIDENTAL-DEATH-DISMEMBER                      
                                         VALUE 'HADB'.                  
                 88 PER-REPUBLIC-METAL-ROOF-CREDIT VALUE 'RMRC'.        
                 88 CVG-VALID-FOR-TX-FAIR-PLAN                          
                                         VALUE 'HRCE' 'HFOF' 'HFDP'     
                    'HFAI' 'HFPP' 'H810' 'HFSD' 'HFSP' 'HFLH'.          
                 88 CVG-INVALID-FOR-TX-FAIR-PLAN                        
                                         VALUE 'HAAD' 'HAEC' 'HAGE'     
                                         'HAPP' 'HARI' 'HARM' 'HARR'    
                    'HBSP' 'HBUP' 'HBVP' 'HCCP' 'HCMP' 'HCOM' 'HCPL'    
                    'HCRC' 'HDRP' 'HFPD' 'HIFR' 'HJWF' 'HLAP' 'HLSE'    
                    'HMON' 'HNCR' 'HNLA' 'HOOS' 'HPIP' 'HPSD' 'HRGC'    
                    'HRTA' 'HRTO' 'HSPR' 'HTLA' 'HWCA' 'HWCG' 'HWCR'    
                    'R109'                                              
                    'RDRC' 'RRPP' 'HMLD' 'R110' 'HMLA' 'RFND' 'RSMP'.   
                 88 PER-FAIR-PLAN-DWELL-REPLC-COST  VALUE 'HFDP'.       
                 88 PER-FAIR-PLAN-CONT-REPLC-COST   VALUE 'HFPP'.       
                 88 PER-FAIR-PLAN-OFF-SCHOOL-STUD   VALUE 'HFOF'.       
                 88 PER-FAIR-PLAN-ADDL-INSURED      VALUE 'HFAI'.       
                 88 PER-FAIR-PLAN-LOSS-HISTORY      VALUE 'HFLH'.       
                 88 PER-FAIR-PLAN-HOME-SEC-DEV      VALUE 'HFSD'.       
                 88 PER-FAIR-PLAN-HOME-SPRINKL      VALUE 'HFSP'.       
                 88 PER-DWELLING-LIAB-USE-FOR-FM-1  VALUE 'FARE' 'FARI' 
                                                          'RDBE' 'RAEE'.
                 88 PER-DWELLING-LIAB-USE-FOR-FM-2  VALUE 'FIML'        
                    'FBSP' 'FARI' 'FARE' 'FPIO' 'FAIN' 'FANO' 'FWCL'    
                    'RDBE' 'RAEE'.                                      
                 88 PER-EQUIPMENT-BREAKDOWN         VALUE 'HOEB'.       
              10 PER-SEQUENCE-NUMBER     PIC 9(01).                     
           05 PER-AMENDMENT-NUMBER       PIC X(01).                     
           05 PER-ENTERED-DATE           PIC X(08).                     
           05 PER-PARAMETERS.                                           
              10 PER-DESC-LINE-1         PIC X(80).                     
              10 PER-DESC-LINE-2.                                       
                 15  PER-DESC-LINE-2-FB  PIC X.                         
                 15  FILLER              PIC X(67).                     
                 15  PER-DESC-LINE-2-LAST2 PIC X(02).
              10 PER-DESC-LINE-3.                                       
                 15  PER-DESC-LINE-3-FB  PIC X.                         
                 15  FILLER              PIC X(67).                     
                 15  PER-DESC-LINE-3-LAST2 PIC X(02).
              10 PER-STATUS           PIC X(01).                        
                    88 PER-CURRENT-STATUS VALUE 'C'.                    
                    88 PER-DELETED-COVERAGE VALUE 'D'.                  
              10 PER-ORIGINAL-RATE-BOOK PIC X(01).                      
                    88 PER-NO-ORIGINAL-RATE-BOOK VALUE SPACES.          
              10 PER-RATE-BOOK        PIC X(01).                        
                    88 PER-NO-RATE-BOOK  VALUE SPACES.                  
              10 PER-ENDORSEMENT-PREM PIC 9(04).                        
              10 PER-H390-FIRE-RB     PIC X(01).                        
              10 PER-H390-EC-RB       PIC X(01).                        
              10 PER-H390-VMM-RB      PIC X(01).                        
              10 FILLER               PIC X(40).                        
           05 PER-PMS-FUTURE-USE            PIC X(51).                  
           05 PER-ACA-IDENTIFIER-SAVE2.                                 
              10 PER-ACA-FIXED-IDENTIFIER-2 PIC 9(04).                  
           05 PER-FLAP-ENDORSEMENT-IND   PIC X(01).                     
           05 PER-FLAP-AMT-PARM          PIC 9(05).                     
           05 PER-FLIP-HO0461-TO-RH0461  PIC X(01).                     
           05 PER-CUST-FUTURE-USE        PIC X(01).                     
           05 PER-ALDT-PARM              PIC X(05).                     
           05 PER-ALDT-PARM-DATE         PIC X(08).                     
           05 PER-HOEB-NOTICE-SW         PIC X(01).                     
           05 PER-ALTR-RCVD-PARM         PIC X(05).                     
           05 PER-ALTR-RCVD-DATE         PIC X(08).                     
           05 PER-YR2000-CUST-USE        PIC X(73).                     
           05 PER-DUPLICATE-KEY-SEQUENCE PIC X(03).                     
           05 NEEDED-SORT-DATA          PIC X(179).
           05 BLANK-OUT                 PIC X(44).