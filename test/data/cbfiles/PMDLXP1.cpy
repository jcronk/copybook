        01 PMDLXP1-INS-LINE-RATE-RECORD.
           03 PMDLXP1-INS-LINE-RATE-SEG.                                
            05 PMDLXP1-SEGMENT-KEY.                                     
              07 PMDLXP1-SEGMENT-ID                   PIC X(02).        
                 88 PMDLXP1-SEGMENT-43                 VALUE '43'.      
              07 PMDLXP1-SEGMENT-STATUS               PIC X(01).        
                 88 PMDLXP1-ACTIVE-STATUS              VALUE 'A'.       
                 88 PMDLXP1-DELETED-STATUS             VALUE 'D'.       
                 88 PMDLXP1-HISTORY-STATUS             VALUE 'H'.       
                 88 PMDLXP1-NOT-ACTIVE-STATUS          VALUE 'H' 'D'.   
                 88 PMDLXP1-RENEWAL-FS-STATUS          VALUE 'R' 'F'.   
              07 PMDLXP1-TRANSACTION-DATE             PIC 9(08).                              
              07 PMDLXP1-SEGMENT-ID-KEY.                                
                 09 PMDLXP1-SEGMENT-LEVEL-CODE        PIC X(01).        
                    88 PMDLXP1-SEG-LEVEL-L             VALUE 'L'.       
                 09 PMDLXP1-SEGMENT-PART-CODE         PIC X(01).        
                    88 PMDLXP1-SEG-PART-X              VALUE 'X'.       
                 09 PMDLXP1-SUB-PART-CODE             PIC X(01).        
                 09 PMDLXP1-INSURANCE-LINE            PIC X(02).        
                    88 PMDLXP1-FIRE                    VALUE 'CF'.      
              07 PMDLXP1-LEVEL-KEY.                                     
                 09 PMDLXP1-LOCATION-NUMBER           PIC 9(04).        
                    88 PMDLXP1-TERRORISM-COV-LOC      VALUE 0911.       
              07 PMDLXP1-ITEM-EFFECTIVE-DATE          PIC 9(08).
              07 PMDLXP1-PROCESS-DATE                 PIC 9(08).
             05 PMDLXP1-SEGMENT-DATA.                                   
                 09 PMDLXP1-PMA-CODE                  PIC X(02).        
                 09 PMDLXP1-IRPM-EXPER-PERCENT-A.                       
                    11 PMDLXP1-IRPM-EXPER-PERCENT     PIC 9(01)V9(03).  
                 09 PMDLXP1-SCHEDULED-PERCENT-A.                        
                    11 PMDLXP1-SCHEDULED-PERCENT      PIC 9(01)V9(03).  
                 09 PMDLXP1-EXPENSE-PERCENT-A.                          
                    11 PMDLXP1-EXPENSE-PERCENT        PIC 9(01)V9(03).  
                 09 PMDLXP1-OTHER-PERCENT-A.                            
                    11 PMDLXP1-OTHER-PERCENT          PIC 9(01)V9(03).  
                 09 PMDLXP1-COMM-REDUCTION-A.                           
                    11 PMDLXP1-COMM-REDUCTION         PIC 9(01)V9(03).  
                 09 PMDLXP1-COMM-RATE-A.                                
                    11 PMDLXP1-COMM-RATE              PIC 9(01)V9(05).  
                 09 PMDLXP1-BLDG-DEDUCTIBLE-A.                          
                    11 PMDLXP1-BLDG-DEDUCTIBLE        PIC 9(07).        
                 09 PMDLXP1-CONTS-DEDUCTIBLE-A.                         
                    11 PMDLXP1-CONTS-DEDUCTIBLE       PIC 9(07).        
                 09 PMDLXP1-TE-DEDUCTIBLE-A.                            
                    11 PMDLXP1-TE-DEDUCTIBLE          PIC 9(07).        
                 09 PMDLXP1-BLDG-COIN-A.                                
                    11 PMDLXP1-BLDG-COIN              PIC 9(03).        
                 09 PMDLXP1-CONTS-COIN-A.                               
                    11 PMDLXP1-CONTS-COIN             PIC 9(03).        
                 09 PMDLXP1-TE-COIN-A.                                  
                    11 PMDLXP1-TE-COIN                PIC 9(03).        
                 09 PMDLXP1-TEX-OCCURRENCE-DED-A.                       
                    11 PMDLXP1-TEX-OCCURRENCE-DED     PIC 9(06).        
                 09 PMDLXP1-TEX-AMT-INS-A.                              
                    11 PMDLXP1-TEX-AMT-INS            PIC 9(09).        
                 09 PMDLXP1-RATE-LEVEL-DATE           PIC X(08).        
                 09 PMDLXP1-WORK-SHEET-IND            PIC X(01).        
                 09 PMDLXP1-TEX-VAR-DEVIA-A.                            
                    13 PMDLXP1-TEX-VAR-DEVIA          PIC 9(03)V9(03).  
                 09 PMDLXP1-W-H-DEDUCT-A.                               
                    13 PMDLXP1-W-H-DEDUCT             PIC 9(07).        
                 09 PMDLXP1-THFT-DEDUCT-A.                              
                    13 PMDLXP1-THFT-DEDUCT            PIC 9(07).        
                 09 PMDLXP1-ALL-OTHER-DEDUCT-A.                         
                    13 PMDLXP1-ALL-OTHER-DEDUCT       PIC 9(07).        
                 09 PMDLXP1-W-H-AMT-INS-A.                              
                    13 PMDLXP1-W-H-AMT-INS            PIC 9(10).        
                 09 PMDLXP1-THEFT-AMT-INS-A.                            
                    13 PMDLXP1-THEFT-AMT-INS          PIC 9(10).        
                 09 PMDLXP1-ALL-OTHER-AMT-INS-A.                        
                    13 PMDLXP1-ALL-OTHER-AMT-INS      PIC 9(10).        
                 09 PMDLXP1-PMS-FUTURE-USE            PIC X(147).       
                 09 PMDLXP1-CUST-SPL-USE              PIC X(30).        
                 09 PMDLXP1-YR2000-CUST-USE           PIC X(100).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).