        01 PMDNXI1-SUB-LOC-RATING-RECORD.
           03 PMDNXI1-SUB-LOC-RATING-SEG.                               
            05 PMDNXI1-SEGMENT-KEY.                                     
              07 PMDNXI1-SEGMENT-ID                   PIC X(02).        
                 88 PMDNXI1-SEGMENT-43                 VALUE '43'.      
              07 PMDNXI1-SEGMENT-STATUS               PIC X(01).        
                 88 PMDNXI1-ACTIVE-STATUS              VALUE 'A'.       
                 88 PMDNXI1-DELETED-STATUS             VALUE 'D'.       
                 88 PMDNXI1-HISTORY-STATUS             VALUE 'H'.       
                 88 PMDNXI1-NOT-ACTIVE-STATUS          VALUE 'H' 'D'.   
                 88 PMDNXI1-RENEWAL-FS-STATUS          VALUE 'R' 'F'.   
              07 PMDNXI1-TRANSACTION-DATE             PIC 9(08).
              07 PMDNXI1-SEGMENT-ID-KEY.                                
                 09 PMDNXI1-SEGMENT-LEVEL-CODE        PIC X(01).        
                    88 PMDNXI1-SEG-LEVEL-N             VALUE 'N'.       
                 09 PMDNXI1-SEGMENT-PART-CODE         PIC X(01).        
                    88 PMDNXI1-SEG-PART-X              VALUE 'X'.       
                 09 PMDNXI1-SUB-PART-CODE             PIC X(01).        
                 09 PMDNXI1-INSURANCE-LINE            PIC X(02).        
                    88 PMDNXI1-INLAND-MARINE           VALUE 'IM'.      
              07 PMDNXI1-LEVEL-KEY.                                     
                 09 PMDNXI1-LOCATION-NUMBER-A.                          
                    11 PMDNXI1-LOCATION-NUMBER        PIC 9(04).        
                 09 PMDNXI1-SUB-LOCATION-NUMBER-A.                      
                    11 PMDNXI1-SUB-LOCATION-NUMBER    PIC 9(03).        
              07 PMDNXI1-ITEM-EFFECTIVE-DATE          PIC 9(08).
              07 PMDNXI1-VARIABLE-KEY                 PIC X(06).        
              07 PMDNXI1-PROCESS-DATE                 PIC 9(08).
            05 PMDNXI1-SEGMENT-DATA.                                    
                 09 PMDNXI1-RATING-STATE              PIC X(02).        
                 09 PMDNXI1-TERR-CODE-A.                                
                    11 PMDNXI1-TERR-CODE              PIC 9(03).        
                       88 PMDNXI1-NY-SPECIAL-TERRITORIES                
                          VALUE 030 240 310 410 430.                    
                       88 PMDNXI1-MONROE-COUNTY                         
                          VALUE 440 445.                                
                       88 PMDNXI1-KEY-WEST           VALUE 440.         
                 09 PMDNXI1-CLASS-DESC-SOURCE         PIC X(01).        
                    88 PMDNXI1-CLASS-TABLE             VALUE 'C'.       
                    88 PMDNXI1-CLASS-DESC-TABLE        VALUE 'D'.       
                    88 PMDNXI1-CLASS-ON-SEGMENT        VALUE 'S'.       
                    88 PMDNXI1-CLASS-BLANK             VALUE ' '.       
                 09 PMDNXI1-BLDG-CSP-CLASS-CODE       PIC X(04).        
                    88 PMDNXI1-TENANT-CSP-CODE       VALUE '0321'       
                        '0322' '0323' '0581' '0582' '0701' '0702'.      
                 09 PMDNXI1-RATE-PLAN-A.                                
                    11 PMDNXI1-RATE-PLAN              PIC 9(01).        
                       88 PMDNXI1-CLASS-RATED          VALUE 1.         
                       88 PMDNXI1-SPECIFIC-RATED       VALUE 2.         
                       88 PMDNXI1-TENTATIVE-RATED      VALUE 4.         
                 09 PMDNXI1-CONT-CSP-CLASS-CODE       PIC X(04).        
                    88 PMDNXI1-TENANT-A-CSP-CODE      VALUE '0511'      
                       '0701' '0702' '0745' '0746' '0747' '0851'        
                       '0852' '0900' '0921' '0923' '0931' '1000'        
                       '1052' '1070'.                                   
                    88 PMDNXI1-TENANT-B-CSP-CODE      VALUE '0520'      
                       '0541' '0562' '0564' '0570' '0580' '0832'        
                       '0940' '1051'.                                   
                    88 PMDNXI1-VACANCY-EXP-CLS-CODE   VALUE '0580'      
                       '0745' '0746' '0747' '0833' '0841' '0842'        
                       '0843' '0844' '0900' '1051' '1052'.              
                    88 PMDNXI1-STORAGE-CLS-CODE       VALUE '0511'      
                       '0512' '0520' '0531' '0532' '0541' '0561'        
                       '0562' '0563' '0564' '0565' '0566' '0567'        
                       '0570' '0581' '0582'.                            
                    88 PMDNXI1-GA-W-AND-H-EXCL-CODES  VALUE '0074'      
                       '0075' '0076' '0077' '0078' '0079' '0311'        
                       '0312' '0313' '0321' '0322' '0323'.              
                    88 PMDNXI1-APARTMENT-CLASS                          
                         VALUE '0311' '0312' '0313'                     
                               '0321' '0322' '0323'                     
                               '0074' '0075' '0076'.                    
                    88 PMDNXI1-OFFICE-CLASS     VALUE '0701' '0702'.    
                    88 PMDNXI1-CSP-SPECIAL-CLASS  VALUE                 
                         '1190' '1200' '0833'.                          
                    88 PMDNXI1-BOSTON-SPECIAL-CODES   VALUE '0074'      
                       '0075' '0076' '0745' '0746' '0747' '0311'        
                       '0312' '0313'.                                   
                 09 PMDNXI1-CSP-CONSTR-COD-A.                           
                    11 PMDNXI1-CSP-CONSTR-COD         PIC 9(01).        
                 09 PMDNXI1-PROTECTION-CLASS.                           
                    11 PMDNXI1-PROT-CLASS-PART1-A.                      
                       13 PMDNXI1-PROTECTION-CLASS-PART1 PIC 9(02).     
                    11 PMDNXI1-PROTECTION-CLASS-PART2 PIC X(02).        
                 09 PMDNXI1-GRPII-SYMBOL.                               
                    11 PMDNXI1-GRPII-EC-SYMBOL        PIC X(02).        
                    11 PMDNXI1-GRPII-NUM-PREFIX-A.                      
                       13 PMDNXI1-GRPII-NUM-PREFIX    PIC 9(01)V9(01).  
                         88 PMDNXI1-PREF-IS-ONE-HALF   VALUE 05.        
                 09 PMDNXI1-GRPI-SPECIFIC-RATE-A.                       
                    11 PMDNXI1-GRPI-SPECIFIC-RATE     PIC 9(03)V9(03).  
                 09 PMDNXI1-GRPII-SPECIFIC-RATE-A.                      
                    11 PMDNXI1-GRPII-SPECIFIC-RATE    PIC 9(03)V9(03).  
                 09 PMDNXI1-AR-REPORT-PERIOD          PIC X(01).        
                 09 PMDNXI1-AR-ADJ-PERIOD             PIC X(01).        
                 09 PMDNXI1-GENL-CLS-RT-GRP           PIC X(04).        
                 09 PMDNXI1-GRPI-CODE-A.                                
                    11 PMDNXI1-GRPI-CODE              PIC 9(03).        
                       88  PMDNXI1-NY-GRPI-OFFICE     VALUE 054.        
                 09 PMDNXI1-ZONE-CODE.                                  
                    88 PMDNXI1-SEACOAST-AREA-1         VALUE 'S1'.      
                    88 PMDNXI1-SEACOAST-AREA-2         VALUE 'S2'.      
                    88 PMDNXI1-SEACOAST-AREA-3         VALUE 'S3'.      
                    11 PMDNXI1-ZONE                   PIC X(01).        
                       88 PMDNXI1-SEACOAST             VALUE 'S'.       
                       88 PMDNXI1-BEACH                VALUE 'B'.       
                       88 PMDNXI1-INLAND               VALUE 'I'.       
                    11 PMDNXI1-ZONE-AREA              PIC X(01).        
                 09 PMDNXI1-OCC-CLS-SYM.                                
                    11 PMDNXI1-OCC-CLS                PIC X(01).        
                    11 PMDNXI1-OCC-SYM                PIC X(03).        
                 09 PMDNXI1-GRPII-TYPE                PIC X(01).        
                    88 PMDNXI1-HABITATIONAL            VALUE 'H'.       
                    88 PMDNXI1-NON-HABITATIONAL        VALUE 'N'.       
                 09 PMDNXI1-GRPII-CODE-A.                               
                    11 PMDNXI1-GRPII-CODE             PIC 9(03).        
                       88  PMDNXP1-ALL-OTH-RISKS-NO-BR     VALUE 013.   
                       88  PMDNXP1-ALL-OTH-RISKS-BLD-RISK  VALUE 015.   
                       88  PMDNXP1-VALID-NY-GRPII-CODES                 
                                                     VALUE 051 052.     
                 09 PMDNXI1-CLASS-DESCRIPTION         PIC X(60).        
                 09 PMDNXI1-MASS-DISTRICT             PIC X(01).        
                 09 PMDNXI1-MASS-TEN-RELO-EXP-A.                        
                    11 PMDNXI1-MASS-TEN-RELO-EXP      PIC 9(03).        
                 09 PMDNXI1-DEC-CHANGE-FLAG           PIC X(01).        
                    88 PMDNXI1-DEC-CHANGE-NEEDED      VALUE 'A'.        
                    88 PMDNXI1-DEC-CHANGE-NOTED       VALUE 'B'.        
                 09 PMDNXI1-PMS-FUTURE-USE            PIC X(157).       
                 09 PMDNXI1-CE-AGG-DEDUCT             PIC X(11).        
                 09 PMDNXI1-CUSTOMER-USE              PIC X(19).        
                 09 PMDNXI1-YR2000-CUST-USE           PIC X(100).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).