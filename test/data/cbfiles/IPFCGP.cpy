        01 IPFCGP-CLAIM-OCCURRENCE-RECORD.
           05 IPFCGP-CLAIM-OCCURRENCE-SEG.                             
              07 IPFCGP-SEGMENT-ID-KEY.                                
                 09 IPFCGP-SEGMENT-ID            PIC X(02).            
                 09 IPFCGP-SEGMENT-LEVEL-CODE    PIC X(01).            
                 09 IPFCGP-SEGMENT-PART-CODE     PIC X(01).            
                 09 IPFCGP-SUB-PART-CODE         PIC X(01).            
              07 IPFCGP-ITEM-EFFECTIVE-DATE      PIC 9(08).                           
              07 IPFCGP-GP-VARIABLE-KEY.                               
                 09 IPFCGP-LOSS-OCCURENCE    PIC 9(03).                
              07 IPFCGP-PROCESS-DATE         PIC 9(08).
              07 IPFCGP-CHANGE-ENTRY-GROUP.                            
                 09 IPFCGP-CHANGE-ENTRY-DATE      PIC 9(08).
                 09 IPFCGP-SEQUENCE-CHANGE-ENTRY  PIC 9(02).           
              07 IPFCGP-45GP-SEGMENT-DATA.                             
                 09 IPFCGP-SEGMENT-STATUS PIC X(01).                   
                    88 IPFCGP-ACTIVE-STATUS VALUE 'A'.                 
                    88 IPFCGP-CREATED-DUE-TO-AUDIT VALUE 'I' 'J' 'K'   
                          'L' 'M' 'N' 'O' 'P' 'Q' 'S' 'T' 'U' 'W'.     
                    88 IPFCGP-DELETED-STATUS VALUE 'D'.                
                    88 IPFCGP-HISTORY-STATUS VALUE 'H'.                
                    88 IPFCGP-NOT-ACTIVE-STATUS VALUE 'H' 'D' 'R'.     
                    88 IPFCGP-RENEWAL-CHANGE VALUE 'R'.                
                 09 IPFCGP-ENTRY-OPERATOR PIC X(03).                   
                 09 IPFCGP-REINSURANCE-INDICATOR PIC X(01).            
                 09 IPFCGP-ADJUSTOR-NUMBER.                            
                    11 IPFCGP-LOSS-ADJUSTOR-NO PIC X(03).              
                    11 FILLER            PIC X(01).                    
                 09 IPFCGP-LOSS-EXAMINER PIC X(04).                    
                 09 IPFCGP-OTHER-ASSIGNMENT PIC X(04).                 
                 09 IPFCGP-ACCOUNT-ENTERED-DATE PIC X(06).             
                 09 IPFCGP-LOSS-HANDLING-OFFICE PIC X(03).             
                 09 FILLER                      PIC X(01).             
                 09 IPFCGP-LOSS-HANDLING-BRANCH PIC X(02).             
                 09 IPFCGP-LOSS-LOCATION.                              
                    11 IPFCGP-LOSS-PLANT-LOCATION PIC X(06).           
                    11 IPFCGP-LOSS-ACCIDENT-STATE PIC X(02).           
                 09 IPFCGP-LOSS-CATASTROPHE-NO PIC X(02).              
                 09 IPFCGP-EXCESS-CATASTROPHE PIC X(05).               
                 09 IPFCGP-COMPANY-RECEIVED-DATE  PIC X(08).                      
                 09 IPFCGP-LOSS-TIME.                                  
                    11 IPFCGP-LOSS-2-POS PIC 9(02).                    
                    11 FILLER            PIC 9(02).                    
                 09 IPFCGP-LOSS-DAY      PIC X(03).                    
                 09 IPFCGP-POLICE-AUTHORITY PIC X(01).                 
                 09 IPFCGP-AGENT-RECEIVED-DATE    PIC X(08).
                 09 IPFCGP-BULK-CLAIMANT-COUNT PIC X(04).              
                 09 IPFCGP-DATE-OF-DISCOVERY   PIC X(08).
                 09 IPFCGP-CLAIM-GROUP.                                
                    11 IPFCGP-CLAIM-NUMBER PIC X(12).                  
                    11 IPFCGP-CLAIM-FILLER PIC X(02).                  
                 09 IPFCGP-EXCESS-LOSS-IND PIC X(01).                  
                 09 IPFCGP-CLAIM-STATUS  PIC X(01).                    
                 09 IPFCGP-SUMMARY-TOTALS-DETAIL.                      
                    11 IPFCGP-LOSS-ORIGINAL-RESERVE PIC S9(10)V9(02)   
                                                    COMP-3.            
                    11 IPFCGP-OS-LOSS-RESERVE PIC S9(10)V9(02) COMP-3. 
                    11 IPFCGP-OS-EXPENSE-RESERVE PIC S9(10)V9(02)      
                                                    COMP-3.            
                    11 IPFCGP-TOTAL-LOSS PIC S9(10)V9(02)  COMP-3.     
                    11 IPFCGP-TOTAL-EXPENSE PIC S9(10)V9(02) COMP-3.   
                    11 IPFCGP-TOTAL-RECOVERIES PIC S9(10)V9(02) COMP-3.
                 09 IPFCGP-BEG-OS-LOSS-RESERVE PIC S9(10)V9(02) COMP-3.
                 09 IPFCGP-ACCIDENT-DESCRIPTION    PIC X(30).          
                 09 IPFCGP-NUMBER-OF-PART78  PIC S9(03) COMP-3.        
                 09 IPFCGP-NOTICE-OF-OCCURRENCE PIC X(08).             
                 09 IPFCGP-OFFSET-ONSET-IND     PIC X(01).             
                    88  IPFCGP-OFFSET-RECORD    VALUE 'O'.             
                 09 IPFCGP-MASTER-CLAIM-NUMBER PIC X(15).              
                 09 IPFCGP-PROCESSING-LOCATION  PIC X(03).             
                 09 IPFCGP-PMS-FUTURE-USE-GP PIC X(92).                
                 09 IPFCGP-PREV-REP-CLMNT   PIC X(01).                 
                 09 IPFCGP-REP-CLMNT        PIC X(01).                 
                 09 IPFCGP-CWS-ORIGIN-IND   PIC X(01).                 
                 09 IPFCGP-WC-CLM-RPT-DATE  PIC X(08).                 
                 09 IPFCGP-SIU-ENTERED-DATE PIC X(08).                 
                 09 IPFCGP-CUST-SPL-USE-GP  PIC X(03).                 
                 09 IPFCGP-YR2000-CUST-USE   PIC X(100).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).