        01 PMD4Z-AUDIT-RATES-RECORD.
           03 PMD4Z-AUDIT-RATES-SEG.                                    
              07 PMD4Z-SEGMENT-ID                      PIC X(02).       
                 88 PMD4Z-SEGMENT-43                    VALUE '43'.     
              07 PMD4Z-SEGMENT-STATUS                  PIC X(01).       
                 88 PMD4Z-ACTIVE-STATUS                 VALUE 'A'.      
                 88 PMD4Z-DELETED-STATUS                VALUE 'D'.      
                 88 PMD4Z-HISTORY-STATUS                VALUE 'H'.      
                 88 PMD4Z-NOT-ACTIVE-STATUS             VALUE 'H' 'D'.  
                 88 PMD4Z-RENEWAL-FS-STATUS             VALUE 'R' 'F'.  
              07 PMD4Z-TRANSACTION-DATE                PIC 9(08).                                
              07 PMD4Z-SEGMENT-ID-KEY.                                  
                 09 PMD4Z-SEGMENT-LEVEL-CODE           PIC X(01).       
                    88 PMD4Z-SEG-LEVEL-U                VALUE 'U'.      
                    88 PMD4Z-SEG-LEVEL-X                VALUE 'X'.      
                 09 PMD4Z-SEGMENT-PART-CODE            PIC X(01).       
                    88 PMD4Z-AUDIT-RATES                VALUE 'Z'.      
                 09 PMD4Z-SUB-PART-CODE                PIC 9(01).       
                 09 PMD4Z-INSURANCE-LINE               PIC X(02).       
                    88 PMD4Z-GENERAL-LIABILITY          VALUE 'GL'.     
                    88 PMD4Z-COMMERCIAL-FIRE            VALUE 'CF'.     
                    88 PMD4Z-GARAGE                     VALUE 'AG'.     
              07 PMD4Z-LEVEL-KEY.                                       
                 09 PMD4Z-LOCATION-NUMBER              PIC X(04).       
                 09 PMD4Z-SUB-LOCATION-NUMBER          PIC X(03).       
                 09 PMD4Z-RISK-UNIT-GROUP-KEY          PIC X(06).       
                 09 PMD4Z-RISK-UNIT                    PIC X(06).       
                 09 PMD4Z-SEQUENCE-RISK-UNIT.                           
                    11 PMD4Z-RISK-SEQUENCE             PIC 9(01).       
                    11 PMD4Z-RISK-TYPE-IND             PIC X(01).       
                 09 PMD4Z-TYPE-EXPOSURE                PIC X(03).       
              07 PMD4Z-ITEM-EFFECTIVE-DATE             PIC 9(08).                             
              07 PMD4Z-VARIABLE-KEY.                                    
                 09 PMD4Z-ADD-INTEREST-CODE.                            
                    11 PMD4Z-ADD-INTEREST-TYPE         PIC X(01).       
                    11 PMD4Z-ADD-INTEREST-SEQ          PIC 9(02).       
                 09 FILLER                             PIC X(03).       
                 09 PMD4Z-SEQ-AUDIT-RATES              PIC 9(03).       
              07 PMD4Z-PROCESS-DATE                    PIC 9(08).                                    
              07 PMD4Z-SEGMENT-DATA.                                    
                 09 PMD4Z-RATE-DATA.                     
                    11 PMD4Z-RATE-TABLE  OCCURS 7 TIMES                 
                       INDEXED BY PMD4Z-RATE-INDX.                      
                       13 PMD4Z-AUDIT-NUMBER         PIC 9(02).         
                       13 PMD4Z-EXPIRATION-DATE      PIC 9(08).         
                       13 PMD4Z-RATE                 PIC 9(05)V9(03).   
                       13 PMD4Z-PREMIUM              PIC 9(09)V9(02).   
                       13 PMD4Z-AUDIT-BUCKET-STATUS    PIC X(01).       
                          88 PMD4Z-BUCKET-ACTIVE        VALUE ' '.      
                          88 PMD4Z-BUCKET-DELETE        VALUE 'D'.      
                          88 PMD4Z-BUCKET-CANCEL        VALUE 'C'.      
                          88 PMD4Z-BUCKET-REINST        VALUE 'R'.      
                          88 PMD4Z-BUCKET-ADDED         VALUE 'A'.      
                       13 PMD4Z-PREMIUM-MG-IND      PIC X(01).          
                          88 PMD4Z-MANUAL-PREM          VALUE 'M'.      
                          88 PMD4Z-GENERATED-PREM       VALUE ' '.      
                       13 FILLER                     PIC X(02).         
                 09 PMD4Z-PMS-FUTURE-USE             PIC X(46).         
                 09 PMD4Z-CUSTOMER-USE               PIC X(10).         
                 09 PMD4Z-YR2000-CUST-USE            PIC X(100).        
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).