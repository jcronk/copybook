        01 PMD4T-FORMS-RECORD.
           03 PMD4T-FORMS-SEG.                                          
            05 PMD4T-SEGMENT-KEY.                                       
              07 PMD4T-SEGMENT-ID                   PIC X(02).          
                 88 PMD4T-SEGMENT-43                 VALUE '43'.        
              07 PMD4T-SEGMENT-STATUS               PIC X(01).          
                 88 PMD4T-ACTIVE-STATUS              VALUE 'A'.         
                 88 PMD4T-DELETED-STATUS             VALUE 'D'.         
                 88 PMD4T-HISTORY-STATUS             VALUE 'H'.         
                 88 PMD4T-NOT-ACTIVE-STATUS          VALUE 'H' 'D'.     
                 88 PMD4T-RENEWAL-FS-STATUS          VALUE 'R' 'F'.     
              07 PMD4T-TRANSACTION-DATE             PIC 9(08).                                
              07 PMD4T-SEGMENT-ID-KEY.                                  
                 09 PMD4T-SEGMENT-LEVEL-CODE        PIC X(01).          
                    88 PMD4T-SEG-LEVEL-G             VALUE 'G'.         
                    88 PMD4T-SEG-LEVEL-L             VALUE 'L'.         
                    88 PMD4T-SEG-LEVEL-I             VALUE 'I'.         
                    88 PMD4T-SEG-LEVEL-N             VALUE 'N'.         
                    88 PMD4T-SEG-LEVEL-R             VALUE 'R'.         
                 09 PMD4T-SEGMENT-PART-CODE         PIC X(01).          
                    88 PMD4T-SEG-PART-T              VALUE 'T'.         
                 09 PMD4T-SUB-PART-CODE             PIC X(01).          
                 09 PMD4T-INSURANCE-LINE            PIC X(02).          
              07 PMD4T-LEVEL-KEY.                                       
                 09 PMD4T-LOCATION-NUMBER           PIC X(04).          
                 09 PMD4T-SUB-LOCATION-NUMBER       PIC X(03).          
                 09 PMD4T-RISK-UNIT-GROUP-KEY.                          
                    11 PMD4T-RISK-UNIT-GROUP        PIC 9(03).          
                    11 PMD4T-SEQ-RSK-UNT-GRP        PIC 9(03).          
                 09 PMD4T-RISK-UNIT                 PIC X(06).          
                 09 PMD4T-SEQUENCE-RISK-UNIT.                           
                    11 PMD4T-RISK-SEQUENCE          PIC 9(01).          
                    11 PMD4T-RISK-TYPE-IND          PIC X(01).          
              07 PMD4T-ITEM-EFFECTIVE-DATE          PIC 9(08).                             
              07 PMD4T-VARIABLE-KEY.                                    
                    11 PMD4T-SEQUENCE-OF-FORMS      PIC 9(03).          
              07 PMD4T-PROCESS-DATE                 PIC 9(08).                                    
             05 PMD4T-SEGMENT-DATA.                                     
                 09 PMD4T-FORMS-SECTIONS.                               
                    11 PMD4T-FORMS-ENTRIES OCCURS 16 TIMES              
                                         INDEXED BY                     
                                         PMD4T-FORMS-4T.                
                       13 PMD4T-FORM-NO              PIC X(08).         
                       13 PMD4T-FORM-DATE.                              
                          15 PMD4T-FORM-MONTH-A.                        
                             17 PMD4T-FORM-MONTH     PIC 9(02).         
                          15 PMD4T-FORM-YEAR-A.                         
                             17 PMD4T-FORM-YEAR      PIC 9(02).         
                       13 PMD4T-FORM-ACTION          PIC X(01).         
                          88 PMD4T-FORM-DELETED-GEN    VALUE '4'.       
                          88 PMD4T-FORM-DELETED-MAN    VALUE '5'.       
                          88 PMD4T-FORM-NEW-GENERATED  VALUE '0'.       
                          88 PMD4T-FORM-NEW-MANUAL     VALUE '2'.       
                          88 PMD4T-FORM-OLD-GENERATED  VALUE '1'.       
                          88 PMD4T-FORM-OLD-MANUAL     VALUE '3'.       
                          88 PMD4T-FORM-NEW-MANUAL-SCREEN  VALUE '7'.   
                          88 PMD4T-FORM-DELETED-GEN-RATED  VALUE '8'.   
                          88 PMD4T-FORM-DELETED-MAN-RATED  VALUE '9'.   
                          88 PMD4T-FORM-DELETED-MAN-SCREEN VALUE 'A'.   
                          88 PMD4T-FORM-DELETED-GEN-RATING VALUE 'B'.   
                 09 PMD4T-STATE-CODE                 PIC X(02).         
                 09 PMD4T-PMS-FUTURE-USE             PIC X(70).         
                 09 PMD4T-CUST-SPL-USE               PIC X(16).         
                 09 PMD4T-YR2000-CUST-USE            PIC X(100).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).