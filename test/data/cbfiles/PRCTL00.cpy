       01 PROCESS-CONTROL-REC.
           03 PROCESS-CONTROL-SEG.                                     
           05 PCR-CTL-ID                 PIC X(02).                    
              88 PROCESS-CONTROL-RECORD  VALUE '00'.                   
           05 PCR-REASON-AMENDED         PIC X(03).
           05 PCR-AGENTS-CASH-PREM       PIC S9(10)V9(02).             
           05 PCR-ISSUE-CODE             PIC X(01).                    
              88 PCR-AMENDED-DECLARATION VALUE 'A'.                    
              88 PCR-MANUAL-ISSUE        VALUE 'M'.                    
              88 PCR-NEW-COMPUTER-ISSUED VALUE 'N'.                    
              88 PCR-PRO-RATA-CANCEL     VALUE 'P'.                    
              88 PCR-QUOTATION           VALUE 'Q' 'O'.                
              88 PCR-ONLINE-QUOTES       VALUE 'O'.                    
              88 PCR-ONLINE-ENDORSEMENT  VALUE 'Z'.                    
              88 PCR-ONLINE-QUOTE-ENDT   VALUE 'O' 'Z'.                
              88 PCR-QUOTE-DECLINED      VALUE 'J'.                    
              88 PCR-QUOTE-REJECTED      VALUE 'K'.                    
              88 PCR-SHORT-RATE-CANCEL   VALUE 'S'.                    
              88 PCR-CORRECTION-REISSUE  VALUE 'X'.                    
              88 PCR-REINSTATE-SHORTRATE VALUE '6'.                    
              88 PCR-REINSTATE-PRO-RATA  VALUE '9'.                    
              88 PCR-RENEWAL-COMP-ISSUED VALUE 'R'.                    
              88 PCR-REINSTATEMENT       VALUE '6' '9'.                
              88 PCR-CANCELLATION        VALUE 'S' 'P'.                
              88 PCR-DEC-ISSUE-CODES     VALUE 'N' 'R' 'A' '6' '9' 'X' 
                    'Q' 'O' 'Z'.                                       
           05 PCR-EFFECTIVE-DATE         PIC 9(08).                                      
           05 PCR-NUMBER-STAT-REQ        PIC X(01).                    
           05 PCR-BILLING-CODE           PIC X(01).                    
              88 PCR-OUTPUT-TO-BCF-MOE   VALUE '1' '2'.                
              88 PCR-SELF-DESTRUCT-DELETED VALUE '2'.                  
           05 PCR-LOSS-CODE              PIC X(01).                    
              88 PCR-OUTPUT-TO-LOS-MOE   VALUE '1'.                    
           05 PCR-ALPHA-LOSS-INDEX       PIC X(01).                    
           05 PCR-RISK-STAT-PROV         PIC X(02).                    
              88 PCR-NORTH-CAROLINA      VALUE '32'.                   
              88 PCR-SOUTH-CAROLINA      VALUE '39'.                   
           05 PCR-AGENCY-TAX-LOCATION    PIC X(06).                    
           05 PCR-ORIG-ISSUE-CODE        PIC X(01).                    
              88 PCR-ORIG-MANUAL-ISSUED  VALUE 'M'.                    
              88 PCR-ORIG-QUOTE          VALUE 'Q'.                    
           05 PCR-WRITTEN-PREMIUM        PIC S9(10)V9(02).             
           05 PCR-TEXAS-IM-LIABILITY     PIC 9(06).                    
           05 PCR-RICO-AUTO              PIC X(01).                    
              88 PCR-RICO-ONN            VALUE '1'.                    
              88 PCR-RICO-OFF            VALUE '0'.                    
           05 FILLER                     PIC X(01).                    
           05 PCR-POL-STATUS-SD-CP       PIC X(01).                    
              88 PCR-CP-STATUS           VALUE 'T'.                    
              88 PCR-SD-STATUS           VALUE 'S'.                    
           05 PCR-AUTO-ON-CPP-POLICY-IND PIC X(01).                    
              88 PCR-MONO-COMM-AUTO      VALUE 'M'.                    
              88 PCR-PKG-COMM-AUTO       VALUE 'P'.                    
           05 PCR-ERROR-CODES.                                         
              88 PCR-NO-ERRORS-ON-POL    VALUE SPACES.                 
              10 PCR-ERR-CODES           OCCURS 20 TIMES.              
                 15 PCR-ERR-FIELD        PIC X(04).                    
           05 PCR-ADDITIONAL-DEC-HEADINGS.                             
              88 NO-EXTRA-HEADINGS       VALUE SPACES.                 
              10 PCR-ADDL-DEC-HEADERS OCCURS 6 TIMES INDEXED BY AD-I.  
                 15 PCR-ADH-USE          PIC X(02).                    
                 15 PCR-ADH-COPIES       PIC X(02).                    
           05 PCR-AUDIT-WORKSHEET-IND    PIC X(01).                    
              88  PCR-PRINT-WORKSHEET    VALUE 'W'.                    
              88  PCR-PRT-WKSHEETS-DECS  VALUE 'B'.                    
              88  PCR-PRT-DECS-ONLY      VALUE ' '.                    
           05 PCR-REVISED-SIMPLE-SW      PIC X(01).                    
              88  PROCESS-TEXAS-DEC      VALUE 'T'.                    
           05 PCR-TEXAS-CF-ON-POLICY-IND PIC X(01).                    
              88  PROCESS-TEXAS-CF-DEC   VALUE 'Y'.                    
           05 PCR-DEC-CHANGE-SW          PIC X(01).                    
              88 PCR-DEC-CHANGE          VALUE 'Y'.                    
           05 PCR-OFFSET-ONSET-POLICY-IND PIC X(01).                   
              88 PCR-OFFSET-ONSET-POLICY  VALUE 'Y' 'M'.               
              88 PCR-CROSS-POLICY-OFFSET  VALUE 'M'.                   
           05 PCR-MVR-ORDERS.                                          
              10 PCR-MVRS-ORDERED OCCURS 9 TIMES                       
                         INDEXED BY MVR-ORDER-INDEX                    
                                         PIC X(001).                   
           05 PCR-CPP-IM-IND             PIC X(001).                   
              88 PCR-PROCESS-CPP-IM-DECS VALUE 'Y'.                    
           05 PCR-WCP-POLICY-TAX-AMT     PIC 9(10).                    
           05 PCR-PMS-FUTURE-USE         PIC X(130).                   
           05 PCR-CLAIMS-STATUS          PIC X(01).                    
           05 PCR-ZZCG-BYPASS-INDICATOR  PIC X(01).                    
           05 PCR-TEXAS-TOTAL-POLICY-PREM PIC S9(05)V9(02).            
           05 PCR-TOTAL-PREMIUM          PIC S9(10)V9(02).             
           05 PCR-POLICY-FLAG            PIC X(01).                    
           05 PCR-IN-SUIT                PIC X(01).                    
           05 PCR-BILLING-FREEZE         PIC X(01).                    
           05 PCR-CHANGE-PREMIUM         PIC S9(10)V9(02).             
           05 PCR-YR2000-CUST-USE        PIC X(087).                   
           05 PCR-INSTEC-ZI-COMMNT       PIC X(01).                    
           05 PCR-SEPARATE-BILL          PIC X(01).                    
           05 PCR-MULTI-TIER             PIC X(01).                    
           05 PCR-UNDERWRITER            PIC X(02).                    
           05 PCR-LOSS-FREE              PIC X(01).
           05 PCR-AGCY-RENEW             PIC X(01).
           05 PCR-RENEWAL-CR             PIC X(01).
           05 PCR-UPLOAD-INDICATOR       PIC X(001).                   
              88  PCR-STAR-SOLUTIONS-UPLOAD             VALUE 'S'.
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).
