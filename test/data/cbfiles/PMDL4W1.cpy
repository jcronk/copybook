        01 PMDL4W1-POLICY-RATING-RECORD.
           03 PMDL4W1-POLICY-RATING-REC.                                   
              05 PMDL4W1-SEGMENT-KEY.                                     
               07  PMDL4W1-SEGMENT-ID                     PIC X(02).    
                   88  PMDL4W1-SEGMENT-43                   VALUE '43'. 
               07  PMDL4W1-SEGMENT-STATUS                 PIC X(01).    
                   88  PMDL4W1-ACTIVE-STATUS                VALUE 'A'.  
                   88  PMDL4W1-DELETED-STATUS               VALUE 'D'.  
                   88  PMDL4W1-HISTORY-STATUS               VALUE 'H'.  
               07  PMDL4W1-TRANSACTION-DATE               PIC 9(08).                            
               07  PMDL4W1-SEGMENT-ID-KEY.                              
                   09  PMDL4W1-SEGMENT-LEVEL-CODE         PIC X(01).    
                       88  PMDL4W1-SEGMENT-LEVEL-L          VALUE 'L'.  
                   09  PMDL4W1-SEGMENT-PART-CODE          PIC X(01).    
                       88  PMDL4W1-RATING-RECORD            VALUE 'X'.  
                       88  PMDL4W1-AUDIT-RECORD             VALUE 'Z'.  
                   09  PMDL4W1-SUB-PART-CODE              PIC X(01).    
                   09  PMDL4W1-INSURANCE-LINE             PIC X(02).    
                       88  PMDL4W1-WORKERS-COMPENSATION     VALUE 'WC'. 
               07  PMDL4W1-LEVEL-KEY.                                   
                   09  PMDL4W1-LOCATION-NUMBER            PIC X(04).    
                   09  FILLER                             PIC X(17).    
                   09  PMDL4W1-SPLIT-RATE-SEQ             PIC X(02).    
                       88  PMDL4W1-SPLIT-RATE-SEQ-00        VALUE '00'. 
                       88  PMDL4W1-SPLIT-RATE-SEQ-01        VALUE '01'. 
                       88  PMDL4W1-SPLIT-RATE-SEQ-02        VALUE '02'. 
               07  PMDL4W1-ITEM-EFFECTIVE-DATE            PIC 9(08).                         
               07  PMDL4W1-VARIABLE-KEY.                                
                   09  PMDL4W1-AUDIT-NUMBER               PIC 9(02).    
                   09  PMDL4W1-AUDIT-NUM-SEQ              PIC 9(01).    
                   09  FILLER                             PIC X(03).    
               07  PMDL4W1-PROCESS-DATE                   PIC 9(08).                                
           05  PMDL4W1-SEGMENT-DATA.                                    
               07  PMDL4W1-ITEM-EXPIRE-DATE               PIC 9(08).                            
               07  PMDL4W1-RATING-PROGRAM-TYPE            PIC X(01).    
                   88  PMDL4W1-RATE-TYPE-3-YR-FIXED         VALUE 'F'.  
                   88  PMDL4W1-RATE-TYPE-GUARNTD-COST       VALUE 'G'.  
                   88  PMDL4W1-RATE-TYPE-RETRO-RATED        VALUE 'R'.  
                   88  PMDL4W1-RATE-TYPE-WRAP-UP            VALUE 'W'.  
               07  PMDL4W1-POLICY-TYPE                    PIC X(01).    
                   88  PMDL4W1-POLICY-TYPE-ASSIGN-RSK       VALUE 'A'.  
                   88  PMDL4W1-POLICY-TYPE-EMP-LIAB         VALUE 'E'.  
                   88  PMDL4W1-POLICY-TYPE-STANDARD         VALUE 'S'.  
                   88  PMDL4W1-POLICY-TYPE-PER-ARSK         VALUE 'R'.  
                   88  PMDL4W1-POLICY-TYPE-PER-CAP       VALUE 'R' 'C'. 
               07  PMDL4W1-FED-EMP-ID-NUMBER              PIC X(09).    
               07  PMDL4W1-COMMISSION-TYPE                PIC X(01).    
                   88  PMDL4W1-COMMISSION-TYPE-FLAT         VALUE 'X'.  
                   88  PMDL4W1-COMMISSION-TYPE-GRADED       VALUE 'G'.  
                   88  PMDL4W1-COMMISSION-TYPE-STD          VALUE 'S'.  
               07  PMDL4W1-COMMISSION-SCHEDULE            PIC X(04).    
               07  PMDL4W1-COMM-SCHED-MG-IND              PIC X(01).    
                   88  PMDL4W1-COMM-SCHED-NOT-ENTERED       VALUE ' '.  
                   88  PMDL4W1-COMM-SCHED-MAN-ENTRD         VALUE 'M'.  
                   88  PMDL4W1-COMM-SCHED-GENERATED         VALUE 'G'.  
               07  PMDL4W1-COMMISSION-RATE                PIC 9V9(05).  
               07  PMDL4W1-COMM-RATE-MG-IND               PIC X(01).    
                   88  PMDL4W1-COMM-RATE-NO-ENTERED       VALUE ' '.    
                   88  PMDL4W1-COMM-RATE-MAN-ENTRD        VALUE 'M'.    
                   88  PMDL4W1-COMM-RATE-GENERATED        VALUE 'G'.    
               07  PMDL4W1-INTERSTATE-RISK-ID-NO          PIC X(09).                       
               07  PMDL4W1-INTERSTATE-EXP-MOD-1     PIC 9(01)V9(04).    
               07  PMDL4W1-INTER-MOD-1-TYPE               PIC X(01).    
               07  PMDL4W1-INTERSTATE-EXP-MOD-2     PIC 9(01)V9(04).    
               07  PMDL4W1-INTER-MOD-2-TYPE               PIC X(01).    
               07  PMDL4W1-ANN-RATE-DTE-MMDD.                           
                   09  PMDL4W1-ANN-RATE-DTE-MM            PIC 9(02).    
                   09  PMDL4W1-ANN-RATE-DTE-DD            PIC 9(02).    
               07  PMDL4W1-SPLIT-RATE-IND                 PIC X(01).    
                   88  PMDL4W1-SPLIT-RATE-APPLICABLE  VALUE 'S' 'M'.    
                   88  PMDL4W1-NOT-SPLIT-YET                VALUE 'S'.  
               07  PMDL4W1-PREV-CANC-REASON               PIC X(03).    
               07  PMDL4W1-PREV-CANC-DATE                 PIC X(08).    
               07  PMDL4W1-COVERAGE-II-LMTS-STD.                        
                   09  PMDL4W1-COV-II-LMTS-STD-EACH       PIC 9(05).    
                   09  PMDL4W1-COV-II-LMTS-STD-POL        PIC 9(05).    
               07  PMDL4W1-COVERAGE-II-LMTS-VC.                         
                   09  PMDL4W1-COV-II-LMTS-VC-EACH        PIC 9(05).    
                   09  PMDL4W1-COV-II-LMTS-VC-POL         PIC 9(05).    
               07  PMDL4W1-COVERAGE-II-LMTS-FED.                        
                   09  PMDL4W1-COV-II-LMTS-FED-EACH       PIC 9(05).    
                   09  PMDL4W1-FED-PROGRAM-TYPE           PIC X(01).    
                       88  PMDL4W1-FED-PROGRAM-TYPE-1       VALUE '1'.  
                       88  PMDL4W1-FED-PROGRAM-TYPE-2       VALUE '2'.  
               07  PMDL4W1-USLH-COVERAGE-FLAG             PIC X(01).    
                   88  PMDL4W1-USLH-COV-FLAG-N              VALUE 'N'.  
                   88  PMDL4W1-USLH-COV-FLAG-Y              VALUE 'Y'.  
               07  PMDL4W1-VOL-COMP-COVERAGE-FLAG         PIC X(01).    
                   88  PMDL4W1-VOL-COMP-COV-FLAG-N          VALUE 'N'.  
                   88  PMDL4W1-VOL-COMP-COV-FLAG-Y          VALUE 'Y'.  
               07  PMDL4W1-RETRO-OPTION-ID                PIC X(01).    
                   88  PMDL4W1-SUPPRESS-PREM-DISCOUNT       VALUE 'A'   
                                                                  'B'   
                                                                  'C'   
                                                                  'D'   
                                                                  'J'.  
                   88  PMDL4W1-RETRO-OPTION-A               VALUE 'A'.  
                   88  PMDL4W1-RETRO-OPTION-B               VALUE 'B'.  
                   88  PMDL4W1-RETRO-OPTION-C               VALUE 'C'.  
                   88  PMDL4W1-RETRO-OPTION-D               VALUE 'D'.  
                   88  PMDL4W1-RETRO-OPTION-J               VALUE 'J'.  
                   88  PMDL4W1-NOT-RETRO-RATED              VALUE 'N'.  
               07  PMDL4W1-OTHER-ST-COVERAGE-FLAG         PIC X(01).    
                   88  PMDL4W1-DEFAULT-OS-COV               VALUE 'D'.  
                   88  PMDL4W1-SPECIFIC-OS-COV              VALUE 'S'.  
                   88  PMDL4W1-NO-OS-COV-APPLIES            VALUE 'N'.  
                   88  PMDL4W1-EXCEPTION-OS-COV             VALUE 'E'.  
               07  PMDL4W1-OTHER-STATES-COVERED-X.                      
                   09  PMDL4W1-OTHER-STATES-COVERED                     
                       OCCURS  24  TIMES                                
                       INDEXED BY                                       
                       PMDL4W1-COV-ST-INDX.                             
                       11  PMDL4W1-COVERED-STATE          PIC X(02).    
               07  PMDL4W1-STANDARD-PREMIUM               PIC 9(10).    
               07  PMDL4W1-PREMIUM-DISCOUNT-AMT           PIC S9(09).   
                   88 PMDL4W1-NO-PREMIUM-DISCOUNT         VALUE ZERO.   
               07  PMDL4W1-EXPENSE-CONSTANT-STATE         PIC X(02).    
               07  PMDL4W1-EXPENSE-CONSTANT-AMT           PIC 9(04).    
               07  PMDL4W1-POLICY-MIN-PREM-STATE          PIC X(02).    
               07  PMDL4W1-POLICY-MIN-PREMIUM             PIC 9(05).    
               07  PMDL4W1-POLICY-TERM-PREMIUM            PIC 9(10).    
               07  PMDL4W1-TAX-ASSESS-CHARGE              PIC 9(10).    
               07  PMDL4W1-DEPOSIT-PREMIUM-PCT            PIC 9(06).    
               07  PMDL4W1-DEP-PREM-PCT-MG-IND            PIC X(01).    
                   88  PMDL4W1-DEP-PREM-PCT-NOT-ENTRD       VALUE ' '.  
                   88  PMDL4W1-DEP-PREM-PCT-MAN-ENTRD       VALUE 'M'.  
                   88  PMDL4W1-DEP-PREM-PCT-GENERATED       VALUE 'G'.  
               07  PMDL4W1-DEPOSIT-PREMIUM                PIC 9(10).    
               07  PMDL4W1-DEP-PREM-MG-IND                PIC X(01).    
                   88  PMDL4W1-DEP-PREM-NOT-ENTRD          VALUE ' '.   
                   88  PMDL4W1-DEP-PREM-MAN-ENTRD          VALUE 'M'.   
                   88  PMDL4W1-DEP-PREM-GENERATED          VALUE 'G'.   
               07  PMDL4W1-FEDERAL-COVERAGE-CODE          PIC X(01).    
                   88  PMDL4W1-FED-EMP-LIABLTY-ACT          VALUE 'L'.  
                   88  PMDL4W1-ADMIRALTY                    VALUE 'A'.  
                   88  PMDL4W1-NO-FEDERAL-COVERAGE          VALUE ' '.  
               07  PMDL4W1-POL-JULIAN-EFF-DATE.                         
                   09  PMDL4W1-POL-JULIAN-EFF-DTE-YY      PIC 9(04).    
                   09  PMDL4W1-POL-JULIAN-EFF-DTE-DDD     PIC 9(03).    
               07  PMDL4W1-PREV-CANCEL-ENTRY-DATE         PIC 9(08).    
           05  PMDL4W1-INTERST-ARAP-RATE-1             PIC 9(01)V9(04). 
           05  PMDL4W1-INTERST-ARAP-RATE-2             PIC 9(01)V9(04). 
           05  PMDL4W1-LATEST-COVII-IDS.                                
               10  PMDL4W1-CURR-COVII-STD-ID              PIC X(02).    
                   88  PMDL4W1-CURR-BASIC-LIMITS          VALUE 'BL'.   
               10  PMDL4W1-CURR-COVII-VOL-ID              PIC X(02).    
               10  PMDL4W1-CURR-COVII-ADM-ID              PIC X(02).    
           05  PMDL4W1-PMS-FUTURE-USE                     PIC X(38).    
           05  PMDL4W1-MMS-CHANGE-BYTE                    PIC X(01).    
           05  PMDL4W1-EXPER-RATING-CODE                  PIC X(01).    
           05  PMDL4W1-CUSTOMER-FUTURE-USE                PIC X(02).    
           05  PMDL4W1-AUTO-TERRORISM-CHG-X.                            
               10  PMDL4W1-AUTO-TERRORISM-CHG             PIC S9(10).   
           05  PMDL4W1-AUTO-TERRORISM-TOT-X.                            
               10  PMDL4W1-AUTO-TERRORISM-TOT             PIC S9(10).   
           05  PMDL4W1-YR2000-CUST-USE                    PIC X(80).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).