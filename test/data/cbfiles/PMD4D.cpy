        01 PMD4D-AUDIT-SCHEDULE-RECORD.
           03 PMD4D-AUDIT-SCHEDULE-SEG.                                 
              07 PMD4D-SEGMENT-ID                   PIC X(02).          
                 88 PMD4D-SEGMENT-43                 VALUE '43'.        
              07 PMD4D-SEGMENT-STATUS               PIC X(01).          
                 88 PMD4D-ACTIVE-STATUS              VALUE 'A'.         
                 88 PMD4D-DELETED-STATUS             VALUE 'D'.         
                 88 PMD4D-HISTORY-STATUS             VALUE 'H'.         
                 88 PMD4D-NOT-ACTIVE-STATUS          VALUE 'H' 'D'.     
                 88 PMD4D-RENEWAL-FS-STATUS          VALUE 'R' 'F'.     
              07 PMD4D-TRANSACTION-DATE             PIC 9(08).                                
              07 PMD4D-SEGMENT-ID-KEY.                                  
                 09 PMD4D-SEGMENT-LEVEL-CODE        PIC X(01).          
                    88 PMD4D-SEG-LEVEL-G             VALUE 'G'.         
                    88 PMD4D-SEG-LEVEL-L             VALUE 'L'.         
                    88 PMD4D-SEG-LEVEL-R             VALUE 'R'.         
                    88 PMD4D-SEG-LEVEL-J             VALUE 'J'.         
                 09 PMD4D-SEGMENT-PART-CODE         PIC X(01).          
                    88 PMD4D-AUDIT-SCHEDULE          VALUE 'D'.         
                 09 PMD4D-SUB-PART-CODE             PIC 9(01).          
                 09 PMD4D-INSURANCE-LINE            PIC X(02).          
                    88 PMD4D-GENERAL-LIABILITY       VALUE 'GL'.        
                    88 PMD4D-COMMERCIAL-FIRE         VALUE 'CF'.        
                    88 PMD4D-GARAGE                  VALUE 'AG'.        
                    88 PMD4D-WORKERS-COMP            VALUE 'WC'.        
              07 PMD4D-LEVEL-KEY.                                       
                 09 PMD4D-LOCATION-NUMBER           PIC 9(04).          
                 09 PMD4D-RISK-UNIT-GROUP-KEY.                          
                    11 PMD4D-RISK-UNIT-GROUP        PIC 9(03).          
                    11 PMD4D-SEQ-RSK-UNT-GRP        PIC 9(03).          
                 09 PMD4D-RISK-UNIT                 PIC X(06).          
                 09 PMD4D-SEQUENCE-RISK-UNIT.                           
                    11 PMD4D-RISK-SEQUENCE          PIC 9(01).          
                    11 PMD4D-RISK-TYPE-IND          PIC X(01).          
              07 PMD4D-ITEM-EFFECTIVE-DATE          PIC 9(08).                             
              07 PMD4D-VARIABLE-KEY.                                    
                 09 PMD4D-SEQ-AUDIT-SCHEDULE        PIC 9(03).          
              07 PMD4D-PROCESS-DATE                 PIC 9(08).
              07 PMD4D-SEGMENT-DATA.                                    
                 09 PMD4D-FREQUENCY                 PIC X(01).          
                    88 PMD4D-ANNUAL                    VALUE 'A'.       
                    88 PMD4D-SEMI-ANNUAL               VALUE 'S'.       
                    88 PMD4D-QUARTERLY                 VALUE 'Q'.       
                    88 PMD4D-MONTHLY                   VALUE 'M'.       
                    88 PMD4D-TERMINAL                  VALUE 'T'.       
                    88 PMD4D-DAILY                     VALUE 'D'.       
                    88 PMD4D-WEEKLY                    VALUE 'K'.       
                    88 PMD4D-NOT-APPLICABLE    VALUES 'N' ' ' 'W'.      
                 09 PMD4D-TYPE                      PIC X(01).          
                    88 PMD4D-PHYSICAL                  VALUE 'P'.       
                    88 PMD4D-VOLUNTARY                 VALUE 'V'.       
                    88 PMD4D-PHYSICAL-FINAL            VALUE 'X'.       
                    88 PMD4D-UNDECIDED                 VALUE 'U'.       
                 09 PMD4D-AUDIT-TABLE.                                  
                    11 PMD4D-AUDIT-DATA OCCURS 4 TIMES                  
                       INDEXED BY AUDIT-INDEX.                          
                         13 PMD4D-AUDIT-NUMBER      PIC 9(02).          
                            88 PMD4D-NO-AUDIT-DATA      VALUE 00.       
                         13 PMD4D-STATUS            PIC X(01).          
                            88 PMD4D-UNASSIGNED         VALUE 'N'.      
                            88 PMD4D-COMPLETED          VALUE 'C'.      
                            88 PMD4D-AMENDED            VALUE 'A'.      
                            88 PMD4D-REQUESTED          VALUE 'R'.      
                            88 PMD4D-BYPASSED           VALUE 'B'.      
                            88 PMD4D-ESTIMATED          VALUE 'E'.      
                            88 PMD4D-REVERSED           VALUE 'V'.      
                            88 PMD4D-OVERDUE            VALUE 'O'.      
                            88 PMD4D-COVERAGE-CANCELLED VALUE 'X'.      
                            88 PMD4D-UNSCHED-REQUEST    VALUE 'U'.      
                            88 PMD4D-UNSCHED-COMPLETE   VALUE 'K'.      
                            88 PMD4D-UNSCHED-OVERDUE    VALUE 'P'.      
                            88 PMD4D-DELETED            VALUE 'D'.      
                         13 PMD4D-TRANSACTION-TYPE  PIC X(01).          
                            88 PMD4D-INQUIRY-MODE       VALUE 'I'.      
                            88 PMD4D-RETURN-AUDIT       VALUE 'C'.      
                            88 PMD4D-AMEND-AUDIT        VALUE 'A'.      
                            88 PMD4D-PRINT-WS           VALUE 'W'.      
                            88 PMD4D-PRINT-STATEMENT    VALUE 'S'.      
                            88 PMD4D-BYPASS-TRANS       VALUE 'B'.      
                            88 PMD4D-ESTIMATE-TRANS     VALUE 'E'.      
                            88 PMD4D-UNSCHED-AUDIT      VALUE 'U'.      
                            88 PMD4D-REVERSE-AUDIT      VALUE 'V'.      
                            88 PMD4D-OVERDUE-AUDIT      VALUE 'O'.      
                            88 PMD4D-GROUP-AUDIT        VALUE 'G'.      
                            88 PMD4D-DELETE-PERIOD      VALUE 'D'.      
                         13 PMD4D-AUDIT-TYPE        PIC X(01).          
                            88 PMD4D-UNSCHEDULED        VALUE 'U'.      
                            88 PMD4D-PHYSICAL-DET       VALUE 'P'.      
                            88 PMD4D-VOLUNTARY-DET      VALUE 'V'.      
                            88 PMD4D-PHYSICAL-INTERIM   VALUE 'F'.      
                            88 PMD4D-VOLUNTARY-INTERIM  VALUE 'G'.      
                            88 PMD4D-PHYSICAL-FINAL-DET VALUE 'P'.      
                            88 PMD4D-VOLUNTARY-FINAL    VALUE 'V'.      
                         13 PMD4D-EFFECTIVE-DATE    PIC 9(08).          
                         13 PMD4D-EXPIRATION-DATE   PIC 9(08).          
                         13 PMD4D-AUDIT-REQ-DATE    PIC 9(08).          
                         13 PMD4D-ENTERED-DATE      PIC 9(08).          
                         13 PMD4D-AUDITOR-CODE      PIC X(06).          
                            88 PMD4D-CANCEL-TERM-AUDIT VALUE 'CANCEL'.  
                         13 PMD4D-AUDIT-COST        PIC 9(05)V9(02).    
                         13 PMD4D-AUDIT-TIME        PIC 9(03)V9(01).    
                         13 PMD4D-AUDIT-PREMIUM     PIC 9(09)V9(02).    
                 09 PMD4D-CANCELLATION-DATE         PIC X(08).          
                 09 PMD4D-NUMBER-OF-REQSTS          PIC X(02).          
                 09 PMD4D-AUDIT-DATES-IND           PIC X(01).          
                    88 PMD4D-CALENDAR-MONTHS        VALUE 'M'.          
                    88 PMD4D-POLICY-DATES           VALUE 'P'.          
                    88 PMD4D-NO-AUDIT-DATE-IND      VALUE ' '.          
                 09 PMD4D-PMS-FUTURE-USE            PIC X(12).          
                 09 PMD4D-CANCELLATION-TYPE         PIC X(01).          
                 09 PMD4D-PREVIOUS-AUDIT-TYPE       PIC X(01).          
                 09 PMD4D-CUSTOMER-FUTURE-USE       PIC X(12).          
                 09 PMD4D-YR2000-CUST-USE           PIC X(100).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).