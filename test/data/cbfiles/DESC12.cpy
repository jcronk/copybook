        01 DESCRIPTION-RECORD.
           03 DESCRIPTION-FULL-SEG.                                    
           04 DESCRIPTION-SEG.                                         
           04 DESCRIPTION-ID             PIC X(02).                    
              88 DESCRIPTION-REC         VALUE IS '12'.                
           04 DESCRIPTION-KEY.                                         
              10 DESCRIPTION-USE.                                      
                 15 USE-CODE             PIC X(02).                    
                    88 DESCRIPTION-BANK-INFO    VALUE 'BI'.            
                    88 DESCRIPTION-ARS-ACTIVITY VALUE 'AA'.            
                    88 DESCRIPTION-RESPOND-FIRE-DEPT  VALUE 'RS'.      
                    88 DESCRIPTION-RESPOND-FIRE-DIST  VALUE 'RZ'.      
                    88 DESCRIPTION-ARS-MESSAGES VALUE 'AB'.            
                    88 DESCRIPTION-ADDL-COMMENT VALUE 'AC'.            
                    88 DESCRIPTION-ADDL-INSD VALUE 'AI'.               
                    88 DESCRIPTION-ADDL-LOC VALUE 'AL'.                
                    88 DESCRIPTION-BUSINESS-DESC   VALUE 'BD'.         
                    88 DESCRIPTION-VOLUNTEER-FIREMEN VALUE 'VF'.       
                    88 DESCRIPTION-NAMED-INSURED VALUE 'NI'.           
                    88 DESCRIPTION-FEDERAL-ID VALUE 'FI'.              
                    88 DESCRIPTION-FEDERAL-TAX-ID VALUE 'FT'.          
                    88 DESCRIPTION-AUTO-OWNER  VALUE 'AO'.             
                    88 DESCRIPTION-BUREAU-FILE-NUMBER VALUE 'BN'.      
                    88 DESCRIPTION-CASE-NUMBER VALUE 'CN'.             
                    88 DESCRIPTION-TIER-CHANGE VALUE 'TC'.             
                    88 DESCRIPTION-MISC-INFO VALUE 'MI'.               
                    88 DESCRIPTION-COMMERCIAL-REPORT VALUE 'CR'.       
                    88 DESCRIPTION-CR-FOR-EXIST-INS  VALUE 'CE'.       
                    88 DESCRIPTION-LEGAL-ADDRESS VALUE 'LA'.           
                    88 DESCRIPTION-REPUBLIC-RISK-LOC VALUE 'RL'.       
                    88 DESCRIPTION-MORTGAGEE VALUE 'MT'.               
                    88 DESCRIPTION-COUNTY-TERRITORY VALUE 'CT'.        
                    88 DESCRIPTION-SECOND-MORTGAGEE VALUE 'SM'.        
                    88 DESCRIPTION-THIRD-MORTGAGEE VALUE 'TM'.         
                    88 DESCRIPTION-MORTGAGEES VALUE 'MT' 'SM' 'TM'.    
                    88 DESCRIPTION-LOSS-PAYEE VALUE 'LP'.              
                    88 DESCRIPTION-JOINT-OWNER VALUE 'JO'.             
                    88 DESCRIPTION-LEASING-COMPANY   VALUE 'LC'.       
                    88 DESCRIPTION-BIND-VERIFICATION VALUE 'BV'.       
                    88 DESCRIPTION-REG-OWNER         VALUE 'RO'.       
                    88 DESCRIPTION-POLICY-INQUIRY    VALUE 'IQ'.       
                    88 DESCRIPTION-CLEAR-UNPAID-PREM VALUE 'CU'.       
                    88 DESCRIPTION-VIN-NUMBER        VALUE 'VN'.       
                    88 DESCRIPTION-PLATE-NUMBER      VALUE 'PL'.       
                    88 DESCRIPTION-DRIVER-LICENSE    VALUE 'DL'.       
                    88 DESCRIPTION-BROK-AGENT VALUE 'BA'.              
                    88 DESCRIPTION-PREM-FIN-CO VALUE 'PF'.             
                    88 DESCRIPTION-GARAGING-LOCATION VALUE 'GL'.       
                    88 DESCRIPTION-DOCUMENT-REC-DATE VALUE 'DT'.       
                    88 DESCRIPTION-BILL-OTHERS VALUE 'BO'.             
                    88 DESCRIPTION-CLAIMS-PAYEE VALUE 'CP'.            
                    88 DESCRIPTION-PAYEE-NAME VALUE 'PN'.              
                    88 DESCRIPTION-COMM-RATE-MODS VALUE 'CF' 'OB' 'RM'.
                    88 DESCRIPTION-COMM-COMPOS-FACTOR VALUE 'CF'.      
                    88 DESCRIPTION-COMM-RATE-MOD-RM VALUE 'RM'.        
                    88 DESCRIPTION-COMM-RATE-DEPART VALUE 'RD'.        
                    88 DESCRIPTION-COMM-RATE-MOD-OB VALUE 'OB'.        
                    88 DESCRIPTION-COMM-COV-AUTO VALUE 'CA'.           
                    88 DESCRIPTION-COMM-SIZE-RISK VALUE 'SR'.          
                    88 DESCRIPTION-COMM-TORT-LIMIT VALUE 'TL'.         
                    88 DESCRIPTION-REASON-AMENDED VALUE 'RA'.          
                    88 DESCRIPTION-REWRITE  VALUE 'RW'.                
                    88 DESCRIPTION-RATE-CHANGE VALUE 'RI'.             
                    88 DESCRIPTION-STANDARD-TIME VALUE 'ST'.           
                    88 DESCRIPTION-WAIVER-OF-SUBRO VALUE 'WS'.         
                    88 DESCRIPTION-IN-CARE-OF VALUE 'NC'.              
                    88 DESCRIPTION-FLAT-BILL VALUE 'FB'.               
                    88 DESCRIPTION-AGENT-COMM VALUE 'SC' 'IC'.         
                    88 DESCRIPTION-CLASS-OVERRIDE VALUE 'CD'.          
                    88 DESCRIPTION-CERTIFICATE-HOLDER VALUE 'CH'.      
                    88 DESCRIPTION-GROUP-BILL-DATA VALUE 'GB'.         
                    88 DESCRIPTION-OTHER-STATES VALUE 'OS'.            
                    88 DESCRIPTION-COMMISSION-OVRD VALUE 'CO'.         
                    88 DESCRIPTION-INTEREST-ADDL-INSD VALUE 'IN'.      
                    88 DESCRIPTION-LEGAL-ENTITY-EXTRA VALUE 'LO'.      
                    88 DESCRIPTION-HIRED-AUTO-PD-RATE VALUE 'HP'.      
                    88 DESCRIPTION-AUTOMOBILE VALUE 'AI' 'AL' 'BA'     
                          'CA' 'CF' 'CH' 'GL' 'IN' 'LA' 'OB' 'PF' 'RM' 
                          'SR' 'TL' 'CP' 'LP' 'BO' 'PN' 'CO' 'SU' 'ZZ' 
                          'RD' 'RA' 'CN' 'AA' 'AB' 'TI' 'PR' 'NE'      
                          'DC' 'EC' 'UC' 'TE' 'AO' 'FT'                
                          'LC' 'BV' 'RO' 'IQ' 'CU' 'VN' 'PL' 'DL' 'TC' 
                          'PH' 'AC' 'NI' 'PD' 'HG' 'AR' 'LO' 'HP' 'JO'.
                    88 DESCRIPTION-PROPERTY VALUE 'BA' 'LA' 'LP' 'MT'  
                          'SM' 'TM' 'CP' 'BO' 'PN' 'AI' 'AA' 'AB' 'MH' 
                          'IN' 'AL' 'BD' 'TE' 'BC'                     
                          'CN'                                         
                          'PH'.                                        
                    88 DESCRIPTION-PROPDEC-COVDESC VALUE  'AI' 'AL'    
                          'IN' 'LA' 'LP' 'PF' 'PH' 'PN' 'TM' 'CP'.     
                    88 DESCRIPTION-MORT-LOSS-PAYEE VALUE 'BA' 'LP' 'MT'
                          'SM' 'TM' 'PF'.                              
                    88 DESCRIPTION-SC-SURCHRG-DISC    VALUE 'ZZ'.      
                    88 DESCRIPTION-ON-LINE-QUOTE      VALUE '$$'.      
                    88 DESCRIPTION-OR-MANDAYS         VALUE 'OR'.      
                    88 DESCRIPTION-TRANSFER-INS       VALUE 'TI'.      
                    88 DESCRIPTION-MOBILEHOME         VALUE 'MH'.      
                    88 DESCRIPTION-HAP-MED-PAYMENTS   VALUE 'MP'.      
                    88 DESCRIPTION-HAP-PERSONAL-INJ   VALUE 'PI'.      
                    88 DESCRIPTION-PRE-INSPECTION  VALUE 'PR'.         
                    88 DESCRIPTION-NON-OWNER-WV VALUE 'EN'.            
                    88 DESCRIPTION-DRVR-RESTR-CERT  VALUE 'DC'.        
                    88 DESCRIPTION-USE-LIMIT-CERT   VALUE 'UC'.        
                    88 DESCRIPTION-EXCLUSION-CERT   VALUE 'EC'.        
                    88 DESCRIPTION-AMENDED-DEC-CHNG VALUE 'AD'.        
                    88 DESCRIPTION-TAX-EXEMPTION    VALUE 'TE'.        
                    88 DESCRIPTION-MISC-ADJ-TAX-ID  VALUE 'TX'.        
                    88 DESCRIPTION-PAYROLL-ADDR VALUE 'PA'.            
                    88 DESCRIPTION-PICS-ENDS    VALUE 'PE'.            
                    88 DESCRIPTION-PICS-PREMIUM VALUE 'PP'.            
                    88 DESCRIPTION-INSURED-PHONE-NUM VALUE 'PH'.       
                    88 DESCRIPTION-PREMIUM-DISC VALUE 'PD'.            
                    88 DESCRIPTION-NON-ELIGIBLE  VALUE 'NE'.           
                    88 DESCRIPTION-OVERRIDE-DATE VALUE 'RR'.           
                    88 DESCRIPTION-COMMISSION-SCH VALUE 'CS'.          
                    88 DESCRIPTION-3RD-PARTY      VALUE 'TP'.          
                    88 DESCRIPTION-NON-PAY-LET-RECIP  VALUES           
                       'LP' 'MT' 'AI' 'AO'.                            
                 15 USE-LOCATION         PIC X(03).                    
             10 DESCRIPTION-SEQUENCE    PIC X(01).                    
                 88 CF-SEQ               VALUE 'C' 'L' 'O'.            
                 88 CF-SEQ-FLEET         VALUE 'F'.                    
           04 DESCRIPTION-INFORMATION.                                 
              05 DESCRIPTION-SORT-NAME   PIC X(04).                    
              05 DESCRIPTION-ZIP-CODE.                                 
                 10 FILLER                 PIC X(01).                  
                 10 DESC-ZIP-5-DIGIT-CODE  PIC X(05).                  
              05 DESCRIPTION-LINE-1      PIC X(30).                    
              05 DESCRIPTION-LINE-2      PIC X(30).                    
              05 DESCRIPTION-LINE-3      PIC X(30).                    
              05 DESCRIPTION-LINE-4      PIC X(30).                    
           04 DESCRIPTION-NAME-CODE      PIC X(04).                    
           88 DESCR-NAME-CODES-NOT-ON-MORT VALUE '    ' '9999'.        
           04 DESC-UK-POSTAL-CODE        PIC X(08).                    
           04 DESC-MISC-ADJ-ZIP-CODE     PIC X(06).                    
           04 DESC-MISC-ADJ-TAX-ID       PIC X(09).                    
           04 DESC-PMS-FUTURE-USE        PIC X(138).                   
           04 DESCRIPTION-LOAN-NUMBER    PIC X(10).                    
           04 DESC-ACA-IDENTIFIER-SAVE.                                
              05 DESC-ACA-FIXED-IDENTIFIER PIC 9(04).                  
           04 DESC-CUST-FUTURE-USE         PIC X(24).                  
           04 DESC-CLAIM-TRAN-CATEGORY     PIC X(02).
           04 DESC-DIRECTION-INDICATOR     PIC X.                      
           88 DESC-NORTH                   VALUE 'N'.                  
           88 DESC-SOUTH                   VALUE 'S'.                  
           88 DESC-EAST                    VALUE 'E'.                  
           88 DESC-WEST                    VALUE 'W'.                  
           04 DESCRIPTION-LOAN-NUMBER2     PIC X(05).                  
           04 DESC-YR2000-CUST-USE       PIC X(049).                   
           04 DESC-ISO-LOC-FD-OVERRIDE   PIC X.  
           04 DESC-G1GC-INFORMATION.                                   
              10 DESC-G1GC-ZIP-PLUS06.                                 
                 15 DESC-G1GC-ZIP-PLUS04 PIC X(04).                    
                 15 DESC-G1GC-ZIP-PLUS0506 PIC X(02).                  
              10 DESC-G1GC-DPBC-CK-DIGIT PIC X(01).                    
              10 DESC-G1GC-CARRIER-ROUTE PIC X(04).                    
              10 DESC-G1GC-FIPS-ST-COUNTY.                             
                 15 DESC-G1GC-FIPS-STATE PIC X(02).                    
                 15 DESC-G1GC-FIPS-COUNTY PIC X(03).                   
              10 DESC-G1GC-COUNTY        PIC X(15).                    
              88 DESCRIPTION-CK-THESE-LA-PARISH VALUE 'CAMERON        '
                 'IBERIA         ' 'JEFFERSON      ' 'LAFOURCHE      ' 
                 'ORLEANS        ' 'PLAQUEMINES    ' 'ST BERNARD     ' 
                 'ST MARY        ' 'TERREBONE      ' 'VERMILLION     '.
              10 DESC-G1GC-LONGITUDE.                                  
                 15  DESC-G1GC-LONGITUDE-N PIC 9(3)V9(04).             
                 15  DESC-G1GC-LONGITUDE-D PIC X(01).                  
              10 DESC-G1GC-LATITUDE.                                   
                 15  DESC-G1GC-LATITUDE-N  PIC 9(3)V9(04).             
                 15  DESC-G1GC-LATITUDE-D  PIC X(01).                  
              10 FILLER                  PIC X(03).                    
           04 DESC-DUP-KEY-SEQ-NUM       PIC X(03).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).