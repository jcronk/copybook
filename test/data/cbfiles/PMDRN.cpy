        01 PMDRN-UNIT-GRP-PRINT-VERB-RECORD.
           03 PMDRN-UNIT-GRP-PRINT-VERB-SEG.                            
             05 PMDRN-SEGMENT-KEY.                                      
              07 PMDRN-SEGMENT-ID                   PIC X(02).          
                 88 PMDRN-SEGMENT-43                 VALUE '43'.        
              07 PMDRN-SEGMENT-STATUS               PIC X(01).          
                 88 PMDRN-ACTIVE-STATUS              VALUE 'A'.         
                 88 PMDRN-DELETED-STATUS             VALUE 'D'.         
                 88 PMDRN-HISTORY-STATUS             VALUE 'H'.         
                 88 PMDRN-NOT-ACTIVE-STATUS          VALUE 'H' 'D'.     
                 88 PMDRN-RENEWAL-FS-STATUS          VALUE 'R' 'F'.     
              07 PMDRN-TRANSACTION-DATE             PIC 9(08).
              07 PMDRN-SEGMENT-ID-KEY.                                  
                 09 PMDRN-SEGMENT-LEVEL-CODE        PIC X(01).          
                    88 PMDRN-SEG-LEVEL-R             VALUE 'R'.         
                 09 PMDRN-SEGMENT-PART-CODE         PIC X(01).          
                    88 PMDRN-SEG-PART-N              VALUE 'N'.         
                 09 PMDRN-SUB-PART-CODE             PIC X(01).          
                 09 PMDRN-INSURANCE-LINE            PIC X(02).          
              07 PMDRN-LEVEL-KEY.                                       
                 09 PMDRN-LOCATION-NUMBER-A.                            
                    11 PMDRN-LOCATION-NUMBER        PIC 9(04).          
                 09 PMDRN-SUB-LOCATION-NUMBER-A.                        
                    11 PMDRN-SUB-LOCATION-NUMBER    PIC 9(03).          
                 09 PMDRN-RISK-UNIT-GROUP-KEY.                          
                    11 PMDRN-RISK-UNIT-GROUP        PIC 9(03).          
                    11 PMDRN-SEQ-RSK-UNT-GRP        PIC 9(03).          
                 09 PMDRN-RISK-TYPE-IND             PIC X(01).          
              07 PMDRN-ITEM-EFFECTIVE-DATE          PIC 9(08).
              07 PMDRN-PROCESS-DATE                 PIC 9(08).
             05 PMDRN-SEGMENT-DATA.                                     
                 09 PMDRN-OVERRIDE-CLASS-DESC       PIC X(60).          
                 09 PMDRN-DEC-CHANGE-FLAG           PIC X(01).          
                    88 PMDRN-DEC-CHANGE-NEEDED      VALUE 'A'.          
                    88 PMDRN-DEC-CHANGE-NOTED       VALUE 'B'.          
                 09 PMDRN-PMS-FUTURE-USE            PIC X(215).         
                 09 PMDRN-CUSTOMER-USE              PIC X(30).          
                 09 PMDRN-YR2000-CUST-USE           PIC X(100).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).