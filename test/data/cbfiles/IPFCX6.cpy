        01 IPFCX6-CLAIM-RECORD.
           05 IPFCX6-CLAIM-SEGMENT.                                    
              07 IPFCX6-SEGMENT-ID-KEY.                                
                 09 IPFCX6-SEGMENT-ID                PIC X(02).        
                 09 IPFCX6-SEGMENT-LEVEL-CODE        PIC X(01).        
                 09 IPFCX6-SEGMENT-PART-CODE         PIC X(01).        
                 09 IPFCX6-SUB-PART-CODE             PIC X(01).        
              07 IPFCX6-X-LEVEL-KEY.                                   
                 09 IPFCX6-INSURANCE-LINE            PIC X(02).        
                    88 IPFCX6-COMMERCIAL-CRIME       VALUE 'CR'.       
                    88 IPFCX6-EMPTY-INS-LINE         VALUE SPACES.     
                    88 IPFCX6-NOT-ON-TM523X          VALUE 'IM'        
                                                 'FF' 'BM' 'CG' 'BT'.  
                 09 IPFCX6-LOCATION-NUMBER-A.                          
                    11 IPFCX6-LOCATION-NUMBER        PIC 9(04).        
                 09 IPFCX6-SUB-LOCATION-NUMBER-A.                      
                    11 IPFCX6-SUB-LOCATION-NUMBER    PIC 9(03).        
                 09 IPFCX6-RISK-UNIT-GROUP-KEY.                        
                    11 IPFCX6-RISK-UNIT-GROUP        PIC X(03).        
                    11 IPFCX6-SEQ-RSK-UNT-GRP.                         
                       13 IPFCX6-CLASS-CODE-GROUP    PIC 9(02).        
                       13 IPFCX6-CLASS-CODE-MEMBER   PIC 9(01).        
                 09 IPFCX6-RISK-UNIT-KEY.                              
                    11 IPFCX6-RISK-UNIT.                               
                       13 IPFCX6-LOSS-UNIT           PIC X(03).        
                       13 FILLER                     PIC X(03).        
                    11 IPFCX6-SEQUENCE-RISK-UNIT.                      
                       13 IPFCX6-RISK-SEQUENCE       PIC 9(01).        
                       13 IPFCX6-RISK-TYPE-IND       PIC X(01).        
                 09 IPFCX6-TYPE-EXPOSURE             PIC X(03).        
                 09 IPFCX6-MAJOR-PERIL               PIC X(03).        
                 09 IPFCX6-SEQUENCE-TYPE-EXPOSURE    PIC X(02).        
              07 IPFCX6-ITEM-EFFECTIVE-DATE          PIC 9(08).                           
              07 IPFCX6-TRANSACTION-DATE             PIC 9(08).
              07 IPFCX6-X6-VARIABLE-KEY.                               
                 09 IPFCX6-LOSS-KEY.                                   
                    11 IPFCX6-LOSS-OCCURENCE.                          
                       13 IPFCX6-LOSS-OCC-FDIGIT  PIC 9(01).           
                       13 IPFCX6-USR-LOSS-OCCURENCE PIC 9(02).         
                    11 IPFCX6-LOSS-CLAIMANT       PIC 9(03).           
                    11 IPFCX6-LOSS-DETAIL-KEY.                         
                       13 IPFCX6-MEMBER  PIC X(02).                    
                       13 IPFCX6-LOSS-DISABILITY PIC X(02).            
                          88 MEDICAL-MD-MH-MO-WM     VALUE             
                                                 'MD' 'MH' 'MO' 'WM'.  
                          88 MEDICAL   VALUE 'MH' 'MC' 'MD' 'MO'       
                                             'AM' 'DM' 'IM' 'SS'.      
                          88 INDEMNITY VALUE 'TE' 'TD' 'PD' 'VI' 'VT'  
                             'VE' 'FT' 'PI' 'PV' 'FA' 'LS' 'WL'        
                             'NS' 'LL'.                                
                          88 CONVERTED-CLAIM        VALUE '  '.        
                          88 INCUR-MEDICAL-AMT      VALUE 'WM'.        
                          88 INCUR-INDEMNITY-AMT    VALUE 'WI'.        
                          88 PAID-MED-HOSP-BENE     VALUE 'MH'.        
                          88 PAID-MED-LEGAL-EXP-CMT VALUE 'MC'.        
                          88 PAID-MED-LEGAL-EXP-DEF VALUE 'MD'.        
                          88 PAID-MED-OTHER-EXP     VALUE 'MO'.        
                          88 PAID-INDEMN-LEGAL-EXP  VALUE 'TE'.        
                          88 INCUR-TEMP-INDEMN      VALUE 'TD'.        
                          88 INCUR-PERM-INDEMN      VALUE 'PD'.        
                          88 INCUR-INDEMN-VOC-REHAB VALUE 'VI'.        
                          88 INCUR-TRNG-VOC-REHAB   VALUE 'VT'.        
                          88 INCUR-EVAL-VOC-REHAB   VALUE 'VE'.        
                          88 INCUR-INDEMN-FATALITY  VALUE 'FT'.        
                          88 PAID-PENS-INDEMN-BENE  VALUE 'PI'.        
                          88 PRESENT-VAL-FUT-INDEMN VALUE 'PV'.        
                          88 INCUR-FUNERAL-ALLOW    VALUE 'FA'.        
                          88 INCUR-LUMP-SUM-REMAR   VALUE 'LS'.        
                          88 INCUR-SS               VALUE 'OS'.        
                          88 INCUR-WAGE-LOSS        VALUE 'WL'.        
                          88 INCUR-NON-SCHED-INDEMN VALUE 'NS'.        
                          88 EMPLOYERS-LIAB-LOSS    VALUE 'LL'.        
                       13 IPFCX6-RESERVE-CATEGORY PIC X(01).           
                 09 IPFCX6-REINS-KEY.                                  
                    11 IPFCX6-LAYER      PIC 9(02).                    
                    11 IPFCX6-REINS-KEY-ID PIC X(01).                  
                    11 IPFCX6-REINS-TREATY.                            
                       13 IPFCX6-REINS-CO-NO PIC X(04).                
                       13 IPFCX6-REINS-BROKER PIC 9(04).               
             07 IPFCX6-PROCESS-DATE                 PIC 9(08).                                   
             07 IPFCX6-CHANGE-ENTRY-GROUP.                             
                09 IPFCX6-CHANGE-ENTRY-DATE         PIC 9(08).
                09 IPFCX6-SEQUENCE-CHANGE-ENTRY     PIC 9(02).         
             07 IPFCX6-SEGMENT-DATA           PIC X(360).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).