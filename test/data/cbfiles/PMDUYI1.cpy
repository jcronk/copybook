        01 PMDUYI1-RATE-OVERRIDE-RECORD.
           03 PMDUYI1-RATE-OVERRIDE-SEG.                                
            05 PMDUYI1-SEGMENT-KEY.                                     
              07 PMDUYI1-SEGMENT-ID                   PIC X(02).        
                 88 PMDUYI1-SEGMENT-43                 VALUE '43'.      
              07 PMDUYI1-SEGMENT-STATUS               PIC X(01).        
                 88 PMDUYI1-ACTIVE-STATUS              VALUE 'A'.       
                 88 PMDUYI1-DELETED-STATUS             VALUE 'D'.       
                 88 PMDUYI1-HISTORY-STATUS             VALUE 'H'.       
                 88 PMDUYI1-NOT-ACTIVE-STATUS          VALUE 'H' 'D'.   
                 88 PMDUYI1-RENEWAL-FS-STATUS          VALUE 'R' 'F'.   
              07 PMDUYI1-TRANSACTION-DATE          PIC 9(08).
              07 PMDUYI1-SEGMENT-ID-KEY.                                
                 09 PMDUYI1-SEGMENT-LEVEL-CODE        PIC X(01).        
                    88 PMDUYI1-SEG-LEVEL-U             VALUE 'U'.       
                 09 PMDUYI1-SEGMENT-PART-CODE         PIC X(01).        
                    88 PMDUYI1-SEG-PART-Y              VALUE 'Y'.       
                 09 PMDUYI1-SUB-PART-CODE             PIC X(01).        
                 09 PMDUYI1-INSURANCE-LINE            PIC X(02).        
                    88 PMDUYI1-INLAND-MARINE           VALUE 'IM'.      
              07 PMDUYI1-LEVEL-KEY.                                     
                 09 PMDUYI1-LOCATION-NUMBER-A.                          
                    11 PMDUYI1-LOCATION-NUMBER        PIC 9(04).        
                       88 PMDUYI1-TERRORISM-COV-LOC   VALUE 0911.       
                 09 PMDUYI1-SUB-LOCATION-NUMBER-A.                      
                    11 PMDUYI1-SUB-LOCATION-NUMBER    PIC 9(03).        
                 09 PMDUYI1-RISK-UNIT-GROUP-KEY.                        
                    11 PMDUYI1-RISK-UNIT-GROUP-A.                       
                       13 PMDUYI1-RISK-UNIT-GROUP        PIC 9(03).     
                          88 PMDUYI1-ACCOUNTS-RECEIVABLE  VALUE 404.    
                          88 PMDUYI1-COMM-ART-CAMERAS     VALUE 460.    
                          88 PMDUYI1-COMM-ART-MUS-INSTR   VALUE 461.    
                          88 PMDUYI1-PHYS-AND-SURGEONS    VALUE 430.    
                          88 PMDUYI1-SIGNS                VALUE 432.    
                          88 PMDUYI1-VALUABLE-PAPERS      VALUE 438.    
                    11 PMDUYI1-RISK-UNIT-GROUP-SEQ-A.                   
                       13 PMDUYI1-RISK-UNIT-GROUP-SEQ    PIC 9(03).     
                 09 PMDUYI1-RISK-UNIT.                                  
                    11 PMDUYI1-ITEM-NUMBER            PIC 9(06).        
                 09 PMDUYI1-SEQUENCE-RISK-UNIT.                         
                    11 PMDUYI1-RISK-SEQUENCE          PIC 9(01).        
                    11 PMDUYI1-RISK-TYPE-IND          PIC X(01).        
              07 PMDUYI1-ITEM-EFFECTIVE-DATE          PIC 9(08).
              07 PMDUYI1-VARIABLE-KEY.                                  
                 09 FILLER                            PIC X(06).        
              07 PMDUYI1-PROCESS-DATE          PIC 9(08).
             05 PMDUYI1-SEGMENT-DATA.                                   
                 09 PMDUYI1-SEGMENT-MG-IND            PIC X(01).        
                 09 PMDUYI1-1ST-BASE-RATE-A.                            
                    11 PMDUYI1-1ST-BASE-RATE          PIC 9(03)V9(03).  
                 09 PMDUYI1-1ST-BASE-RATE-MG-IND      PIC X(01).        
                 09 PMDUYI1-2ND-BASE-RATE-A.                            
                    11 PMDUYI1-2ND-BASE-RATE          PIC 9(03)V9(03).  
                 09 PMDUYI1-2ND-BASE-RATE-MG-IND      PIC X(01).        
                 09 PMDUYI1-3RD-BASE-RATE-A.                            
                    11 PMDUYI1-3RD-BASE-RATE          PIC 9(03)V9(03).  
                 09 PMDUYI1-3RD-BASE-RATE-MG-IND      PIC X(01).        
                 09 PMDUYI1-4TH-BASE-RATE-A.                            
                    11 PMDUYI1-4TH-BASE-RATE          PIC 9(03)V9(03).  
                 09 PMDUYI1-4TH-BASE-RATE-MG-IND      PIC X(01).        
                 09 PMDUYI1-MODIFIED-RATE-A.                            
                    11 PMDUYI1-MODIFIED-RATE          PIC 9(03)V9(03).  
                 09 PMDUYI1-MOD-RAT-MG-IND            PIC X(01).        
                 09 PMDUYI1-PKG-MOD-FACTOR-A.                           
                    11 PMDUYI1-PKG-MOD-FACTOR         PIC 9(01)V9(03).  
                 09 PMDUYI1-PKG-MOD-MG-IND            PIC X(01).        
                 09 PMDUYI1-DEDUCTIBLE-FACTOR-A.                        
                    11 PMDUYI1-DEDUCTIBLE-FACTOR      PIC 9(01)V9(03).  
                 09 PMDUYI1-DED-FCTR-MG-IND           PIC X(01).        
                 09 PMDUYI1-TERM-FACTOR-A.                              
                    11 PMDUYI1-TERM-FACTOR            PIC 9(01)V9(03).  
                 09 PMDUYI1-PREMIUM-TERM-A.                             
                    11 PMDUYI1-PREMIUM-TERM           PIC 9(09).        
                 09 PMDUYI1-PREM-TM-MG-IND            PIC X(01).        
                 09 PMDUYI1-TERR-MULTIPLIER-A.                          
                    11 PMDUYI1-TERR-MULTIPLIER        PIC 9(01)V9(03).  
                 09 PMDUYI1-TERR-MULT-MG-IND          PIC X(01).        
                 09 PMDUYI1-COMPANY-DEVIATION-A.                        
                    11 PMDUYI1-COMPANY-DEVIATION      PIC 9(01)V9(03).  
                 09 PMDUYI1-RDF-A.                                      
                    11 PMDUYI1-RDF                    PIC 9(01)V9(03).  
                 09 PMDUYI1-RDF-MG-IND                PIC X(01).        
                 09 PMDUYI1-RMF-A.                                      
                    11 PMDUYI1-RMF                    PIC 9(01)V9(03).  
                 09 PMDUYI1-PROT-CLASS-MULT-A.                          
                    11 PMDUYI1-PROT-CLASS-MULT        PIC 9(03)V9(03).  
                 09 PMDUYI1-AR-FACTOR                 PIC 9(01)V9(03).  
                 09 PMDUYI1-RECEPTACLE-FACTOR         PIC 9(01)V9(03).  
                 09 PMDUYI1-DUP-RECS-FACTOR           PIC 9(01)V9(03).  
                 09 PMDUYI1-CLASS-OF-RISK-FACTOR-A.                     
                    11 PMDUYI1-CLASS-OF-RISK-FACTOR   PIC 9(01)V9(03).  
                 09 PMDUYI1-AR-MINIMUM-BASE-RATE-A.                     
                    11 PMDUYI1-AR-MINIMUM-BASE-RATE   PIC 9(03)V9(03).  
                 09 PMDUYI1-AR-MIN-BASE-RATE-IND      PIC X(01).        
                 09 PMDUYI1-PREMIUM1-A.                                 
                    11 PMDUYI1-PREMIUM1               PIC 9(09).        
                 09 PMDUYI1-AWAY-PREM-MOD-RATE        PIC 9(03)V9(03).  
                 09 PMDUYI1-PREMIUM2-A.                                 
                    11 PMDUYI1-PREMIUM2               PIC 9(09).        
                 09 PMDUYI1-AR-AVG-RATE-PREM-A.                         
                    11 PMDUYI1-AR-AVG-RATE-PREM       PIC 9(09).        
                 09 PMDUYI1-GRPII-RATE-A.                               
                    11 PMDUYI1-GRPII-RATE             PIC 9(03)V9(03).  
                 09 PMDUYI1-RATE-BOOK-ID-CF           PIC X(01).        
                 09 PMDUYI1-RATE-BOOK-ID-IM           PIC X(01).        
                 09 PMDUYI1-LOAD1-PREM-A.                               
                    11 PMDUYI1-LOAD1-PREM             PIC 9(09).        
                 09 PMDUYI1-LOAD2-PREM-A.                               
                    11 PMDUYI1-LOAD2-PREM             PIC 9(09).        
                 09 PMDUYI1-LOAD3-PREM-A.                               
                    11 PMDUYI1-LOAD3-PREM             PIC 9(09).        
                 09 PMDUYI1-LOAD-TOTAL-A.                               
                    11 PMDUYI1-LOAD-TOTAL             PIC 9(09).        
                 09 PMDUYI1-BASIC-CHG-PREM-A.                           
                    11 PMDUYI1-BASIC-CHG-PREM         PIC 9(09).        
                 09 PMDUYI1-MOD-GRPI-RATE-A.                            
                    11 PMDUYI1-MOD-GRPI-RATE          PIC 9(05)V9(03).  
                 09 PMDUYI1-MOD-GRPII-RATE-A.                           
                    11 PMDUYI1-MOD-GRPII-RATE         PIC 9(05)V9(03).  
                 09 PMDUYI1-DEC-CHANGE-FLAG           PIC X(01).        
                    88 PMDUYI1-DEC-CHANGE-NEEDED      VALUE 'A'.        
                    88 PMDUYI1-DEC-CHANGE-NOTED       VALUE 'B'.        
                 09 PMDUYI1-CONTS-RLAF-MG-IND         PIC X(01).        
                 09 PMDUYI1-CONTS-RLAF-A.                               
                    11 PMDUYI1-CONTS-RLAF             PIC 9(01)V9(03).  
                 09 PMDUYI1-NY-INCIDENTAL-IND         PIC X(01).        
                    88  PMDUYI1-NY-INCIDENTAL-OFFICE  VALUE 'Y'.        
                 09 PMDUYI1-PMS-FUTURE-USE            PIC X(47).        
                 09 PMDUYI1-TERRCOV                   PIC X(01).        
                 09 PMDUYI1-CUSTOMER-USE              PIC X(29).        
                 09 PMDUYI1-YR2000-CUST-USE           PIC X(100).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).