        01 PMDUXA1-RECORD.
           03 PMDUXA1-UNIT-RATING-SEG.
           05 PMDUXA1-SEGMENT-KEY.                                      
             10 PMDUXA1-SEGMENT-ID                      PIC X(02).      
                88 PMDUXA1-SEGMENT-43                   VALUE '43'.     
             10 PMDUXA1-SEGMENT-STATUS                  PIC X(01).      
                88 PMDUXA1-ACTIVE-STATUS                VALUE 'A'.      
                88 PMDUXA1-DELETED-STATUS               VALUE 'D'.      
                88 PMDUXA1-HISTORY-STATUS               VALUE 'H'.      
                88 PMDUXA1-NOT-ACTIVE-STATUS            VALUE 'H' 'D'.  
                88 PMDUXA1-RENEWAL-FS-STATUS            VALUE 'R' 'F'.  
             10 PMDUXA1-TRANSACTION-DATE                PIC 9(08).
             10 PMDUXA1-SEGMENT-ID-KEY.                                 
                15 PMDUXA1-SEGMENT-LEVEL-CODE           PIC X(01).      
                   88 PMDUXA1-SEG-LEVEL-U               VALUE 'U'.      
                15 PMDUXA1-SEGMENT-PART-CODE            PIC X(01).      
                   88 PMDUXA1-SEG-PART-X                VALUE 'X'.      
                15 PMDUXA1-SUB-PART-CODE                PIC X(01).      
                15 PMDUXA1-INSURANCE-LINE               PIC X(02).      
                   88 PMDUXA1-GARAGE                    VALUE 'GA'.     
             10 PMDUXA1-LEVEL-KEY.                                      
                15 PMDUXA1-LOCATION-NUMBER-A.                           
                   20 PMDUXA1-LOCATION-NUMBER           PIC 9(04).      
                15 PMDUXA1-SUB-LOCATION-NUMBER          PIC X(03).      
              13 PMDUXA1-RUG-RU-KEY.                                    
                15 PMDUXA1-RISK-UNIT-GROUP-KEY.                         
                   20 PMDUXA1-RISK-UNIT-GROUP.                          
                      25 PMDUXA1-PMS-RIDER-CODE         PIC X(03).      
                   20 PMDUXA1-SEQ-RISK-UNIT-GROUP.                      
                      25 FILLER                         PIC X(03).      
                15 PMDUXA1-RISK-UNIT.                                   
                   20 PMDUXA1-RU.                                       
                      25 PMDUXA1-RU-VALUE               PIC X(06).      
                         88 PMDUXA1-DEALER              VALUE '100   '. 
                         88 PMDUXA1-NON-DEALER          VALUE '100   '. 
                         88 PMDUXA1-OWNS-PREMISES       VALUE '200   '. 
                         88 PMDUXA1-DEALER-PLATES       VALUE '250   '. 
                         88 PMDUXA1-GKLL                VALUE '300   '. 
                         88 PMDUXA1-DPD                 VALUE '350   '. 
                         88 PMDUXA1-MED-PAY             VALUE '400   '. 
                         88 PMDUXA1-HIRED-CAR           VALUE '450   '. 
                         88 PMDUXA1-PKP-DELIV           VALUE '500   '. 
                         88 PMDUXA1-NAMED-DRIVER        VALUE '525   '. 
                         88 PMDUXA1-DRIVE-AWAY-COLL     VALUE '550   '. 
                         88 PMDUXA1-REGIS-PLATES        VALUE '600   '. 
                         88 PMDUXA1-DRIVE-OTHER-CAR     VALUE '650   '. 
                         88 PMDUXA1-FALSE-PRETENSE      VALUE '700   '. 
                15 PMDUXA1-SEQUENCE-RISK-UNIT.                          
                   20 PMDUXA1-DPD-TYPE-VALUE.                           
                      88 PMDUXA1-DPD-DEC-PRINT          VALUE '0'.      
                      88 PMDUXA1-DPD-BUILDING           VALUE '1'.      
                      88 PMDUXA1-DPD-STD-OPN-LOT        VALUE '2'.      
                      88 PMDUXA1-DPD-NON-STD-OPN-LOT    VALUE '3'.      
                      88 PMDUXA1-DPD-BLDG-MISC-TYPE     VALUE '4'.      
                      88 PMDUXA1-DPD-OPEN-LOT-MISC      VALUE '5'.      
                      88 PMDUXA1-DPD-BLANKET-COLL       VALUE '6'.      
                      88 PMDUXA1-DPD-OTHER              VALUE '6'.      
                         25 PMDUXA1-DPD-TYPE-NUMB       PIC 9(01).      
                   20 PMDUXA1-SEQUENCE                  PIC X(01).      
             10 PMDUXA1-ITEM-EFFECTIVE-DATE             PIC 9(08).
             10 PMDUXA1-VARIABLE-KEY.                                   
                15 PMDUXA1-ADD-INTEREST-CODE.                           
                   20 PMDUXA1-ADD-INTEREST-TYPE         PIC X(01).      
                   20 PMDUXA1-ADD-INTEREST-SEQ-A.                       
                      25 PMDUXA1-ADD-INTEREST-SEQ       PIC 9(02).      
                15 FILLER                               PIC X(03).      
             10 PMDUXA1-PROCESS-DATE                    PIC 9(08).
           05 PMDUXA1-SEGMENT-DATA.                                     
              10 PMDUXA1-EXPIRATION                     PIC 9(08).
              10 PMDUXA1-VARYING-DATA                   PIC X(150).     
           10 PMDUXA1-PMSC-FUTURE-USE                   PIC X(99).      
           10 PMDUXA1-CUST-FUTURE-USE                   PIC X(36).      
           10 PMDUXA1-YR2000-CUST-USE                   PIC X(100).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).