        01 VEHICLE-COVERAGE-RECORD.
           03 VEHICLE-COVERAGE-SEG.                                     
           05 VCR-ID                     PIC X(02).                     
              88 VEHICLE-COVERAGE        VALUE '35'.                    
           05 VCR-KEY.                                                  
              10 VCR-UNIT-NUMBER         PIC X(03).                     
              10 VCR-SEGMENT-NUMBER      PIC X(01).                     
                 88 USVD-COVERAGE-SEGMENT VALUE '1'.                    
                 88 CVC-SEGMENT          VALUE '3'.                     
                 88 SIII-RT-PREMIUM-SEGMENT VALUE '9'.                  
              10 VCR-AMEND-NUMBER        PIC X(01).                     
           05 VCR-CHANGE-EFFECT-DATE     PIC X(08).                                   
           05 SII-VEHICLE-DATA.                                         
           07 VEHICLE-COVERAGE-CONTROL.                                 
              10 VEHICLE-REASON-AMENDED  PIC X(03).                     
              10 VEHICLE-TOTAL-PREMIUM   PIC S9(06)V99.                 
              10 VEHICLE-OLD-PREMIUM     PIC S9(06)V99.                 
           07 VCR-TABLE-OF-COVERAGES.                                   
              10 VCR-COVERAGE-DATA       OCCURS 14 TIMES INDEXED BY     
                    COVERAGE VCR-COVERAGE-I.                            
                 15 VCR-COVERAGE-CODE    PIC X(02).                     
                    88 VCR-NO-MORE-COVERAGES VALUE ' '.                 
                    88 VCR-SLIMIT-BI-PD  VALUE '01' '02' '05'.          
                    88 VCR-COMP-COLL-THEFT VALUE '46' '47' '44'.        
                    88 VCR-SL-BI-PD-CL-THFT VALUE '01' '02' '05' '47'   
                          '44'.                                         
                    88 VCR-CLASS-2-LIAB-COV VALUE '01' THRU '28'.       
                    88 VCR-CLASS-2-COLL-COV VALUE '47' '48' '49'.       
                    88 VCR-CLASS-2-PHY-DAM-COV VALUE '29' THRU '32'     
                                                     '36' THRU '50'     
                                                     '60'.              
                    88 VCR-PROP-DAMAGE   VALUE '05'.                    
                    88 VCR-COLLISION     VALUE '47'.                    
                    88 VCR-PIP           VALUE '14'.                    
                    88 VCR-PENN-PIP                                     
                           VALUE '9H' '9I' '9J' '9K' '9L' '9M'.         
                    88 VCR-OPT-ECON-LOSS VALUE '97'.                    
                    88 VCR-BI            VALUE '02'.                    
                    88 VCR-SINGLE-LIMITS VALUE '01'.                    
                    88 VCR-COMPREHENSIVE VALUE '46' '50'.               
                    88 VCR-AUTO-LOAN VALUE '93'.                        
                    88 VCR-BROADENED-COLL VALUE '48'.                   
                    88 VCR-LIMITED-COLL VALUE '49'.                     
                    88 VCR-FIRE-THEFT    VALUE '37'.                    
                    88 VCR-MASS-MR-COLL-SURCHARGE VALUE '54'.           
                    88 VCR-MASS-MR-PD-SURCHARGE VALUE '56'.             
                    88 VCR-MASS-MR-PD-CREDIT VALUE '57'.                
                    88 VCR-MASS-MR-COLL-OVERDUE VALUE '58'.             
                    88 VCR-MASS-MR-PD-OVERDUE VALUE '59'.               
                    88 VCR-PDB           VALUE '69'.                    
                    88 VCR-CELLULAR-TELEPHONE  VALUE '9Z'.              
                    88 VCR-NO-COVERAGE-LEFT VALUE SPACES.               
                    88 CVC-DC            VALUE '00'.                    
                    88 CVC-TPL           VALUE '01'.                    
                    88 CVC-CMP           VALUE '08'.                    
                    88 CVC-COL           VALUE '09'.                    
                    88 CVC-SP            VALUE '10'.                    
                    88 CVC-AP            VALUE '11'.                    
                    88 CVC-SEF16         VALUE '65'.                    
                    88 CVC-SEF17         VALUE '66'.                    
                    88 CVC-CFC           VALUE '99'.                    
                    88 CVC-AJA-AJ6       VALUE '65' '66' '80' THRU      
                          '83' '85' THRU '88'.                          
                    88 CVC-SEF43         VALUE '35' '36' '37'.          
                    88 VCR-SPECIFIED-PERILS VALUE '60'.                 
                 15 VCR-COVERAGE-LIMIT   PIC X(07).                     
                    88 VCR-COVERAGE-LIMIT-X VALUE 'XXXXXXX'.            
                 15 VCR-COVERAGE-PREMIUM PIC S9(05)V99.                 
                 15 VCR-COVERAGE-RATE-BOOK-ID PIC X(01).                
           07 VCR-HAP-MOD-INDICATOR       PIC X(01).                    
              88 HAP-MOD-APPLIES                    VALUE 'Y'.          
           07 VCR-FACTOR-HOLD-AREA.                    
              10 COMM-HIRED-PRIM-FACTOR   PIC S9(04)V99.                
              10 COMM-HIRED-PD-RATE       PIC S9(04)V99.                
              10 FILLER                   PIC 9(06).                    
           07 VCR-UMS-UMP-SW              PIC X(01).                    
              88  VCR-SEPARATE-UNS-UMP    VALUE 'Y'.                    
           05 VCR-PMS-FUTURE-USE          PIC X(51).                    
           05 VCR-CUST-FUTURE-USE         PIC X(03).                    
           05 VCR-LA-IMFR-SW              PIC X(01).                    
           05 VCR-TX-IMFR-SW              PIC X(01).                    
           05 VCR-OKLA-IMFR-SW            PIC X(01).                    
           05 VCR-YR2000-CUST-USE         PIC X(100).                   
           05 BLANK-OUT         PIC X(03).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).