        01 IPFCGQ-CLAIMANT-RECORD.
           05 IPFCGQ-CLAIMANT-SEG.                                     
              07 IPFCGQ-SEGMENT-ID-KEY.                                
                 09 IPFCGQ-SEGMENT-ID           PIC X(02).             
                 09 IPFCGQ-SEGMENT-LEVEL-CODE   PIC X(01).             
                 09 IPFCGQ-SEGMENT-PART-CODE    PIC X(01).             
                 09 IPFCGQ-SUB-PART-CODE        PIC X(01).             
              07 IPFCGQ-ITEM-EFFECTIVE-DATE     PIC 9(08).                           
              07 IPFCGQ-GQ-VARIABLE-KEY.                               
                 09 IPFCGQ-LOSS-OCCURENCE PIC 9(03).                   
                 09 IPFCGQ-LOSS-CLAIMANT PIC 9(03).                    
                 09 IPFCGQ-CLAIMANT-USE-CODE PIC X(03).                
                    88 IPFCGQ-AUTOMOBILE-LOSSES VALUE 'AUT'.           
                    88 IPFCGQ-ARBITRATION-DATA  VALUE 'ARB'.           
                    88 IPFCGQ-BOND-LOSSES VALUE 'BND'.                 
                    88 IPFCGQ-CLAIMANT-INFO VALUE 'CMT'.               
                    88 IPFCGQ-FORMS-SENT-DATA VALUE 'FMS'.             
                    88 IPFCGQ-LITIGATION VALUE 'LIT'.                  
                    88 IPFCGQ-MISCELLANEOUS-1 VALUE 'MS1'.             
                    88 IPFCGQ-MISCELLANEOUS-2 VALUE 'MS2'.             
                    88 IPFCGQ-MISCELLANEOUS-3 VALUE 'MS3'.             
                    88 IPFCGQ-PROPERTY-MARINE VALUE 'PTY'.             
                    88 IPFCGQ-REHABILITATION VALUE 'RHB'.              
                    88 IPFCGQ-SALVAGE    VALUE 'SAL'.                  
                    88 IPFCGQ-SUBROGATION VALUE 'SUB'.                 
                    88 IPFCGQ-VOC-REHAB  VALUE 'VRE'.                  
                    88 IPFCGQ-WORKERS-COMP-CLAIM-1 VALUE 'WC1'.        
                    88 IPFCGQ-WORKERS-COMP-CLAIM-2 VALUE 'WC2'.        
                    88 IPFCGQ-WORKERS-COMP-CLAIM-3 VALUE 'WC3'.        
                 09 IPFCGQ-CLAIMANT-USE-SEQ PIC 9(03).                 
              07 IPFCGQ-PROCESS-DATE        PIC 9(08).                                  
              07 IPFCGQ-CHANGE-ENTRY-GROUP.                            
                 09 IPFCGQ-CHANGE-ENTRY-DATE         PIC 9(08).
                 09 IPFCGQ-SEQUENCE-CHANGE-ENTRY     PIC 9(02).        
              07 IPFCGQ-GQ-SEGMENT-DATA.                               
                 09 IPFCGQ-SEGMENT-STATUS PIC X(01).                   
                    88 IPFCGQ-ACTIVE-STATUS VALUE 'A'.                 
                    88 IPFCGQ-CREATED-DUE-TO-AUDIT VALUE 'I' 'J' 'K'   
                          'L' 'M' 'N' 'O' 'P' 'Q' 'S' 'T' 'U' 'W'.     
                    88 IPFCGQ-DELETED-STATUS VALUE 'D'.                
                    88 IPFCGQ-HISTORY-STATUS VALUE 'H'.                
                    88 IPFCGQ-NOT-ACTIVE-STATUS VALUE 'H' 'D' 'R'.     
                    88 IPFCGQ-RENEWAL-CHANGE VALUE 'R'.                
                 09 IPFCGQ-ENTRY-OPERATOR PIC X(03).                   
                 09 IPFCGQ-VARIABLE-AREA-GQ PIC X(232).                
                 09 IPFCGQ-NUMBER-OF-PART78  PIC S9(03) COMP-3.        
                 09 IPFCGQ-OFFSET-ONSET-IND  PIC X(01).                
                    88 IPFCGQ-OFFSET-RECORD  VALUE 'O'.                
                 09 IPFCGQ-PMS-FUTURE-USE-GQ PIC X(63).                
                 09 IPFCGQ-CWS-ORIGIN-IND  PIC X(01).                  
                 09 IPFCGQ-CUST-SPL-USE-GQ PIC X(06).                  
                 09 IPFCGQ-YR2000-CUST-USE   PIC X(100).               
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).
