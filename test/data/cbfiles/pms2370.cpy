       01  PERSONAL-LINES-UMBRELLA-RATING.                                    
           05  PERSONAL-LINES-UMBRELLA-RATE.                                  
               10  PLUMBRR-KEY.                                               
                   15  PLUMBRR-ID                  PIC X(02).                 
                       88 PLUMBRR-REC                       VALUE '44'.       
                   15  PLUMBRR-LEVEL               PIC X(01).                 
                       88 PLUMBRR-UMBRELLA         VALUE 'U'.                 
                   15  PLUMBRR-PART                PIC X(01).                 
                       88 PLUMBRR-RATING           VALUE 'R'.                 
                       88 PLUMBRR-UNDERLYING       VALUE 'U'.                 
                   15  PLUMBRR-TYPE                PIC X(09).                 
                       88 PLUMBRR-UMBRELLA-RATING  VALUE 'U-RATE 01'.         
                   15  PLUMBRR-ISSUE-DATE          PIC X(08).                 
                   15  PLUMBRR-EFFECTIVE-DATE      PIC X(08).                 
                   15  PLUMBRR-STATUS              PIC X(01).                 
                       88  PLUMBRR-STATUS-ACTIVE            VALUE 'A'.        
                       88  PLUMBRR-STATUS-INACTIVE          VALUE 'I'.        
                   15  PLUMBRR-KEY-FILLER          PIC X(11).                 
               10  PLUMBRR-DATA.                                       
                   15  PLUMBRR-RATEBOOK            PIC X(01).           
      * LIMIT OF LIABILITY (E.G. 1,000,000 OR 2,000,000)                
                   15  PLUMBRR-LIMITN              PIC S9(9) COMP-3.    
      * DEDUCTIBLE / SELF-INSURED RETENTION (E.G. $1,000 PER PERSON)    
                   15  PLUMBRR-DEDUCTN             PIC S9(7) COMP-3.    
      * ADDITIONAL 1UTOMOBILES                                          
                   15  PLUMBRR-AUTOSN              PIC S9(3) COMP-3.    
      * YOUNG DRIVER SURCHARGE                                          
                   15  PLUMBRR-YOUNGN              PIC S9(3) COMP-3.    
      * RECREATIONAL VEHICLES                                           
                   15  PLUMBRR-RECVHN              PIC S9(3) COMP-3.    
      * WATERCRAFT                                                      
                   15  PLUMBRR-WATERN              PIC S9(3) COMP-3.    
      * ADDITIONAL RESIDENCE(S)                                         
                   15  PLUMBRR-ADDRSN              PIC S9(3) COMP-3.    
      * BUSINESS PROPERTIES                                             
                   15  PLUMBRR-BUSPPN              PIC S9(3) COMP-3.    
      * BUSINESS PURSUIT(S)                                             
                   15  PLUMBRR-BUSPUN              PIC S9(3) COMP-3.    
      * PREMIUM SECTION                                                 
                   15  PLUMBRR-BASIC-WP            PIC S9(5) COMP-3.    
                   15  PLUMBRR-BASIC-AP            PIC S9(5) COMP-3.    
                   15  PLUMBRR-AUTOS-WP            PIC S9(5) COMP-3.    
                   15  PLUMBRR-AUTOS-AP            PIC S9(5) COMP-3.    
                   15  PLUMBRR-YOUNG-WP            PIC S9(5) COMP-3.    
                   15  PLUMBRR-YOUNG-AP            PIC S9(5) COMP-3.    
                   15  PLUMBRR-RECVH-WP            PIC S9(5) COMP-3.    
                   15  PLUMBRR-RECVH-AP            PIC S9(5) COMP-3.    
                   15  PLUMBRR-WATER-WP            PIC S9(5) COMP-3.    
                   15  PLUMBRR-WATER-AP            PIC S9(5) COMP-3.    
                   15  PLUMBRR-ADDRS-WP            PIC S9(5) COMP-3.    
                   15  PLUMBRR-ADDRS-AP            PIC S9(5) COMP-3.    
                   15  PLUMBRR-BUSPP-WP            PIC S9(5) COMP-3.    
                   15  PLUMBRR-BUSPP-AP            PIC S9(5) COMP-3.    
                   15  PLUMBRR-BUSPU-WP            PIC S9(5) COMP-3.    
                   15  PLUMBRR-BUSPU-AP            PIC S9(5) COMP-3.    
                   15  PLUMBRR-TOTAL-WP            PIC S9(5) COMP-3.    
                   15  PLUMBRR-TOTAL-AP            PIC S9(5) COMP-3.    
                   15  PLUMBRR-ZIP                 PIC X(05).           
                   15  PLUMBRR-COUNTY              PIC X(25).           
                   15  PLUMBRR-TERRITORY           PIC 9(03).           
                   15  PLUMBRR-LAST-CHANGE-WP      PIC S9(5) COMP-3.    
                   15  PLUMBRR-LAST-CHANGE-AP      PIC S9(5) COMP-3.    
      * UNINSURED                                                       
                   15  PLUMBRR-UMN                 PIC S9(3) COMP-3.    
                   15  PLUMBRR-UM-WP               PIC S9(5) COMP-3.    
                   15  PLUMBRR-UM-AP               PIC S9(5) COMP-3.    
                   15  PLUMBRR-UM-ECONOMIC         PIC X.               
                   15  PLUMBRR-TOTAL-AP-OLD        PIC S9(5) COMP-3.    
                   15  PLUMBRR-OUT-OF-SEQUENCE     PIC X.               
                   15  PLUMBRR-GEN-344             PIC X(01).           
                   15  PLUMBRR-MHOMEN              PIC S9(3) COMP-3.    
                   15  PLUMBRR-MHOME-WP            PIC S9(5) COMP-3.    
                   15  PLUMBRR-MHOME-AP            PIC S9(5) COMP-3.    
                   15  PLUMBRR-CYCLEN              PIC S9(3) COMP-3.    
                   15  PLUMBRR-CYCLE-WP            PIC S9(5) COMP-3.    
                   15  PLUMBRR-CYCLE-AP            PIC S9(5) COMP-3.    
      * YOUNG DRIVER SURCHARGE                                          
                   15  PLUMBRR-YOUNG-16-20         PIC X(02).           
                   15  PLUMBRR-YOUNG-21-25         PIC X(02).           
                   15  PLUMBRR-WATER-INCLUDED      PIC X(02).           
                   15  PLUMBRR-WT-25-100-HP-LT-26         PIC X(02).    
                   15  PLUMBRR-WT-101-250-HP-LT-26         PIC X(02).   
                   15  PLUMBRR-WT-25-100-HP-26-32         PIC X(02).    
                   15  PLUMBRR-WT-101-250-HP-26-32         PIC X(02).   
                   15  PLUMBRR-WT-GT-250-HP-GT-32         PIC X(02).    
                   15  PLUMBRR-WT-WC1-HP           PIC X(03).           
                   15  PLUMBRR-WT-WC1-LN           PIC X(02).           
                   15  PLUMBRR-WT-WC2-HP           PIC X(03).           
                   15  PLUMBRR-WT-WC2-LN           PIC X(02).           
                   15  PLUMBRR-WT-WC3-HP           PIC X(03).           
                   15  PLUMBRR-WT-WC3-LN           PIC X(02).           
                   15  PLUMBRR-STATE               PIC X(2).            
                   15  PLUMBRR-METRO               PIC X(001).          
                   15  PLUMBRR-WT-WC4-HP           PIC X(03).           
                   15  PLUMBRR-WT-WC4-LN           PIC X(02).           
                   15  PLUMBRR-UND-LMT-CAR-CODE    PIC X(1).            
                   15  PLUMBRR-UND-LMT-CYC-CODE    PIC X(1).            
                   15  PLUMBRR-UND-LMT-MH-CODE     PIC X(1).            
                   15  PLUMBRR-UND-LMT-WC1-CODE    PIC X(1).            
                   15  PLUMBRR-UND-LMT-WC2-CODE    PIC X(1).            
                   15  PLUMBRR-UND-LMT-WC3-CODE    PIC X(1).            
                   15  PLUMBRR-UND-LMT-WC4-CODE    PIC X(1).            
      *POSSIBLE VALUES FOR LENGTH ARE 1 OR 2                            
                   15  PLUMBRR-LENGTH-WC1-CODE     PIC X(1).            
                   15  PLUMBRR-LENGTH-WC2-CODE     PIC X(1).            
                   15  PLUMBRR-LENGTH-WC3-CODE     PIC X(1).            
                   15  PLUMBRR-LENGTH-WC4-CODE     PIC X(1).            
      *POSSIBLE VALUES FOR LENGTH ARE 1,2, 3, 4, 5, 6, AND 7            
                   15  PLUMBRR-HP-WC1-CODE         PIC X(1).            
                   15  PLUMBRR-HP-WC2-CODE         PIC X(1).            
                   15  PLUMBRR-HP-WC3-CODE         PIC X(1).            
                   15  PLUMBRR-HP-WC4-CODE         PIC X(1).            
      *INITIALLY WILL HOLD AGES 16-20                                   
                   15  PLUMBRR-YD-SLOT1-WP         PIC S9(5) COMP-3.    
                   15  PLUMBRR-YD-SLOT1-AP         PIC S9(5) COMP-3.    
      *INITIALLY WILL HOLD AGES 21-24                                   
                   15  PLUMBRR-YD-SLOT2-WP         PIC S9(5) COMP-3.    
                   15  PLUMBRR-YD-SLOT2-AP         PIC S9(5) COMP-3.    
                   15  PLUMBRR-UNREGISTERED-VEH    PIC X(02).           
                   15  PLUMBRR-INCIDENTAL-OCCUP    PIC X(01).           
                   15  PLUMBRR-INCIDENTAL-FARMING  PIC X(01).           
                   15  PLUMBRR-UM-HAVE             PIC X(01).           
                   15  FILLER                      PIC X(190).          
                   15  PLUMBRR-SEQ                 PIC X(03).           
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).