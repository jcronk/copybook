	       01 POLICY-INFORMATION-REC.
           03 POLICY-INFORMATION-SEG.                                  
           05 PIF-ID                     PIC X(02).                    
              88 POLICY-INFO-REC         VALUE '02'.                   
              88 ID-GREATER-THAN-FORMS   VALUE '00' '38' THRU '99'.    
              88 RECIEPT-RECORDS         VALUE '95' THRU '99'.         
              88 PIF-PROPERTY-LINE-RATING-REC VALUE '14' '17'.         
              88 PIF-GREATER-THAN-PROP-RATE-REC VALUE '00' '21' '23'   
                    THRU '99'.                                         
              88 PIF-GREATER-THAN-EXT-REC VALUE '31' THRU '99' '00'.   
              88 PIF-GREATER-THAN-UK-RATE-REC VALUE '00'               
                                                    '24' THRU '99'.    
              88 PIF-END-OF-FILE         VALUE HIGH-VALUE.             
           05 PIF-KEY.                                                 
              10 PIF-SYMBOL              PIC X(03).                                           
                 88 PIF-RE-BOP           VALUE 'BP1'.                  
                 88 TEXAS-LV-HOMEOWNERS  VALUE 'HP'.                   
                 88 TEXAS-RHP-HOMEOWNERS VALUE 'HP'.                   
                 88 TEXAS-LV-DWELLING    VALUE 'FP'.                   
                 88 TEXAS-RHP-DWELLING   VALUE 'FP'.                   
              10 PIF-POLICY-NUMBER       PIC X(07).                    
              10 PIF-MODULE              PIC X(02).                    
           05 PIF-MASTER-CO-NUMBER       PIC 9(02).                    
           88 PIF-REPUBLIC               VALUE   1.                    
           88 PIF-VANGUARD               VALUE   2.                    
           88 PIF-BLUE-RIDGE             VALUE   3.                    
           88 PIF-SOUTHERN               VALUE   4.                    
           88 PIF-REPUBLIC-VANGUARD      VALUE   5.                    
           88 PIF-REPUBLIC-UNDERWRITERS  VALUE   6.                    
           88 PIF-VANGUARD-UNDERWRITERS  VALUE   7.                    
           88 PIF-REPUBLIC-LLOYDS        VALUE   8.                    
           88 PIF-SOUTHERN-UNDERWRITERS  VALUE  11.                    
           88 PIF-REP-FIRE-AND-CASUALTY  VALUE  12.                    
           88 PIF-SOUTHERN-COUNTY-MUTUAL VALUE  13.                    
           88 PIF-SOUTHERN-VANGUARD      VALUE  14.                    
           88 PIF-NEW-MEXICO-FAIR-PLAN   VALUE  44.                    
           88 PIF-TEJAS-FAIR-PLAN        VALUE  58.                    
           88 SOUTHERN-VANGUARD-LVD      VALUE  58.                    
           05 PIF-LOCATION               PIC 9(02).                    
           05 PIF-AMEND-NUMBER           PIC X(02).                    
           05 PIF-EFFECTIVE-DATE         PIC 9(08).                                      
              88 PIF-NO-EFFECTIVE-DATE   VALUE SPACES.                 
              88 PIF-PURGED-EFF-DATE     VALUE '99999999'.             
           05 PIF-EXPIRATION-DATE        PIC 9(08).                                     
              88 PIF-PURGED-EXP-DATE     VALUE '99999999'.             
           05 PIF-INSTALLMENT-TERM       PIC 9(03).                    
              88 ONE-MONTH-POLICY        VALUE 001.                    
              88 SIX-MTS-POLICY          VALUE 006.                    
              88 THREE-MTS-POLICY        VALUE 003.                    
              88 TWELVE-MTS-POLICY       VALUE 012.                    
              88 TWENTY-FOUR-MTS-POLICY  VALUE 024.                    
              88 THIRTY-SIX-MTS-POLICY   VALUE 036.                    
              88 FORTY-EIGHT-MONTHS-POLICY VALUE 048.                  
              88 SIXTY-MONTHS-POLICY     VALUE 060.                    
              88 SEVENTY-TWO-MONTHS-POLICY VALUE 072.                  
              88 EIGHTY-FOUR-MONTHS-POLICY VALUE 084.                  
              88 ODD-TERM-POLICY         VALUE 999.                    
           05 PIF-NUMBER-INSTALLMENTS    PIC 9(02).                    
              88 PIF-CONTINUOUS-RENEWAL-POLICY VALUE ZERO.             
              88 PIF-DEFERRED-PAY-PLAN-INSTL VALUE 2 THRU 99.          
              88 PIF-ACCOUNT-CURRENT-INSTL VALUE 1.                    
           05 PIF-RISK-STATE-PROV        PIC X(02).                    
              88 PIF-SW-STATES           VALUE '42' '03' '35' '17' '09'
              88 PIF-SW-ISO-STATES       VALUE      '03' '35' '17' '09'
              88 PIF-RM-STATES           VALUE '43' '27' '02' '30' '05'
              88 PIF-COLORADO            VALUE '05'.                   
              88 PIF-NEVADA              VALUE '27'.                   
              88 PIF-UTAH                VALUE '43'.                   
              88 PIF-NEW-MEXICO          VALUE '30'.                   
              88 PIF-OKLAHOMA            VALUE '35'.                   
              88 PIF-ARKANSAS            VALUE '03'.                   
              88 PIF-LOUISIANA           VALUE '17'.                   
              88 PIF-MISSISSIPPI         VAL
              88 PIF-SOUTH-DAKOTA        VALUE '40'.                   
              88 PIF-MISSOURI            VALUE '24'.                   
              88 PIF-CALIFORNIA          VALUE '04'.                   
              88 PIF-NORTH-CAROLINA      VALUE '32'.                   
              88 PIF-ARIZONA             VALUE '02'.                   
              88 PIF-MASSACHUSETTS-POLICY VALUE '20'.                  
              88 PIF-NON-H076-STATE      VALUE '28'.                   
              88 PIF-CANADIAN-POLICY     VALUE '60' THRU '80'.         
              88 PIF-PEI                 VALUE '66'.                   
              88 PIF-ONTARIO             VALUE '67'.                   
              88 PIF-AUSTRALIAN-POLICY   VALUE '71' THRU '79' '81'     
                    THRU '89'.                                         
              88 PIF-GEORGIA             VALUE '10'.                   
              88 PIF-SOUTH-CAROLINA      VALUE '39'.                   
              88 PIF-FLORIDA             VALUE '09'.                   
              88 PIF-NEBRASKA            VALUE '26'.                   
              88 PIF-VIRGINIA            VALUE '45'.                   
              88 PIF-MARYLAND            VALUE '19'.                   
              88 PIF-NEW-JERSEY          VALUE '29'.                   
              88 PIF-PENNSYLVANIA        VALUE '37'.                   
              88 PIF-WASHINGTON          VALUE '46'.                   
              88 PIF-NEW-YORK            VALUE '31'.                   
              88 PIF-KENTUCKY            VALUE '16'.                   
              88 PIF-AUTO-PIP-STATES     VALUE '06' '21' '37'.         
              88 PIF-WFS-AUTO-STATES-1   VALUE '02' '03' '04' '08'     
                    '09' THRU '16' '18' '21' '22' '24' '25' '26' '30'  
                    '31' '33' '34' '36' '38' '40' '43' '46' '48' '52'  
                    '54'.                                              
              88 PIF-WFS-AUTO-STATES-2   VALUE '17' '19' '29' '37'     
                    '45'.                                              
              88 PIF-WFS-AUTO-STATES-3   VALUE '32' '39' '47'.         
              88 PIF-AUTO-HOLD-CANCEL    VALUE '10'.                   
              88 PIF-SWITCH-HO1-STATE    VALUE '10' '20' '36' '48'.    
              88 PIF-UK-POLICY           VALUE '99'.                   
              88 PIF-TEXAS               VALUE '42'.                   
              88 PIF-SDIP-STATE          VALUE '05' '17' '42'.         
           05 PIF-COMPANY-NUMBER         PIC X(02).                    
              88 JUA-COMPANY             VALUE '18'.                   
              88 ASSIGN-RISK-COMPANY     VALUE '35'.                   
              88 PIF-TEXAS-JUA-COMPANY   VALUE '46'.                   
              88 PIF-TEJAS-FAIR-PLAN-STD VALUE '51'.                   
              88 PIF-TEJAS-LVD-STD       VALUE '51'.                   
              88 PIF-TEJAS-FAIR-PLAN-PRE VALUE '52'.                   
              88 PIF-TEJAS-LVD-PREFER    VALUE '52'.                   
              88 PIF-TEJAS-FAIR-PLAN-SELECT VALUE '53'.                
              88 PIF-TEJAS-FAIR-PLAN-UPRATED VALUE '54'.               
           05 PIF-BRANCH                 PIC X(02).                    
           05 PIF-PROFIT-CENTER          PIC X(03).                    
           05 PIF-AGENCY-NUMBER          PIC X(07).                                       
           05 PIF-ENTERED-DATE           PIC 9(08).                    
           05 PIF-TOTAL-AGEN-PREM        PIC S9(10)V9(02).             
           05 PIF-LINE-BUSINESS          PIC X(03).                    
              88 REPUBLIC-BUSINESSOWNERS VALUE 'BP1'.                  
              88 REPUBLIC-FARM-AND-RANCH VALUE 'FR1'.                  
              88 REPUBLIC-UMBRELLA       VALUE 'UMB' 'UP '.            
              88 REPUBLIC-CL-UMBRELLA    VALUE 'UMB'.                  
              88 REPUBLIC-PL-UMBRELLA    VALUE 'UP '.                  
              88 REPUBLIC-COMMERCIAL     VALUE 'ACV' 'AFV' 'CPP' 'WCP' 
                                         'BP1' 'FR1' 'CL1' 'CL2' 'CL3' 
                                             'UMB'.                    
              88 PIF-ASSIGNED-RISK-BILLING VALUE 'ACA' 'AFA' 'APA'.    
              88 PIF-ASSIGNED-RISK       VALUE 'ACA' 'AFA' 'APA' 'ATA'.
              88 PIF-DEDUCTIBLE-AMT-APPLIES VALUE 'APA' 'APV' 'APC'.   
              88 PIF-JUA                 VALUE 'ACJ' 'AFJ' 'APJ'.      
              88 MASSACHUSETTS-AUTO      VALUE 'AMC' 'AMV'.            
              88 COMMERCIAL-NON-FLEET    VALUE 'ACC' 'ACV'.            
              88 COMMERCIAL-FLEET        VALUE 'AFC' 'AFV'.            
              88 COMMERCIAL-AUTO         VALUE 'ACA' 'ACC' 'ACV' 'ACJ' 
                    'AFA' 'AFC' 'AFJ' 'AFV'.                           
              88 PRIVATE-PASS-AUTO       VALUE 'AMC' 'AMV' 'APC' 'APV'.
              88 ASSIGNED-RISK-AUTO      VALUE 'APA' 'ATA' 'APJ'.      
              88 TEXAS-AUTO              VALUE 'ATA' 'ATV'.            
              88 AUTO-POLICY             VALUE 'APV'.                  
              88 PERS-AUTO-POLICY        VALUE 'APV' 'AMC' 'AMV' 'APC' 
                    'APA' 'ATA' 'ATV'.                                 
              88 SPECIAL-AUTO-POLICY     VALUE 'ASV'.                  
              88 INLAND-MARINE-POLICY    VALUE 'IMP'.                  
              88 CANADIAN-AUTO           VALUE 'ANV' 'ANC'.            
              88 FARMOWNERS-RANCHOWNERS  VALUE 'FR '.                  
              88 HOMEOWNERS              VALUE 'HP '.                  
              88 BUSINESSOWNERS          VALUE 'BO ' 'BOP'.            
              88 FIRE                    VALUE 'FP '.                  
              88 COMMERCIAL-FIRE         VALUE 'CF '.                  
              88 WORKERS-COMP            VALUE 'WC '.                  
              88 WORKERS-COMP-PLUS       VALUE 'WCP'.                  
              88 HOMEOWNER-AUTO-POLICY   VALUE 'HAP'.                  
              88 CANADIAN-HOMEOWNERS     VALUE 'HPC'.                  
              88 TEXAS-HOMEOWNERS        VALUE 'HPT'.                  
              88 TEXAS-FIRE              VALUE 'FPT'.                  
              88 COMMERCIAL-PKG-POL      VALUE 'CPP'.                  
              88 COMMERCIAL-MANUAL-LINES VALUE 'SMP' 'GL ' 'CA ' 'CF ' 
                                         'CG ' 'CR ' 'IM ' 'BM ' 'BO'. 
              88 UK-HOUSEHOLDERS         VALUE 'HHL'.                  
              88 UK-MOTOR-CAR            VALUE 'MPC' 'MGD'.            
              88 UK-LINE-OF-BUSINESS     VALUE 'HHL' 'MPC' 'MGD'.      
              88 REPUBLIC-PERSONAL-LINES VALUE 'APV' 'HPT' 'HP ' 'FP ' 
                               'PL1' 'PL2' 'PL3' 'PL4' 'PL5'           
                                               'FPT' 'UP '.            
              88 COMPUTER-ISSUED-LOB     VALUE 'ACC' 'ACV' 'AFC' 'AFV' 
                                               'BOP' 'CCL' 'CPP' 'WC ' 
                                               'WCP'.                  
              88 BONDS                   VALUE 'BND'.                  
              88 UMBRELLA                VALUE 'UP '.                  
              88 COMM-UMBRELLA           VALUE 'UMB'.                  
           05 PIF-ISSUE-CODE             PIC X(01).                    
              88 PIF-AMENDED-DECLARATION VALUE 'A'.                    
              88 PIF-MANUAL-ISSUE        VALUE 'M'.                    
              88 PIF-NEW-COMPUTER-ISSUED VALUE 'N' 'F'.                
              88 PIF-OL-BATCH-DEC-PRNT   VALUE 'O'.                    
              88 PIF-QUICK-QUOTE         VALUE 'F'.                    
              88 PIF-PRO-RATA-CANCEL     VALUE 'P'.                    
              88 PIF-QUOTATION           VALUE 'O' 'Q'.                
              88 PIF-ONLINE-QUOTE        VALUE 'O'.                    
              88 PIF-BATCH-QUOTE         VALUE 'Q'.                    
              88 PIF-QUOTE-DECLINED      VALUE 'J'.                    
              88 PIF-QUOTE-REJECTED      VALUE 'K'.                    
              88 PIF-SHORT-RATE-CANCEL   VALUE 'S'.                    
              88 PIF-CORRECTION-REISSUE  VALUE 'X'.                    
              88 PIF-REINSTATE           VALUE '6' '9'.                
              88 PIF-REINSTATE-SHORTRATE VALUE '6'.                    
              88 PIF-REINSTATE-PRO-RATA  VALUE '9'.                    
              88 PIF-RENEWAL-COMP-ISSUED VALUE 'R'.                    
              88 PIF-COMPUTER-ISSUED     VALUE 'N' 'R'.                
              88 PIF-COMPUTER-RATED      VALUE 'A' 'N' 'R'.            
              88 PIF-CANCELLATION        VALUE 'S' 'P'.                
              88 PIF-CASH-APPLIED-CS     VALUE 'C'.                    
              88 PIF-CASH-APPLIED-DB     VALUE 'D'.                    
              88 PIF-NEW-BUSS-CLAIMS     VALUE ' '.                    
              88 PIF-VALID-ISSUE-CODE    VALUE '6' '9' 'A' 'H' 'N' 'O' 
                                               'P' 'Q' 'R' 'S' 'X'.    
           05 PIF-COMPANY-LINE           PIC X(01).                    
              88 PIF-PRIVATE-PASS-AUTO   VALUE '6'.                    
              88 PIF-PERSONAL-LINE       VALUE '1' '2' '6' '9'.        
              88 PIF-CO-LINE-PPA-COM     VALUE '6' '7'.                
              88 PIF-CASUALTY-COVERAGE   VALUE '5' '6' '7'.            
              88 PIF-CPP-PACK-DISCOUNT   VALUE '3'.                    
              88 PIF-BONDS               VALUE 'C'.                    
              88 PIF-UMBREALLA           VALUE 'O'.                    
           05 PIF-PAY-SERVICE-CODE       PIC X(01).                    
              88 VALID-REPUBLIC-PAYBY    VALUE 'U' '5' '3' '4' 'H'     
                                          
                                               '6' '7' 'K' 'L'.        
              88 REPUBLIC-CLAIMS-MADE    VALUE 'C'.                    
              88 REPUBLIC-DB-INSURED     VALUE 'U'.                    
              88 REPUBLIC-DB-AGENT       VALUE '4'.                    
              88 REPUBLIC-PREMIUM-FINANCE VALUE 'Z'.                   
              88 REPUBLIC-ITEM-BILL      VALUE 'U' '4'.                
              88 REPUBLIC-DB-MORTGAGEE   VALUE '3'.                    
              88 REPUBLIC-ACCT-CURRENT   VALUE '5'.                    
              88 REPUBLIC-HOLD-BILL      VALUE 'H'.                    
              88 REPUBLIC-ACCT-BILL      VALUE '6' '7' 'K' 'L'.        
              88 REPUBLIC-ACCT-BILL-AGT  VALUE 'K' 'L'.                
              88 REPUBLIC-ACCT-BILL-1    VALUE '7' 'K'.                
              88 PIF-ACCOUNT-CURRENT     VALUE 'A' 'R'.                
              88 PIF-ARS-PAYBY           VALUE 'R'.                    
              88 PIF-DIRECT-BILL         VALUE 'D' 'M' 'P' 'I' 'E'.    
              88 PIF-DIRECT-BILL-MORT1   VALUE 'M'.                    
              88 PIF-DIRECT-BILL-PREMFIN VALUE 'P'.                    
              88 PIF-DIRECT-BILL-INSURED VALUE 'D'.                    
              88 PIF-BROKER-BILL         VALUE 'B' 'F' 'J'.            
              88 PIF-BROKER-DIRECT-BILL  VALUE 'B'.                    
              88 PIF-BROKER-INSURED-BILL VALUE 'J'.                    
              88 PIF-BROKER-PREM-FINANCE-BILL VALUE 'F'.               
              88 PIF-VARIABLE-BILL       VALUE 'V'.                    
              88 PIF-ENDORSEMENT-BILL    VALUE 'E'.                    
              88 PIF-INSURED-BILL        VALUE 'I'.                    
              88 PIF-DIRECT-BILL-POLICY  VALUE '1' '2'.                
              88 PIF-DIRECT-DEBIT        VALUE '9'.                    
              88 PIF-SPLIT-BILL          VALUE 'S'.                    
              88 PIF-MIXED-BILL          VALUE 'X'.                    
           05 PIF-MODE-CODE              PIC X(01).                    
              88 VALID-REP-BILL-PLANS    VALUE '1' '2' '3' '4' '8'     
                                                   'B' 'D' 'E'         
                                               'A' 'C' '9'.            
              88 VALID-TFP-BILL-PLANS    VALUE 'F' 'P' 'T'.            
              88 VALID-REP-PROP-BILL-PLAN VALUE '1' '3'.               
              88 REPUBLIC-FULLPAY         VALUE '1'.                   
              88 REPUBLIC-TWO-PAY         VALUE '2'.                   
              88 REPUBLIC-40-30-30        VALUE '3'.                   
              88 REPUBLIC-EFT             VALUE 'A' 'B' 'C'.           
              88 PIF-PREPAID-POLICY      VALUE '0'.                    
              88 PIF-MODE-GREATER-THAN-ZERO     VALUE '1' THRU '9'     
                                                  'A' THRU 'Z'.        
           05 PIF-AUDIT-CODE             PIC X(01).                    
              88 PIF-NOT-APPLICABLE      VALUE 'N'.                    
              88 PIF-MONTHLY-AUDIT       VALUE 'M'.                    
              88 PIF-QUARTERLY-AUDIT     VALUE 'Q'.                    
              88 PIF-ANNUAL-AUDIT        VALUE 'A'.                    
              88 PIF-TERMINAL-AUDIT      VALUE 'T'.                    
              88 PIF-WAIVED-AUDIT        VALUE 'W'.                    
              88 PIF-SEMI-ANNUAL         VALUE 'S'.                    
              88 PIF-AUDITABLE           VALUE 'Y'.                    
           05 PIF-KIND-CODE              PIC X(01).                    
              88 PIF-ASSUMED-BUSINESS    VALUE 'A'.                    
           05 PIF-VARIATION-CODE         PIC X(01).                    
              88 MASS-AUTO-PROVISIONAL   VALUE 'P'.                    
              88 VAR-FRENCH-PRINT        VALUE 'F'.                    
           05 PIF-SORT-NAME              PIC X(04).                    
           05 PIF-BROKER-DIGITS          PIC 9(04) COMP.               
           05 PIF-UNDERWRITING-CODE.                                   
              10 PIF-REVIEW-CODE         PIC X(01).                    
                 88 PIF-CANCEL-WITH-CRMEMO VALUE '4' '7'.              
                 88 PIF-CANCEL-WITHOUT-CRMEMO VALUE '5' '8'.           
                 88 PIF-PRINT-WINDSTORM-COPY VALUE 'W'.                
                 88 PIF-MVR-SUBSYSTEM VALUE 'A'.                       
              10 PIF-MVR-REPORT-YEAR     PIC X(01).                    
                 88 PIF-NON-MVR-ISSUE    VALUE 'N'.                    
                 88 PIF-SPECIFIC-YR-MVR-ORD VALUE '0' THRU '9'.        
              10 PIF-RISK-GRADE-GUIDE    PIC X(01).                    
                 88 PIF-RG-AVE           VALUE '5'.                    
                 88 PIF-RG-POOR          VALUE '8' '9'.                
                 88 PIF-RG-POOR-NC-SC    VALUE '8' '9'.                
              10 PIF-RISK-GRADE-UNDWR    PIC X(01).                    
                 88 PIF-RETAIL-CREDIT-RPT-ORD VALUE '4' '6' '8'.       
                 88 PIF-NO-CREDIT-RPT-ORD VALUE '1' '2' '3' '5' '7'    
                       '9'.                                            
                 88 PIF-RGU-AVE          VALUE '4' '5'.                
                 88 PIF-RGU-POOR         VALUE '6' '7'.                
                 88 PIF-RGU-POOR-NC-SC   VALUE '8' '9'.                
              10 PIF-RENEWAL-CODE        PIC X(01).                    
                 88 PIF-AGENT-RENEW      VALUE '0'.                    
                 88 PIF-COMPUTER-RENEW   VALUE '1'.                    
                 88 PIF-HOME-OFFICE-RENEW VALUE '2'.                   
                 88 PIF-COMPUTER-ALREADY-RENEWED VALUE '3'.            
                 88 PIF-AGENTS-FIRST-ISSUE VALUE '4'.                  
                 88 PIF-NON-RENEWAL-N-STATUS VALUE '7'.                
                 88 PIF-NON-RENEWAL-W-STATUS VALUE '8'.                
                 88 PIF-CANCELLED-POLICY VALUE '9'.                    
                 88 PIF-CANCELLED-NON-RENEWAL VALUE '7' '8' '9'.       
           05 PIF-REASON-AMENDED         PIC X(03).                    
              88 PIF-PRO-RATA-FAIR-PLAN-RENEWAL VALUE 'PFR'.           
              88 PIF-CLOSED-AGENT-REASON VALUE 'OCA'.                  
              88 PIF-FUT-NON-RENEWAL     VALUE 'OBP'.                  
              88 PIF-NON-RENEWAL-REASON  VALUE 'ONR'.                  
              88 PIF-REINST-LAPSE-COVERAGE VALUE 'RLC'.                
              88 PIF-CANC-NONPMT-PREM    VALUE 'PNP' 'SNP'.            
              88 PIF-CANC-NONPMT-PREMFIN VALUE 'PPF' 'SPF'.            
              88 PIF-CANC-INSU-REQUEST   VALUE 'PIF' 'SIF'.            
              88 PIF-CANC-INSU-REQU-LOS  VALUE 'PIL' 'SIL'.            
              88 PIF-CANC-UNDER-REASONS  VALUE 'PUN' 'SUN'.            
              88 PIF-CANC-INSU-REQ-ORIG  VALUE 'PIO' 'SIO'.            
              88 PIF-CANC-COMP-REQU-LOS  VALUE 'PCL' 'SCL'.            
              88 PIF-CANC-COMP-REQ-ORIG  VALUE 'PCO' 'SCO'.            
              88 PIF-CANC-DEL-REGISTRY   VALUE 'PDR' 'SDR'.            
              88 PIF-CANC-SELF-DEST      VALUE 'PSD'.                  
              88 PIF-BILLING-MODE-CHNG   VALUE 'CB ' 'CBH' 'CHH' 'CH '.
              88 PIF-COMML-NEW-BILL      VALUE 'CBP'.                  
              88 PIF-REAS-AMEND-WK       VALUE 'WK ' 'WA '.            
              88 PIF-REAS-OL-DEC-PRNT    VALUE 'Q  '.                  
              88 PIF-RETURNED-AUDIT      VALUE 'WJ ' 'WK '.            
              88 PIF-AMENDED-AUDIT       VALUE 'WA ' 'WT '.            
              88 PIF-REAS-AMEND-ONZ      VALUE 'ONZ'.                  
              88 PIF-INTERIM-AUDIT-OR-CRRCTION  VALUE 'WJ ' 'WT '.     
              88 PIF-INTERIM-OR-FINAL-AUDIT     VALUE 'WJ ' 'WK '.     
              88 PIF-INTERIM-AUDIT              VALUE 'WJ '.           
              88 PIF-FINAL-AUDIT                VALUE 'WK '.           
              88 PIF-CORRECTION-TO-INTERIM-AUDT VALUE 'WT '.           
              88 PIF-FINAL-AUDIT-OR-CORRECTION  VALUE 'WK ' 'WA '.     
              88 PIF-REASON-AMEND-DELETE-CVRGE  VALUE 'DC '.           
              88 PIF-SAVE-BK-DATED-STD-AMND  VALUE 'BDA'.              
              88 PIF-SAVE-BK-DATED-VOL-AMND  VALUE 'BDJ'.              
              88 PIF-SAVE-BK-DATED-ADM-AMND  VALUE 'BDK'.              
              88 PIF-COVII-OUT-OF-SEQ-STD    VALUE 'OSS'.              
              88 PIF-COVII-OUT-OF-SEQ-VOL    VALUE 'OSV'.              
              88 PIF-COVII-OUT-OF-SEQ-ADM    VALUE 'OSA'.              
           05 PIF-RENEW-PAY-CODE         PIC X(01).                    
              88 PIF-RENEW-ARS-PAYBY     VALUE 'R'.                    
           05 PIF-RENEW-MODE-CODE        PIC X(01).                    
           05 PIF-RENEW-POLICY-SYMBOL    PIC X(03).                    
           05 PIF-RENEW-POLICY-NUMBER    PIC X(07).                    
              88 NEW-BUSINESS            VALUE '       ' '0000000'.    
           05 PIF-ORIGINAL-INCEPT        PIC X(06).                    
           05 PIF-CUSTOMER-NUMBER        PIC X(10).                    
           05 PIF-SPECIAL-USE-A.                                       
              10 PIF-POLICY-FLAG         PIC X(01).                    
              10 PIF-IN-SUIT             PIC X(01).                    
              10 PIF-BILLING-FREEZE      PIC X(01).                    
              10 PIF-CLAIMS-STATUS       PIC X(01).                    
           05 PIF-SPECIAL-USE-B          PIC X(07).                    
           05 PIF-ZIP-POSTAL-CODE.                                     
              10 FILLER                  PIC X(01).                    
              10 PIF-ZIP-5-DIGIT-CODE    PIC X(05).                    
           05 PIF-ADDRESS-LINE-1         PIC X(30).                    
           05 PIF-ADDRESS-LINE-2         PIC X(30).                                      
           05 PIF-ADDRESS-LINE-3         PIC X(30).                    
           05 PIF-ADDRESS-LINE-4         PIC X(30).                    
           05 PIF-HISTORY-OPTION         PIC X(01).                    
           05 PIF-FINAL-AUDIT-IND        PIC X(02).                    
              88 FINAL-AUDIT-PERFORMED   VALUE 'WK' 'WA'.              
           05 PIF-EXCESS-CLAIM-IND       PIC X(01).                    
              88 EXCESS-AMOUNT-SENT      VALUE 'A'.                    
              88 EXCESS-NUMBER-SENT      VALUE 'N'.                    
              88 AMT-AND-NUMBER-SENT     VALUE 'B'.                    
           05 PIF-PMS-TYPE-LEVEL         PIC X(01).                    
           05 PIF-ANNIVERSARY-RERATE     PIC X(01).                    
           05 PIF-LEGAL-ENTITY           PIC X(01).                    
           05 PIF-REINS-PROCESS-IND      PIC X(01).                    
           05 PIF-POLICY-STATUS-ON-PIF   PIC X(01).                    
           05 PIF-ACA-POLICY-CODE        PIC X(01).                    
              88 PIF-ACA-POLICY-A        VALUE 'A'.                    
           05 PIF-ACA-DESTINATION        PIC X(01).                    
              88 PIF-ACA-NO-DESTINATION  VALUE SPACES.                 
              88 PIF-ACA-AGENT-DEC       VALUE 'A'.                    
              88 PIF-ACA-AGENT-INSD-DEC  VALUE 'B'.                    
              88 PIF-ACA-AGENT-DEC-QUOTE VALUE 'C'.                    
              88 PIF-ACA-AGENT-INSD-DEC-QUOTE VALUE 'D'.               
              88 PIF-ACA-AGENT-QUOTE     VALUE 'Q'.                    
              88 PIF-ACA-AGENT-INSD-QUOTE VALUE 'R'.                   
           05 PIF-PC-ADDRESS-AREA        PIC X(08).                    
           05 PIF-PC-SEQUENCE            PIC X(04).                    
           05 PIF-UK-POSTAL-CODE         PIC X(08).                    
           05 PIF-POLICY-START-TIME-A.                                 
              10 PIF-POLICY-START-TIME   PIC 9(04).                    
           05 PIF-INSURED-OCCUPATION-CODE PIC 9(02).                   
           05 PIF-DEC-CHANGE-SW          PIC X(01).                    
              88 PIF-DEC-CHANGE-NEEDED   VALUE 'A'.                    
              88 PIF-DEC-CHANGE-NOTED    VALUE 'B'.                    
           05 PIF-SIC-CODE               PIC X(04).                    
           05 PIF-TEX-VOL-FD-FEE         PIC S9(3)  COMP-3.            
           05 PIF-PMS-FUTURE-USE         PIC X(11).                    
           05 PIF-EFT-DRAFT-DATE         PIC X(02).                    
              88 PIF-EFT-DRAFT-DATE-NONE          VALUE SPACES.        
              88 PIF-EFT-DRAFT-DATE-VALID         VALUE '01' THRU '28'.
           05 PIF-DATE-STAMP             PIC X(04).                    
           05 PIF-NAICS-CODE             PIC X(06).                    
           05 PIF-REPUBLINK-IND          PIC X(01).                    
              88 PIF-REPUBLINK                      VALUE 'Y'.         
           05 PIF-UPLOAD-TIME-STAMP.                                   
              10 PIF-UPLOAD-TIME-STAMP-1 PIC X(01).                    
              10 FILLER                  PIC X(13).                    
           05 PIF-BROKER-ALPHA           PIC X(01).                    
           05 PIF-NON-RENEW-NOTICE-IND   PIC X(01).                    
              88 PIF-RESEND-NON-RENEW    VALUE 'R'.                    
              88 PIF-NON-RENEW-SENT      VALUE 'S'.                    
              88 PIF-NON-RENEW-NOT-SENT  VALUE ' '.                    
           05 PIF-SPEC-AGT-DO            PIC X(01).                    
           88 PIF-SOUTHWEST-GENERAL      VALUE 'S'.                    
           05 PIF-ORDER-PROP-INSP        PIC X(01).                    
           05 PIF-ADDL-CANC-ACTION       PIC X(001).                   
           05 PIF-LOSS-FREE-CR           PIC X(001).                   
           05 PIF-AGCY-RENEW-CR          PIC X(001).                   
           05 PIF-RENEW-CR               PIC X(001).                   
           05 PIF-CANCEL-COUNTA          PIC X(02).                    
           05 PIF-PIA-CODE-AT-ISSUE      PIC X(01).                    
           05 PIF-RECORD-COUNT           PIC 9(05) COMP-3.             
           05 PIF-PIA-STATUS             PIC X(01).                    
           05 PIF-BILLING-ROLLOVER       PIC X(01).                    
              88 PIF-BILLING-ROLLOVER-NO   VALUE 'N' SPACE.            
              88 PIF-BILLING-ROLLOVER-YES  VALUE 'Y'.                  
           05 PIF-RECEIVED-DATE.                                       
              10 PIF-RECEIVED-DATE-CC    PIC X(02).                    
              10 PIF-RECEIVED-DATE-YMD.                                
                 15 PIF-RECEIVED-DATE-YY PIC X(02).                    
                 15 PIF-RECEIVED-DATE-MM PIC X(02).                    
                 15 PIF-RECEIVED-DATE-DD PIC X(02).                    
           05 PIF-TOTAL-PREMIUM          PIC S9(10)V9(02).             
           05 PIF-COMPANION              PIC X(10).                    
           05 PIF-CANCEL-EFF-DATE        PIC X(08).                    
           05 PIF-G1GC-INFORMATION.                                    
              10 PIF-G1GC-ZIP-PLUS06.                                  
                 15 PIF-G1GC-ZIP-PLUS04  PIC X(04).                    
                 15 PIF-G1GC-ZIP-PLUS0506 PIC X(02).                   
              10 PIF-G1GC-DPBC-CK-DIGIT  PIC X(01).                    
              10 PIF-G1GC-CARRIER-ROUTE  PIC X(04).                    
              10 PIF-G1GC-FIPS-ST-COUNTY.                              
                 15 PIF-G1GC-FIPS-STATE  PIC X(02).                    
                 15 PIF-G1GC-FIPS-COUNTY PIC X(03).                    
              10 PIF-G1GC-COUNTY         PIC X(15).                    
              88 PIF-CK-THESE-LA-PARISH      VALUE 'CAMERON        '   
                 'IBERIA         ' 'JEFFERSON      ' 'LAFOURCHE      ' 
                 'ORLEANS        ' 'PLAQUEMINES    ' 'ST BERNARD     ' 
                 'ST MARY        ' 'TERREBONE      ' 'VERMILLION     '.
              10 PIF-G1GC-LONGITUDE.                                   
                 15  PIF-G1GC-LONGITUDE-W PIC X(04).                   
                 15  PIF-G1GC-LONGITUDE-R PIC X(04).                   
              10 PIF-G1GC-LATITUDE.                                    
                 15  PIF-G1GC-LATITUDE-W PIC X(04).                    
                 15  PIF-G1GC-LATITUDE-R PIC X(04).                    
              10 PIF-MULTI-TIER          PIC X(01).                    
              10 FILLER                  PIC X(02).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).
