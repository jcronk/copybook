        01 PMDU4W1-CLASS-RATING-RECORD.
        03 PMDU4W1-CLASS-RATING-SEG.                                    
           05  PMDU4W1-SEGMENT-KEY.                                     
               07  PMDU4W1-SEGMENT-ID                     PIC X(02).    
                   88  PMDU4W1-SEGMENT-43                   VALUE '43'. 
               07  PMDU4W1-SEGMENT-STATUS                 PIC X(01).    
                   88  PMDU4W1-ACTIVE-STATUS                VALUE 'A'.  
                   88  PMDU4W1-DELETED-STATUS               VALUE 'D'.  
                   88  PMDU4W1-HISTORY-STATUS               VALUE 'H'.  
                   88  PMDU4W1-PRORATA-DELETED              VALUE 'P'.  
               07  PMDU4W1-TRANSACTION-DATE               PIC 9(08).
               07  PMDU4W1-SEGMENT-ID-KEY.                              
                   09  PMDU4W1-SEGMENT-LEVEL-CODE         PIC X(01).    
                       88  PMDU4W1-SEGMENT-LEVEL-U          VALUE 'U'.  
                   09  PMDU4W1-SEGMENT-PART-CODE          PIC X(01).    
                       88  PMDU4W1-RATING-RECORD            VALUE 'X'.  
                       88  PMDU4W1-AUDIT-RECORD             VALUE 'Z'.  
                   09  PMDU4W1-SUB-PART-CODE              PIC X(01).    
                   09  PMDU4W1-INSURANCE-LINE             PIC X(02).    
                       88  PMDU4W1-WORKERS-COMPENSATION     VALUE 'WC'. 
               07  PMDU4W1-LEVEL-KEY.                                   
                   09  PMDU4W1-WC-RATING-STATE            PIC X(02).    
                   09  PMDU4W1-LOCATION-NUMBER            PIC X(04).    
                   09  FILLER                             PIC X(09).    
                   09  PMDU4W1-RISK-UNIT.                               
                       11  PMDU4W1-CLASS-CODE             PIC X(04).    
                       11  PMDU4W1-CLASS-CODE-SEQ         PIC X(02).    
                   09  PMDU4W1-SPLIT-RATE-SEQ             PIC X(02).    
                       88  PMDU4W1-SPLIT-RATE-SEQ-00        VALUE '00'. 
                       88  PMDU4W1-SPLIT-RATE-SEQ-01        VALUE '01'. 
                       88  PMDU4W1-SPLIT-RATE-SEQ-02        VALUE '02'. 
               07  PMDU4W1-ITEM-EFFECTIVE-DATE            PIC 9(08).                         
               07  PMDU4W1-VARIABLE-KEY.                                
                   09  PMDU4W1-AUDIT-NUMBER               PIC 9(02).    
                   09  PMDU4W1-AUDIT-NUM-SEQ              PIC 9(01).    
                   09  FILLER                             PIC X(03).    
               07  PMDU4W1-PROCESS-DATE                   PIC 9(08).
           05  PMDU4W1-SEGMENT-DATA.                                    
               07  PMDU4W1-ITEM-EXPIRE-DATE               PIC 9(08).                            
               07  PMDU4W1-CLASS-TABLE-FLAG               PIC X(01).    
                   88  DISPLAY-CLASS-TABLE                VALUE 'Y'.    
                   88  DONT-DISPLAY-CLASS-TABLE           VALUE 'N' 'R'.
                   88  REDISPLAY-WD-FROM-WD               VALUE 'R'.    
                   88  CLASS-TABLE-PROCESS-COMPLETE       VALUE 'C'.    
               07  PMDU4W1-CLASS-CODE-SUFFIX.                           
                   88  PMDU4W1-NON-RATEABLE                 VALUE 'N '  
                                                             'FN' 'UN'. 
                   09  PMDU4W1-CLASS-CODE-SUFFIX-1        PIC X(01).    
                       88  PMDU4W1-CLASS-CODE-SUFFIX-A      VALUE 'A'.  
                       88  PMDU4W1-CLASS-CODE-SUFFIX-F      VALUE 'F'.  
                       88  PMDU4W1-CLASS-CODE-SUFFIX-L      VALUE 'L'.  
                       88  PMDU4W1-CLASS-CODE-SUFFIX-O      VALUE 'O'.  
                       88  PMDU4W1-CLASS-CODE-SUFFIX-STD    VALUE ' '.  
                       88  PMDU4W1-CLASS-CODE-SUFFIX-U      VALUE 'U'.  
                       88  PMDU4W1-CLASS-CODE-SUFFIX-V      VALUE 'V'.  
                       88  PMDU4W1-CLASS-CODES-NOT-LOADED   VALUE ' '   
                                                      'A' 'F' 'L' 'V'.  
                       88  PMDU4W1-CLASS-CODES-LOADED       VALUE 'O'   
                                                                  'U'.  
                   09  PMDU4W1-CLASS-CODE-SUFFIX-2        PIC X(01).    
                       88  PMDU4W1-DISEASE-INDICATOR        VALUE 'D'.  
                       88  PMDU4W1-EX-MEDICAL-INDICATOR     VALUE 'X'.  
                       88  PMDU4W1-CLASS-CODE-SUFFIX-E      VALUE 'E'.  
               07  PMDU4W1-PREMIUM-BASIS-TYP              PIC X(01).    
                   88  PMDU4W1-PAYROLL-BASIS                VALUE 'P'.  
                   88  PMDU4W1-IF-ANY-BASIS                 VALUE 'I'.  
                   88  PMDU4W1-PER-UNIT-BASIS               VALUE 'U'.  
                   88  PMDU4W1-EXEC-OFF-BASIS               VALUE 'E'.  
                   88  PMDU4W1-SOLE-PROP-BASIS              VALUE 'S'.  
                   88  PMDU4W1-EXEC-OFF-UNINC-BASIS         VALUE 'X'.  
               07  PMDU4W1-PREMIUM-BASIS-AMT              PIC 9(10).    
               07  PMDU4W1-GOVERN-CLASS-IND               PIC X(01).    
               07  PMDU4W1-RATE                     PIC 9(03)V9(02).    
               07  PMDU4W1-RATE-MG-IND                    PIC X(01).    
                   88  PMDU4W1-RATE-NOT-ENTERED             VALUE ' '.  
                   88  PMDU4W1-RATE-GENERATED               VALUE 'G'.  
                   88  PMDU4W1-RATE-MANUAL-ENTERD           VALUE 'M'.  
               07  PMDU4W1-CLASS-TERM-PREM                PIC 9(09).    
               07  PMDU4W1-CLASS-TERM-PRM-MG-IND          PIC X(01).    
                   88  PMDU4W1-CLASS-TERM-NOT-ENTERED       VALUE ' '.  
                   88  PMDU4W1-CLASS-TERM-GENERATED         VALUE 'G'.  
                   88  PMDU4W1-CLASS-TERM-MAN-ENTRD         VALUE 'M'.  
               07  PMDU4W1-CLASS-DESC-IND                 PIC X(02).    
               07  PMDU4W1-CLASS-CODE-DESCRIPTION         PIC X(32).                      
               07  PMDU4W1-SIC-CODE                       PIC X(04).    
               07  PMDU4W1-CLASS-DESC-FLAG                PIC X(01).    
                   88  PMDU4W1-CLASS-DESC-CONT            VALUE 'C'.    
               07  PMDU4W1-MINIMUM-PREMIUM                PIC 9(05).    
               07  PMDU4W1-MIN-PRM-MG-IND                 PIC X(01).    
                   88  PMDU4W1-MIN-PREM-NOT-ENTERED         VALUE ' '.  
                   88  PMDU4W1-MIN-PREM-GENERATED           VALUE 'G'.  
                   88  PMDU4W1-MIN-PREM-MANUAL-ENTERD       VALUE 'M'.  
               07  PMDU4W1-SURCHARGE-AMOUNT               PIC 9(06).    
               07  PMDU4W1-SURCHARGE-TYPE                 PIC X(01).    
                   88  PMDU4W1-NO-SURCHARGE-APPLIES         VALUE ' '.  
                   88  PMDU4W1-FLAT-DOLLAR-SURCHARGE        VALUE '$'.  
                   88  PMDU4W1-PERCENTAGE-SURCHARGE         VALUE '%'.  
               07  PMDU4W1-WAIVER-PCT                  PIC 9(02)V9(01). 
               07  PMDU4W1-WAIVER-PCT-MG-IND              PIC X(01).    
                   88  PMDU4W1-WAIV-PCT-NOT-ENTERED         VALUE ' '.  
                   88  PMDU4W1-WAIV-PCT-GENERATED           VALUE 'G'.  
                   88  PMDU4W1-WAIV-PCT-MANUAL-ENTERD       VALUE 'M'.  
               07  PMDU4W1-WAIVER-TYPE.                                 
                   09  PMDU4W1-WAIVER-TYPE-1              PIC X(01).    
                       88  PMDU4W1-NO-WAIVER-APPLIES        VALUE ' '.  
                       88  PMDU4W1-BLANKET-WAIVER-APPLIES   VALUE 'B'.  
                       88  PMDU4W1-SPECIFIC-WAIVR-APPLIES   VALUE 'S'.  
                   09  PMDU4W1-WAIVER-TYPE-2              PIC X(01).    
                       88  PMDU4W1-VALID-WAIVER-PCT-CODE    VALUE 'A'   
                                                             THRU 'Z'.  
               07  PMDU4W1-WAIV-TYPE-MG-IND               PIC X(01).    
                   88  PMDU4W1-WAIV-TYP-NOT-ENTERED         VALUE ' '.  
                   88  PMDU4W1-WAIV-TYP-GENERATED           VALUE 'G'.  
                   88  PMDU4W1-WAIV-TYP-MANUAL-ENTERD       VALUE 'M'.  
               07  PMDU4W1-EXPOSURE-COVERAGE-CODE.                      
                   09  PMDU4W1-EXPOS-COVERAGE-CODE-1      PIC X(01).    
                   09  PMDU4W1-EXPOS-COVERAGE-CODE-2      PIC X(01).    
               07  PMDU4W1-EXPOS-CODE-MG-IND              PIC X(01).    
                   88  PMDU4W1-EXPS-COD-NOT-ENTERED         VALUE ' '.  
                   88  PMDU4W1-EXPS-COD-GENERATED           VALUE 'G'.  
                   88  PMDU4W1-EXPS-COD-MANUAL-ENTERD       VALUE 'M'.  
               07  PMDU4W1-SEASONAL-RISK-IND              PIC X(01).    
               07  PMDU4W1-LOSS-CONSTANT                  PIC 9(03).    
               07  PMDU4W1-HAZARD-GROUP                   PIC X(01).    
               07  PMDU4W1-DEPOSIT-PREMIUM                PIC S9(10).   
               07  PMDU4W1-TAX-LOCATION                   PIC X(06).    
               07  PMDU4W1-WAIVER-PREMIUM                 PIC 9(10).    
               07  PMDU4W1-SURCHARGE-PREMIUM              PIC 9(10).    
               07  PMDU4W1-AUDIT-SEG-BUILT-IND            PIC X(01).    
                   88  PMDU4W1-AUDIT-SEGMENT-BUILT        VALUE 'B'.    
                   88  PMDU4W1-AUDIT-SEGMENT-ENTERED      VALUE 'E'.    
               07  PMDU4W1-RATING-EXPIRE-DATE             PIC 9(08).                          
               07  PMDU4W1-MANUALLY-RATED-IND             PIC X(01).    
                   88  PMDU4W1-MANUALLY-RATED-CLASS       VALUE 'M'.    
               07  PMDU4W1-ANNIVERSARY-RATE-DATE          PIC 9(08).                       
               07  PMDU4W1-BUREAU-RATE                PIC 9(03)V9(02).  
               07  PMDU4W1-A-RATED-INDICATOR              PIC X(01).    
                   88  PMDU4W1-A-RATED-CLASS              VALUE 'A'.    
           05  PMDU4W1-PMS-FUTURE-USE                     PIC X(107).   
           05  PMDU4W1-PRIOR-ACTION                       PIC X(01).    
           05  PMDU4W1-CUSTOMER-FUTURE-USE                PIC X(17).    
           05  PMDU4W1-YR2000-CUST-USE                    PIC X(100).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).