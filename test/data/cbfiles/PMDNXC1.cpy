        01 PMDNXC1-SUB-LOC-RATING-RECORD.
           03 PMDNXC1-SUB-LOC-RATING-SEG.                               
            05 PMDNXC1-SEGMENT-KEY.                                     
              07 PMDNXC1-SEGMENT-ID                   PIC X(02).        
                 88 PMDNXC1-SEGMENT-43                 VALUE '43'.      
              07 PMDNXC1-SEGMENT-STATUS               PIC X(01).        
                 88 PMDNXC1-ACTIVE-STATUS              VALUE 'A'.       
                 88 PMDNXC1-DELETED-STATUS             VALUE 'D'.       
                 88 PMDNXC1-HISTORY-STATUS             VALUE 'H'.       
                 88 PMDNXC1-NOT-ACTIVE-STATUS          VALUE 'H' 'D'.   
                 88 PMDNXC1-RENEWAL-FS-STATUS          VALUE 'R' 'F'.   
              07 PMDNXC1-TRANSACTION-DATE             PIC 9(08).
              07 PMDNXC1-SEGMENT-ID-KEY.                                
                 09 PMDNXC1-SEGMENT-LEVEL-CODE        PIC X(01).        
                    88 PMDNXC1-SEG-LEVEL-N             VALUE 'N'.       
                 09 PMDNXC1-SEGMENT-PART-CODE         PIC X(01).        
                    88 PMDNXC1-SEG-PART-X              VALUE 'X'.       
                 09 PMDNXC1-SUB-PART-CODE             PIC X(01).        
                 09 PMDNXC1-INSURANCE-LINE            PIC X(02).        
                    88 PMDNXC1-COMMERCIAL-CRIME       VALUE 'CR'.       
              07 PMDNXC1-LEVEL-KEY.                                     
                 09 PMDNXC1-LOCATION-NUMBER-A.                          
                    11 PMDNXC1-LOCATION-NUMBER        PIC 9(04).        
                 09 PMDNXC1-SUB-LOCATION-NUMBER-A.                      
                    11 PMDNXC1-SUB-LOCATION-NUMBER    PIC 9(03).        
              07 PMDNXC1-ITEM-EFFECTIVE-DATE          PIC 9(08).
              07 PMDNXC1-VARIABLE-KEY                 PIC X(06).        
              07 PMDNXC1-PROCESS-DATE                 PIC 9(08).
             05 PMDNXC1-SEGMENT-DATA.                                   
                 09 PMDNXC1-STATE-CODE             PIC X(02).           
                 09 PMDNXC1-RATE-PLAN-A.                                
                    11 PMDNXC1-RATE-PLAN           PIC 9(02).           
                       88 PMDNXC1-RATE-PLAN-1             VALUE 01.     
                       88 PMDNXC1-RATE-PLAN-2             VALUE 02.     
                       88 PMDNXC1-RATE-PLAN-3             VALUE 03.     
                       88 PMDNXC1-RATE-PLAN-4             VALUE 04.     
                       88 PMDNXC1-RATE-PLAN-5             VALUE 05.     
                       88 PMDNXC1-RATE-PLAN-6             VALUE 06.     
                       88 PMDNXC1-RATE-PLAN-7             VALUE 07.     
                       88 PMDNXC1-RATE-PLAN-8             VALUE 08.     
                       88 PMDNXC1-RATE-PLAN-9             VALUE 09.     
                       88 PMDNXC1-RATE-PLAN-10            VALUE 10.     
                 09 PMDNXC1-INITIAL-PREMISES       PIC X(01).           
                    88 PMDNXC1-INITIAL-PREMISES-YES   VALUE 'Y'.        
                    88 PMDNXC1-INITIAL-PREMISES-NO    VALUE 'N'.        
                 09 PMDNXC1-TERRITORY-CODE-A.                           
                    11 PMDNXC1-TERRITORY-CODE      PIC 9(03).           
                 09 PMDNXC1-CSP-CLASS-CODE-A.                           
                    11 PMDNXC1-CSP-CLASS-CODE      PIC 9(04).           
                       88 PMDNXC1-CONDOMINIUMS     VALUE 6500.          
                       88 PMDNXC1-RESTAURANTS      VALUE 5812.          
                 09 PMDNXC1-CLASS-DESC-CODE        PIC X(06).           
                 09 PMDNXC1-CLASS-DESC-SOURCE         PIC X(01).        
                    88 PMDNXC1-CLASS-TABLE          VALUE 'C'.          
                    88 PMDNXC1-CLASS-DESC-TABLE     VALUE 'D'.          
                    88 PMDNXC1-CLASS-ON-SEGMENT     VALUE 'S'.          
                 09 PMDNXC1-PMA-CODE               PIC X(02).           
                    88 PMDNXC1-MOTEL-HOTEL              VALUE 'MH'.     
                    88 PMDNXC1-APARTMENT                VALUE 'A '.     
                    88 PMDNXC1-OFFICE                   VALUE 'O '.     
                    88 PMDNXC1-MERCANTILE               VALUE 'M '.     
                    88 PMDNXC1-INSTITUTIONAL            VALUE 'I '.     
                    88 PMDNXC1-SERVICES                 VALUE 'S '.     
                    88 PMDNXC1-INDUS-PROCESS            VALUE 'IP'.     
                    88 PMDNXC1-CONTRACTOR               VALUE 'C '.     
                 09 PMDNXC1-BLANKET-IND            PIC X(01).           
                    88 PMDNXC1-SCHEDULED                VALUE 'Y'.      
                    88 PMDNXC1-BLANKET                  VALUE 'N'.      
                 09 PMDNXC1-PROPERTY-COVERED       PIC X(01).           
                    88 PMDNXC1-INC-M-S                  VALUE 'I'.      
                    88 PMDNXC1-M-S-ONLY                 VALUE 'M'.      
                    88 PMDNXC1-OTHER-M-S                VALUE 'O'.      
                 09 PMDNXC1-CLASS-LIMIT            PIC 9(10).           
                 09 PMDNXC1-SAFE-CLASS             PIC X(03).           
                 09 PMDNXC1-PERSONS-ON-DUTY        PIC 9(01).           
                 09 PMDNXC1-NUMBER-OF-ROOMS        PIC 9(04).           
                 09 PMDNXC1-NUMBER-RENTED-BOXES    PIC 9(04).           
                 09 PMDNXC1-PA-EXTENT-PROTECT      PIC X(01).           
                 09 PMDNXC1-PA-CONNECT-WITH        PIC X(01).           
                    88 PMDNXC1-PA-CENTRAL-STATION       VALUE 'C'.      
                    88 PMDNXC1-PA-LOCAL-GONG            VALUE 'G' 'L'.  
                    88 PMDNXC1-PA-POLICE-CONNECT        VALUE 'P'.      
                    88 PMDNXC1-PA-LIM-MERC-W-GRD        VALUE 'W'.      
                    88 PMDNXC1-PA-LIM-MERC-NO-GRD       VALUE 'M'.      
                    88 PMDNXC1-PA-LIM-MERC-GRD-COMM-R   VALUE 'R'.      
                    88 PMDNXC1-PA-LIM-MERC-DIG-COMM     VALUE 'D'.      
                 09 PMDNXC1-PA-GRADE-OF-ALARM      PIC X(01).           
                    88 PMDNXC1-PA-GRADE-A               VALUE 'A'.      
                    88 PMDNXC1-PA-GRADE-B               VALUE 'B'.      
                    88 PMDNXC1-PA-GRADE-C               VALUE 'C'.      
                 09 PMDNXC1-PA-KEYS                PIC X(01).           
                    88 PMDNXC1-PA-WITH-KEYS             VALUE 'W'.      
                    88 PMDNXC1-PA-WITHOUT-KEYS          VALUE 'O'.      
                 09 PMDNXC1-SV-EXTENT-PROTECT      PIC X(01).           
                    88 PMDNXC1-PARTIAL                  VALUE 'P'.      
                    88 PMDNXC1-COMPLETE                 VALUE 'C'.      
                 09 PMDNXC1-SV-CONNECT-WITH        PIC X(01).           
                    88 PMDNXC1-SV-CENTRAL-STATION       VALUE 'C'.      
                    88 PMDNXC1-SV-LOCAL-GONG            VALUE 'G'.      
                    88 PMDNXC1-SV-POLICE-CONNECT        VALUE 'P'.      
                 09 PMDNXC1-SV-GRADE-OF-ALARM      PIC X(01).           
                    88 PMDNXC1-SV-GRADE-A               VALUE 'A'.      
                    88 PMDNXC1-SV-GRADE-B               VALUE 'B'.      
                    88 PMDNXC1-SV-GRADE-C               VALUE 'C'.      
                 09 PMDNXC1-SV-KEYS                PIC X(01).           
                    88 PMDNXC1-SV-WITH-KEYS             VALUE 'W'.      
                    88 PMDNXC1-SV-WITHOUT-KEYS          VALUE 'O'.      
                 09 PMDNXC1-SCHEDULE-MOD           PIC 9(01)V9(03).     
                 09 PMDNXC1-EXPERIENCE-MOD         PIC 9(01)V9(03).     
                 09 PMDNXC1-COM-RED-MOD            PIC 9(01)V9(03).     
                 09 PMDNXC1-OTHER-MOD              PIC 9(01)V9(03).     
                 09 PMDNXC1-GROSS-REVENUE          PIC 9(10).           
                 09 PMDNXC1-RATE-GROUP-C           PIC X(02).           
                 09 PMDNXC1-RATE-GROUP-DR          PIC X(02).           
                 09 PMDNXC1-RATE-GROUP-DS          PIC X(02).           
                 09 PMDNXC1-RATE-GROUP-E           PIC X(02).           
                 09 PMDNXC1-RATE-GROUP-H           PIC X(02).           
                 09 PMDNXC1-RATE-GROUP-SBF         PIC X(02).           
                 09 PMDNXC1-RATE-GROUP-SBR         PIC X(02).           
                 09 PMDNXC1-COMPANY-NUMBER         PIC X(02).           
                 09 PMDNXC1-PA-FLOOR               PIC X(01).           
                    88 PMDNXC1-PA-ABOVE-GRADE           VALUE 'A'.      
                    88 PMDNXC1-PA-BELOW-GRADE           VALUE 'B'.      
                 09 PMDNXC1-CLASS-DESCRIPTION      PIC X(60).           
                 09 PMDNXC1-EXPENSE-MOD-A.                              
                    11 PMDNXC1-EXPENSE-MOD         PIC 9(01)V9(03).     
                 09 PMDNXC1-DEC-CHANGE-FLAG        PIC X(01).           
                    88 PMDNXC1-DEC-CHANGE-NEEDED   VALUE 'A'.           
                    88 PMDNXC1-DEC-CHANGE-NOTED    VALUE 'B'.           
                 09 PMDNXC1-NUM-ADDL-LOCS          PIC 9(04).           
                 09 PMDNXC1-NUM-PER-ACCTS          PIC 9(04).           
                 09 PMDNXC1-PERSONAL-ACCT-LIMIT    PIC 9(10).           
                 09 PMDNXC1-PCO-NUM                PIC X(02).           
                 09 PMDNXC1-STAT-CODE              PIC X(01).           
                 09 PMDNXC1-COVERAGE-CODE          PIC X(02).           
                 09 PMDNXC1-PMS-FUTURE-USE         PIC X(93).           
                 09 PMDNXC1-CUSTOMER-USE           PIC X(30).           
                 09 PMDNXC1-YR2000-CUST-USE        PIC X(100).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).