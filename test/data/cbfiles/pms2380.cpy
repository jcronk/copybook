       01  PERSONAL-LINES-UMBRELLA-UNDERL.                              
           05  PERSONAL-LINES-UMBRELLA-UNDER.                           
               10  PLUMBUR-KEY.                                         
                   15  PLUMBUR-ID                  PIC X(02).           
                       88 PLUMBUR-REC                       VALUE '44'. 
                   15  PLUMBUR-LEVEL               PIC X(01).           
                       88 PLUMBUR-UMBRELLA         VALUE 'U'.           
                   15  PLUMBUR-PART                PIC X(01).           
                       88 PLUMBUR-RATING           VALUE 'R'.           
                       88 PLUMBUR-UNDERLYING       VALUE 'U'.           
                       88 PLUMBUR-NOTES            VALUE 'W'.           
                   15  PLUMBUR-TYPE                PIC X(09).           
                       88 PLUMBUR-UMBRELLA-UNDER   VALUE 'U-UNDER01'.   
                   15  PLUMBUR-KEY-SEQUENCE        PIC X(03).           
                   15  PLUMBUR-KEY-FILLER          PIC X(08).           
               10  PLUMBUR-DATA.                                        
      * UNDERLYING FIELD A INFORMATION                                  
                   15  PLUMBUR-FA-TYPE             PIC X(02).           
                   15  PLUMBUR-FA-CARRIER.                              
                       88  PLUMBUR-FA-CARRIER-NC   VALUE 'NOT COVERED'. 
                       88  PLUMBUR-FA-CARRIER-U344 VALUE                
                           'U-344 SELF-INSURED'.                        
                       88  PLUMBUR-FA-CARRIER-SAS  VALUE                
                           'SEE ATTACHED SCHEDULE'.                     
                       20  PLUMBUR-FA-CARRIER-1ST2 PIC X(02).           
                       20  PLUMBUR-FA-CARRIER-LAST PIC X(28).           
                   15  PLUMBUR-FA-POLICY-NO.                            
                       20  PLUMBUR-FA-POLICY-NO-A  PIC X(07).           
                       20  PLUMBUR-FA-POLICY-NO-B  PIC X(13).           
                   15  PLUMBUR-FA-POL-PERIOD.                           
                       20  PLUMBUR-FA-POL-PERIOD1.                      
                           25  PLUMBUR-FA-PP1-MM   PIC X(02).           
                           25  PLUMBUR-FA-PP1-DASH PIC X(01).           
                           25  PLUMBUR-FA-PP1-YY   PIC X(02).           
                       20  PLUMBUR-FA-PP-DASH      PIC X.               
                       20  PLUMBUR-FA-POL-PERIOD2.                      
                           25  PLUMBUR-FA-PP2-MM   PIC X(02).           
                           25  PLUMBUR-FA-PP2-DASH PIC X(01).           
                           25  PLUMBUR-FA-PP2-YY   PIC X(02).           
                   15  PLUMBUR-FA-BI-PER-LIMITN    PIC S9(7) COMP-3.    
                   15  PLUMBUR-FA-BI-OCC-LIMITN    PIC S9(7) COMP-3.    
                   15  PLUMBUR-FA-PD-CSL-LIMITN    PIC S9(7) COMP-3.    
                   15  PLUMBUR-FA-REPUBLIC-POL     PIC X(01).           
      * UNDERLYING FIELD B INFORMATION                                  
                   15  PLUMBUR-FB-TYPE             PIC X(02).           
                   15  PLUMBUR-FB-CARRIER.                              
                       88  PLUMBUR-FB-CARRIER-NC   VALUE 'NOT COVERED'. 
                       88  PLUMBUR-FB-CARRIER-U344 VALUE                
                           'U-344 SELF-INSURED'.                        
                       88  PLUMBUR-FB-CARRIER-SAS  VALUE                
                           'SEE ATTACHED SCHEDULE'.                     
                       20  PLUMBUR-FB-CARRIER-1ST2 PIC X(02).           
                       20  PLUMBUR-FB-CARRIER-LAST PIC X(28).           
                   15  PLUMBUR-FB-POLICY-NO.                            
                       20  PLUMBUR-FB-POLICY-NO-A  PIC X(07).           
                       20  PLUMBUR-FB-POLICY-NO-B  PIC X(13).           
                   15  PLUMBUR-FB-POL-PERIOD.                           
                       20  PLUMBUR-FB-POL-PERIOD1.                      
                           25  PLUMBUR-FB-PP1-MM   PIC X(02).           
                           25  PLUMBUR-FB-PP1-DASH PIC X(01).           
                           25  PLUMBUR-FB-PP1-YY   PIC X(02).           
                       20  PLUMBUR-FB-PP-DASH      PIC X.               
                       20  PLUMBUR-FB-POL-PERIOD2.                      
                           25  PLUMBUR-FB-PP2-MM   PIC X(02).           
                           25  PLUMBUR-FB-PP2-DASH PIC X(01).           
                           25  PLUMBUR-FB-PP2-YY   PIC X(02).           
                   15  PLUMBUR-FB-BI-PER-LIMITN    PIC S9(7) COMP-3.    
                   15  PLUMBUR-FB-BI-OCC-LIMITN    PIC S9(7) COMP-3.    
                   15  PLUMBUR-FB-PD-CSL-LIMITN    PIC S9(7) COMP-3.    
                   15  PLUMBUR-FB-REPUBLIC-POL     PIC X(01).           
      * UNDERLYING FIELD C INFORMATION                                  
                   15  PLUMBUR-FC-TYPE             PIC X(02).           
                   15  PLUMBUR-FC-CARRIER.                              
                       88  PLUMBUR-FC-CARRIER-NC   VALUE 'NOT COVERED'. 
                       88  PLUMBUR-FC-CARRIER-U344 VALUE                
                           'U-344 SELF-INSURED'.                        
                       88  PLUMBUR-FC-CARRIER-SAS  VALUE                
                           'SEE ATTACHED SCHEDULE'.                     
                       20  PLUMBUR-FC-CARRIER-1ST2 PIC X(02).           
                       20  PLUMBUR-FC-CARRIER-LAST PIC X(28).           
                   15  PLUMBUR-FC-POLICY-NO.                            
                       20  PLUMBUR-FC-POLICY-NO-A  PIC X(07).           
                       20  PLUMBUR-FC-POLICY-NO-B  PIC X(13).           
                   15  PLUMBUR-FC-POL-PERIOD.                           
                       20  PLUMBUR-FC-POL-PERIOD1.                      
                           25  PLUMBUR-FC-PP1-MM   PIC X(02).           
                           25  PLUMBUR-FC-PP1-DASH PIC X(01).           
                           25  PLUMBUR-FC-PP1-YY   PIC X(02).           
                       20  PLUMBUR-FC-PP-DASH      PIC X.               
                       20  PLUMBUR-FC-POL-PERIOD2.                      
                           25  PLUMBUR-FC-PP2-MM   PIC X(02).           
                           25  PLUMBUR-FC-PP2-DASH PIC X(01).           
                           25  PLUMBUR-FC-PP2-YY   PIC X(02).           
                   15  PLUMBUR-FC-BI-PER-LIMITN    PIC S9(7) COMP-3.    
                   15  PLUMBUR-FC-BI-OCC-LIMITN    PIC S9(7) COMP-3.    
                   15  PLUMBUR-FC-PD-CSL-LIMITN    PIC S9(7) COMP-3.    
                   15  PLUMBUR-FC-REPUBLIC-POL     PIC X(01).           
      * UNDERLYING FIELD C INFORMATION                                  
                   15  PLUMBUR-FD-TYPE             PIC X(02).           
                   15  PLUMBUR-FD-CARRIER.                              
                       88  PLUMBUR-FD-CARRIER-NC   VALUE 'NOT COVERED'. 
                       88  PLUMBUR-FD-CARRIER-U344 VALUE                
                           'U-344 SELF-INSURED'.                        
                       88  PLUMBUR-FD-CARRIER-SAS  VALUE                
                           'SEE ATTACHED SCHEDULE'.                     
                       20  PLUMBUR-FD-CARRIER-1ST2 PIC X(02).           
                       20  PLUMBUR-FD-CARRIER-LAST PIC X(28).           
                   15  PLUMBUR-FD-POLICY-NO.                            
                       20  PLUMBUR-FD-POLICY-NO-A  PIC X(07).           
                       20  PLUMBUR-FD-POLICY-NO-B  PIC X(13).           
                   15  PLUMBUR-FD-POL-PERIOD.                           
                       20  PLUMBUR-FD-POL-PERIOD1.                      
                           25  PLUMBUR-FD-PP1-MM   PIC X(02).           
                           25  PLUMBUR-FD-PP1-DASH PIC X(01).           
                           25  PLUMBUR-FD-PP1-YY   PIC X(02).           
                       20  PLUMBUR-FD-PP-DASH      PIC X.               
                       20  PLUMBUR-FD-POL-PERIOD2.                      
                           25  PLUMBUR-FD-PP2-MM   PIC X(02).           
                           25  PLUMBUR-FD-PP2-DASH PIC X(01).           
                           25  PLUMBUR-FD-PP2-YY   PIC X(02).           
                   15  PLUMBUR-FD-BI-PER-LIMITN    PIC S9(7) COMP-3.    
                   15  PLUMBUR-FD-BI-OCC-LIMITN    PIC S9(7) COMP-3.    
                   15  PLUMBUR-FD-PD-CSL-LIMITN    PIC S9(7) COMP-3.    
                   15  PLUMBUR-FD-REPUBLIC-POL     PIC X(01).           
                   15  PLUMBUR-FA-WEB-TYPE         PIC X(2).            
                   15  PLUMBUR-FA-WEB-TYPE-SEQ     PIC 9(2).            
                   15  PLUMBUR-FB-WEB-TYPE         PIC X(2).            
                   15  PLUMBUR-FB-WEB-TYPE-SEQ     PIC 9(2).            
                   15  PLUMBUR-FC-WEB-TYPE         PIC X(2).            
                   15  PLUMBUR-FC-WEB-TYPE-SEQ     PIC 9(2).            
                   15  PLUMBUR-FD-WEB-TYPE         PIC X(2).            
                   15  PLUMBUR-FD-WEB-TYPE-SEQ     PIC 9(2).            
                   15  FILLER                      PIC X(105).          
                   15  PLUMBUR-SEQ                 PIC X(03).           
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).      