        01 COMPUTER-ISSUE-FORMS-RECORD.
           03 COMPUTER-ISSUE-FORMS-SEG.                                 
           05 FORMS-ID                   PIC X(02).                     
              88 FORMS-REC               VALUE '37'.                    
           05 FORMS-UNIT.                                               
              10 FORMS-LINE-INDICATOR    PIC X(01).                     
                 88 HAP-FORMS-HO          VALUE '1'.                    
                 88 HAP-FORMS-AUTO        VALUE '2'.                    
                 88 HAP-FORMS-UMBRELLA    VALUE '3'.                    
                 88 HAP-FORMS-POLICY-LVL  VALUE '4'.                    
              10 FORMS-HAP-UNIT-NUM      PIC X(02).                     
           05 FORMS-SECTIONS.                                           
              10 FORMS-ENTRIES           OCCURS 23 TIMES INDEXED BY     
                    FORM-I.                                             
                 88 FORMS-TABLE-EMPTY    VALUE SPACES.                  
                 15 FORM-NO              PIC X(07).                     
                    88 VALID-MOLD-TX-HO-FORMS  VALUE 'HO-161 ' 'HO-162'
                                 'HO-163 ' 'HO-164 ' 'HO-165 ' 'HO-166'
                                 'HO-167 '.
                    88 VALID-MOLD-TX-FIRE-FORMS  VALUE
                                 'TDP-004' 'TDP-005'.
                    88 VALID-MOLD-TX-FORMS  VALUE 'HO-161 ' 'HO-162'
                                 'HO-163 ' 'HO-164 ' 'HO-165 ' 'HO-166'
                                 'HO-167 ' 'TDP-004' 'TDP-005'.
                 15 FORM-DATE.                                          
                    20 FORM-MONTH        PIC X(02).                     
                    20 FORM-YEAR         PIC X(02).                     
                 15 FORM-ACTION          PIC X(01).                     
                    88 SEG-LEVEL-NEW-GEN-0     VALUE '0'.               
                    88 SEG-LEVEL-OLD-GEN-1     VALUE '1'.               
                    88 SEG-LEVEL-NEW-MAN-2     VALUE '2'.               
                    88 SEG-LEVEL-OLD-MAN-3     VALUE '3'.               
                    88 POL-LEVEL-NEW-GEN-4     VALUE '4'.               
                    88 POL-LEVEL-OLD-GEN-5     VALUE '5'.               
                    88 POL-LEVEL-NEW-MAN-6     VALUE '6'.               
                    88 POL-LEVEL-OLD-MAN-7     VALUE '7'.               
                    88 SEG-LEVEL-NEW-MAN-8     VALUE '8'.               
                    88 POL-LEVEL-NEW-MAN-9     VALUE '9'.               
                    88 FORMS-POLICY-LEVEL-FORM VALUE '4' '5' '6' '7'.   
                    88 FORM-NEW          VALUE '0' '2' '4' '6'.         
                    88 FORM-MANUALLY-ENTERED VALUE '6' '7' '2' '3'.     
            05 FORMS-PMS-FUTURE-USE      PIC X(59).                     
            05 NEW-FORM-PROCESSING-SW    PIC X(01).                     
               88 NEW-FORM-PROCESSING    VALUE 'Y'.                     
            05 FORMS-ACA-IDENTIFIER-SAVE.                               
               10 FORMS-ACA-FIXED-IDENTIFIER PIC 9(04).                 
            05 FORMS-OLD-ACTION-CODE     PIC X(01).                     
            05 FORMS-CUST-FUTURE-USE     PIC X(03).                     
            05 FORMS-YR2000-CUST-USE     PIC X(100).                    
            05 FORMS-DUP-KEY-SEQ-NUM     PIC X(03).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).