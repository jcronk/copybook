        01 PMDRXI1-UNIT-GRP-RATING-RECORD.
           03 PMDRXI1-UNIT-GRP-RATING-SEG.                              
             05 PMDRXI1-SEGMENT-KEY.                                    
              07 PMDRXI1-SEGMENT-ID                   PIC X(02).        
                 88 PMDRXI1-SEGMENT-43                 VALUE '43'.      
              07 PMDRXI1-SEGMENT-STATUS               PIC X(01).        
                 88 PMDRXI1-ACTIVE-STATUS              VALUE 'A'.       
                 88 PMDRXI1-DELETED-STATUS             VALUE 'D'.       
                 88 PMDRXI1-HISTORY-STATUS             VALUE 'H'.       
                 88 PMDRXI1-NOT-ACTIVE-STATUS          VALUE 'H' 'D'.   
                 88 PMDRXI1-RENEWAL-FS-STATUS          VALUE 'R' 'F'.   
              07 PMDRXI1-TRANSACTION-DATE             PIC 9(08).
              07 PMDRXI1-SEGMENT-ID-KEY.                                
                 09 PMDRXI1-SEGMENT-LEVEL-CODE        PIC X(01).        
                    88 PMDRXI1-SEG-LEVEL-R             VALUE 'R'.       
                 09 PMDRXI1-SEGMENT-PART-CODE         PIC X(01).        
                    88 PMDRXI1-SEG-PART-X              VALUE 'X'.       
                 09 PMDRXI1-SUB-PART-CODE             PIC X(01).        
                 09 PMDRXI1-INSURANCE-LINE            PIC X(02).        
                    88 PMDRXI1-INLAND-MARINE           VALUE 'IM'.      
              07 PMDRXI1-LEVEL-KEY.                                     
                 09 PMDRXI1-LOCATION-NUMBER-A.                          
                    11 PMDRXI1-LOCATION-NUMBER        PIC 9(04).        
                       88 PMDRXI1-TERRORISM-COV-LOC   VALUE 0911.       
                 09 PMDRXI1-SUB-LOCATION-NUMBER-A.                      
                    11 PMDRXI1-SUB-LOCATION-NUMBER    PIC 9(03).        
                 09 PMDRXI1-RISK-UNIT-GROUP-KEY.                        
                    11 PMDRXI1-RISK-UNIT-GROUP-A.                       
                       13 PMDRXI1-RISK-UNIT-GROUP     PIC 9(03).        
                          88 PMDRXI1-ACCOUNTS-RECEIVABLE  VALUE 404.    
                          88 PMDRXI1-COMM-ART-CAMERAS     VALUE 460.    
                          88 PMDRXI1-COMM-ART-MUS-INSTR   VALUE 461.    
                          88 PMDRXI1-PHYS-AND-SURGEONS    VALUE 430.    
                          88 PMDRXI1-SIGNS                VALUE 432.    
                          88 PMDRXI1-VALUABLE-PAPERS      VALUE 438.    
                          88 PMDRXI1-BUILDERS-RISK                      
                          VALUE 450 451 452 453 454 455 456 457 458 459.
                    11 PMDRXI1-SEQUENCE-NUMBER-A.                       
                       13 PMDRXI1-SEQUENCE-NUMBER     PIC 9(03).        
              07 PMDRXI1-ITEM-EFFECTIVE-DATE          PIC 9(08).
              07 PMDRXI1-VARIABLE-KEY                 PIC X(06).        
              07 PMDRXI1-PROCESS-DATE                 PIC 9(08).
             05 PMDRXI1-SEGMENT-DATA.                                   
                 09 PMDRXI1-RATING-STATE              PIC X(02).        
                 09 PMDRXI1-COMPANY-NUMBER-A.                           
                    11 PMDRXI1-COMPANY-NUMBER         PIC 9(02).        
                 09 PMDRXI1-BASIS-OF-COVERAGE         PIC X(01).        
                    88 PMDRXI1-SCHEDULED-BASIS         VALUE 'S'.       
                    88 PMDRXI1-UNSCHEDULED-BASIS       VALUE 'U'.       
                 09 PMDRXI1-BASIS-LOC0-IND            PIC X(01).        
                    88 PMDRXI1-LOC-ZERO-BASIS          VALUE 'Y'.       
                 09 PMDRXI1-FORM-TYPE                 PIC X(02).        
                    88 PMDRXI1-REPORTING-FORM          VALUE 'R '.      
                    88 PMDRXI1-NON-REPORTING-FORM      VALUE 'NR'.      
                    88 PMDRXI1-MOTION-PICT-CA          VALUE 'MP'.      
                    88 PMDRXI1-NOT-MOTION-PICT-CA      VALUE 'NP'.      
                    88 PMDRXI1-NON-MOBILE-ORGAN        VALUE 'NO'.      
                    88 PMDRXI1-INDIVIDUAL              VALUE 'I '.      
                    88 PMDRXI1-DANCE-BANDS             VALUE 'DB'.      
                    88 PMDRXI1-OTHER-BANDS             VALUE 'OB'.      
                    88 PMDRXI1-BASIC-FORM              VALUE 'B '.      
                    88 PMDRXI1-BASIC-PLUS-EXTEN        VALUE 'BE'.      
                    88 PMDRXI1-LIMITED-PROP-CARR       VALUE 'LP'.      
                 09 PMDRXI1-FORM-LOC0-IND             PIC X(01).        
                    88 PMDRXI1-LOC-ZERO-FORM           VALUE 'Y'.       
                 09 PMDRXI1-LIMIT-OF-INSURANCE-A.                       
                    11 PMDRXI1-LIMIT-OF-INSURANCE     PIC 9(10).        
                 09 PMDRXI1-PMA-CODE                  PIC X(02).        
                 09 PMDRXI1-FILING-STATUS             PIC X(01).        
                    88 PMDRXI1-FILED                   VALUE 'F'.       
                    88 PMDRXI1-NONFILED                VALUE 'N'.       
                 09 PMDRXI1-AR-VP-RECEPTACLE-CR       PIC X(01).        
                 09 PMDRXI1-AR-VP-AFP-LIMIT-A.                          
                    11 PMDRXI1-AR-VP-AFP-LIMIT        PIC 9(10).        
                 09 PMDRXI1-AR-AVERAGE-LIMIT-A.                         
                    11 PMDRXI1-AR-AVERAGE-LIMIT       PIC 9(10).        
                 09 PMDRXI1-AR-DUP-RECORDS-PCT        PIC X(02).        
                 09 PMDRXI1-AR-CLASS-OF-RISK          PIC X(01).        
                    88 PMDRXI1-AR-VALID-CLASS         VALUE             
                                                      'W' 'R' 'I'.      
                 09 PMDRXI1-DEDUCTIBLE.                                 
                    11 PMDRXI1-DEDUCTIBLE-N           PIC 9(07).        
                 09 PMDRXI1-DEDUCTIBLE-LOC0-IND       PIC X(01).        
                    88 PMDRXI1-LOC-ZERO-DEDUCTIBLE     VALUE 'Y'.       
                 09 PMDRXI1-IRPM-A.                                     
                    11 PMDRXI1-IRPM                   PIC 9(01)V9(03).  
                 09 PMDRXI1-IRPM-LOC0-IND             PIC X(01).        
                    88 PMDRXI1-LOC-ZERO-IRPM           VALUE 'Y'.       
                 09 PMDRXI1-SCHEDULE-MOD-A.                             
                    11 PMDRXI1-SCHEDULE-MOD           PIC 9(01)V9(03).  
                 09 PMDRXI1-SCHED-LOC0-IND            PIC X(01).        
                    88 PMDRXI1-LOC-ZERO-SCHED          VALUE 'Y'.       
                 09 PMDRXI1-EXPENSE-MOD-A.                              
                    11 PMDRXI1-EXPENSE-MOD            PIC 9(01)V9(03).  
                 09 PMDRXI1-EXPENSE-LOC0-IND          PIC X(01).        
                    88 PMDRXI1-LOC-ZERO-EXPENSE        VALUE 'Y'.       
                 09 PMDRXI1-OTHER-MOD-A.                                
                    11 PMDRXI1-OTHER-MOD              PIC 9(01)V9(03).  
                 09 PMDRXI1-OTHER-LOC0-IND            PIC X(01).        
                    88 PMDRXI1-LOC-ZERO-OTHER          VALUE 'Y'.       
                 09 PMDRXI1-COMM-RED-MOD-A.                             
                    11 PMDRXI1-COMM-RED-MOD           PIC 9(01)V9(03).  
                 09 PMDRXI1-COMM-RED-LOC0-IND         PIC X(01).        
                    88 PMDRXI1-LOC-ZERO-COMM-RED       VALUE 'Y'.       
                 09 PMDRXI1-AGENTS-COMM-RATE-A.                         
                    11 PMDRXI1-AGENTS-COMM-RATE       PIC 9(01)V9(05).  
                 09 PMDRXI1-AGENTS-COMM-IND           PIC X(01).        
                    88 PMDRXI1-LOC-ZERO-AGENTS-COMM    VALUE 'Y'.       
                 09 PMDRXI1-SIGNS-POSITION            PIC X(01).        
                    88 PMDRXI1-INSIDE-SIGNS            VALUE 'I'.       
                 09 PMDRXI1-DEC-CHANGE-FLAG           PIC X(01).        
                    88 PMDRXI1-DEC-CHANGE-NEEDED      VALUE 'A'.        
                    88 PMDRXI1-DEC-CHANGE-NOTED       VALUE 'B'.        
                 09 PMDRXI1-LOSS-COST-MULT-1          PIC 9(03)V9(03).  
                 09 PMDRXI1-LOSS-COST-MULT-2          PIC 9(03)V9(03).  
                 09 PMDRXI1-LOSS-COST-MULT-3          PIC 9(03)V9(03).  
                 09 PMDRXI1-PMSC-FUTURE-USE           PIC X(165).       
                 09 PMDRXI1-CUSTOMER-FUTURE-USE       PIC X(30).        
                 09 PMDRXI1-YR2000-CUST-USE           PIC X(100).
            05 NEEDED-SORT-DATA          PIC X(179).
            05 BLANK-OUT                 PIC X(44).