000001  01  MAIN-RECORD.
000005    05  RECORD-HEADER                         PIC X(30).
000010    05  DETAILED-HEADER REDEFINES
                  RECORD-HEADER.
000020        10  GROUP-NAME                        PIC X(4).
000030        10  GROUP-LENGTH                      PIC 999.
000040        10  FILLER                            PIC X.
000050        10  GROUP-VERSION                     PIC X.
000060        10  FILLER                            PIC X.
000070        10  PROCESSING-LEVEL                  PIC XX.
000080        10  ITERATION-NUMBER                  PIC 9(4).
000090        10  GROUP-PARENT                      PIC X(4).
000100        10  GROUP-PARENT-LEVEL                PIC XX.
000110        10  GROUP-PARENT-ITERATION            PIC 9(4).
000120        10  FILLER                            PIC X(4).
000130    05  POLICY-NUMBER                         PIC X(25).
000140    05  BILLING-ACCT-NBR                      PIC X(10).
000150    05  COMPANY-CODE                          PIC X(6).
000160    05  LINE-BUSINESS-CODE                    PIC X(5).
000161        88  LOB-HOME      VALUE 'HOME'.
000162        88  LOB-DWELL     VALUE 'DFIRE'.
000170*   05  LINE-BUSINESS-SUBCODE        PIC X(4).
000171    05  LINE-BUSINESS-SECOND-CODE             PIC X(4).
000180    05  POL-EFFECTIVE-DATE                    PIC 9(06) COMP-3.
000190    05  POL-EXP-DATE                          PIC 9(6)  COMP-3.
000200    05  PRODUCER-SUB-CODE                     PIC X(7).
000210    05  ORIG-POL-INC-DATE                     PIC 9(8)  COMP-3.
000220    05  RENEWAL-TERM                          PIC 999.
000224    05  PREMIUM-INFO.
000230        10  CURR-TERM-AMT                         
                                   PIC S9(09)V99    COMP-3.
000240        10  NET-CHANGE-AMT                        
                                   PIC S9(09)V99    COMP-3.
      *   this is to test repeating groups
000250    05  ENDORSEMENTS  OCCURS 5 TIMES.
000260        10  FORM-NAME                         PIC X(40).
000270        10  FORM-NUMBER                       PIC X(6).
000280        10  FORM-REVISION-DATE                PIC 9(4).
000290        10  FORM-REVISION-MONTH REDEFINES 
                   FORM-REVISION-DATE PIC 9(2).
          05  FILLER                                PIC X(25).
