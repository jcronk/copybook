# frozen_string_literal: true

require 'test_helper'
require 'copybook/multi_parser'

class TestMultiParser < Minitest::Test
  include Copybook

  context 'a multi-parser' do
    should 'read a file with multiple copybooks' do
      cbfile = File.join(DATADIR, 'multiple_copybooks.cpy')
      listing = MultiParser.new(cbfile).parse_all
      %w[ CWS-ABCLC2 CWS-PL02LC5 CWS-AUDRVR CWS-AUVEHOWN CWS-PL02
          CIS-EXTRACT-HEADER CWS-PL02LOC ].each do |seg_name|
        assert_includes listing, seg_name
      end
    end

    should 'read in multiple files' do
      files = Dir[File.join(DATADIR, 'cbfiles', '*.cpy')]
      listing = MultiParser.new(files).parse_all
      %w[ PMDUXI1-UNIT-RATING-RECORD GARAGE-KEEPERS-LIAB-UY-RECORD
          PERSONAL-LINES-UMBRELLA-UNDERL PROCESS-CONTROL-REC
          VEHICLE-COVERAGE-RECORD STAT-ACCOUNT-RECORD
          VEHICLE-DESCRIPTION-RECORD ].each do |seg_name|
        assert_includes listing, seg_name
      end
    end
  end
end
