# frozen_string_literal: true

require 'test_helper'
require 'copybook'

class TestDataReader < Minitest::Test
  def setup
    @cb = Copybook::Parse(File.join(DATADIR, 'sample_copybook.cpy'))
    @asc_reader = Copybook::DataReader.new(
      File.open(File.join(DATADIR, 'fixed_ascii.txt'), 'rb'),
      separator: "\x0a",
      ebcdic: false
    )
    @ebc_reader = Copybook::DataReader.new(
      File.open(File.join(DATADIR, 'fixed_ebcdic.txt'), 'rb'),
      separator: "\x0d\x0a"
    )
    @redf_reader = Copybook::DataReader.new(
      File.open(File.join(DATADIR, 'fixed_ascii.txt'), 'rb'),
      separator: "\x0a",
      ebcdic: false,
      with_redefines: true
    )
    @lw_reader = Copybook::DataReader.new(
      File.open(File.join(DATADIR, 'fixed_ascii.txt'), 'rb'),
      separator: "\x0a",
      ebcdic: false,
      read_linewise: true
    )
  end

  def get_defs(reader)
    list = []
    reader.get_field_values(@cb) do |field, value|
      list << [field, value]
    end
    list
  end

  context 'an ASCII data reader' do
    setup do
      @defs = get_defs(@asc_reader)
    end

    should 'get fields and values from a data file' do
      @defs.each do |defn|
        assert_instance_of Copybook::RecordDefinition, defn.first
      end
    end

    should 'pick up the correct field values' do
      policy_nbr = @defs.find { |d| d.first.name == 'POLICY-NUMBER' }.last
      assert_equal 'GL1086534', policy_nbr
    end
  end

  context 'an EBCDIC data reader' do
    setup do
      @defs = get_defs(@ebc_reader)
    end

    should 'get fields and values from a data file' do
      @defs.each do |defn|
        assert_instance_of Copybook::RecordDefinition, defn.first
      end
    end

    should 'pick up the correct field values' do
      policy_nbr = @defs.find { |d| d.first.name == 'POLICY-NUMBER' }.last
      assert_equal 'GL1086534', policy_nbr
    end
  end

  context "a reader with the 'redefines' option set" do
    setup do
      @defs = get_defs(@redf_reader)
    end

    should 'show the expected redefined values' do
      group_name = @defs.find { |d| d.first.name == 'GROUP-NAME' }.last
      assert_equal '5BPI', group_name
    end
  end

  context "a reader with 'linewise' set" do
    setup do
      @defs = get_defs(@lw_reader)
    end

    should 'get a second line correctly' do
      next_line = get_defs(@lw_reader)
      polnbr = next_line.find { |d| d.first.name == 'POLICY-NUMBER' }.last
      assert_equal 'POLICY#2', polnbr
    end
  end
end
