# frozen_string_literal: true

require 'copybook'
require 'yaml'
require 'test_helper'

class TestCopybookParser < Minitest::Test
  def setup
    @cbfile_path = File.join(DATADIR, 'sample_copybook.cpy')
  end

  context 'a copybook parser' do
    should 'wrap content when requested' do
      cb = Copybook::Parse(@cbfile_path, wrap_content: true)
      assert cb.name == 'DATA-DIVISION'
    end

    should 'leave content alone when wrap option not sent' do
      cb = Copybook::Parse(@cbfile_path)
      assert cb.name == 'MAIN-RECORD'
    end
  end
end

class TestCopybookReader < Minitest::Test
  def setup
    @cbfile_path = File.join(DATADIR, 'sample_copybook.cpy')
    cbtext = File.open(@cbfile_path)
    @lines = Copybook::Reader.new(cbtext).lines
  end

  context 'a copybook reader' do
    should 'accept a file path as input' do
      r = Copybook::Reader.new(@cbfile_path)
      refute r.lines.empty?
    end

    should 'remove line numbers from input' do
      assert @lines.select { |line| line =~ /^\d{6}/ }.empty?
    end

    should 'remove comments from input' do
      assert @lines.select { |line| line =~ /\*/ }.empty?
    end
  end
end

class TestParser < Minitest::Test
  context 'a parser' do
    should "correctly parse 'PIC XXX'" do
      parsed_pic = Copybook::PicParser.new('PIC XXX').parse
      assert_equal :char, parsed_pic.type
      assert_equal 3, parsed_pic.length
    end

    should "correctly parse 'PIC X(5)'" do
      parsed_pic = Copybook::PicParser.new('PIC X(5)').parse
      assert_equal :char, parsed_pic.type
      assert_equal 5, parsed_pic.length
    end

    should "correctly parse 'PIC X'" do
      parsed_pic = Copybook::PicParser.new('PIC X').parse
      assert_equal :char, parsed_pic.type
      assert_equal 1, parsed_pic.length
    end

    should "correctly parse 'PIC 999'" do
      parsed_pic = Copybook::PicParser.new('PIC 999').parse
      assert_equal :num, parsed_pic.type
      assert_equal 3, parsed_pic.length
    end

    should "correctly parse 'PIC 9(4)'" do
      parsed_pic = Copybook::PicParser.new('PIC 9(4)').parse
      assert_equal :num, parsed_pic.type
      assert_equal 4, parsed_pic.length
    end

    should "correctly parse 'PIC 99V99'" do
      parsed_pic = Copybook::PicParser.new('PIC 99V99').parse
      assert_equal :num, parsed_pic.type
      assert_equal 4, parsed_pic.length
      assert_equal 2, parsed_pic.base_length
      assert_equal 2, parsed_pic.imp_decimals
    end

    should "correctly parse 'PIC 9(3)V99'" do
      parsed_pic = Copybook::PicParser.new('PIC 9(3)V99').parse
      assert_equal :num, parsed_pic.type
      assert_equal 5, parsed_pic.length
      assert_equal 3, parsed_pic.base_length
      assert_equal 2, parsed_pic.imp_decimals
    end

    should "correctly parse 'PIC 9(3)V9(3)'" do
      parsed_pic = Copybook::PicParser.new('PIC 9(3)V9(3)').parse
      assert_equal :num, parsed_pic.type
      assert_equal 6, parsed_pic.length
      assert_equal 3, parsed_pic.base_length
      assert_equal 3, parsed_pic.imp_decimals
    end

    should "correctly parse 'PIC 99V9(3)'" do
      parsed_pic = Copybook::PicParser.new('PIC 99V9(3)').parse
      assert_equal :num, parsed_pic.type
      assert_equal 5, parsed_pic.length
      assert_equal 2, parsed_pic.base_length
      assert_equal 3, parsed_pic.imp_decimals
    end

    should "correctly parse 'PIC V99'" do
      parsed_pic = Copybook::PicParser.new('PIC V99').parse
      assert_equal :num, parsed_pic.type
      assert_equal 2, parsed_pic.length
      assert_equal 0, parsed_pic.base_length
      assert_equal 2, parsed_pic.imp_decimals
    end

    should "correctly parse 'PIC V9(4)'" do
      parsed_pic = Copybook::PicParser.new('PIC V9(4)').parse
      assert_equal :num, parsed_pic.type
      assert_equal 4, parsed_pic.length
      assert_equal 0, parsed_pic.base_length
      assert_equal 4, parsed_pic.imp_decimals
    end

    should "correctly parse 'PIC S999'" do
      parsed_pic = Copybook::PicParser.new('PIC S999').parse
      assert_equal :signed, parsed_pic.type
      assert_equal 3, parsed_pic.length
    end

    should "correctly parse 'PIC S9(4)'" do
      parsed_pic = Copybook::PicParser.new('PIC S9(4)').parse
      assert_equal :signed, parsed_pic.type
      assert_equal 4, parsed_pic.length
    end

    should "correctly parse 'PIC S99V99'" do
      parsed_pic = Copybook::PicParser.new('PIC S99V99').parse
      assert_equal :signed, parsed_pic.type
      assert_equal 4, parsed_pic.length
      assert_equal 2, parsed_pic.base_length
      assert_equal 2, parsed_pic.imp_decimals
    end

    should "correctly parse 'PIC S9(3)V99'" do
      parsed_pic = Copybook::PicParser.new('PIC S9(3)V99').parse
      assert_equal :signed, parsed_pic.type
      assert_equal 5, parsed_pic.length
      assert_equal 3, parsed_pic.base_length
      assert_equal 2, parsed_pic.imp_decimals
    end

    should "correctly parse 'PIC S9(3)V9(3)'" do
      parsed_pic = Copybook::PicParser.new('PIC S9(3)V9(3)').parse
      assert_equal :signed, parsed_pic.type
      assert_equal 6, parsed_pic.length
      assert_equal 3, parsed_pic.base_length
      assert_equal 3, parsed_pic.imp_decimals
    end

    should "correctly parse 'PIC S99V9(3)'" do
      parsed_pic = Copybook::PicParser.new('PIC S99V9(3)').parse
      assert_equal :signed, parsed_pic.type
      assert_equal 5, parsed_pic.length
      assert_equal 2, parsed_pic.base_length
      assert_equal 3, parsed_pic.imp_decimals
    end

    should "correctly parse 'PIC 99.9'" do
      parsed_pic = Copybook::PicParser.new('PIC 99.9').parse
      assert_equal :num, parsed_pic.type, 'The type is numeric'
      assert_equal 4, parsed_pic.length, 'Length is 4, including literal decimal'
      assert_equal 0, parsed_pic.imp_decimals, 'No implied decimals'
      assert_equal 3, parsed_pic.base_length, 'the length without the literal is 3'
    end

    should 'correctly parse a PIC with a usage clause' do
      parser = Copybook::PicParser.new('PIC 99.9  COMP-3')
      parsed_pic = parser.parse
      assert_equal :num, parsed_pic.type, 'The type is numeric'
      assert_equal 'PIC 99.9  ', parser.parsed_string
    end

    should 'tell the difference between a decimal and a line-end char' do
      parsed_pic = Copybook::PicParser.new('PIC 99.99.').parse
      assert_equal :num, parsed_pic.type, 'The type is numeric'
      assert_equal 5, parsed_pic.length, 'Length is 4, including literal decimal'
      assert_equal 0, parsed_pic.imp_decimals, 'No implied decimals'
      assert_equal 4, parsed_pic.base_length, 'the length without the literal is 3'
    end
  end
end
