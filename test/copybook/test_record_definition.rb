# frozen_string_literal: true

require 'copybook'
require 'test_helper'

class TestRecordDefinition < Minitest::Test
  def setup
    @record_def_main = Copybook::Parse(File.join(DATADIR, 'sample_copybook.cpy'))
    @rdef_redfn = @record_def_main['DETAILED-HEADER']
    @redefine_child = @rdef_redfn['GROUP-NAME']
    @rdef_occ = @record_def_main['ENDORSEMENTS'].first
    @occurs_child = @rdef_occ['FORM-NUMBER']
    @segment_with_children = @rdef_occ
  end

  context 'a record definition object' do
    should 'allow access to children using []' do
      assert_instance_of Copybook::RecordDefinition, @record_def_main['MAIN-RECORD']
    end

    should 'show the expected number of children for MAIN-RECORD' do
      assert_equal 19, @record_def_main['MAIN-RECORD'].children.count
    end

    should 'allow access to ANY descendant with []' do
      assert_instance_of Copybook::RecordDefinition, @record_def_main['CURR-TERM-AMT']
    end

    should 'allow access to redefine descendents with []' do
      assert_instance_of Copybook::RecordDefinition, @record_def_main['DETAILED-HEADER']
    end

    should 'not show redefines in default call to #list' do
      assert @record_def_main.list.select(&:redefines).empty?
    end

    should 'show redefines in call to #list when with_redefines: true' do
      refute @record_def_main.list(with_redefines: true).select(&:redefines).empty?
    end

    should 'show the correct record length' do
      assert_equal 390, @record_def_main.entire_record_length
    end

    should 'allow access to descendants with multiple occurrences as an array' do
      assert_instance_of Copybook::RecordDefinition, @record_def_main['ENDORSEMENTS'][0]
    end
  end

  context 'a field with an occurs clause' do
    should 'have an occurs value greater than 1' do
      assert @rdef_occ.occurs > 1
    end

    should 'be part of an occurs group' do
      assert @rdef_occ.part_of_occurs?
    end

    should 'occur the specified number of times' do
      assert_equal @rdef_occ.occurs, @record_def_main['ENDORSEMENTS'].count
    end
  end

  context 'a field following one with an occurs clause' do
    setup do
      @filler = @record_def_main['FILLER']
      @all_occurs = @record_def_main['ENDORSEMENTS']
    end

    should 'start at the correct offset' do
      expected_offset = @all_occurs.last.offset + @all_occurs.last.length(:record)
      assert_equal expected_offset, @filler.offset
    end
  end

  context 'a redefines where the referent is a container' do
    should 'successfully parse' do
      rdef = Copybook::Parse(File.join(DATADIR, 'redefines-example2.cpy'))
      assert_instance_of Copybook::RecordDefinition, rdef['C86-M7-DESCR-LINE1']
      assert_instance_of Copybook::RecordDefinition, rdef['C86-ZIP']
    end
  end

  context 'a redefinition' do
    should 'have the same offset as its referent' do
      assert_equal @rdef_redfn.redefines.offset, @rdef_redfn.offset
    end

    should 'be part of a redefines group' do
      assert @rdef_redfn.part_of_redefines?
    end
  end

  context 'the child of a redefinition' do
    should 'have a redefining parent' do
      refute_nil @redefine_child.redefining_parent
    end

    should 'show as part of a redefines group' do
      assert @redefine_child.part_of_redefines?
    end

    should 'calculate offset based on its parent' do
      assert_equal @redefine_child.parent.offset + @redefine_child.parent.length, @redefine_child.offset
    end
  end

  context 'the child of an occurs clause' do
    should 'have an occurs parent' do
      refute_nil @occurs_child.occurs_parent
    end

    should 'be part of an occurs group' do
      assert @occurs_child.part_of_occurs?
    end
  end

  context 'a segment with children' do
    should 'be able to calculate its length' do
      assert_equal 50, @segment_with_children.length(:record)
    end
  end

  context 'a duplicated segment with children' do
    setup do
      @dup = @segment_with_children.dup
    end

    should 'have the same name' do
      assert_equal @dup.name, @segment_with_children.name
    end

    should 'be a different object from the original' do
      refute_equal @dup.__id__, @segment_with_children.__id__
    end

    should 'have the same number of children as the original' do
      assert_equal @dup.children.count, @segment_with_children.children.count
    end

    should 'have different children from the original' do
      dup_child_ids = @dup.children.map(&:__id__)
      segment_child_ids = @segment_with_children.children.map(&:__id__)
      refute_equal dup_child_ids, segment_child_ids
    end

    should 'not have a parent' do
      refute @dup.parent
    end
  end

  context 'a duplicated segment with redefines' do
    setup do
      @dup = @segment_with_children.dup
      @redup = @dup.children.find(&:redefines)
      @reorig = @segment_with_children.children.find(&:redefines)
    end

    should 'be a different object from the original field' do
      refute_equal @redup.__id__, @reorig.__id__
    end

    should 'refer to a different object from the original' do
      refute_equal @redup.redefines.__id__, @reorig.redefines.__id__
    end
  end

  context 'a segment without children' do
    setup do
      @rdef_num = @record_def_main['RENEWAL-TERM']
    end

    should 'have a length equal to its pic length' do
      assert_equal @rdef_num.length, @rdef_num.length(:record)
    end

    should 'be able to duplicate itself' do
      dupl = @rdef_num.dup
      assert_equal @rdef_num.name, dupl.name
      refute_equal @rdef_num.__id__, dupl.__id__
    end
  end
end
