# frozen_string_literal: true

require 'test_helper'
require 'copybook'
require 'copybook/segment_builder'

class TestRecordSegment < Minitest::Test
  context 'a record segment builder' do
    setup do
      @cb = Copybook::Parse(File.join(DATADIR, 'sample_copybook.cpy'))
      @data = File.open(File.join(DATADIR, 'fixed_ascii.txt'))
      @reader = Copybook::DataReader.new(
        @data,
        separator: "\x0a",
        ebcdic: false
      )
      @builder = Copybook::SegmentBuilder.new(@reader)
    end

    should 'build a record segment' do
      assert_instance_of Copybook::RecordSegment, @builder.build(@cb)
    end

    should 'be able to be reused without collision' do
      seg1 = @builder.build(@cb)
      seg2 = @builder.build(@cb)
      assert_instance_of Copybook::RecordSegment, seg2
    end
  end

  context 'when building a segment with occurs' do
    should 'repeat occurs group for the number of occurrences'
  end

  context 'when building a segment with redefines' do
    setup do
      @cb = Copybook::Parse(File.join(DATADIR, 'redefines-example.cpy'))
      @data = File.open(File.join(DATADIR, '2n_record.txt'))
      @reader = Copybook::DataReader.new(
        @data,
        ebcdic: false,
        with_redefines: true
      )
      @builder = Copybook::SegmentBuilder.new(@reader)
    end

    should 'build a record segment' do
      assert_instance_of Copybook::RecordSegment, @builder.build(@cb)
    end

    should 'be able to access original values' do
      seg = @builder.build(@cb)
      assert_equal 'CHERYL', seg['B27-MINS-FIRST-NAME']
    end

    should 'be able to access redefined values' do
      seg = @builder.build(@cb)
      assert_equal 'CHERYL', seg['B27-MINS-NAME1']
    end

    should 'get the correct length for a literal decimal field' do
      seg = @builder.build(@cb)
      assert_equal 23.49, seg['SOME-DECIMAL-VALUE']
    end
  end
end
