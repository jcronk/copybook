# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/reporters'
require 'shoulda/context'
require 'minitest/byebug' if ENV['DEBUG']

Minitest::Reporters.use! Minitest::Reporters::DefaultReporter.new

THISDIR = File.expand_path(__dir__)
DATADIR = File.join(THISDIR, 'data')
